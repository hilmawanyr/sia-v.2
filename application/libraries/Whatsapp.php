<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Whatsapp
{
	function sendMsg($rec,$msg)
	{
		$my_apikey 	= "QK8CEBOZQHVMD8F7DZGP";
		$destination = $rec;
		$message = $msg;
		$api_url = "http://panel.apiwha.com/send_message.php";
		$api_url .= "?apikey=". urlencode ($my_apikey);
		$api_url .= "&number=". urlencode ($destination);
		$api_url .= "&text=". urlencode ($message);
		$my_result_object = json_decode(file_get_contents($api_url, false));
		echo "<br>Result: ". $my_result_object->success;
		echo "<br>Description: ". $my_result_object->description;
		echo "<br>Code: ". $my_result_object->result_code; 
	}

	function pullMsg()
	{
		// Pull messages (for push messages please go to settings of the number)
		$my_apikey = "QK8CEBOZQHVMD8F7DZGP";
		$number = "085710356506";
		$type = "OUT";
		$markaspulled = "1";
		$getnotpulledonly = "1";
		$api_url  = "http://panel.apiwha.com/get_messages.php";
		$api_url .= "?apikey=". urlencode ($my_apikey);
		$api_url .= "&number=". urlencode ($number);
		$api_url .= "&type=". urlencode ($type);
		$api_url .= "&markaspulled=". urlencode ($markaspulled);
		$api_url .= "&getnotpulledonly=". urlencode ($getnotpulledonly);
		$my_json_result = file_get_contents($api_url, false);
		$my_php_arr = json_decode($my_json_result);
		foreach($my_php_arr as $item)
		{
		  $from_temp = $item->from;
		  $to_temp = $item->to;
		  $text_temp = $item->text;
		  $type_temp = $item->type;
		  echo "<br>". $from_temp ." -> ". $to_temp ." (". $type_temp ."): ". $text_temp;
		  // Do something
		} 
	}

	function hookMsg()
	{
		// This is your webhook. You must configure it in the number settings section.

		$data = json_decode($_POST["data"]);

		// When you receive a message an INBOX event is created
		if ($data->event=="INBOX")
		{
		  // Default answer
		  $my_answer = "This is an autoreply from APIWHA!. You (". $data->from .") wrote: ". $data->text;

		  // You can evaluate the received message and prepare your new answer.
		  if(!(strpos(strtoupper($data->text), "PRICING")===false)){
		    $my_answer = "Sing up in our platform and you will get our pricing list!";

		  }else if(!(strpos(strtoupper($data->text), "INFORMATION")===false)){
		    $my_answer = "Of course! For more information you can access to our website http://www.apiwha.com!";

		  }

		  $response = new StdClass();
		  $response->apiwha_autoreply = $my_answer; // Attribute "apiwha_autoreply" is very important!

		  echo json_encode($response); // Don't forget to reply in JSON format

		  /* You don't need any APIKEY to answer to your customer from a webhook */
	}
}


 ?>