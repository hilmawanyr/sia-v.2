<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "page"){
        $log_tipe  = 5;
    }
    else{
        $log_tipe  = 4;
    }
 
    // paramter
    $param['userid'] = $CI->session->userdata('userid');
    $param['datetime_activity'] = date('Y-m-d H:i:s');
    $param['tipe'] = $log_tipe;
    $param['keterangan'] = $str;
 
    //load model log
    $CI->load->model('m_log');
 
    //save to database
    $CI->m_log->save_log($param);
}