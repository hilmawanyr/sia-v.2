<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Validasi Semester Pendek</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url();?>sp/validasi/simpan" method="post" accept-charset="utf-8">
                    <input class="btn btn-success btn-block" type="submit" value="Submit"><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM</th>
                                <th>MAHASISWA</th>
                                <th>TOTAL SKS SP</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($rows as $row){?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->npm_mahasiswa; ?></td>
	                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
	                        	<td><?php echo $row->jml_sks; ?></td>
	                        	<td class="td-actions">
	                        		<?php $q = $this->db->query("select npm_mahasiswa from tbl_sinkronisasi_renkeu_sp where npm_mahasiswa = '".$row->npm_mahasiswa."' and tahunajaran = '".$ta."' ")->row();
	                        			if (!is_null($q)) { ?>
	                        				<input type="checkbox" value="<?php echo $row->npm_mahasiswa; ?>911<?php echo $ta; ?>" name="mhs_bayar[]" checked disabled>
	                        			<?php }else{ ?>
	                        				<input type="checkbox" value="<?php echo $row->npm_mahasiswa; ?>911<?php echo $ta; ?>" name="mhs_bayar[]">
	                        			<?php } ?>
									
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
	               	</form>
				</div>
			</div>
		</div>
	</div>
</div>