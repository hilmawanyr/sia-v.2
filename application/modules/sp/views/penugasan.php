<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script type="text/javascript">
$(document).ready(function(){

    $('#jam_masuk').timepicker();
    
    $('#gedung').change(function(){
        $.post('<?php echo base_url();?>sp/validasisp/get_lantai/'+$(this).val(),{},function(get){
            $('#lantai').html(get);
        });
    });

    $('#lantai').change(function(){
        $.post('<?php echo base_url();?>sp/validasisp/get_ruangan/'+$(this).val(),{},function(get){
            $('#ruangan').html(get);
        });
    });

});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('sp/validasisp/getdosen');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;
        }
    });
});
</script>

<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class="modal-title">FORM PENUGASAN</h4>

</div>

<form class ='form-horizontal' action="<?php echo base_url();?>sp/validasisp/save_jdl_sp" method="post">
    <div class="modal-body">	
		<div class="control-group">										
			<label class="control-label" for="kode_mk">Kode MK</label>
			<div class="controls">
				<input type="text" class="span2" name="kd_mk" value="<?php echo $mk->kd_matakuliah ?>" id="kode_mk" readonly>
                <input type="hidden" class="span2" name="id_mk" value="<?php echo $mk->id_matakuliah ?>">
			</div>
		</div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Nama MK</label>
            <div class="controls">
                <input type="text" class="span4" name="nm_mk" value="<?php echo $mk->nama_matakuliah ?>" id="nama_mk" readonly>
            </div>
        </div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Dosen</label>
            <div class="controls">
                <input type="text" class="span4" name="dosen" placeholder="ketikan Nama dosen" id="dosen" required>
                <input type="hidden" class="span4" name="kd_dosen" id="kd_dosen">
            </div>
        </div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Hari</label>
            <div class="controls">
                <select class="span2" name="hari_kuliah" id="hari_kuliah">
                    <option disabled>--Hari Kuliah--</option>
                    <option value="1">Senin</option>
                    <option value="2">Selasa</option>
                    <option value="3">Rabu</option>
                    <option value="4">Kamis</option>
                    <option value="5">Jum'at</option>
                    <option value="6">Sabtu</option>
                    <option value="7">Minggu</option>
                </select>
            </div>
        </div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Gedung</label>
            <div class="controls">
                <select class="span2" name="gedung" id="gedung">             
                    <option value="">--Pilih Gedung--</option>
                    <?php foreach ($gedung as $isi) {?>
                        <option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>   
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Lantai</label>
            <div class="controls">
                <select class="span2" name="lantai" id="lantai">
                    <option value="">--Pilih Lantai--</option>
                </select>
            </div>
        </div>

        <div class="control-group">                                     
            <label class="control-label" for="kode_mk">Ruangan</label>
            <div class="controls">
                <select class="span2" name="ruangan" id="ruangan" required>
                    <option>--Pilih Ruangan--</option>
                </select>
            </div>
        </div>

        <div class="control-group">                                         
            <label class="control-label" for="jam">Jam Masuk</label>
            <div class="controls">
                <input type="text" class="form-control span2" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk" required>
            </div> <!-- /controls -->               
        </div> <!-- /control-group -->
    </div> 


    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save"/>
    </div>

</form>