<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Validasi Semester Pendek</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM</th>
                                <th>MAHASISWA</th>
                                <th>TOTAL SKS SP</th>
                                <th>STATUS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($rows as $isi){?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $isi->npm_mahasiswa; ?></td>
	                        	<td><?php echo $isi->NMMHSMSMHS; ?></td>
	                        	<td><?php echo $isi->jml_sks; ?></td>
	                        	<?php $q = $this->db->query("select npm_mahasiswa from tbl_sinkronisasi_renkeu_sp where npm_mahasiswa = '".$isi->npm_mahasiswa."' and tahunajaran = '".$ta."' ")->row();
	                        			if (!is_null($q)) { ?>
	                        				<td>-</td>
	                        			<?php }else{ ?>
	                        				<td>LUNAS</td>
	                        			<?php } ?>
	                        	<td class="td-actions">
									<a class="btn btn-primary btn-small" href=""><i class="btn-icon-only icon-print"> </i></a>
									
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>