<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuti_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getJurByFak($kode)
	{
		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi a');
		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');
		$this->db->where('a.kd_fakultas', $kode);
		return $this->db->get();
	}

	function loadCuti($uid,$year,$param=NULL)
	{
		$this->db->distinct();
		$this->db->select('b.`NIMHSMSMHS`,b.`NMMHSMSMHS`,b.`TAHUNMSMHS`,c.`prodi`');
		$this->db->from('tbl_status_mahasiswa d');
		$this->db->join('tbl_sinkronisasi_renkeu a', 'd.npm = a.npm_mahasiswa', 'left');
		$this->db->join('tbl_mahasiswa b', 'd.npm = b.NIMHSMSMHS');
		$this->db->join('tbl_jurusan_prodi c', 'c.kd_prodi = b.KDPSTMSMHS');
		$this->db->where('d.tahunajaran', $year);
		$this->db->where('d.status', 'C');

		if ($uid == 9) {
			$this->db->where('c.kd_fakultas', $param);
		} elseif ($uid == 8) {
			$this->db->where('c.kd_prodi', $param);
		}

		return $this->db->get();
	}

	function forForm($npm)
	{
		$l='a.NIMHSMSMHS,a.NMMHSMSMHS,b.prodi,e.transaksi_terakhir,a.SMAWLMSMHS,e.tahunajaran,ct.alamat';
		$this->db->select($l);
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		$this->db->join('tbl_permohonan_cuti ct', 'ct.npm_mhs = a.NIMHSMSMHS');
		$this->db->join('tbl_sinkronisasi_renkeu e', 'e.npm_mahasiswa = a.NIMHSMSMHS', 'left');
		$this->db->join('tbl_biodata_old d', 'd.nomor_pokok=a.NIMHSMSMHS', 'left');
		$this->db->where('a.NIMHSMSMHS', $npm);
		$this->db->order_by('e.transaksi_terakhir', 'desc');
		return $this->db->get();
	}

	function printForm($id)
	{
		$get = $this->db->query('SELECT DISTINCT v.`npm_mahasiswa`,v.`id_pembimbing`, b.*,c.*,d.fakultas AS fak,f.*
								FROM tbl_mahasiswa b
								JOIN tbl_jurusan_prodi c ON c.kd_prodi=b.KDPSTMSMHS
								JOIN tbl_fakultas d ON d.kd_fakultas=c.kd_fakultas
								JOIN tbl_verifikasi_krs v ON v.`npm_mahasiswa` = b.`NIMHSMSMHS`
								JOIN tbl_karyawan f ON f.nid= v.`id_pembimbing`
								LEFT JOIN tbl_biodata_old e ON e.nomor_pokok=b.NIMHSMSMHS
								WHERE b.NIMHSMSMHS ="'.$id.'" AND v.`id_verifikasi` = 
								(SELECT MAX(id_verifikasi) FROM tbl_verifikasi_krs WHERE npm_mahasiswa = '.$id.')
								GROUP BY v.`id_verifikasi`');
		return $get;
	}

	function getJurMhs($npm)
	{
		$this->db->select('fak.fakultas,jrs.prodi');
		$this->db->from('tbl_permohonan_cuti cuti');
		$this->db->join('tbl_mahasiswa mhs', 'cuti.npm_mhs = mhs.NIMHSMSMHS');
		$this->db->join('tbl_jurusan_prodi jrs', 'jrs.kd_prodi = mhs.KDPSTMSMHS');
		$this->db->join('tbl_fakultas fak', 'fak.kd_fakultas = jrs.kd_fakultas');
		$this->db->where('mhs.`NIMHSMSMHS`', $npm);
		return $this->db->get();
	}

	function printCuty($id)
	{
		$this->db->select('a.*,b.*,c.*,d.fakultas as fak,e.*,f.nama as mana');
		$this->db->from('tbl_permohonan_cuti a ');
		$this->db->join('tbl_mahasiswa b', 'a.npm_mhs = b.NIMHSMSMHS');
		$this->db->join(' tbl_jurusan_prodi c','c.kd_prodi = b.KDPSTMSMHS');
		$this->db->join('tbl_fakultas d','d.kd_fakultas = c.kd_fakultas');
		$this->db->join('tbl_karyawan f','f.nid = a.pembimbing_akademik');
		$this->db->join('tbl_biodata e','e.nomor_pokok = a.npm_mhs', 'left');
		$this->db->where('a.id_cuti', $id);
		return $this->db->get();
	}

	function updateStatus($npm,$year)
	{
		$obj = ['validate' => 1];
		$this->db->where('npm', $npm);
		$this->db->where('tahunajaran', $year);
		$this->db->update('tbl_status_mahasiswa', $obj);

		$code = $npm.$year;
		$this->db->like('kd_krs', $code, 'after');
		$this->db->delete('tbl_verifikasi_krs');

		$this->db->like('kd_krs', $code, 'after');
		$this->db->delete('tbl_krs');
	}
}

/* End of file Cuti_model.php */
/* Location: ./application/models/Cuti_model.php */