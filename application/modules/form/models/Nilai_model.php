<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_matakuliah_by_jadwal($kodeJadwal)
	{
		$matakuliah = $this->db->query('SELECT distinct 
											a.kd_matakuliah,
											b.nama_matakuliah,
											b.semester_matakuliah
	        							FROM tbl_jadwal_matkul a
										JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
										WHERE a.`kd_jadwal` = "' . $kodeJadwal . '"
										AND b.kd_prodi = SUBSTR(a.kd_jadwal,1,5)')->row();
		return $matakuliah;
	}

	function get_participant($kodeJadwal)
	{
		$participants = $this->db->query('SELECT count(distinct npm_mahasiswa) AS jumlah FROM tbl_krs WHERE kd_jadwal = "' . $kodeJadwal . '"')->row();
		return $participants;
	}

	function get_krs_transaction($kodeKrs)
	{
		$krs = $this->db->query('SELECT * FROM tbl_verifikasi_krs WHERE kd_krs LIKE "'.$kodeKrs.'%"')->row();
		return $krs;
	}

	function get_matakuliah($kodeMatakuliah, $prodi)
	{
		$matakuliah = $this->db->query('SELECT DISTINCT 
											kd_matakuliah, 
											nama_matakuliah, 
											semester_matakuliah 
										FROM tbl_matakuliah
										WHERE kd_matakuliah = "'.$kodeMatakuliah.'" 
										AND kd_prodi = SUBSTR("'.$prodi.'",1,5)')->row();
		return $matakuliah;
	}

	function participant_validation($npm, $kodeJadwal)
	{
		$participant = $this->db->query("SELECT * FROM tbl_krs WHERE npm_mahasiswa = '" . $npm . "' AND kd_jadwal = '" . $kodeJadwal . "'");
		return $participant;
	}

	function get_detail_participant($kodeJadwal)
	{
		$detail = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa mhs
									JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
									WHERE krs.kd_jadwal = "'.$kodeJadwal.'" ');
		return $detail;
	}

}

/* End of file Nilai_model.php */
/* Location: ./application/models/Nilai_model.php */