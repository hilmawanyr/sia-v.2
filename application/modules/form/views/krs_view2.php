<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/plugins/TableTools/js/dataTables.tableTools.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	
	var table_tambahan = $('#tabel_tambahan');
	
	var oTable_tambahan = table_tambahan.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
	});
	
	var table_rekam = $('#tabel_rekam');
	
	var oTable_rekam = table_rekam.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	
	$("#semester").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		var prodi = <?php echo $prodi; ?>;
		$.ajax({
                url: "<?php echo base_url('form/formkrs/get_matkul'); ?>",
                type: "post",
                data: {semester: semester, npm:npm, prodi:prodi},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable.fnClearTable();
                    oTable.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'],'<input type="checkbox" name="kd_matkuliah[]" value="'+arr[i]['kd_matakuliah']+'"/><input type="hidden" name="semester_matakuliah[]" value="'+arr[i]['semester_matakuliah']+'"/>']);
                    }

                }
            });
	});
	
	$("#semester_rekam").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		$.ajax({
                url: "<?php echo base_url('form/formkrs/get_krs'); ?>",
                type: "post",
                data: {semester: semester, npm:npm},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable_rekam.fnClearTable();
                    oTable_rekam.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_rekam.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'],'<input type="checkbox" name="kd_matkuliah[]" value="'+arr[i]['kd_matakuliah']+'"/><input type="hidden" name="semester_matakuliah[]" value="'+arr[i]['semester_matakuliah']+'"/>']);
                    }

                }
            });
	});
	
	$("#semester_tambahan").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		var prodi = <?php echo $prodi; ?>;
		$.ajax({
                url: "<?php echo base_url('form/formkrs/get_matkul'); ?>",
                type: "post",
                data: {semester: semester, npm:npm, prodi:prodi},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable_tambahan.fnClearTable();
                    oTable_tambahan.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_tambahan.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'], arr[i]['prasyarat_matakuliah'],'<button type="button" class="btn btn-primary btn-small edit pluss"><i class="btn-icon-only icon-plus"> </i></button>',arr[i]['semester_kd_matakuliah']]);
                    }

                }
            });
	});
	
	oTable_tambahan.on('click', 'tbody tr .pluss', function() {
			var nRow = $(this).parents('tr')[0];
			var aData = oTable_tambahan.fnGetData(nRow);
			var kd_mk = aData[0];
			var nama_mk = aData[1];
			var sks_mk = aData[2];
			var prasyarat = aData[3];
			var sms_mk = aData[5];
			
			if(prasyarat!='[]'){
				$.ajax({
					url: "<?php echo base_url('form/formkrs/cek_prasyarat'); ?>",
					type: "post",
					data: {prasyarat: prasyarat},
					success: function(d) {
						if(d>0){
							var a = oTable.fnAddData([kd_mk, nama_mk, sks_mk, '<button type="button" class="btn btn-danger btn-small edit delete"><i class="btn-icon-only icon-remove"> </i></button><input type="hidden" name="kd_matkuliah[]" value="'+kd_mk+'"/><input type="hidden" name="semester_matakuliah[]" value="'+sms_mk+'"/>']);
							var nTr = oTable.fnSettings().aoData[ a[0] ].nTr;
							nTr.className = "cek";
							oTable_tambahan.fnDeleteRow(nRow);
						}else{
							alert('Prasyarat matakuliah tidak terpenuhi');
						}
					}
				});
			}else{
				var a = oTable.fnAddData([kd_mk, nama_mk, sks_mk, '<button type="button" class="btn btn-danger btn-small edit delete"><i class="btn-icon-only icon-remove"> </i></button><input type="hidden" name="kd_matkuliah[]" value="'+kd_mk+'"/><input type="hidden" name="semester_matakuliah[]" value="'+sms_mk+'"/>']);
				var nTr = oTable.fnSettings().aoData[ a[0] ].nTr;
				nTr.className = "cek";
				
				oTable_tambahan.fnDeleteRow(nRow);
			}	
		sum_sks();
    });
	
	oTable.on('click', 'tbody tr .delete', function() {
		var nRow = $(this).parents('tr')[0];
		oTable.fnDeleteRow(nRow);
		sum_sks();
	});
	
	function sum_row(oTable, nRow) {
		var intVal = function(i) {
        return typeof i === 'string' ?
			i.replace(/[\$,]/g, '') * 1 :
            typeof i === 'number' ?
            i : 0;
        };

        var api = oTable.api(), data;

        var total = api
			.cells(nRow,2)
            .data()
            .reduce(function(a, b) {
				return intVal(a) + intVal(b);
			});
        return total;
	}
	
	function sum_sks(){
		$.ajax({
			url: "<?php echo base_url('form/formkrs/cekjumlahsks'); ?>",
			type: "post",
			data: {sks: sum_row(oTable, oTable.api().rows('.cek').indexes())},
			success: function(d) {
				if(d>0){
					document.getElementById("simpan").disabled = false; //harus falefalse
					$('#total').html('Total SKS : '+sum_row(oTable, oTable.api().rows('.cek').indexes()));
					$("#total_sks").val(sum_row(oTable, oTable.api().rows('.cek').indexes()));
				}else{
					alert('Jumlah SKS Melebihi Ketentuan');
					document.getElementById("simpan").disabled = true;
					$('#total').html('Total SKS : '+sum_row(oTable, oTable.api().rows('.cek').indexes()));
					$("#total_sks").val(sum_row(oTable, oTable.api().rows('.cek').indexes()));
				}
			}
		});
	}
	
	oTable.on('click', 'tbody tr .td-actions .checks', function() {
		var nRow = $(this).parents('tr')[0];
		var aData = oTable.fnGetData(nRow);
		if($(this).is(":checked")){
			$(this).parents('tr').addClass('cek');
		 }else{
			$(this).parents('tr').removeClass('cek');
		 }
		//alert(aData[2]);
		sum_sks();
	});
	
	sum_sks();
});
</script>
<div class="row">
  <!-- /span6 -->
  <div class="span12">
     <div class="widget widget-nopad" style="margin-top:30px">
      <div class="widget-header"> <i class="icon-edit"></i>
        <h3>Form KRS <?php echo $nama_mahasiswa.' ( '.$npm.' )'; ?></h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content" style="padding:30px;">
            <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#form_pendaftar" data-toggle="tab">Form KRS</a>
              </li>
              <li><a href="#data_peserta" data-toggle="tab">Data Akademik Mahasiswa</a></li>
            </ul>           
            <br>            
              <div class="tab-content">
                <div class="tab-pane active" id="form_pendaftar">
                <form id="edit-profile" class="form-horizontal" action="<?php echo base_url(); ?>form/formkrsfeeder/save_data" method="post" onsubmit="simpan.disabled = true; simpan.value='Please wait ..'; return true;">
                  <fieldset>                    
					<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Matakuliah </a>
                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
					<!--select class="form-control span2" name="semester" id="semester" style="float: right;">
						<option value="">--Pilih Semester--</option>
						<option value="1">Semester 1</option>
						<option value="2">Semester 2</option>
						<option value="3">Semester 3</option>
						<option value="4">Semester 4</option>
						<option value="5">Semester 5</option>
						<option value="6">Semester 6</option>
						<option value="7">Semester 7</option>
						<option value="8">Semester 8</option>
                    </select-->
					<table id="tabel_krs" class="table table-bordered table-striped">
                    <thead>
                          <tr> 
                            <th>Kode MK</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
						<?php $no = 1; foreach ($data_matakuliah as $row) { ?>
	                        <tr class="<?php if($status_krs==1) echo 'cek'; ?>">
	                        	<td><?php echo $row->kd_matakuliah;?></td>
	                        	<td><?php echo $row->nama_matakuliah;?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
	                        	<td class="td-actions">
									<input type="hidden" name="semester_matakuliah[]" value="<?php echo $row->semester_kd_matakuliah; ?>"/>
									<input type="checkbox" class="checks" name="kd_matkuliah[]" value="<?php echo $row->kd_matakuliah; ?>" <?php if($status_krs==1) echo 'checked'; ?>/>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
                      </tbody>
                  </table>
				  <div id="total" style="font-weight: bolder;font-size: 14px;text-align: right;"></div>
				  <input type="hidden" name="kode_krs" value="<?php if($status_krs==1) echo $kode_krs; ?>"/>
				  <input type="hidden" name="jumlah_sks" id="total_sks"/>
                  <label class="control-label" for="firstname">Catatan Pembimbing</label>
				  <textarea name="keterangan_krs" class="form-control span10" rows="4"><?php if($status_krs==1) echo $catatan_pembimbing; ?></textarea>
                      <div class="form-actions">
						<input type="hidden" name="npm_mahasiswa" value="<?php echo $npm; ?>" />
                        <button type="submit" id="simpan" name="simpan" class="btn btn-primary">Simpan</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
				<div class="tab-pane " id="data_peserta">
				
				<fieldset>
                    <table id="tabel_rekam" class="table table-bordered table-striped">
                    <thead>
                          <tr> 
                            <th>No</th>
                            <th>Semester</th>
                            <th>Total SKS</th>
							<th>IPS</th>
							<th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
						<?php $no = 1; foreach ($data_krs as $row) { ?>
							<?php 
							$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "'.$row->THSMSTRLNM.'" ')->result();

			                $st=0;
			                $ht=0;
			                foreach ($hitung_ips as $iso) {
			                    $h = 0;


			                    $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			                    $ht=  $ht + $h;

			                    $st=$st+$iso->sks_matakuliah;
			                }

			                $ips_nr = $ht/$st;
							?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<?php $a = $this->app_model->get_semester_khs($row->SMAWLMSMHS,$row->THSMSTRLNM); ?>
	                        	<td><?php echo $a; ?></td>
	                        	<!-- <td><?php //echo $row->semester_khs;?></td> -->
                                <td><?php echo $st; ?></td>
								<td><?php echo number_format($ips_nr,2); ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" target="_blank" href="<?php echo base_url(); ?>form/formkrs/view/<?php echo $npm; ?>/<?php echo $a; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
                      </tbody>
                  </table>
                 </fieldset>
				</div>
              </div>
			  
			</div>
          </div> <!-- /widget-content -->
</div>  
</div>
</div>        


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Matakuliah</h4>
            </div>
            <div class="modal-body">    
                <select class="form-control span2" id="semester_tambahan">
						<option value="">--Pilih Semester--</option>
						<!--?php if(($semester %2) == 1 ){ 
							?-->							
							<option value="1">Semester 1</option>							<option value="2">Semester 2</option>
							<option value="3">Semester 3</option>							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>							<option value="8">Semester 8</option>
						<!--?php }else{ ?-->
						<!--?php } ?-->
                </select>
                <table id="tabel_tambahan" class="table table-bordered table-striped">
                  <thead>
                        <tr> 
                          <th>Kode MK</th>
                          <th>Mata Kuliah</th>
                          <th>SKS</th>
						  <th>Prasyarat</th>
                          <th>Aksi</th>
						  <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Selesai</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->