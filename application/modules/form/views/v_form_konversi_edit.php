<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class="modal-title">FORM EDIT DATA</h4>

</div>

<form class ='form-horizontal' action="<?php echo base_url();?>form/formnilai_konversi/exe_edit_mk_mhs" method="post">

<div class="modal-body">
    <div class="control-group">                                         

        <label class="control-label" for="matkul">Kode Matakuliah Asal</label>

        <div class="controls">

           <input type="text" class="form-control" name="kd_mk_asal" value="<?php echo $mk->kd_mk_asal; ?>" required/>

        </div> <!-- /controls -->               

    </div> <!-- /control-group -->

    <div class="control-group">                                         

        <label class="control-label" for="matkul">Nama Matakuliah Asal</label>

        <div class="controls">

           <input type="text" class="form-control" name="nm_mk_asal" value="<?php echo $mk->nm_mk_asal; ?>" required/>

        </div> <!-- /controls -->               

    </div> <!-- /control-group -->

    <div class="control-group">                                         

        <label class="control-label" for="matkul">SKS Matakuliah Asal</label>

        <div class="controls">

           <input type="number" class="form-control" name="sks_mk_asal" value="<?php echo $mk->sks_mk_asal; ?>" required/>

        </div> <!-- /controls -->               

    </div> <!-- /control-group -->

    <div class="control-group">                                         

        <label class="control-label" for="matkul">Nilai Matakuliah Asal</label>

        <div class="controls">

           <input type="text" class="form-control" name="nilai_mk_asal" value="<?php echo $mk->nilai_mk_asal; ?>" required/>

        </div> <!-- /controls -->               

    </div> <!-- /control-group -->
<div class="control-group">                                         

    <label class="control-label" for="matkul">Nama Matakuliah</label>

    <div class="controls">
    	<input type="hidden" name="id_nilai" value="<?php echo $mk->id ?>"/>
        <select id="matkul_edit" name="matkul_edit" class="form-control"  required>
            <option >-- Pilih Matakuliah --</option>
            <?php foreach ($slct_mk as $isi) {
            	if ($mk->KDKMKTRLNM == $isi->kd_matakuliah) {
            		$selected = 'selected';
            	}else{
            		$selected = '';
            	}

                echo '<option value="'.$isi->kd_matakuliah.'"  '.$selected.' >'.$isi->kd_matakuliah.' - '.$isi->nama_matakuliah.'</option>';
            } ?>
        </select>

    </div> <!-- /controls -->               

</div> <!-- /control-group -->

<div class="control-group">                                     

    <label class="control-label" for="nilai2">Nilai</label>

    <div class="controls">

        <select id="nilai_edit" name="nilai_edit" class="form-control"  required>
            <option >-- Pilih Nilai --</option>
            <?php foreach ($nilai as $isi) {
            	if ($mk->BOBOTTRLNM == $isi->nilai_mutu) {
            		$selected = 'selected';
            	}else{
            		$selected = '';
            	}

                echo '<option value="'.$isi->nilai_huruf.'/'.$isi->nilai_mutu.'"  '.$selected.' >'.$isi->nilai_huruf.' / '.$isi->nilai_mutu.'</option>';
            } ?>
        </select>
    </div> <!-- /controls -->               
</div> <!-- /control-group -->
</div> 

<div class="modal-footer">

<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-primary" value="Save"/>

</div>