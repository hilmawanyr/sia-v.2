<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('form/formnilai/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {



            // $('#hargasatuan').html(ui.item.harga_jual);

            // $('#satuan').html(ui.item.satuan);

            // $('#stok').html(ui.item.stok);

            // this.form.kode.value = ui.item.kode;

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;

            // this.form.hargabeli.value = ui.item.harga_beli;

            // this.form.hargajual.value = ui.item.harga_jual;

            //$('#qtyk').focus();

        }

    });

});

</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>FORM PENGISIAN NILAI</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
         <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#banyak" data-toggle="tab">Form Monitoring</a></li>
               
              <li><a href="#satuan" data-toggle="tab">Rekapitulasi</a></li>
             
            </ul>           
            <br>

                        
              <div class="tab-content">
               
                  <div class="tab-pane active" id="banyak">
                    <div class="span11">
                      <b><center>FORM MONITORING MENGAJAR</center></b><br>
                      <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>form/monitoring/cetak_monitoring">
                        <fieldset>
                        <?php
                          $logged = $this->session->userdata('sess_login');
                          $pecah = explode(',', $logged['id_user_group']);
                          $jmlh = count($pecah);
                          for ($i=0; $i < $jmlh; $i++) { 
                            $grup[] = $pecah[$i];
                          }
                          if ( (in_array(8, $grup))) {
                        ?>
                          <div class="control-group">                     
                            <label class="control-label">NID/NAMA</label>
                            <div class="controls">
                              <input type="text" class="span3" name="dosen" placeholder="Masukan NID Dosen" id="dosen" required>
                              <input type="hidden" id='kd_dosen' name="kd_dosen" >
                            </div> <!-- /controls -->       
                          </div> <!-- /control-group -->
                          <?php } else { ?> 
                              <input type="hidden" id='kd_dosen' name="kd_dosen" value="<?php echo $logged['userid'];?>">
                          <?php } ?>
                          <!-- <div class="control-group">                     
                            <label class="control-label">Tahun Akademik</label>
                            <div class="controls">
                              <select name="ta" class="span3" required>
                                <option disabled selected>-- Pilih Tahun Akademik --</option>
                                <option value="20171">2017/2018 - Ganjil</option>
                              </select>
                            </div> <!-- /controls -->       
                          <!-- </div> --> <!-- /control-group -->
                          <div class="control-group">                     
                            <label class="control-label">Tahun</label>
                            <div class="controls">
                              <select name="tahun" class="span3" required>
                                <option disabled selected>-- Pilih Tahun --</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                              </select>
                            </div> <!-- /controls -->       
                          </div> <!-- /control-group -->
                          <?php
                          $mons = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
                          ?>
                          <div class="control-group">                     
                            <label class="control-label">Bulan</label>
                            <div class="controls">
                              <select name="bulan" class="span3" required>
                                <option disabled selected>-- Pilih Bulan --</option>
                                <?php for ($i=1; $i < 13; $i++) { ?>
                                  <option value="<?php echo $i; ?>"><?php echo $mons[$i]; ?></option>
                                <?php } ?>
                              </select>
                            </div> <!-- /controls -->       
                          </div> <!-- /control-group -->
                         
                          <div class="form-actions">
                            <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                            <input type="reset" class="btn btn-warning" value="Reset"/>
                          </div> <!-- /form-actions -->
                        </fieldset>
                      </form>
                    </div>

                  </div>

                  <div class="tab-pane" id="satuan">
                    <div class="span11">
                      <b><center>FORM MONITORING MENGAJAR</center></b><br>
                      <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>form/monitoring/cetak_rekap">
                        <fieldset>
                        <?php
                          $logged = $this->session->userdata('sess_login');
                          $pecah = explode(',', $logged['id_user_group']);
                          $jmlh = count($pecah);
                          for ($i=0; $i < $jmlh; $i++) { 
                            $grup[] = $pecah[$i];
                          }
                          if ( (in_array(8, $grup))) {
                        ?>
                          <div class="control-group">                     
                            <label class="control-label">NID/NAMA</label>
                            <div class="controls">
                              <input type="text" class="span3" name="dosen" placeholder="Masukan NID Dosen" id="dosen" required>
                              <input type="hidden" id='kd_dosen' name="kd_dosen" >
                            </div> <!-- /controls -->       
                          </div> <!-- /control-group -->
                          <?php } else { ?> 
                              <input type="hidden" id='kd_dosen' name="kd_dosen" value="<?php echo $logged['userid'];?>">
                          <?php } ?>
                          <div class="control-group">                     
                            <label class="control-label">Tahun Ajaran</label>
                            <div class="controls">
                              <select name="tahun" class="span3" required>
                                <option disabled selected>-- Pilih Tahun --</option>
                                <option value="20162">2016/2017 - Genap</option>
                                <option value="20171">2017/2018 - Ganjil</option>
                              </select>
                            </div> <!-- /controls -->       
                          </div> <!-- /control-group -->
                          
                          <div class="form-actions">
                            <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                            <input type="reset" class="btn btn-warning" value="Reset"/>
                          </div> <!-- /form-actions -->
                        </fieldset>
                      </form>
                    </div>

                  </div>
              </div>


        
      </div>
    </div>
  </div>
</div>