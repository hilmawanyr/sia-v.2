<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>



<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Validasi Mahasiswa</h3>

            </div> <!-- /widget-header -->

            
            <div class="widget-content">

                <div class="span11">

                   <a href="#" class="btn btn-success "><i class="btn-icon-only icon-print"> </i> Export Excel</a><hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th width='50'>No</th>

                                <th>NIM</th>

                                <th>Nama Mahasiswa</th>

                                <th>Angkatan</th>

                                <th width='40'>UTS</th>

                                <th width='40'>UAS</th>

                            </tr>

                        </thead>

                        <tbody>

                            <tr>
                            	<td></td>
                            	<td></td>
                            	<td></td>
                            	<td></td>
                            	<td><a href="#" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a></td>
                            	<td><a href="#" class="btn btn-primary btn-small"><i class="btn-icon-only icon-print"> </i></a></td>
                            </tr>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



