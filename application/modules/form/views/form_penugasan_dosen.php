<script type="text/javascript">
	$(document).ready(function(){
		$('#mengajar').hide();
		$('#pa').hide();
		$('#ps').hide();
		$('#pk').hide();
		
		$('#perihal').change(function(){
			if($('#perihal').val() == 'pa'){
				$('#mengajar').hide();
				$('#pa').show();
				$('#ps').hide();
				$('#pk').hide();
			}else if($('#perihal').val() == 'ps1' || $('#perihal').val() == 'ps2'){
				$('#mengajar').hide();
				$('#pa').hide();
				$('#ps').show();
				$('#pk').hide();
			}else if($('#perihal').val() == 'pk'){
				$('#mengajar').hide();
				$('#pa').hide();
				$('#ps').hide();
				$('#pk').show();
			} else if($('#perihal').val() == '0'){
				$('#mengajar').hide();
				$('#pa').hide();
				$('#ps').hide();
				$('#pk').hide();
			}
		});
	});
</script>

<div class="row">
  <!-- /span6 -->
  <div class="span12">
     <div class="widget widget-nopad" >
      <div class="widget-header"> <i class="icon-edit"></i>
        <h3>Form Penugasan Dosen</h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content" style="padding:30px;">
      	<div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#form" data-toggle="tab">Form</a>
              </li>
              <li><a href="#data" data-toggle="tab">Data</a></li>
            </ul>           
            <br>            
              <div class="tab-content">
                <div class="tab-pane active" id="form">
                	<form id="edit-profile" class="form-horizontal" action="<?php echo base_url(); ?>form/penugasandosen/savedata" method="post">
						<input type="hidden" name="unit" value="<?php echo $divisi['kd_divisi'];?>"/>
						<input type="hidden" name="user" value="<?php echo $user;?>"/>
						<fieldset>
							<div class="control-group">											
								<label class="control-label" for="username">Dari</label>
								<div class="controls">
									<input type="text" class="span4" id="username" value="<?php echo $divisi['divisi']; ?>" readonly>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">
				              <label class="control-label">Perihal</label>
				              <div class="controls">
							      <select id="perihal" class="form-control" name="perihal" required>
				                  <option value="0">--Pilih Perihal--</option>
				                  <?php foreach($perihal as $row){ ?>
										<option value="<?php echo $row->kd_perihal; ?>"><?php echo $row->perihal; ?></option>
								  <?php } ?>
								  </select>
				              </div>
				            </div>
							<div class="control-group">											
								<label class="control-label" for="username">Deskripsi</label>
								<div class="controls">
									<textarea name="deskripsi" required></textarea>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">
			              		<label class="control-label">Ditujukan</label>
			              		<div class="controls">
			                		<select class="form-control" name="dosen" required>
			                  			<option disabled selected>--Pilih Dosen--</option>
							  			<?php foreach($dosen as $row){ ?>
										<option value="<?php echo $row->nid; ?>"><?php echo $row->nama; ?></option>
							  			<?php } ?>
			                		</select>
			              		</div>
			            	</div>

							<div id="pa">		
								<div class="control-group">
			                      <label class="control-label">Angkatan</label>
			                      <div class="controls">
			                        <select class="form-control" name="angkatan">
			                          <option value="">--Pilih Angkatan--</option>
									  <?php foreach($tahunajaran as $row){ ?>
											<option value="<?php echo $row->kode; ?>"><?php echo $row->tahun_akademik; ?></option>
									  <?php } ?>
			                        </select>
			                      </div>
			                    </div>
							</div>
							
							<div id="ps">		
								<div class="control-group">
			                      <label class="control-label">Mahasiswa</label>                                      
			                      <div class="controls">

			                      	<table id="example3" class="table table-bordered table-striped">
					                	<thead>
					                        <tr>
				                                <th>NIM</th>
				                                <th>Nama</th>
					                            <th width="40">Aksi</th>
					                        </tr>
					                    </thead>
					                    <tbody>
					                    	<?php foreach($mahasiswaskripsi as $row){ ?>
					                    	<tr>
					                    		<td><?php echo $row->NIMHSMSMHS; ?></td>
					                    		<td><?php echo $row->NMMHSMSMHS; ?></td>
					                    		<td><input type="checkbox" name="mhs[]" value="<?php echo $row->NIMHSMSMHS; ?>"/></td>
					                    	</tr>
					                    	<?php } ?>
					                    </tbody>
					                </table>

			                      </div>
			                    </div>
							</div>

							<div id="pk">		
								<div class="control-group">
			                      <label class="control-label">Mahasiswa</label>                                      
			                      <div class="controls">
									  
			                      	<table id="example4" class="table table-bordered table-striped">
					                	<thead>
					                        <tr>
				                                <th>NIM</th>
				                                <th>Nama</th>
					                            <th width="40">Aksi</th>
					                        </tr>
					                    </thead>
					                    <tbody>
					                    	<?php foreach($mahasiswakp as $row){ ?>
					                    	<tr>
					                    		<td><?php echo $row->NIMHSMSMHS; ?></td>
					                    		<td><?php echo $row->NMMHSMSMHS; ?></td>
					                    		<td><input type="checkbox" name="mhs[]" value="<?php echo $row->NIMHSMSMHS; ?>"/></td>
					                    	</tr>
					                    	<?php } ?>
					                    </tbody>
					                </table>

			                      </div>
			                    </div>
							</div>
							<br />
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Submit</button> 
								<button class="btn">Cancel</button>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
                </div>
                <div class="tab-pane" id="data">
                	<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
                                <th>No Identitas</th>
                                <th>Nama</th>
                                <th>Penugasan</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($getData as $row){?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $row->nik; ?></td>
	                        	<?php $q = $this->app_model->getdetail('tbl_karyawan','nid',$row->nik,'nid','asc')->row(); ?>
	                        	<td><?php echo $q->nama; ?></td>
	                        	<?php $q1 = $this->app_model->getdetail('tbl_perihal','kd_perihal',$row->perihal,'kd_perihal','asc')->row(); ?>
	                        	<?php 
	                        		if ($row->perihal == 'pa') {
	                        			$q2 = $this->app_model->getdetail('tbl_tahunakademik','kode',$row->penugasan,'kode','asc')->row();
	                        			$hasil = $q2->tahun_akademik;
	                        		} else {
	                        			$hasil = '';
	                        		}
	                        	?>
                                <td><?php echo $q1->perihal.' '.$hasil; ?></td>
	                        	<td class="td-actions">
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url(); ?>form/penugasandosen/delete/<?php echo $row->id; ?>"><i class="btn-icon-only icon-remove"> </i></a>
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>form/penugasandosen/view/<?php echo $row->perihal.'zz'.$row->nik; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
                </div>
              </div>
          </div>
	</div>
</div>	
</div>
</div>				
								
								
								