<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Form Pengajuan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM PENGAJUAN SIDANG/YUDISIUM</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>form/formsidyud/form">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">Password</label>
                <div class="controls">
                  <input type="password" class="span3" name="password" placeholder="Password" required autofocus>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <div class="control-group">                     
                <label class="control-label">Form</label>
                <div class="controls">
                  <select name="kelas" required class="span3">
                    <option value="1">Sidang</option>
                    <option value="2">Yudisium</option>
                    <!-- <option value="3">Wisuda</option> -->
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <!-- <div class="control-group">                     
                <label class="control-label">Verifikasi</label>
                <div class="controls">
                  <div class="g-recaptcha" data-sitekey="6LeLLAoUAAAAAPFxEQf5C2sIkhPKj2zNj2C9gaOp"></div>     
                </div>  
              </div> -->

              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>