<?php $this->load->model('hilman_model'); ?>

<script type="text/javascript">
$(document).ready(function () {
	<?php  
	if ((strtoupper($mk->nama_matakuliah) == 'SKRIPSI') 
		OR (strtoupper($mk->nama_matakuliah) == 'KULIAH KERJA MAHASISWA')
		OR (strtoupper($mk->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
		OR ($mk->nama_matakuliah == 'Seminar dan Tugas Akhir') 
		OR ($mk->nama_matakuliah == 'KULIAH KERJA NYATA')
		OR ($mk->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') 
		OR ($mk->nama_matakuliah == 'Kuliah Kerja Praktek')
		OR (strtoupper($mk->nama_matakuliah) == 'MAGANG KERJA') 
		OR ($mk->nama_matakuliah == 'Kerja Praktek') 
		OR (strtoupper($mk->nama_matakuliah) == 'TESIS')
		OR (strtoupper($mk->nama_matakuliah) == 'THESIS') ) { ?>

		window.onload=hitung_kp;

	<?php } else { ?>

		window.onload=hitung;

	<?php } ?>

	function hitung_kp()
	{
		var kp = parseInt($('#nilaiahir_kp').val());

		if (kp >= 100) {
			kp = 100;
		}

		if ((kp >= 80) && (kp <= 100)) {
			$('#huruf2').val("A");
		} else if ((kp >= 76) && (kp <= 79.99)) {
			$('#huruf2').val("A-");
		} else if ((kp >= 72) && (kp <= 75.99)) {
			$('#huruf2').val("B+");
		} else if ((kp >= 68) && (kp <= 71.99)) {
			$('#huruf2').val("B");
		} else if ((kp >= 64) && (kp <= 67.99)) {
			$('#huruf2').val("B-");
		} else if ((kp >= 60) && (kp <= 63.99)) {
			$('#huruf2').val("C+");
		} else if ((kp >= 56) && (kp <= 59.99)) {
			$('#huruf2').val("C");
		} else if ((kp >= 45) && (kp <= 55.99)) {
			$('#huruf2').val("D");
		} else if ((kp >= 0) && (kp <= 44.99)) {
			$('#huruf2').val("E");
		} else if (kp >100) {
			$('#huruf2').val("T");
		}

	}
	
	function hitung()
	{
	 	var dsn = parseInt($('#dsn').val());
		var mhs = parseInt($('#mhs').val());
		var tsatu = parseInt($('#tgs1').val());
		var tdua  = parseInt($('#tgs2').val());
		var ttiga = parseInt($('#tgs3').val());
		var tempat = parseInt($('#tgs4').val());
		var tlima = parseInt($('#tgs5').val());
		var uts = parseInt($('#uts').val());
		var uas = parseInt($('#uas').val());
		var bagi = parseInt($('#bags').val());

		if (isNaN(tsatu)) {
			tsatu = 0;
		}
		if (isNaN(tdua)) {
			tdua = 0;
		}
		if (isNaN(ttiga)) {
			ttiga = 0;
		}
		if (isNaN(tempat)) {
			tempat = 0;
		}
		if (isNaN(tlima)) {
			tlima = 0;
		}
		if (isNaN(uts)) {
			uts = 0;
		}
		if (isNaN(uas)) {
			uas = 0;
		}

		var trata_temp = ((tsatu+tdua+ttiga+tempat+tlima) / 5 );
		var absen_temp = ((mhs /dsn) * 100);

		if (isNaN(absen_temp)) {
			absen_temp = 0;
		}
		
		if (trata_temp >= 100) {
			trata_temp = 100;
		}

		if (absen_temp >= 100) {
			absen_temp = 100;
		}

		var nrata = ((absen_temp*0.1)+(trata_temp*0.2)+(uts*0.3)+(uas*0.4));

		$('#tugasrata').val(trata_temp);
		$('#absen').val(absen_temp);
		$('#nilaiahir').val(nrata);
		$('#hdnilai').val(nrata); 

		var hdnilai = parseInt($('#hdnilai').val());

		if ((hdnilai >= 80) && (hdnilai <= 100)) {
			$('#huruf').val("A");
		} else if ((hdnilai >= 76) && (hdnilai <= 79.99)) {
			$('#huruf').val("A-");
		} else if ((hdnilai >= 72) && (hdnilai <= 75.99)) {
			$('#huruf').val("B+");
		} else if ((hdnilai >= 68) && (hdnilai <= 71.99)) {
			$('#huruf').val("B");
		} else if ((hdnilai >= 64) && (hdnilai <= 67.99)) {
			$('#huruf').val("B-");
		} else if ((hdnilai >= 60) && (hdnilai <= 63.99)) {
			$('#huruf').val("C+");
		} else if ((hdnilai >= 56) && (hdnilai <= 59.99)) {
			$('#huruf').val("C");
		} else if ((hdnilai >= 45) && (hdnilai <= 55.99)) {
			$('#huruf').val("D");
		} else if ((hdnilai >= 0) && (hdnilai <= 44.99)) {
			$('#huruf').val("E");
		} else if (hdnilai >100) {
			$('#huruf').val("T");
		}	
	}	 

	$('#tgs1').keyup(function() {
		hitung();
	});

	$('#tgs2').keyup(function() {
		hitung();
	});

	$('#tgs3').keyup(function() {
		hitung();
	});

	$('#tgs4').keyup(function() {
		hitung();
	});

	$('#tgs5').keyup(function() {
		hitung();
	});

	$('#dsn').keyup(function() {
		hitung();
	});

	$('#mhs').keyup(function() {
		hitung();
	});

	$('#uts').keyup(function() {
		hitung();
	});

	$('#uas').keyup(function() {
		hitung();
	});

	$('#nilaiahir_kp').keyup(function() {
		hitung_kp();
	});

	<?php
		if ($nilai_edit == TRUE) {
			echo "$('#nilai_nilai').show();";
		} else {
			echo "$('#nilai_nilai').hide();";
		}

		if ($ctgs_1 == $byk_mhs) {
			echo "$('#tg1').hide(); $('#tk1').hide(); $('#tgs1').hide();";
		} else {
			echo "$('#tg1').show(); $('#tk1').show(); $('#tg1s').show();";
		}

		if ($ctgs_2 == $byk_mhs) {
			echo "$('#tg2').hide(); $('#tk2').hide(); $('#tgs2').hide();";
		} else {
			echo "$('#tg2').show(); $('#tk2').show(); $('#tgs2').show();";
		}

		if ($ctgs_3 == $byk_mhs) {
			echo "$('#tg3').hide(); $('#tk3').hide(); $('#tgs3').hide();";
		} else {
			echo "$('#tg3').show(); $('#tk3').show(); $('#tgs3').show();";
		}

		if ($ctgs_4 == $byk_mhs) {
			echo "$('#tg4').hide(); $('#tk4').hide(); $('#tgs4').hide();";
		} else {
			echo "$('#tg4').show(); $('#tk4').show(); $('#tgs4').show();";
		}

		if ($ctgs_5 == $byk_mhs) {
			echo "$('#tg5').hide(); $('#tk5').hide(); $('#tgs5').hide();";
		} else {
			echo "$('#tg5').show(); $('#tk5').show(); $('#tgs5').show();";
		}
	?>
	
});

<?php 
	if ($tayang_nilai == 'ada') {
		echo "$('#nilai_nilai').show();";
	}else{
		echo "$('#nilai_nilai').hide();";
	} 
?>

jQuery(document).ready(function($) {
    $('input[name^=npm]').autocomplete({
        source: '<?= base_url('form/formnilai/load_mhs_autocomplete');?>',
        minLength: 1,
        select: function (evt, ui) {
            this.form.npm.value = ui.item.value;
            this.form.npm_mhs.value = ui.item.npm;
            this.form.kd_krs.value = ui.item.kd_krs;
        }
    });
});

</script>

<div class="widget-content">
    <div class="tabbable">
        <ul class="nav nav-tabs">

        	<?php if ($tayang_nilai == 'ada') { 

        		$revisi = 'class="active"';
        		$isival = '';

        	}else{
  				
				$pecah = explode(',', $logged['id_user_group']);
				$jmlh = count($pecah);

				for ($i=0; $i < $jmlh; $i++) { 
					$grup[] = $pecah[$i];
				}

				$isAssessmentScheduleActive = $this->setting_model->getaktivasi('nilai')->result();
				if ( (in_array(8, $grup) || in_array(6, $grup)) and (count($isAssessmentScheduleActive) != 0) ) {
					
					$revisi = '';
					$isival = 'class="active"';

				} 
			} ?>

  			<li <?= $isival ?>><a href="#banyak" data-toggle="tab">Pengisian Nilai</a></li>
  			<!-- <li <?= $revisi ?>><a href="#satuan" data-toggle="tab">Revisi Nilai</a></li> -->

        </ul>           
        <br>
        <div class="tab-content">         
          	<div class="tab-pane <?php if ($tayang_nilai == 'ada') {echo '';}else{echo 'active';}?>" id="banyak">
                <div class="row">
					<div class="span12">      
		  				<center>
			  				<h4>
			  					Data Nilai Mahasiswa ( '<?= $mk->nama_matakuliah; ?>' ) - 
			  					<?= $mk->kelas; ?> (<i><u><?php $ta = $this->app_model->getdetail('tbl_tahunakademik','kode',$mk->kd_tahunajaran,'kode','asc')->row(); echo $ta->tahun_akademik; ?>)</i></u>
			  				</h4>
					 	</center>
					
						<br>
						<div class="span11">
							<a href="<?php echo base_url(); ?>form/formnilai/authdosenlog" class="btn btn-warning"> << Kembali </a>

							<!-- kondisi untuk button upload nilai -->
							<?php 
								
								$getIdGroup = explode(',', $logged['id_user_group']);
								$numberOfGroup = count($getIdGroup);
								for ($i=0; $i < $numberOfGroup; $i++) { 
									$grup[] = $getIdGroup[$i];
								}

								$isAssessmentScheduleActive = $this->setting_model->getaktivasi('nilai')->result();
								// if  ((in_array(6, $grup)) and ((count($isAssessmentScheduleActive) != 0))) {
								if  ((in_array(8, $grup) || in_array(19, $grup)) && count($isAssessmentScheduleActive) != 0 && is_final_assignment($mk->nama_matakuliah)) { ?>
							
									<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai</a>
									<a href="<?= base_url('form/formnilai/dwld_excel/'.$id_jadwal); ?>" class="btn btn-primary">Download Form</a>

								<?php } elseif (in_array(19, $grup) && count($isAssessmentScheduleActive) != 0 && !is_final_assignment($mk->nama_matakuliah)) { ?>


									<a href="<?= base_url('form/formnilai/dwld_excel/'.$id_jadwal); ?>" class="btn btn-primary">Download Form</a>


								<?php } elseif (in_array(6, $grup) && count($isAssessmentScheduleActive) != 0 && !is_final_assignment($mk->nama_matakuliah)) {

									// manage button upload nilai
									$getuts = array('id_jadwal' => $id_jadwal, 'tipe_uji' => 1);
									$uts = $this->hilman_model->moreWhere($getuts, 'tbl_jadwaluji');

									$getuas = array('id_jadwal' => $id_jadwal, 'tipe_uji' => 2);
									$uas = $this->hilman_model->moreWhere($getuas, 'tbl_jadwaluji');

									// jika jadwal uts sudah ada
									$bool = true;
									// if ($bool == true) {
									if ($uts->num_rows() > 0 && $uts->row()->end_date >= date('Y-m-d')) {
										
										if (date('Y-m-d') >= $uts->row()->start_date and date('Y-m-d') <= $uts->row()->end_date) { ?>
										
											<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai UTS</a>

											<?php $tipe = "<input type=\"hidden\" name=\"tipeuji\" value=\"uts\">"; ?>

										<?php } ?>

									<!-- jika jadwal uas sudah ada -->
									<?php } elseif ($uas->num_rows() > 0 && $uas->row()->end_date >= date('Y-m-d')) {
										
										if (date('Y-m-d') >= $uas->row()->start_date and date('Y-m-d') <= $uas->row()->end_date) { ?>
										
											<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai UAS</a>

											<?php $tipe = "<input type=\"hidden\" name=\"tipeuji\" value=\"uas\">"; ?>		

										<?php } ?>									

									<!-- jika kedua jadwal belum ada -->
									<?php } elseif ($uts->num_rows() == 0 && $uas->num_rows() == 0) { ?>

										<a class="btn btn-danger" onclick="alert('Prodi belum menentukan masa pengunggahan nilai!')">
											<i class="icon icon-remove"></i> Upload Nilai 
										</a>

									<!-- jika sudah melewati jadwal -->
									<?php } elseif (($uts->num_rows() > 0 && date('Y-m-d') > $uts->row()->end_date) || ($uas->num_rows() > 0 && date('Y-m-d') > $uas->row()->end_date)) { ?>
										
										<!-- jika dosen diberikan dispensasi -->
										<?php 
										$actyear  = getactyear();
										$user     = $this->session->userdata('sess_login');
										$getdosen = $this->db->query("SELECT * FROM tbl_exception 
																	WHERE nid = '".$user['userid']."' 
																	AND tahunakademik = '".$actyear."' 
																	AND is_active = '1'")->num_rows();

										// if yes
										if ($getdosen > 0) { ?>
											<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai</a>

											<?php $tipe = "<input type=\"hidden\" name=\"tipeuji\" value=\"all\">"; ?>

										<!-- if no -->
										<?php } else { ?>
											<a class="btn btn-danger" onclick="alert('Masa pengunggahan nilai sudah melewati batas waktu!')">
												<i class="icon icon-remove"></i> Upload Nilai 
											</a>
										<?php } ?>
										
									<?php } ?>
									<!-- manage button upload nilai end -->

									<a 
										href="<?= base_url('form/formnilai/dwld_excel/'.$id_jadwal); ?>" 
										class="btn btn-primary"> 
										Download Form 
									</a>

								<?php } ?>

							<a href="<?= base_url(); ?>form/formnilai/cetak_nilai/<?= $id_jadwal; ?>" target="_blank" class="btn btn-success"> Cetak </a>
							
							<hr>
							<table id="example1" class="table table-bordered table-striped">
			                	<thead>
			                        <tr> 
			                        	<th width="50">No</th>
		                                <th>NIM</th>
		                                <th>Nama</th>
		                                <?php
		                                if (is_final_assignment($mk->nama_matakuliah)) { ?>

											<th width="40">Angka</th>

										<?php } else { ?>

											<th width="80">Absensi (10%)</th>
			                                <th width="80">Tugas (20%)</th>
			                                <th width="80">UTS (30%)</th>
			                                <th width="80">UAS (40%)</th>
				                            <th width="40">Angka</th>

										<?php } ?>

		                                <th width="40">Huruf</th>
			                        </tr>
			                    </thead>

								<?php 

								# get index nilai
								$this->db->select('nilai_bawah, nilai_atas, nilai_huruf');
								$this->db->where('deleted_at');
								$data = $this->db->get('tbl_index_nilai')->result();

								 ?>

			                    <tbody>
									<?php $no = 1; foreach($look as $row){?>
			                        <tr>
			                        	<td><?php echo $no; ?></td>
			                        	<td><?php echo $row->NIMHSMSMHS; ?></td>
			                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
			                        	<?php 

			                        	$nilaitugas = 0;
			                        	$nilaiabsen = 0;
			                        	$nilaiuts 	= 0;
			                        	$nilaiuas 	= 0; 
			                        	$nilaiakhir	= 0; 
			                        	
		                        		$getNilai = $this->db->query("SELECT distinct tipe,nilai 
		                        								from tbl_nilai_detail 
																where npm_mahasiswa = '".trim($row->NIMHSMSMHS)."'
																and kd_jadwal = '".$row->kd_jadwal."' 
																and tipe IN (1,2,3,4,10)")->result();

											foreach ($getNilai as $key) {

												switch ($key->tipe) {
													case '1':
														$nilaitugas = $key->nilai;
														break;
													case '2':
														$nilaiabsen = $key->nilai;
														break;
													case '3':
														$nilaiuts = $key->nilai;
														break;
													case '4':
														$nilaiuas = $key->nilai;
														break;
													case '10':
														$nilaiakhir = $key->nilai;
														break;
												}
											}

			                        		// buat range tanggal selama 1 bulan untuk pengecekan tipe ujian
											$makeDateRange = strtotime(date('Y-m-d').'+ 4 week');
											$dateRange = date('Y-m-d',$makeDateRange);

			                        		$examtype 	= $this->db->query("SELECT tipe_uji FROM tbl_jadwaluji 
			                        										WHERE id_jadwal = '$id_jadwal' 
			                        										AND start_date <= '$dateRange'
			                        										ORDER BY tipe_uji DESC LIMIT 1")->row()->tipe_uji; 
			                        		
											if($examtype == 1) {
												$rt = $nilaiuts;
											} elseif ($examtype == 2) {
												$rt = $nilaiakhir;
											} else {
												$rt = $nilaiakhir;
											}

											$rtnew = number_format($rt,2);

											# get nilai_huruf dase on poin range
											foreach ($data as $keys => $value) {
												if (in_array((int)$rtnew, range((int)$value->nilai_bawah, (int)$value->nilai_atas, 0.01))) {
													$rw = $value->nilai_huruf;	
												}
											}

		                               	if (is_final_assignment($mk->nama_matakuliah)) { ?>

												<td><?php if($rt > 100){ echo "-"; } else { echo $rtnew; } ?></td>

										<?php } else { ?>

											<td>
												<?php if($nilaiabsen > 100) { 
														echo "-"; 
												} else { 
													echo $nilaiabsen; 
												} ?>
											</td>
			                                <td>
			                                	<?php if($nilaitugas > 100) { 
			                                		echo "-"; 
			                                	} else { 
			                                		echo $nilaitugas; 
			                                	} ?>
			                                </td>
			                                <td>
			                                	<?php if($nilaiuts > 100) { 
			                                		echo "-"; 
			                                	} else { 
			                                		echo $nilaiuts; 
			                                	} ?>
			                                </td>
			                                <td>
			                                	<?php if($nilaiuas > 100) { 
			                                		echo "-"; 
			                                	} else { 
			                                		echo $nilaiuas; 
			                                	} ?>
			                                </td>
				                        	<td>
				                        		<?php if($rtnew > 100) { 
				                        			echo "-"; 
				                        		} else { 
				                        			echo $rtnew; 
				                        		} ?>
				                        	</td>

										<?php } ?>
		                                
			                        	<td><?php echo $rw; ?></td>
			                        </tr>
									
									<?php $no++; } ?>
			                    </tbody>
			               	</table>
						</div>
					</div>
            	</div>
          	</div>
			
          	<style type="text/css" media="screen">
          		.lebar {
          			width: 80%;
          		}	
          	</style>
          	<div class="tab-pane <?php if ($tayang_nilai == 'ada') {echo 'active';}else{echo '';}?>" id="satuan">
          		<div class="row">
          			<div class="span12">
          				<?php 
          				if ((strtoupper($mk->nama_matakuliah) == 'SKRIPSI') 
          					OR (strtoupper($mk->nama_matakuliah) == 'KULIAH KERJA MAHASISWA') 
          					OR (strtoupper($mk->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
	          				OR ($mk->nama_matakuliah == 'Seminar dan Tugas Akhir') 
	          				OR ($mk->nama_matakuliah == 'KULIAH KERJA NYATA') 
	          				OR ($mk->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') 
	          				OR ($mk->nama_matakuliah == 'Kuliah Kerja Praktek') 
	          				OR (strtoupper($mk->nama_matakuliah) == 'MAGANG KERJA') 
	          				OR (strtoupper($mk->nama_matakuliah) == 'KERJA PRAKTEK') 
	          				OR ($mk->nama_matakuliah == 'Tesis') 
	          				OR (strtoupper($mk->nama_matakuliah) == 'THESIS')) { ?>

								<div class="span6">

						<?php } else { ?>

							<div class="span11">

						<?php } ?>
		    				
	                		<fieldset>
	                			<form class="form-horizontal" action="<?= base_url('form/formnilai/histori_nilai'); ?>" method='POST'>
		                  			<div class="control-group">
			                        	<label class="control-label">NPM Mahasiswa </label>
			                        	<div class="controls">
			                        		
		                        			<input class="form-control span3" placeholder="Cari NPM Mahasiswa" type="text" name="npm" required="">
		                        			<input type="hidden" name="npm_mhs" >
		                        			<input type="hidden" name="kd_krs" >
		                        			<input type="hidden" value="<?php echo $kode_jadwal; ?>"  name="kd_jadwal">
		                        			<input type="hidden" value="<?php echo $id_jadwal; ?>"  name="id_jadwal">
		                          			<button id="klik" class="btn btn-primary" type="submit">Cari</button>
			                        				                   
			                        	</div>
			                        </div>

			                        	<?php if ($tayang_nilai == 'ada') {
						        			echo '<div class="control-group">
					                        		<label class="control-label">NPM :</label>
						                        	<div class="controls">
						                        		<input class="form-control span3" value="'.$npm.'"  type="text" disabled >
						                        	</div>
			                        			</div>
					                        	<div class="control-group">
					                        		<label class="control-label">Nama :</label>
						                        	<div class="controls">
						                        		<input class="form-control span3" value="'.$nama.'"  type="text" disabled >
						                        	</div>
					                        	</div>';
					          			} ?>
		                        </form>

		                      	<hr>
		                      	<table id="nilai_nilai" class="table table-bordered">
						            <thead>
							            <?php
		                                if ((strtoupper($mk->nama_matakuliah) == 'SKRIPSI') 
		                                	OR (strtoupper($mk->nama_matakuliah) == 'KULIAH KERJA MAHASISWA') 
		                                	OR (strtoupper($mk->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
			                                OR ($mk->nama_matakuliah == 'Seminar dan Tugas Akhir') 
			                                OR ($mk->nama_matakuliah == 'KULIAH KERJA NYATA') 
			                                OR ($mk->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') 
			                                OR ($mk->nama_matakuliah == 'Kuliah Kerja Praktek') 
			                                OR (strtoupper($mk->nama_matakuliah) == 'MAGANG KERJA') 
			                                OR (strtoupper($mk->nama_matakuliah) == 'KERJA PRAKTEK') 
			                                OR ($mk->nama_matakuliah == 'Tesis') 
			                                OR (strtoupper($mk->nama_matakuliah) == 'THESIS')) { ?>

											<th>Nilai Akhir</th>

										<?php } else { ?>

											<th>Absen Dosen</th>
							            	<th>Absen Mhs</th>
							            	<th id="tg1">Tugas 1</th>
							            	<th id="tg2">Tugas 2</th>
							            	<th id="tg3">Tugas 3</th>
							            	<th id="tg4">Tugas 4</th>
							            	<th id="tg5">Tugas 5</th>
							            	<th>UTS</th>
							            	<th>UAS</th>
							            	<th>Absen</th>
							            	<th>Rata-rata</th>
							            	<th>Nilai Akhir</th>

										<?php } ?>
						            	
						            	<th>Nilai Huruf</th>
						            </thead>
						            <tbody>
						            <form class="form-horizontal" action="<?= base_url(); ?>form/formnilai/update_nilai22" method="post">
						            	<tr >
							            	<?php
			                                if ((strtoupper($mk->nama_matakuliah) == 'SKRIPSI') 
			                                	OR (strtoupper($mk->nama_matakuliah) == 'KULIAH KERJA MAHASISWA') 
			                                	OR (strtoupper($mk->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
				                                OR ($mk->nama_matakuliah == 'Seminar dan Tugas Akhir') 
				                                OR ($mk->nama_matakuliah == 'KULIAH KERJA NYATA') 
				                                OR ($mk->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') 
				                                OR ($mk->nama_matakuliah == 'Kuliah Kerja Praktek') 
				                                OR (strtoupper($mk->nama_matakuliah) == 'MAGANG KERJA') 
				                                OR (strtoupper($mk->nama_matakuliah) == 'KERJA PRAKTEK') 
				                                OR ($mk->nama_matakuliah == 'Tesis') 
				                                OR (strtoupper($mk->nama_matakuliah) == 'THESIS')) { ?>

												<td>
													<input class="form-control span1" id="nilaiahir_kp"  type="text"  value="<?php echo number_format($nilai_edit->nilai_ahir,2); ?>" name="tugas[10]">
												</td>
												<td>
													<input class="form-control span1" id="huruf2"  type="text"  value="" name="nhuruf" disabled>
												</td>
												<input type="hidden" value="<?= $nilai_edit->NIMHSMSMHS; ?>" name="nim_mhs" >

											<?php } else { ?>

												<input type="hidden" name="" id="bags" value="<?= $ada; ?>">
												<td>
													<input class="form-control lebar" id="dsn" type="text" value="<?php echo number_format($absendosen); ?>" name="" readonly>
												</td>
												<td>
													<input class="form-control lebar" id="mhs"  type="text" value="<?php echo number_format($absenmhs); ?>" name="" readonly>
												</td>
							            		<td id="tk1">
							            			<input class="form-control lebar" id="tgs1"  type="text"  value="<?php echo number_format($nilai_edit->tugas1,2); ?>" name="tugas[5]" >
							            		</td>
												<td id="tk2">
													<input class="form-control lebar" id="tgs2"   type="text" value="<?php echo number_format($nilai_edit->tugas2,2); ?>" name="tugas[6]" >
												</td>
												<td id="tk3">
													<input class="form-control lebar" id="tgs3"   type="text" value="<?php echo number_format($nilai_edit->tugas3,2); ?>" name="tugas[7]" >
												</td>
												<td id="tk4">
													<input class="form-control lebar" id="tgs4"   type="text" value="<?php echo number_format($nilai_edit->tugas4,2); ?>" name="tugas[8]" >
												</td>
												<td id="tk5">
													<input class="form-control lebar" id="tgs5"   type="text" value="<?php echo number_format($nilai_edit->tugas5,2); ?>" name="tugas[9]" >
												</td>
							            		<td>
							            			<input class="form-control lebar" id="uts"   type="text" value="<?php echo number_format($nilai_edit->uts); ?>" name="tugas[3]" >
							            		</td>
							            		<td>
							            			<input class="form-control lebar" id="uas"   type="text" value="<?php echo number_format($nilai_edit->uas); ?>"  name="tugas[4]" >
							            		</td>
							            		<td>
							            			<input class="form-control lebar" id="absen"   type="text" value="<?php echo number_format($nilai_edit->absen); ?>"  name="tugas[2]" readonly>
							            		</td>
							            		<td>
							            			<input class="form-control lebar" id="tugasrata"   type="text" value="<?php echo number_format($nilai_edit->tugas, 2); ?>" name="tugas[1]">
							            		</td>
							            		<td>
							            			<input class="form-control lebar" id="nilaiahir"  type="text"  value="<?php echo number_format($nilai_edit->nilai_ahir, 2); ?>" name="tugas[10]" readonly>
							            		</td>
							            		<td>
							            			<input class="form-control lebar" id="huruf"  type="text"  value="" name="nhuruf" disabled>
							            		</td>
							            		<input type="hidden" value="<?= $nilai_edit->NIMHSMSMHS; ?>" name="nim_mhs" >
											<?php } ?>
						            		
						            		
						            		<input class="form-control span1" id="hdnilai" type="hidden" value="hdnilai" name="" disabled>
						            		<input class="form-control span1" id="kd_krs" type="hidden" value="" name="kd_krs" disabled>
						            		<input class="form-control span1" id="npm2" type="hidden" value="" name="npm2" disabled>
						            	</tr>
						            
						            </tbody>
						      	</table>
						      	
		                      	<div class="form-actions">
		                        	<input class="btn btn-large btn-primary" type="submit" value="Update">
		                        	<input class="btn btn-large btn-default" type="reset" value="Clear">
		                      	</div>
		                      	</form>	
	                		</fieldset>
		              		
		              	</div>	
          			</div>
          		</div>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM UPLOAD NILAI</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>form/formnilai/upload_excel" method="post" enctype="multipart/form-data">
	            <div class="modal-body">
	            	
		            <div class="control-group" id="">
		                <label class="control-label">Upload Data Nilai (.xls) </label>
		                <div class="controls">
		                    <input type="file" class="span4 form-control" name="userfile" required/>
		                    <input type="hidden" name="id_jadwal" value="<?php echo $id ?>" />
		                    <input type="hidden" name="kode_jdl" value="<?php echo $kode_jadwal; ?>">
		                    
		                    <!-- sesi ujian (UTS/UAS) -->
		                    <?php echo $tipe ?>

		                </div>
		            </div>
	            </div>
	            <div class="modal-footer">
	              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	              <input type="submit" class="btn btn-primary" value="Submit"/>
	          	</div>
        	</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
