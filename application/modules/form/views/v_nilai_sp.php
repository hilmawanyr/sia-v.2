<div class="widget-content">
    <div class="row">
		<div class="span12">      
				<center>
  				<h4>Data Nilai Mahasiswa ( '<?php echo $rows->nama_matakuliah; ?>' ) - <?php echo $rows->kelas; ?> (<i><u><?php $ta = $this->app_model->getdetail('tbl_tahunakademik','kode',$rows->kd_tahunajaran,'kode','asc')->row(); // echo $ta->tahun_akademik; ?>)</i></u></h4>
		 	</center>
		
			<br>
			<div class="span11">
				<a href="<?php echo base_url('form/formnilaisp/view/'.$year); ?>" class="btn btn-warning"> <i class="icon icon-arrow-left"></i> Kembali </a>
				<?php 
					$logged = $this->session->userdata('sess_login');
					$pecah = explode(',', $logged['id_user_group']);
					$jmlh = count($pecah);
					for ($i=0; $i < $jmlh; $i++) { 
						$grup[] = $pecah[$i];
					}
					$aktif = $this->setting_model->getaktivasi('nilai_rmd')->result();
					// var_dump($aktif);exit();
					if  ((in_array(8, $grup)) and (count($aktif) != 0)) { ?>

				<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai </a>
				
				<?php } ?>				
				
				<a href="<?php echo base_url(); ?>form/formnilaisp/dwld_excel/<?php echo $rows->id_jadwal; ?>" class="btn btn-primary"> Download Form </a>
				<a href="<?php echo base_url(); ?>form/formnilaisp/cetak_nilai/<?php echo $rows->id_jadwal; ?>" target="_blank" class="btn btn-success"> Cetak </a>
				
				<hr>
				<table id="example1" class="table table-bordered table-striped">
                	<thead>
                        <tr> 
                        	<th width="50">No</th>
                            <th>NIM</th>
                            <th>Nama</th>
							<th width="80">Absensi (10%)</th>
                            <th width="80">Tugas (20%)</th>
                            <th width="80">UTS (30%)</th>
                            <th width="80">UAS (40%)</th>
                            <th width="40">Angka</th>
                            <th width="40">Huruf</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php $no = 1; foreach($load as $row){?>
                        <tr>
                        	<td><?php echo $no; ?></td>
                        	<td><?php echo $row->NIMHSMSMHS; ?></td>
                        	<td><?php echo $row->NMMHSMSMHS; ?></td>

							<?php
                    			for ($i = 1; $i < 6; $i++) {
                    			 	$nilai[$i] = $this->temph_model->load_nilai($row->NIMHSMSMHS,$row->kd_krs,$row->kd_jadwal,$i)->row();
                    			 } 
                    		?>

                        	<?php
                    			$rt = $nilai[5]->nilai;
                        		$rtnew = number_format($rt,2);
								if (($rtnew >= 80) and ($rtnew <= 100)) {
									$rw = "B";
								} elseif (($rtnew >= 76) and ($rtnew <= 79.99)) {
									$rw = "B";
								} elseif (($rtnew >= 72) and ($rtnew <= 75.99)) {
									$rw = "B";
								} elseif (($rtnew >= 68) and ($rtnew <= 71.99)) {
									$rw = "B";
								} elseif (($rtnew >= 64) and ($rtnew <= 67.99)) {
									$rw = "B-";
								} elseif (($rtnew >= 60) and ($rtnew <= 63.99)) {
									$rw = "C+";
								} elseif (($rtnew >= 56) and ($rtnew <= 59.99)) {
									$rw = "C";
								} elseif (($rtnew >= 45) and ($rtnew <= 55.99)) {
									$rw = "D";
								} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
									$rw = "E";
								} elseif ($rtnew >100) {
									$rw = "T";
								}
                        	?>
                    		
							<td><?php echo $nilai[2]->nilai; ?></td>
							<td><?php echo $nilai[1]->nilai; ?></td>
							<td><?php echo $nilai[3]->nilai; ?></td>
							<td><?php echo $nilai[4]->nilai; ?></td>
							<td><?php echo $nilai[5]->nilai; ?></td>                   
                        	<td><?php echo $rw; ?></td>
                        </tr>
						<?php $no++; } ?>
                    </tbody>
               	</table>
			</div>
		</div>
	</div>
          	
</div>




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM UPLOAD NILAI</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>form/formnilaisp/upload_excel" method="post" enctype="multipart/form-data">
	            <div class="modal-body">
	            	
		            <div class="control-group" id="">
		                <label class="control-label">Upload Data Nilai (.xls) </label>
		                <div class="controls">
		                    <input type="file" class="span4 form-control" name="userfile" required/>
		                    <input type="hidden" name="id_jadwal" value="<?php echo $idjadwal ?>" />
		                    <input type="hidden" name="kode_jdl" value="<?php echo $kode_jadwal_bro; ?>">
		                    <!-- <input type="hidden" name="kd_trans" value="<?php echo $ding->kd_dosen.'/'.$ding->kd_matakuliah.$ding->NIMHSMSMHS; ?>"> -->
		                </div>
		            </div>
	            </div>
	            <div class="modal-footer">
	              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	              <input type="submit" class="btn btn-primary" value="Submit"/>
	          	</div>
        	</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->