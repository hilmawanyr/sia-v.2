<link href="<?php echo base_url(); ?>assets/js/wizard/css/gsdk-base.css" rel="stylesheet" />


<div class="row">
	<div class="span12">      		  		
  		
				<!-- form wizard -->
				<div class="card wizard-card ct-wizard-green" id="wizardProfile">
                    <form action="<?php echo base_url(); ?>form/formpmbmhs/bio_cmb" method="post" class="form-horizontal" enctype="multipart/form-data">
                <!--        You can switch "ct-wizard-orange"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
                
                      <div class="wizard-header">
                          <h3>
                             <b>FORMULIR PENDAFTARAN CALON MAHASISWA</b><br>
                             
                          </h3>
                      </div>
                        <ul>
                            <li><a href="#about" data-toggle="tab">Data Pribadi</a></li>
                            <li><a href="#account" data-toggle="tab">Data Orang Tua</a></li>
                            <li><a href="#address" data-toggle="tab">Pilihan Jurusan</a></li>
                        </ul>
                        
                        <div class="tab-content">
                            <div class="tab-pane" id="about">
                              <div class="row">
                                  <h4 class="info-text"> Isi data diri anda</h4>
                                  <div class="col-sm-4 col-sm-offset-1">
                                     <div class="picture-container">
                                          <div class="picture">
                                              <img src="<?php echo base_url(); ?>assets/js/wizard/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                              <input type="file" id="wizard-picture" name="userfile">
                                          </div>
                                          <h6>Upload Foto</h6>
                                      </div>
                                  </div>
                                  <br>
                                  <fieldset style="margin-left:20px;">
								    	                <div class="control-group">
                                        <label class="control-label">No Identitas</label>                                
                                        <div class="controls">
                                         <input type="text" class="form-control span6" name="noid"  /> *KTP/SIM/Kartu Pelajar
                                        </div>
                                      </div>
                                      <div class="control-group">
								                        <label class="control-label">Nama</label>                                
								                        <div class="controls">
								            	           <input type="text" class="form-control span6" name="nmcm"  /> *sesuai dengan KTP/ijazah
											                  </div>
										                  </div>
										                  <div class="control-group">
            								            <label class="control-label">Tempat,Tanggal Lahir</label>
            								            <div class="controls">
            								            	<input type="text" class="form-control" name="tptlhr"  /> / <input type="text" id="tgllhr" class="form-control span3" name="tglhr"  />
                  											</div>
                  										</div>
                                      <div class="control-group">
                                        <label class="control-label">Jenis Kelamin</label>                                
                                        <div class="controls">
                                          <input type="radio" name="jk" value="L" > Laki - Laki
                                          <input type="radio" name="jk" value="P"> Wanita
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Alamat</label>                                
                                        <div class="controls">
                                         <textarea name="almt" class="form-control span6" ></textarea>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Nomor Telp</label>                                
                                        <div class="controls">
                                         <input type="text" class="form-control span4" name="tlp"  /> *aktif
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Agama</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="agm"  >
                                            <option>-- Pilih Agama --</option>
                                            <option value="islam">Islam</option>
                                            <option value="protestan">Protestan</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">E-mail</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span4" name="email"  />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Status</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="stts"  >
                                            <option>-- Pilih Status --</option>
                                            <option value="M">Menikah</option>
                                            <option value="BM">Belum Menikah</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Asal Sekolah</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control" name="aslskl"  />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tahun Lulus</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span2" name="thls"  />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">No Ijazah Sekolah</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span6" name="noijz" /> *bila sudah ada
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">NEM</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span2" name="nem" /> *bila sudah ada
                                        </div>
                                      </div>
                  									</fieldset>
                              </div>
                            </div>


                            <div class="tab-pane" id="account">
                                <h4 class="info-text"> What are you doing? (checkboxes) </h4>
                                <div class="row">
                                   <h4 class="info-text"> Let's start with the basic information (with validation)</h4>
                                  <br>
                                  <fieldset style="margin-left:20px;">
                                      <div class="control-group">
                                        <label class="control-label">Nama Ayah</label>                                
                                        <div class="controls">
                                         <input type="text" class="form-control span6" name="nmdad"  />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Nama Ibu</label>                                
                                        <div class="controls">
                                         <input type="text" class="form-control span6" name="nmom"  />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Nomor Telp</label>                                
                                        <div class="controls">
                                         <input type="text" class="form-control span4" name="tlpot"  /> *aktif
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Alamat Orang Tua</label>                                
                                        <div class="controls">
                                         <textarea name="almtot" class="form-control span6" ></textarea>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Agama Ayah</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="agmdad"  >
                                            <option>-- Pilih Agama --</option>
                                            <option value="islam">Islam</option>
                                            <option value="protestan">Protestan</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                            <option value="lain">Lain-lain</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Agama Ibu</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="agmom"  >
                                            <option>-- Pilih Agama --</option>
                                            <option value="islam">Islam</option>
                                            <option value="protestan">Protestan</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                            <option value="lain">Lain-lain</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Status Ayah</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="stdad"  >
                                            <option>-- Pilih Status --</option>
                                            <option value="H">Masih Hidup</option>
                                            <option value="M">Sudah Meninggal</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Status Ibu</label>                                
                                        <div class="controls">
                                          <select class="form-control" name="stmom"  >
                                            <option>-- Pilih Status --</option>
                                            <option value="H">Masih Hidup</option>
                                            <option value="M">Sudah Meninggal</option></select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pendidikan Ayah</label>                                
                                        <div class="controls">
                                          <select class="form-control span3" name="pdkdad"  >
                                            <option>-- Pilih Pendidikan --</option>
                                            <option value="ttsd">Tidak Tamat SD</option>
                                            <option value="tsd">Tamat SD</option>
                                            <option value="tsltp">Tamat SLTP</option>
                                            <option value="tslta">Tamat SLTA</option>
                                            <option value="d3">Diploma</option>
                                            <option value="sm">Sarjana Muda</option>
                                            <option value="s1">Sarjana</option>
                                            <option value="s2">Pasca Sarjana</option>
                                            <option value="s3">Dokor</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pendidikan Ibu</label>                                
                                        <div class="controls">
                                          <select class="form-control span3" name="pdkmom"  >
                                            <option>-- Pilih Pendidikan --</option>
                                            <option value="ttsd">Tidak Tamat SD</option>
                                            <option value="tsd">Tamat SD</option>
                                            <option value="tsltp">Tamat SLTP</option>
                                            <option value="tslta">Tamat SLTA</option>
                                            <option value="d3">Diploma</option>
                                            <option value="sm">Sarjana Muda</option>
                                            <option value="s1">Sarjana</option>
                                            <option value="s2">Pasca Sarjana</option>
                                            <option value="s3">Dokor</option></select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pekerjaan Ayah</label>                                
                                        <div class="controls">
                                          <input type="radio" name="workdad" value="PN" > Pegawai Negeri
                                          <input type="radio" name="workdad" value="TP"> TNI / POLRI
                                          <br>
                                          <input type="text" class="form-control span3" name="workdad_dll" style="height: 20px;"/> *Lain - Lain
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pekerjaan Ibu</label>                                
                                        <div class="controls">
                                          <input type="radio" name="workmom" value="PN" > Pegawai Negeri
                                          <input type="radio" name="workmom" value="TP"> TNI / POLRI
                                          <br>
                                          <input type="text" class="form-control span3" name="workmom_dll" style="height: 20px;" /> *Lain - Lain
                                        </div>
                                      </div>
                                    </fieldset>
                            	</div>
                            </div>
                            <div class="tab-pane" id="address">
                                <div class="row">
                                   <h4 class="info-text"> Let's start with the basic information (with validation)</h4>
                                  <br>
                                  <fieldset style="margin-left:20px;">
                                      <div class="control-group">
                                        <label class="control-label">Pilihan Kampus</label>                                
                                        <div class="controls">
                                          <select class="form-control span3" name="opsikps"  >
                                            <option>-- Pilih Kampus --</option>
                                            <option value="jkt">Jakarta</option>
                                            <option value="bks">Bekasi</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pilihan Kelas</label>                                
                                        <div class="controls">
                                          <select class="form-control span3" name="opsikls"  >
                                            <option>-- Pilih Kelas --</option>
                                            <option value="pagi">Pagi</option>
                                            <option value="sore">Sore</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pilihan Jurusan</label>                                
                                        <div class="controls">
                                          <select class="form-control span4" name="opsijur"  >
                                            <option>-- Pilih Pendidikan --</option>
                                            <option disabled="">-- Fakultas Teknik --</option>
                                            <option value="55201">Teknik Informatika</option>
                                            <option value="26201">Teknik Industri</option>
                                            <option value="24201">Teknik Kimia</option>
                                            <option value="25201">Teknik Lingkungan</option>
                                            <option value="32201">Teknik Perminyakan</option>
                                            <option disabled="">-- Fakultas Psikologi --</option>
                                            <option value="73201">Ilmu Psikologi</option>
                                            <option disabled="">-- Fakultas Hukum --</option>
                                            <option value="74201">Ilmu Hukum</option>
                                            <option disabled="">-- Fakultas Ilmu Komunikasi --</option>
                                            <option value="70201">Ilmu Komunikasi</option>
                                            <option disabled="">-- Fakultas Ekonomi --</option>
                                            <option value="62201">Akuntansi</option>
                                            <option value="61201">Manajemen</option>
                                            <option disabled="">-- Pasca Sarjana --</option>
                                            <option value="61101">Magister Manajemen</option>
                                            <option value="74101">Magister Hukum</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Pernah Kuliah di PTS Lain</label>                                
                                        <div class="controls">
                                          <input type="radio" name="ptsln" value="Y" > Ya
                                          <input type="radio" name="ptsln" value="T"> Tidak
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Jurusan</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span3" id="" name="jurptsln" />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tahun Masuk / Lulus</label>                                
                                        <div class="controls">
                                          <input type="text" class="form-control span2" id="" name="tmtl" />
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Referensi</label>                                
                                        <div class="controls">
                                         <textarea name="rf" class="form-control span6" ></textarea> *bila ada
                                        </div>
                                      </div>
                                    </fieldset>
                            	</div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />
        
                            </div>
                            
                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>  
                    </form>
                </div>

 
    <script>
  $(function() {
    $( "#tgllhr" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd",
      yearRange: "1950:2030"
    });
  });
  </script>
<!-- wizard -->
<script src="<?php echo base_url(); ?>assets/js/wizard/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/wizard/js/wizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wizard/js/jquery.validate.min.js"></script>