<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?php echo base_url();?>form/penugasandosen/load_detail/'+idk);
    }

function status(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status/'+idk);
    }

function status2(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status2/'+idk);
    }
</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Pembimbing</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <!-- <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print</a>
                <hr> -->
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama</th>
                                <th>Judul</th>
                                <th>Tahun Ajaran</th>
                                <th>Pembimbing 1</th>
                                <th>Pembimbing 2</th>
                                <th width="120">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; foreach ($getData as $value) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $value->NIMHSMSMHS; ?></td>
                                <td><?php echo $value->NMMHSMSMHS; ?></td>
                                <?php $cekjudul = $this->db->query("select * from tbl_skripsi where npm_mahasiswa = '".$value->NIMHSMSMHS."' and tahunajaran = '".$tahunajaran->kode."'")->row() ?>
                                <td><?php echo $cekjudul->judul_skripsi; ?></td>
                                <td><?php echo $tahunajaran->tahun_akademik; ?></td>
                                <?php
                                    $nama1 = $this->db->query("SELECT * from tbl_karyawan where nid = '".$cekjudul->pembimbing1."'")->row();
                                    $nama2 = $this->db->query("SELECT * from tbl_karyawan where nid = '".$cekjudul->pembimbing2."'")->row();
                                ?>
                                <td><?php echo $nama1->nama; ?></td>
                                <td><?php echo $nama2->nama; ?></td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo ''.$value->NIMHSMSMHS.''; ?>)"  data-toggle="modal" href="#myModal"><i class="btn-icon-only icon-ok"> </i></a>
                                    <a class="btn btn-success btn-small" onclick="status(<?php echo ''.$value->NIMHSMSMHS.''; ?>)"  data-toggle="modal" href="#myModal1"><i class="btn-icon-only icon-table"> </i></a>
                                    <a class="btn btn-warning btn-small" onclick="status2(<?php echo ''.$value->NIMHSMSMHS.''; ?>)"  data-toggle="modal" href="#myModal1"><i class="btn-icon-only icon-table"> </i></a>
                                </td>
                            </tr>
                        <?php $no++;} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->