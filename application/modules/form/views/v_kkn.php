
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Pengajuan KKN</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<a class="btn btn-primary" href="<?php echo base_url();?>form/kkn/master" data-toggle="tooltip" title="Kelola KKN"><i class="icon-cogs"></i> Kelola KKN</a>
				
				<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr> 
								<th>No</th>
								<th>NPM</th>
								<th>Nama</th>
								<th>Penyakit</th>
								<th>KKN</th>
								<th>Waktu Pengajuan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;
							foreach($pengajuan AS $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->npm; ?></td>
								<td><?php echo getNameMhs($row->npm) ?></td>
								<td><?php echo $row->penyakit ?></td>
								<td><?php echo getKKN($row->kkn) ?></td>
								<td><?php echo datetimeIdn($row->createddate) ?></td>
								<td>
								<a href="<?php echo base_url();?>form/kkn/print_kkn/<?php echo $row->id_kkn?>/<?php echo $row->npm?>" class="btn btn-info btn-xs" title="Cetak Formulir" ><i class="icon-print"> </i></a>
								</td>
							</tr>
							<?php $no++;} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
		$("#bantuan").hide();
		$('#help').css('cursor', 'pointer');
			$("#help").click(function() {
					$("#bantuan").toggle();
			});
	});
</script>
