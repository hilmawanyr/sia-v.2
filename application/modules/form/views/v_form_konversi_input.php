<script>
    function edit(idk){
        $('#edit1').load('<?php echo base_url();?>form/formnilai_konversi/edit_mk_mhs/'+idk);
    }
</script>

<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}

.ada_nilai{
    background-color: #D9534F;
}
</style>



<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Mahasiswa Konversi - <?php echo $this->session->userdata('mhs').' ('.$this->session->userdata('nim').')' ?></h3>
            </div> <!-- /widget-header -->

            

            <div class="widget-content">
                <div class="span11">
                    <fieldset>
                        <h4 style="text-align:center" >Data Matakuliah Terkonversi</h4>                        
                        <a data-toggle="modal" href="#myModal" class="btn btn-success btn-small"> Input Matakuliah</a>
                        <!-- <a href="<?php //echo base_url();?>form/formnilai_konversi/cetak_transkip<?php //echo $isi->NIMHSMSMHS; ?>" 
                           target='_blank' class="btn btn-primary btn-small" data-toggle="tooltip" title="Print Transkip">
                            Transkip
                        </a>  -->                                           
                        <hr>
                    </fieldset>

                    <table id="example1" class="table">
                        <thead>
                            <tr> 

                                <th>No</th>

                                <th>NPM</th>

                                <th>Nama</th>

                                <th>SKS</th>

                                <th>Nilai Huruf</th>

                                <th>Nilai Mutu</th>                                

                                <th width="80">Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($matkul as $isi) { ?>
                                <tr  <?php  
                                        $user = $this->session->userdata('sess_login');
                                        
                                        ?> >
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $isi->KDKMKTRLNM; ?></td>
                                    <td><?php echo $isi->nama_matakuliah; ?></td>
                                    <td><?php echo $isi->sks_matakuliah; ?></td>
                                    <td><?php echo $isi->NLAKHTRLNM; ?></td>
                                    <td><?php echo $isi->BOBOTTRLNM; ?></td>
                                    <td>
                                        <a  onclick="edit(<?php echo $isi->id;?>)" data-toggle="modal" href="#editModal1"> <i class="btn-icon-only icon-pencil"></i></a>
                                        <a  onclick="return confirm('Yakin ingin menghapus Matakuliah ini ?');" 
                                        href="<?php echo base_url(); ?>form/formnilai_konversi/delete_mk/<?php echo $isi->id; ?>"> <i class="btn-icon-only icon-trash"></i></a>
                                    </td>
                               </tr>
                            <?php $no++; } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- modal edit -->
<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">            
            
        </div>
    </div>
</div>


<!-- modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">FORM DATA</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>form/formnilai_konversi/add_matkul" method="post">

                <div class="modal-body">

                    <div class="control-group">                                         

                        <label class="control-label" for="matkul">Kode Matakuliah Asal</label>

                        <div class="controls">

                           <input type="text" class="form-control" name="kd_mk_asal" required/>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                         

                        <label class="control-label" for="matkul">Nama Matakuliah Asal</label>

                        <div class="controls">

                           <input type="text" class="form-control" name="nm_mk_asal" required/>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                         

                        <label class="control-label" for="matkul">SKS Matakuliah Asal</label>

                        <div class="controls">

                           <input type="number" class="form-control" name="sks_mk_asal" required/>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                         

                        <label class="control-label" for="matkul">Nilai Matakuliah Asal</label>

                        <div class="controls">

                           <input type="text" class="form-control" name="nilai_mk_asal" required/>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                         

                        <label class="control-label" for="matkul">Nama Matakuliah</label>

                        <div class="controls">

                            <select id="matkul" name="matkul" class="form-control"  required>
                                <option disabled selected>-- Pilih Matakuliah --</option>
                                <?php foreach ($slct_mk as $isi) { 
                                    echo '<option value="'.$isi->kd_matakuliah.'">'.$isi->kd_matakuliah.' - '.$isi->nama_matakuliah.'</option>';
                                } ?>
                            </select>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->
                    
                    <div class="control-group">                                     

                        <label class="control-label" for="nilai2">Nilai</label>

                        <div class="controls">

                            <select id="nilai" name="nilai" class="form-control"  required>
                                <option disabled selected>-- Pilih Nilai --</option>
                                <?php foreach ($nilai as $isi) { 
                                    echo '<option value="'.$isi->nilai_huruf.'/'.$isi->nilai_mutu.'">'.$isi->nilai_huruf.' / '.$isi->nilai_mutu.'</option>';
                                } ?>
                            </select>
                        </div> <!-- /controls -->               
                    </div> <!-- /control-group -->
                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


