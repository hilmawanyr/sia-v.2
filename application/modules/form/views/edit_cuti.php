            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Persetujuan Cuti</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>form/formcuti/aprove/" method="post">
                <input type="hidden" name="identity" value="<?php echo $a->id_cuti; ?>">
                <input type="hidden" name="kd_ct" value="<?php echo $a->kode_cuti; ?>" class="form-control span4">
                <input type="hidden" name="npmhs" value="<?php echo $a->npm_mhs; ?>" class="form-control span4">
                <input type="hidden" name="tanggal" value="<?php echo $a->tgl_permohonan; ?>">
                <input type="hidden" name="pemakad" value="<?php echo $a->pembimbing_akademik; ?>">
                <input type="hidden" name="thn_akad" value="<?php echo $a->tahun_akademik_cuti; ?>">
                <input type="hidden" name="deskrip" value="<?php echo $a->desksripsi; ?>">
                
                <div class="modal-body" style="margin-left: -60px;">
                    <?php $wk = $this->session->userdata('sess_login'); $kok = $wk['id_user_group'];
                        if ($kok == 11 || $kok == 10) { ?>
                           <div class="control-group" id=""></div>
                    <?php } else { ?> 
                    <div class="control-group" id="">
                        <label class="control-label">Keterangan</label>
                        <div class="controls">
                            <textarea class="form-control span4" name="keterangan"></textarea>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="control-group" id="">
                        <label class="control-label">Status</label>
                        <div class="controls">
                            <select class="form-control span4" name="ketpem" id="tahunajaran">
                                <option>--Pilih Status--</option>
                                <option
                                    <?php $anggun = $this->session->userdata('sess_login'); $kol = $anggun['id_user_group'];
                                        if($kol == 7) {
                                            $iha = '1';
                                        } elseif($kol == 8) {
                                            $iha = '2';
                                        } elseif($kol == 11) {
                                            $iha = '3';
                                        } elseif($kol == 10) {
                                            $iha = '4';
                                        } echo "value=$iha"; ?>>
                                    Setuju
                                </option>
                                <option value="0">Tidak Setuju</option>
                            </select>     
                        </div>                   
                    </div>             
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>