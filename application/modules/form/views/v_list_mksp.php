<script type="text/javascript">
    function edit(idj) {
        $("#edit_jurusan").load('<?php echo base_url()?>data/jurusan/view_edit_jurs/'+idj);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Jumlah Peserta Perbaikan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url('form/formnilaisp/back'); ?>" class="btn btn-warning"><i class="icon icon-arrow-left"></i> Kembali </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kelas</th>
                                <th>Dosen</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>SKS</th>
                                <th>Jumlah Pendaftar</th>
	                            <th width="40">Aksi</th>
                                <th>Publikasi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($query as $row){?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<td><?php echo $row->kelas;?></td>
	                        	<td><?php echo $row->nama;?></td>
                                <td><?php echo $row->kd_matakuliah;?></td>
                                <td><?php echo $row->nama_matakuliah;?></td>
                                <td><?php echo $row->sks_matakuliah;?></td>
                                <?php $reg = $this->temph_model->list_reg($row->kd_jadwal); //var_dump($reg); ?>
                                <td><?php echo $reg->jum; ?></td>
	                        	<td class="td-actions">
									<a href="<?php echo base_url('form/formnilaisp/seepoint/'.$row->id_jadwal); ?>" target="blank" class="btn btn-success btn-small"><i class="btn-icon-only icon-eye-open"> </i></a>
								</td>
                                <td>
                                	<?php 
                                		$cek = $this->temph_model->cek_avbl($row->id_jadwal);
                                		$fix = $this->temph_model->compare_pst($row->id_jadwal);
                                		$krs = $this->temph_model->count_krs($row->id_jadwal);
                                		$tru = $this->temph_model->sudah($row->id_jadwal);

                                        // var_dump($cek.'--'.$fix.'--'.$krs.'--'.$tru);exit();

                                		if ($cek == 0) { ?>
                                			<a href="javascript:void(0);" onclick="alert('Nilai Belum Diunggah !')" class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i></a>
                                		<?php } elseif ($fix != $krs) { ?>
                                			<a href="javascript:void(0);" onclick="alert('Jumlah Peserta Kelas Belum Sesuai !')" class="btn btn-primary btn-small"><i class="btn-icon-only icon-info"> </i></a>
                                		<?php } elseif ($tru == 2) { ?>
                                            <a href="javascript:void(0);" onclick="alert('Sudah Di-Publish !')" class="btn btn-success btn-small"><i class="btn-icon-only icon-check"> </i></a>
                                        <?php } elseif ($fix == $krs) { ?>
                                			<a href="<?php echo base_url('form/formnilaisp/pub_all/'.$row->id_jadwal); ?>" target="blank" class="btn btn-warning btn-small"><i class="btn-icon-only icon-share"> </i></a>
                                		<?php } ?>
                                	
                                </td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_jurusan">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->