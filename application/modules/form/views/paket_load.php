<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>KRS Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>form/krspaket/save" method="post">
                    	<table>
                    		<tr>
                    			<td width="200">Nama Pembimbing Akademik</td>
                    			<td width="20">:</td>
                    			<td><?php echo $dosen; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Kelas</td>
                    			<td>:</td>
                    			<td><?php echo $kelas.' / '.jenis_kelas($kelas,$uid); ?></td>
                    		</tr>
                    	</table>
                    	<hr>
                    	<a href="<?php echo base_url('form/krspaket/destroysess'); ?>" title="" class="btn btn-warning"><i class="icon icon-arrow-left"></i> Kembali</a>
	                    <a href="#myModal" class="btn btn-primary" data-toggle="modal"><i class="btn-icon-only icon-table"> </i> Paket Semester 1</a>
	                    <input type="submit" class="btn btn-success" value="Submit">
	                    <br><hr>
						<table id="" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
	                                <th>NPM MHS</th>
	                                <th>Nama MHS</th>
	                                <th>pembimbing</th>
	                                <th>kelas</th>
	                                <th>kampus</th>
	                                <th>Registrasi</th>
		                            <th width="40" style="text-align:center">aksi</th>
		                            <th width="60">Lihat KRS</th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                            <?php $no = 1; foreach ($getData as $value) {?>
		                        <tr>
		                        	<td><?php echo $no; ?></td>
		                        	<td><?php echo $value->npm_baru; ?></td>
		                        	<td><?php echo getNameMhs($value->npm_baru); ?></td>

		                        	<?php $cekdosen = $this->app_model->getdetail('tbl_pa','npm_mahasiswa',$value->npm_baru,'id','asc')->row()->kd_dosen; ?>
		                        	<?php // $cekdosen = $this->db->query("SELECT * from tbl_verifikasi_krs a join tbl_karyawan b on a.id_pembimbing = b.nid where kd_krs like '".$value->npm_baru."20172%'")->row(); ?>
		                        	
		                        	<td><?php echo get_nm_pa($cekdosen); ?></td>
		                        	<td><?php echo $value->kelas; ?></td>
		                        	<td><?php echo $value->kampus; ?></td>
		                        	<td><?php echo $value->jenis_pmb; ?></td>
		                        	<?php $tahun = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row(); ?>

		                        	<?php $cekkrs = $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs like '".$value->npm_baru.$tahun->kode."%'")->result(); 
		                        		if($cekkrs == TRUE){?>
				                        	<td class="td-actions" style="text-align:center"><i class="btn-icon-only icon-ok"> </i></td>
		                        	<?php } else { ?>
											<td class="td-actions" style="text-align:center"><input type="checkbox" name="mhs[]" value="<?php echo $value->npm_baru; ?>"></td>
											<!-- <input type="hidden" name="cls_kind" value="<?php echo $value->kelas; ?>"> -->
									<?php } ?>

									<td class="td-actions" style="text-align:center">
										<?php $kd = $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs like '".$value->npm_baru.$tahun->kode."%'")->row();
										if (count($kd->kd_krs) == 1) { ?>
											<a class="btn btn-success btn-small" target="_blank" href="<?php echo base_url(); ?>form/krspaket/jadwal/<?php echo $kd->kd_krs; ?>/<?php echo substr(md5($value->kelas), 0,6); ?>"><i class="btn-icon-only icon-ok"> </i></a>
										<?php } else { ?>
											<a class="btn btn-danger btn-small" href="javascript:void(0);" onclick="alert('Lakukan Perwalian Terlebih Dahulu')"><i class="btn-icon-only icon-ban-circle"> </i></a>
										<?php } ?>
									</td>
		                        </tr>
	                            <?php $no++; } ?>
		                    </tbody>
		               	</table>
		            </form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Paket Mata Kuliah Semester I - <?php echo get_jur($user); ?></h4>
            </div>
            <div class="modal-body">
            	<table class="table table-bordered table-stripped">
            		<thead>
            			<tr>
            				<th>Kode Matakuliah</th>
            				<th>Nama Mata Kuliah</th>
	            			<th>SKS Mata Kuliah</th>
	            			<th>Semester</th>
            			</tr>
            		</thead>
            		<tbody>
            			<?php foreach ($pckg as $key) { ?>
            				<tr>
            					<td><?php echo $key->kd_matakuliah; ?></td>
            					<td><?php echo $key->nama_matakuliah; ?></td>
            					<td><?php echo $key->sks_matakuliah; ?></td>
            					<td><?php echo $key->semester_kd_matakuliah; ?></td>
            				</tr>
            			<?php } ?>
            		</tbody>
            	</table>	            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          	</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->