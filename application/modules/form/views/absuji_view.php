<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Absensi Ujian</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url()?>form/cetakabsensiujian/save_session">
              <fieldset>
                <script>
                  $(document).ready(function(){
                    $('#faks').change(function(){
                      $.post('<?php echo base_url()?>form/cetakabsensiujian/get_jurusan/'+$(this).val(),{},function(get){
                        $('#jurs').html(get);
                      });
                    });
                  });
                </script>

                <?php  
                $logged = $this->session->userdata('sess_login');
                $pecah = explode(',', $logged['id_user_group']);
                $jmlh = count($pecah);
                for ($i=0; $i < $jmlh; $i++) { 
                  $grup[] = $pecah[$i];
                }
                ?>

                <?php if ((in_array(10, $grup)) or (in_array(1, $grup))) { ?>
                <div class="control-group">
                  <label class="control-label">Fakultas</label>
                  <div class="controls">
                    <select class="form-control span6" name="fakultas" id="faks">
                      <option>--Pilih Fakultas--</option>
                      <?php foreach ($fakultas as $row) { ?>
                      <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Jurusan</label>
                  <div class="controls">
                    <select class="form-control span6" name="jurusan" id="jurs">
                      <option>--Pilih Jurusan--</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="ta">
                      <option>--Pilih Semester--</option>
                      <?php for ($i=1; $i <= 8 ; $i++) { 
                        echo "<option value='".$i."'>".$i."</option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tahun Akademik</label>
                  <div class="controls">
                    <select class="form-control span6" name="tahunajaran" id="ta">
                      <option>--Pilih Tahun Akademik--</option>
                      <?php foreach ($tahunajar as $value) {
                        echo "<option value='".$value->kode."'> ".$value->tahun_akademik." </option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <?php } elseif ((in_array(9, $grup))) { ?>
                <div class="control-group">
                  <label class="control-label">Jurusan</label>
                  <div class="controls">
                    <select class="form-control span6" name="jurusan" id="jurs">
                      <option>--Pilih Jurusan--</option>
                      
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="ta">
                      <option>--Pilih Semester--</option>
                      <?php for ($i=1; $i <= 8 ; $i++) { 
                        echo "<option value='".$i."'>".$i."</option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tahun Akademik</label>
                  <div class="controls">
                    <select class="form-control span6" name="tahunajaran" id="ta">
                      <option>--Pilih Tahun Akademik--</option>
                      <?php foreach ($tahunajar as $value) {
                        echo "<option value='".$value->kode."'> ".$value->tahun_akademik." </option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <?php } else { ?>
                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="ta">
                      <option>--Pilih Semester--</option>
                      <?php for ($i=1; $i <= 8 ; $i++) { 
                        echo "<option value='".$i."'>".$i."</option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tahun Akademik</label>
                  <div class="controls">
                    <select class="form-control span6" name="tahunajaran" id="ta">
                      <option>--Pilih Tahun Akademik--</option>
                      <?php foreach ($tahunajar as $value) {
                        echo "<option value='".$value->kode."'> ".$value->tahun_akademik." </option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>
              <br/>
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
