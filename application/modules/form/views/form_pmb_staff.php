<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Form Pendaftaran Mahasiswa</h3>

      </div> <!-- /widget-header -->

      
      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>form/formpmb/add_bio">
        <?php if (is_null($IDReg->ID_registrasi)) {
            $idregis = '0001';
        } else {
            $idregis = substr($IDReg->ID_registrasi, -4)+1;
            //var_dump($idregis);exit();
        }
        if ($idregis < 10) {
            $angka = '000'.$idregis;
        } elseif ($idregis < 100) {
            $angka = '00'.$idregis;
        } elseif ($idregis < 1000) {
            $angka = '0'.$idregis;
        }
        
        $registrasi = $gel->nm_gel.''.$angka;
        //var_dump($registrasi);exit();
         ?>
        <input type="hidden" class="form-control span6" name="ID"  value="<?php echo $registrasi; ?>"/>
            <fieldset>
            	<div class="control-group">
                    <label class="control-label">Nama</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span6" name="nama"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Tempat / Tanggal Lahir</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span2" name="ttl"  required/> / <input type="text" class="form-control span3" name="tgl_lhr"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Jenis Kelamin</label>                                
                    <div class="controls">
                    	<input type="radio" name="jk" value="L" required> Laki - Laki
                    	<input type="radio" name="jk" value="P"> Perempuan
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Alamat</label>                                
                    <div class="controls">
                    	<textarea class="form-control span6" name="alamat"  required></textarea>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Asal Sekolah</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span6" name="asal_sch"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Telepon / HP</label>                                
                    <div class="controls">
                    	(+62) <input type="number" class="form-control span4" name="tlp"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Email</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span4" name="email"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Agama</label>                                
                    <div class="controls">
                    	<select class="form-control span2" name="agama"  required>
                    		<option value="Islam">Islam</option>
                    		<option value="Katolik">Katolik</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Budha">Budha</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Lainnya">Lainnya</option>
                    	</select>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Pilihan</label>                                
                    <div class="controls">
                    	<select class="form-control span4" name="choose"  required>
                        <option selected disabled>-- PILIHAN --</option>
                            <?php foreach ($opsi as $row) { ?>
                                <option disabled> <strong> -- FAKULTAS <?php echo strtoupper($row->fakultas); ?> -- </strong> </option>
                                <?php $jurusan = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_fakultas = "'.$row->kd_fakultas.'"')->result(); ?>
                                <?php foreach ($jurusan as $rows) { ?>
                                    <option value="<?php echo $rows->kd_prodi; ?>"> <strong>PRODI <?php echo strtoupper($rows->prodi); ?></strong> </option>
                            <?php } ?>
                            <?php } ?>
                    	</select>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Kelas</label>                                
                    <div class="controls">
                    	<select class="form-control span3" name="kelas"  required>
                            <option selected disabled>-- PILIHAN --</option>
                    		<option value="Pagi">Pagi</option>
                    		<option value="Sore">Sore</option>
                    	</select>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Nama Orang Tua</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span4" name="nm_ortu"  required/>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">Penghasilan Orang Tua</label>                                
                    <div class="controls">
                    	<table>
                			<tr>
                				<td><input type="radio" name="1" value="" required/> Rp <?php echo number_format(500000).' - '.number_format(1000000); ?></td>
                				<td><input type="radio" name="1" value=""/> Rp <?php echo number_format(1000000).' - '.number_format(2000000); ?></td>
                				<td><input type="radio" name="1" value=""/> Rp <?php echo number_format(2000000).' - '.number_format(3000000); ?></td>
                			</tr>
                			<tr>
                				<td><input type="radio" name="1" value=""/> Rp <?php echo number_format(3000000).' - '.number_format(4500000); ?></td>
                				<td><input type="radio" name="1" value=""/> Rp <?php echo number_format(4500000).' - '.number_format(6000000); ?></td>
                				<td><input type="radio" name="1" value=""/> > Rp <?php echo number_format(6000000); ?></td>
                			</tr>
                    	</table>
					</div>
				</div>
            <!-- contoh >
            	<div class="control-group">
                    <label class="control-label">Field1</label>                                
                    <div class="controls">
                    	<input type="text" class="form-control span6" name=""  required/>
					</div>
				</div>

				<div class="control-group">
                    <label class="control-label">Field2</label>                                
                    <div class="controls">
                    	<textarea class="form-control span6" name=""  required></textarea>
					</div>
				</div>

				<div class="control-group">
                    <label class="control-label">Field1</label>                                
                    <div class="controls">
                    	<select class="form-control span4" name=""  required>
                    		<option value=""></option>
                    		<option value=""></option>
                    	</select>
					</div>
				</div>

				<div class="control-group">
                    <label class="control-label">Field1</label>                                
                    <div class="controls">
                    	<input type="radio" name="1" value="" required> aaaaaaaa
                    	<input type="radio" name="1" value="">
					</div>
				</div-->
                
                <div class="form-actions">
					<input type="submit" class="btn btn-large btn-primary" value="Submit"/>
					<input type="reset" class="btn btn-large btn-default" value="Clear"/>
                </div> <!-- /form-actions -->
			</fieldset>

        </form>

          

        </div>

      </div>

    </div>

  </div>

</div>

