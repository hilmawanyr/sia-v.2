<!--script type="text/javascript">
    function edit(edk) {
        $("#editcuti").load('<?php echo base_url()?>form/formcuti/edit_cuti/'+edk);
    }
</script-->
<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=kaprodi]').autocomplete({

        source: '<?php echo base_url('form/formcuti/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.kaprodi.value = ui.item.value;

            //this.form.kd_dosen.value = ui.item.nid;

            // this.form.hargabeli.value = ui.item.harga_beli;

            // this.form.hargajual.value = ui.item.harga_jual;

            //$('#qtyk').focus();

        }

    });

});

</script>

<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Data Pengajuan Cuti</h3>

      </div> <!-- /widget-header -->

      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>form/formcuti/nyimpen">

                        <fieldset>

                      <script>

                              $(document).ready(function(){

                                $('#faks').change(function(){

                                  $.post('<?php echo base_url()?>perkuliahan/jdl_kuliah/get_jurusan/'+$(this).val(),{},function(get){

                                    $('#jurs').html(get);

                                  });

                                });

                              });

                              </script>

                              <div class="control-group">

                                <label class="control-label">NIM</label>

                                <div class="controls">

                                  <input class="form-control span6" name="npm" value="<?php echo $andina->NIMHSMSMHS; ?>" readonly/>

                                </div>

                              </div>

                              <input type="hidden" name="tgl">
                              <input type="hidden" name="tahun" value="<?php echo $syafitri->kode; ?>">
                              
                              <input type="hidden" name="status" value="1">



                              <div class="control-group">

                                <label class="control-label">Deskripsi</label>

                                <div class="controls">

                                <?php if (isset($q->desksripsi)) {
                                	echo "<textarea class='form-control span6' name='desk' disabled>$q->desksripsi</textarea>";
                                } else {
                                echo "<textarea class='form-control span6' name='desk'></textarea>";	
                                	}?>

                                	
                                </div>

                              </div>



                              <div class="control-group">

                              	<?php if (isset($q->pembimbing_akademik)) { ?>
                              		<label class="control-label">Pembimbing Akademik</label>
                              		<div class="controls">
                              			<input type="text" class="form-control span6" value="<?php echo $q->nama; ?>" disabled/>
                              		</div>
                              	<?php } else { ?>
                              		<label class="control-label">Pembimbing Akademik</label>

	                                <div class="controls">

	                                  <select class="form-control span6" name="pea" id="ppp">

	                                    <option>--Pilih Pembimbing Akademik--</option>

	                                    <?php foreach ($pa as $row) { ?>

	                                    <option value="<?php echo $row->userid; ?>"><?php echo $row->nid;?> - <?php echo $row->nama; ?></option>

	                                    <?php } ?>

	                                  </select>

	                                </div>
                              	<?php } ?>
                                
                              </div> 
                              <div class="control-group">
                                  <label class="control-label">Ka. Prodi</label>

                                  <div class="controls">
                                    <input type="text" class="form-control span6" id="kaprodi" name="kaprodi">
                                  </div>
                                </div>  

                            <br/>

                              

                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-success" value="Submit"/>

                                <?php if (isset($q->desksripsi)) { ?>
                                	<a class="btn btn-primary btn-large" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i> Edit</a>
                                <?php } else { ?>
                                	<button class="btn btn-primary btn-large" href="#editModal" data-toggle="modal" disabled="disabled"><i class="btn-icon-only icon-pencil"> </i> Edit</button>
                                <?php } ?>

                                

                            </div> <!-- /form-actions -->

                        </fieldset>

                    </form>

          

        </div>

      </div>

    </div>

  </div>

</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Permohonan Cuti</h4>
            </div>
            <br>
            <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>form/formcuti/update_cuti">

                              <div class="control-group">

                                <label class="control-label">Deskripsi</label>

                                <div class="controls">

                                  <textarea class="form-control span4" name="deskedit"><?php echo $q->desksripsi; ?></textarea>

                                </div>

                              </div>

                              <input type="hidden" name="iden" value="<?php echo $q->id_cuti; ?>">

                              <div class="control-group">

                                <label class="control-label">Pembimbing Akademik</label>

                                <div class="controls">

                                  <select class="form-control span4" name="pem" id="ppp">

                                    <option>--Pilih Pembimbing Akademik--</option>

                                    <?php foreach ($pa as $row) { ?>

                                    <option value="<?php echo $row->userid; ?>"><?php echo $row->nid;?> - <?php echo $row->nama; ?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>   

                            <br/>

                              

                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-primary" value="Update"/>
                                <button type="button" class="btn btn-large btn-danger" data-dismiss="modal">Keluar</button>
                            </div> <!-- /form-actions -->

                        

                    </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

