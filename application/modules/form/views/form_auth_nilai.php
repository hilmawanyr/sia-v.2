<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('form/formnilai/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {



            // $('#hargasatuan').html(ui.item.harga_jual);

            // $('#satuan').html(ui.item.satuan);

            // $('#stok').html(ui.item.stok);

            // this.form.kode.value = ui.item.kode;

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;

            // this.form.hargabeli.value = ui.item.harga_beli;

            // this.form.hargajual.value = ui.item.harga_jual;

            //$('#qtyk').focus();

        }

    });

});

</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>FORM PENGISIAN NILAI</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM PENGISIAN NILAI</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>auth/dosen_auth">
            <fieldset>
            <?php
              $logged = $this->session->userdata('sess_login');
              $pecah = explode(',', $logged['id_user_group']);
              $jmlh = count($pecah);
              for ($i=0; $i < $jmlh; $i++) { 
                $grup[] = $pecah[$i];
              }
              if ( (in_array(8, $grup) or in_array(19, $grup))) {
            ?>
              <div class="control-group">                     
                <label class="control-label">NID</label>
                <div class="controls">
                  <input type="text" class="span3" name="dosen" placeholder="Masukan NID Dosen" id="dosen" required>
                  <input type="hidden" id='kd_dosen' name="kd_dosen" >
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <?php } else { ?> 
                  <input type="hidden" id='kd_dosen' name="kd_dosen" value="<?php echo $logged['userid'];?>">
              <?php } ?>
              <div class="control-group">                     
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select name="tahun" class="span3" required>
                    <option disabled selected>-- Pilih Tahun Akademik --</option>
                    <?php foreach ($tahunajar as $row) { ?>
                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <!--div class="control-group">                     
                <label class="control-label">Password</label>
                <div class="controls">
                  <input type="password" class="span3" name="password" placeholder="Password" required>
                </div> <!-- /controls >       
              </div> <!-- /control-group -->
              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>