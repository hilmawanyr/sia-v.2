<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beasiswa extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		//$id_menu = 55 (database); cek apakah user memiliki akses
		/* if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(79)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		} */
		$this->load->library('Cfpdf');
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		$cek_bea = $this->app_model->getDetail('tbl_beasiswa', 'npm', $nik, 'id_beasiswa', 'desc', '1')->row();
		if ((in_array(5, $grup))) {
			if (!empty($cek_bea)) {
				$data['formulir'] = $this->app_model->getPeriodebeasiswa('tbl_beasiswa', 'npm', $nik, 'id_beasiswa', 'desc', '1')->row();
				$data['list_formulir'] = $this->app_model->getDetail('tbl_beasiswa', 'npm', $nik, 'id_beasiswa', 'desc')->result();
				$data['page'] = 'v_beasiswa_mhs';
			} else {
				redirect('form/beasiswa/isi', 'refresh');
			}
		} elseif ((in_array(20, $grup))) {
			$data['pengajuan'] = $this->app_model->getData('tbl_beasiswa', 'id_beasiswa', 'desc')->result();
			$data['page'] = 'v_beasiswa';
		}
		$this->load->view('template/template', $data);
	}

	function master()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(20, $grup))) {
			$data['master'] = $this->app_model->getData('tbl_master_beasiswa', 'id_bea', 'desc')->result();
			$data['page'] = 'v_beasiswa_master';
			$this->load->view('template/template', $data);
		} else {
			redirect(base_url(), 'refresh');
		}
	}
	function isi()
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$cek_bea = $this->app_model->getDetail('tbl_beasiswa', 'npm', $npm, 'id_beasiswa', 'desc', '1')->row();
		if (empty($cek_bea)) {
			$data['jenis'] = $this->app_model->getMasterbeasiswa()->result();
			$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
			$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
			$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();

			$data['page'] = 'form_beasiswa';
			$this->load->view('template/template', $data);
		} else {
			redirect('form/beasiswa', 'refresh');
		}
	}
	function cetak($id)
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$bea = $this->app_model->getDetail('tbl_beasiswa', 'id_beasiswa', $id, 'id_beasiswa', 'DESC', '1')->row();
		if ($bea->npm == $npm) {
			$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
			$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
			$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();
			$data['bea'] = $bea;
			$this->load->view('cetak_beasiswa', $data);
		} else {
			redirect('form/beasiswa', 'refresh');
		}
	}
	function print_beasiswa($id, $npm)
	{
		$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
		$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
		$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();
		$data['bea'] = $this->app_model->getDetail('tbl_beasiswa', 'id_beasiswa', $id, 'id_beasiswa', 'DESC', '1')->row();
		$this->load->view('cetak_beasiswa', $data);
	}

	function save()
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$createddate = date('Y-m-d H:i:s');
		$data = array(
			'alamat'	=> $this->input->post('alamat'),
			'no_hp'		=> $this->input->post('tlp'),
			'ktp'		=> $this->input->post('ktp')
		);
		$data_bea = array(
			'npm'			=> $npm,
			'prestasi'		=> $this->input->post('prestasi'),
			'beasiswa'		=> $this->input->post('opt_beasiswa'),
			'createddate'	=> $createddate
		);

		$this->app_model->updatedata('tbl_bio_mhs', 'npm', $npm, $data);
		$this->app_model->insertdata('tbl_beasiswa', $data_bea);
		redirect('form/beasiswa', 'refresh');
	}

	function approve($id)
	{
		$data = array(
			'status'	=> 1
		);

		$this->app_model->updatedata('tbl_beasiswa', 'id_beasiswa', $id, $data);
		redirect('form/beasiswa', 'refresh');
	}

	function reject($id)
	{
		$data = array(
			'status'	=> 2
		);

		$this->app_model->updatedata('tbl_beasiswa', 'id_beasiswa', $id, $data);
		redirect('form/beasiswa', 'refresh');
	}
	function add_master()
	{
		$data = array(
			'kd_beasiswa'	=> strtoupper($this->input->post('kd_beasiswa')),
			'nama_beasiswa'	=> $this->input->post('nama_beasiswa'),
			'periode'		=> $this->input->post('periode'),
			'isactivated'	=> $this->input->post('isactivated')
		);

		$this->app_model->insertdata('tbl_master_beasiswa', $data);
		redirect('form/beasiswa/master', 'refresh');
	}
	function edit_master()
	{
		$data = array(
			'nama_beasiswa'	=> $this->input->post('nama_beasiswa'),
			'periode'		=> $this->input->post('periode'),
			'isactivated'	=> $this->input->post('isactivated')
		);

		$this->app_model->updatedata('tbl_master_beasiswa', 'kd_beasiswa', $this->input->post('kd_beasiswa'), $data);
		redirect('form/beasiswa/master', 'refresh');
	}
	function hapus_beasiswa($id)
	{
		$this->app_model->deletedata('tbl_master_beasiswa', 'id_bea', $id);
		redirect('form/beasiswa/master', 'refresh');
	}
	function edit_beasiswa($id)
	{
		$data['beasiswa'] = $this->app_model->getdetail('tbl_master_beasiswa', 'id_bea', $id, 'id_bea', 'desc')->row();
		$this->load->view('v_beasiswa_master_edit', $data);
	}

	function cari()
	{
		if (isset($_POST['kd_beasiswa'])) {
			$kd_beasiswa = $_POST['kd_beasiswa'];
			$results = $this->app_model->cari_kd('tbl_master_beasiswa', 'kd_beasiswa', $kd_beasiswa);
			if ($results === TRUE) {
				$a = '<span style="color:red;">Kode Beasiswa sudah digunakan sebelumnya</span>';
				$b = "<script>document.getElementById('save').disabled = true;</script>";
				echo $a, $b;
			} else {
				$a = '<span style="color:green;">Kode Beasiswa dapat digunakan</span>';
				$b = "<script>document.getElementById('save').disabled = false;</script>";
				echo $a, $b;
			}
		} else {
			$a = '<span style="color:red;">Kode Beasiswa harus diisi</span>';
			$b = "<script>document.getElementById('save').disabled = true;</script>";
			echo $a, $b;
		}
	}
}
