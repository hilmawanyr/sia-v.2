<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpindah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        if (!$this->session->userdata('sess_login')) {
            echo "<script>alert('Silahkan log-in kembali untuk memulai sesi!');</script>";
            redirect(base_url('auth/logout'),'refresh');
        } else {
            $this->load->library('Cfpdf');
            date_default_timezone_set('Asia/Jakarta');
            $this->load->model('form/pindah_model','move');
        }
	}

	public function index()
	{
		$data['year'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = "v_formpindah";
		$this->load->view('template/template', $data);
	}

	function autocomplete()
    {

        $sql  = $this->move->getAutocomplete($_GET['term']);

        $data = [];

        foreach ($sql->result() as $row) {

            $data[] = array(

		                'value'	=> $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS,

		                'npm'	=> $row->NIMHSMSMHS

		                );

        }

        echo json_encode($data);
    
    }

    function addData()
    {
    	extract(PopulateForm());

    	$exp   = explode(' - ', $npm);
    	$prodi = get_mhs_jur($exp[0]);

        // get SKS
    	$get   = $this->move->getSks($exp[0],$prodi);
    	$nol   = 0;
    	foreach ($get->result() as $key) {
    		$nol = $nol + $key->sks_matakuliah;
    	}
        // below is amount of SKS
    	$sks = $nol;

    	// get pa
    	$pa = $this->move->getAcademicGuide($exp[0]);

    	// jika pindah atau keluar
    	if ($destinasi != '' or !empty($destinasi) or !is_null($destinasi)) {
    		$dest = $destinasi;
    	} else {
    		$dest = '';
    	}
    	
    	$arr = array(
    		'npm'			=> $exp[0],
    		'prodi'			=> $prodi,
    		'kelas'			=> $kelas,
    		'sks'			=> $sks,
    		'alamat'		=> $alamat,
    		'tlp'			=> $hp,
    		'tahunakademik'	=> getactyear(),
    		'pa'			=> $pa,
    		'tanggal'		=> date('Y-m-d'),
    		'keterangan'	=> $alasan,
    		'tipe'			=> $tipe,
    		'destinasi'		=> $dest
    	);

        // make condition if student exist in table
        $chk = $this->app_model->getdetail('tbl_pengunduran','npm',$exp[0],'npm','asc')->num_rows();
        if ($chk > 0) {
            $this->app_model->updatedata('tbl_pengunduran','npm',$exp[0],$arr);
        } else {
            $this->app_model->insertdata('tbl_pengunduran', $arr);
        }

    	redirect(base_url('form/formpindah/cetak_pindah/'.$exp[0].'.'.getactyear()));
    }

    function cetak_pindah($param)
    {
    	$exp = explode('.', $param);

        // get data pindahan mahasiswa
    	$data['row'] = $this->move->getDataMove($exp[0],$exp[1]);

        // get semester mahasiswa
    	$smtmhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$exp[0],'NIMHSMSMHS','asc')->row();

        // get semester terakhir mahasiswa
    	$smtmax = $this->move->getMaxSmtMhs($exp[0]);

    	$data['semst'] = $this->app_model->get_semester_khs($smtmhs->SMAWLMSMHS,$smtmax);

    	$this->load->view('v_cetak_pindah', $data);
    }

    function savesess()
    {
    	$array = array(
    		'tipe' => $this->input->post('tipe', TRUE),
    		'akad' => $this->input->post('tahunakademik', TRUE)
    	);
    	
    	$this->session->set_userdata('sessmundur', $array);

    	redirect(base_url('form/formpindah/review'));
    }

    function review()
    {
    	$mundur = $this->session->userdata('sessmundur');
    	$this->load->model('hilman_model','hilmod');
    	$arr = array('tipe' => $mundur['tipe'], 'tahunakademik' => $mundur['akad']);
    	$data['list'] = $this->hilmod->moreWhere($arr, 'tbl_pengunduran');
    	$data['tipp'] = $mundur['tipe'];
    	$data['page'] = "v_review_keluar";
    	$this->load->view('template/template', $data);
    }

    function cetak_surat_pindah($id)
    {
    	$exp = explode('-', $id);

    	$this->load->model('hilman_model','hilmod');
	
	   $lastCorp = $this->db->query("SELECT NIMHSMSMHS as npm FROM tbl_mahasiswa ORDER BY id_mhs DESC LIMIT 1")->row()->npm;

        // jika maba gunakan tahun aktif
        if (substr($exp[0], 0,4) == substr($lastCorp, 0,4)) {
            $choosedYear = getactyear();
        } else {
            $choosedYear = yearBefore();
        }
	
    	$arr = array('npm_mahasiswa' => $exp[0], 'tahunajaran' => $choosedYear);
    	$cekrenkeu = $this->hilmod->moreWhere($arr, 'tbl_sinkronisasi_renkeu');

    	if ($cekrenkeu->num_rows() == 0) {
    		
    		echo "<script>alert('Mahasiswa belum menyelesaikan pembayaran di tahun akademik aktif!');history.go(-1);</script>";

    	} else {

	    	$arr = array('npm' => $exp[0], 'tahunakademik' => $exp[1]);
	    	$data['list'] = $this->hilmod->moreWhere($arr, 'tbl_pengunduran')->row();

	    	// update STMHSMSMHS in tbl_mahasiswa
	    	$obj['STMHSMSMHS'] = 'K';
            $this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$exp[0],$obj);

	    	$smawl = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$exp[0],'NIMHSMSMHS','asc')->row();

	    	$data['smst'] = $this->app_model->get_semester($smawl->SMAWLMSMHS);

	    	$this->load->view('v_ket_pindah', $data);

    	}
    }

    function cetak_review()
    {
        $mundur = $this->session->userdata('sessmundur');

        $this->load->model('hilman_model','hilmod');
        
        $arr = array('tipe' => $mundur['tipe'], 'tahunakademik' => $mundur['akad']);
        $data['list'] = $this->hilmod->moreWhere($arr, 'tbl_pengunduran');

        $data['tipp'] = $mundur['tipe'];

        $this->load->view('excel_keluar', $data);
    }

}

/* End of file Formpindah.php */
/* Location: .//tmp/fz3temp-1/Formpindah.php */
