<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formnilai extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			
			$user = $this->session->userdata('sess_login');
			$akses = $this->role_model->cekakses(58)->result();
			$aktif = $this->setting_model->getaktivasi('nilai')->result();
			
		} else {
			redirect('auth', 'refresh');
		}
		$this->load->model('form/nilai_model','poin');
	}

	function index() {
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$this->session->unset_userdata('sess_dosen');
			redirect('form/formnilai/authdosen', 'refresh');
		} else {
			$this->session->unset_userdata('sess_dosen');
			redirect('form/formnilai/authdosenlog', 'refresh');
		}
	}

	function view($id) 
	{
		ini_set('memory_limit', '256M');
		$this->session->set_userdata('id_jadwal', $id);

		$jadwal = $this->db->select('kd_jadwal')->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();
		$user = $this->session->userdata('sess_dosen');

		$data['mk']          = $this->app_model->get_mk_by_jadwal($jadwal->kd_jadwal)->row();
		$data['kode_jadwal'] = $jadwal->kd_jadwal;
		$data['id_jadwal']   = $id;
		$data['referensi']   = $jadwal->referensi;
		$data['logged']      = $this->session->userdata('sess_login');
		$data['tahunpilih']  = $user['tahun'];
		$data['look']        = $this->db->query("SELECT distinct a.NIMHSMSMHS,a.NMMHSMSMHS,b.kd_matakuliah,b.kd_jadwal
												FROM tbl_mahasiswa a
												JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
												WHERE b.`kd_jadwal` = '" . $data['kode_jadwal'] . "'
												ORDER BY NIMHSMSMHS")->result();
		if ($data['tahunpilih'] < 20171) {
			$data['page'] = 'form/nilai_lihat_lama';
		} else {
			$data['page'] = 'form/nilai_lihat';
		}
		$this->load->view('template/template', $data);
	}

	function dwld_excel($id) 
	{
		$data['rows'] 	= $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`id_matakuliah` = b.`id_matakuliah`
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											JOIN tbl_kurikulum_matkul_new d ON a.`kd_matakuliah` = d.`kd_matakuliah`
											WHERE id_jadwal = '.$id.' ')->row();

		$kdprodi               = substr($data['rows']->kd_jadwal, 0, 5);
		$data['activeStudent'] = $this->poin->get_detail_participant($data['rows']->kd_jadwal)->result();
		$data['prodi']         = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$kdprodi,'kd_prodi','asc')->row();
		$data['absendosen']    = $this->db->query("SELECT MAX(pertemuan) AS satu FROM tbl_absensi_mhs_new_20171
													WHERE kd_jadwal = '" . $data['rows']->kd_jadwal . "'")->row();

		$student = $this->db->query("SELECT count(distinct npm_mahasiswa) AS jumlah FROM tbl_krs
									WHERE kd_jadwal = '" . $data['rows']->kd_jadwal . "' ")->row();

		$this->load->library('excel');

		if (is_final_assignment($data['rows']->nama_matakuliah)) {
			$this->load->view('welcome/phpexcel_nilai_skripsi', $data);

		} elseif (stripos($data['rows']->nama_matakuliah, 'KKN')){
			$this->load->view('nilai_kkn_excel', $data);

		} else {
			$this->load->view('welcome/phpexcel_nilai', $data);
		}
	}

	function upload_excel() 
	{
		$testType = $this->input->post('tipeuji');
		$session  = $this->session->userdata('sess_dosen');
		$user     = $session['userid'];
		$tahun    = $session['tahun'];

		if ($_FILES['userfile']) {

			$kodeJadwal   = $this->input->post('kode_jdl');
			$matakuliah   = $this->poin->get_matakuliah_by_jadwal($kodeJadwal);
			$pesertaKelas = $this->poin->get_participant($kodeJadwal);
			$filename     = $this->_sanitize_file($_FILES);

			$jumlah                  = $pesertaKelas->jumlah + 8;
			$config['upload_path']   = './upload/score/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['file_name']     = $filename;
			$config['max_size']      = 1000000;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('userfile')) {
				$data = array('error', $this->upload->display_errors());
				echo "<script>alert('Gagal');document.location.href='" . base_url() . "form/formnilai/';</script>";

			} else {
				// insert file upload
				$this->_store_file($filename, $kodeJadwal, $config['upload_path']);

				$upload_data = $this->upload->data();
				$file        = $upload_data['full_path'];

				$this->load->library('excel_reader');
				$this->excel_reader->setOutputEncoding('CP1251');
				$this->excel_reader->read($file);
				$dataCell = $this->excel_reader->sheets[0];

				$dataExcel = array();

				if (is_final_assignment($matakuliah->nama_matakuliah)) {
					$this->_read_excel_skripsi($jumlah, $dataExcel, $dataCell, $matakuliah->kd_matakuliah, $tahun, $kodeJadwal);

				} elseif (stripos($matakuliah->nama_matakuliah, 'KKN')) {
					$this->_read_excel_kkn($jumlah, $dataExcel, $dataCell, $tahun, $matakuliah->kd_matakuliah, $kodeJadwal);

				} else {
					$this->_read_excel_nilai_regular($jumlah, $dataExcel, $dataCell, $testType, $kodeJadwal);
				}
			}
		}
	}

	function _sanitize_file($file)
	{
		$this->load->helper('inflector');

		$sanitize   = $this->security->sanitize_filename($file['userfile']['name']);
		$fileName   = rand(1, 10000000) . underscore(str_replace('/', '', $sanitize));
		$explodeDot = explode('.', $fileName);
		if (count($explodeDot) > 2) {
			echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";
			exit();
		}
		return $fileName;
	}

	function _store_file($fileName, $kodeJadwal, $path)
	{
		$file['nama_file'] = $fileName;
		$file['waktu']     = date('Y-m-d H:i:s');
		$file['tipe']      = 1;
		$file['kode']      = $kodeJadwal;
		$file['path']      = $path . $fileName;
		$file['userid']    = $this->session->userdata('sess_login')['userid'];

		$this->app_model->insertdata('tbl_file_upload', $file);
		return;
	}

	function _read_excel_skripsi($jumlahPeserta, $dataExcel, $dataCell, $kodeMatakuliah, $tahunAkademik, $kodeJadwal)
	{
		for ($i = 8; $i < $jumlahPeserta; $i++) {
			$dataExcel[$i - 8]['npm']      = substr($dataCell['cells'][$i][2], 0, 12);
			$dataExcel[$i - 8]['code']     = $kodeJadwal;
			$dataExcel[$i - 8]['nilai_10'] = number_format($dataCell['cells'][$i][4], 2);

			$this->_validasi_peserta_kelas($dataExcel[$i - 8]['npm'], $kodeJadwal);
			$this->_validasi_konten_nilai_akhir($dataExcel[$i - 8]['nilai_10']);
		}
		$this->_tambah_nilai_skripsi($dataExcel, $kodeJadwal, $kodeMatakuliah);
	}

	function _read_excel_kkn($jumlahPeserta, $dataExcel, $dataCell, $tahunAkademik, $kodeMatakuliah, $kodeJadwal)
	{
		for ($i = 8; $i < $jumlahPeserta; $i++) {
			$dataExcel[$i - 8]['npm'] = substr($dataCell['cells'][$i][2], 0, 12);
			// observasi
			$dataExcel[$i - 8]['nilai_1'] = number_format($dataCell['cells'][$i][4], 2);
			// DPL
			$dataExcel[$i - 8]['nilai_2'] = number_format($dataCell['cells'][$i][5], 2);
			// masyarakat
			$dataExcel[$i - 8]['nilai_3'] = number_format($dataCell['cells'][$i][6], 2);
			// laporan
			$dataExcel[$i - 8]['nilai_4'] = number_format($dataCell['cells'][$i][7], 2);
			// responsi
			$dataExcel[$i - 8]['nilai_5'] = number_format($dataCell['cells'][$i][8], 2);
			// total poin
			$dataExcel[$i - 8]['nilai_10'] = number_format($dataCell['cells'][$i][9], 2);						
			// kode jadwal
			$dataExcel[$i - 8]['code'] = $kodeJadwal;

			$this->_validasi_peserta_kelas($dataExcel[$i - 8]['npm'], $kodeJadwal);

			$this->_validasi_konten_nilai_akhir($dataExcel[$i - 8]['nilai_10']);

			$this->_store_kkn_poin();

		}
	}

	function _read_excel_nilai_regular($jumlahPeserta, $dataExcel, $dataCell, $testType, $kodeJadwal)
	{
		for ($i = 8; $i < $jumlahPeserta; $i++) {

			$dataExcel[$i - 8]['npm'] = substr($dataCell['cells'][$i][2], 0, 12);
			// average tugas
			$dataExcel[$i - 8]['nilai_1'] = number_format($dataCell['cells'][$i][12], 2);
			// absen
			$dataExcel[$i - 8]['nilai_2'] = number_format($dataCell['cells'][$i][11], 2);
			// UTS
			$dataExcel[$i - 8]['nilai_3'] = number_format($dataCell['cells'][$i][13], 2);
			// UAS
			$dataExcel[$i - 8]['nilai_4'] = $this->_uas_validation($testType, number_format($dataCell['cells'][$i][14]), $kodeJadwal, $dataExcel[$i - 8]['npm']);
			// detail tugas
			$dataExcel[$i - 8]['nilai_5'] = $this->_validasi_nilai_tugas($dataCell['cells'][$i][6]);
			$dataExcel[$i - 8]['nilai_6'] = $this->_validasi_nilai_tugas($dataCell['cells'][$i][7]);
			$dataExcel[$i - 8]['nilai_7'] = $this->_validasi_nilai_tugas($dataCell['cells'][$i][8]);
			$dataExcel[$i - 8]['nilai_8'] = $this->_validasi_nilai_tugas($dataCell['cells'][$i][9]);
			$dataExcel[$i - 8]['nilai_9'] = $this->_validasi_nilai_tugas($dataCell['cells'][$i][10]);

			// guna menghalangi nilai akhir yg tidak sesuai
			$forTotalPoinValidation = [
				'nilai_1' => $dataExcel[$i - 8]['nilai_1'],
				'nilai_2' => $dataExcel[$i - 8]['nilai_2'],
				'nilai_3' => $dataExcel[$i - 8]['nilai_3'],
				'nilai_4' => $dataExcel[$i - 8]['nilai_4'],
				'cell_4'  => $dataCell['cells'][$i][4],
				'cell_15' => $dataCell['cells'][$i][15]
			];

			// nilai akhir
			$dataExcel[$i - 8]['nilai_10'] = $this->_validasi_nilai_akhir($testType, $kodeJadwal, $forTotalPoinValidation, $dataExcel[$i - 8]['npm']);

			$dataExcel[$i - 8]['code'] = $kodeJadwal;

			$this->_validasi_peserta_kelas($dataExcel[$i - 8]['npm'], $kodeJadwal);

			$datas[] = $dataExcel[$i - 8]['npm'];

			if ($dataExcel[$i - 8]['nilai_1'] < 0
				|| $dataExcel[$i - 8]['nilai_2'] < 0
				|| $dataExcel[$i - 8]['nilai_3'] < 0
				|| $dataExcel[$i - 8]['nilai_4'] < 0
				|| $dataExcel[$i - 8]['nilai_1'] > 101
				|| $dataExcel[$i - 8]['nilai_2'] > 101
				|| $dataExcel[$i - 8]['nilai_3'] > 101
				|| $dataExcel[$i - 8]['nilai_4'] > 101
				|| ctype_alpha($dataExcel[$i - 8]['nilai_1'])
				|| ctype_alpha($dataExcel[$i - 8]['nilai_3'])
				|| ctype_alpha($dataExcel[$i - 8]['nilai_4'])) {

				echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
				exit();

			}

			$this->_validasi_konten_nilai_akhir($dataExcel[$i - 8]['nilai_10']);
		}
		
		$student = $this->db->query('SELECT COUNT(distinct npm_mahasiswa) AS jumlah FROM tbl_krs WHERE kd_jadwal = "' . $kodeJadwal . '" ')->row();

		if ($student->jumlah != count(array_unique($datas))) {
			echo "Gagal Upload, Mohon cek data mahasiswa. <a href='javascript:history.go(-1)'><< Kembali</a>";
			exit();
		}
		$this->_tambah_nilai_reguler($dataExcel, $kodeJadwal, $testType);
	}

	function _uas_validation($testType, $dataPoin, $kodeJadwal, $npm)
	{
		// if dispensasi
		if ($testType == 'all') {
			return number_format($dataPoin, 2);
		} else {
			// prevent filling abnormal values (for UAS)
			$isAbsenFull = $this->app_model->bisa_ujiankah($kodeJadwal,$npm);

			if ($isAbsenFull >= 75) {
				return number_format($dataPoin, 2);
			} else {
				return 0;
			}
		}
	}

	function _validasi_peserta_kelas($npm, $kodeJadwal)
	{
		$isStudentWasParticipant = $this->poin->participant_validation($npm, $kodeJadwal)->num_rows();

		if ($isStudentWasParticipant < 1) {
			echo "Gagal Upload, Inputan Data Kelas Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
			exit();
		}
		return;
	}

	function _validasi_nilai_tugas($poin)
	{
		if ($poin == '-' || empty($poin) || is_null($poin)) {
			$taskPoin = NULL;
		} else {
			$taskPoin = number_format($poin, 2);
		}
		return $taskPoin;
	}

	function _validasi_nilai_akhir($testType, $kodeJadwal, $dataExcel, $npm)
	{
		if ($testType == 'uas') {
			$utsScore = $this->db->query('SELECT * FROM tbl_nilai_detail where kd_jadwal = "' . $kodeJadwal . '" and tipe = "3"')->num_rows();

			if ($utsScore < 1 || is_null($utsScore)) {
				$finalscore = (0.1 * $dataExcel['nilai_2']) + (0.2 * $dataExcel['nilai_1']) + (0.4 * $dataExcel['nilai_4']);

			} else {

				$cekNilaiUts = $this->db->query("SELECT nilai FROM tbl_nilai_detail 
												WHERE tipe = 3 
												AND npm_mahasiswa = '".$npm."' 
												AND kd_jadwal = '".$kodeJadwal ."'")->row()->nilai;

				if ($cekNilaiUts != $dataExcel['nilai_3']) {
					$finalscore = (0.1 * $dataExcel['nilai_2']) + (0.2 * $dataExcel['nilai_1']) + (0.3 * $cekNilaiUts) + (0.4 * $dataExcel['nilai_4']);
				} else {
					$finalscore = (0.1 * $dataExcel['nilai_2']) + (0.2 * $dataExcel['nilai_1']) + (0.3 * $dataExcel['nilai_3']) + (0.4 * $dataExcel['nilai_4']);
				}
				
			}

			$tofloat = number_format($finalscore, 2);

		// masa UTS
		} elseif ($testType == 'uts') {
			$finalscore = (0.1 * $dataExcel['nilai_2']) + (0.2 * $dataExcel['nilai_1']) + (0.3 * $dataExcel['nilai_3']);
			$tofloat    = number_format($finalscore, 2);

		// dispensasi dosen
		} elseif ($testType == 'all') {
			$tofloat = number_format($dataExcel['cell_15'], 2);
		} else {
			$tofloat = number_format($dataExcel['cell_4'], 2);
		}

		return $tofloat;
	}

	function _validasi_konten_nilai_akhir($kontenNilai)
	{
		if ($kontenNilai < 0 || ctype_alpha($kontenNilai)) {
			echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
			exit();
		}
		return;
	}

	function _get_client_ip()
	{
		$ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    } else {
	        $ipaddress = 'UNKNOWN';
	    }
	    return $ipaddress;
	}

	function _tambah_nilai_reguler($dataarray, $kodeJadwal, $ttype) 
	{
		$user          = $this->session->userdata('sess_dosen');
		$nid           = $user['userid'];
		$tahunAkademik = $user['tahun'];

		/**
		 * jika masa UAS, data nilai UTS tidak dapat diganti
		 * maka hanya nilai UTS yang tidak di delete
		 */
		if ($ttype == 'uas') {
			$this->db->where('tipe !=', 3);
			$this->db->where('kd_jadwal', $kodeJadwal);
			$this->db->delete('tbl_nilai_detail');
		} else {
			$this->db->where('kd_jadwal', $kodeJadwal);
			$this->db->delete('tbl_nilai_detail');
		}

		for ($i = 0; $i < count($dataarray); $i++) {
			$matakuliah   = $this->poin->get_matakuliah_by_jadwal($kodeJadwal);
			$transaksiKrs = $this->poin->get_krs_transaction($dataarray[$i]['npm'].$tahunAkademik);

			for ($z = 1; $z < 11; $z++) {

				$a = $dataarray[$i]['nilai_' . $z . ''];
				$b = $a[0];
				$c = $a[1];

				if ($a == '' or is_null($a)) {
					$a = NULL;
				}

				if ($ttype == 'uas') {
					if ($z != 3) {
						$datax[] = [
							'npm_mahasiswa'      => $dataarray[$i]['npm'],
							'kd_krs'             => $transaksiKrs->kd_krs,
							'kd_matakuliah'      => $matakuliah->kd_matakuliah,
							'tahun_ajaran'       => $tahunAkademik,
							'tipe'               => $z,
							'kd_prodi'           => $transaksiKrs->kd_jurusan,
							'nid'                => $nid,
							'audit_date'         => date('Y-m-d H:i:s'),
							'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $dataarray[$i]['npm'], 
							'nilai'              => $a,
							'flag_publikasi'     => 1,
							'kd_jadwal'          => $dataarray[$i]['code'],
						];
					}
				} elseif ($ttype == 'uts') {
					if ($z != 4) {
						$datax[] = [
							'npm_mahasiswa'      => $dataarray[$i]['npm'],
							'kd_krs'             => $transaksiKrs->kd_krs,
							'kd_matakuliah'      => $matakuliah->kd_matakuliah,
							'tahun_ajaran'       => $tahunAkademik,
							'tipe'               => $z,
							'kd_prodi'           => $transaksiKrs->kd_jurusan,
							'nid'                => $nid,
							'audit_date'         => date('Y-m-d H:i:s'),
							'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $dataarray[$i]['npm'],
							'nilai'              => $a,
							'flag_publikasi'     => 1,
							'kd_jadwal'          => $dataarray[$i]['code'],
						];
					}
				} elseif ($ttype == 'all') {
					$datax[] = [
						'npm_mahasiswa'      => $dataarray[$i]['npm'],
						'kd_krs'             => $transaksiKrs->kd_krs,
						'kd_matakuliah'      => $matakuliah->kd_matakuliah,
						'tahun_ajaran'       => $tahunAkademik,
						'tipe'               => $z,
						'kd_prodi'           => $transaksiKrs->kd_jurusan,
						'nid'                => $nid,
						'audit_date'         => date('Y-m-d H:i:s'),
						'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $dataarray[$i]['npm'],
						'nilai'              => $a,
						'flag_publikasi'     => 1,
						'kd_jadwal'          => $dataarray[$i]['code'],
					];
				}
			}
		}
		
		$datax = $this->security->xss_clean($datax);
		$this->db->insert_batch('tbl_nilai_detail', $datax);

		$idjadwal = $this->app_model->getdetail('tbl_jadwal_matkul','kd_jadwal',$kodeJadwal,'kd_jadwal','asc')->row();
		echo "<script>alert('Sukses');
		document.location.href='" . base_url() . "form/formnilai/view/" . $idjadwal->id_jadwal . "';</script>";

	}

	function _tambah_nilai_skripsi($dataarray, $kodeJadwal, $kodeMatakuliah) 
	{
		$user   = $this->session->userdata('sess_dosen');
		$nid    = $user['userid'];
		$tahun  = $user['tahun'];

		// for reset all poin, we delete poins
		$this->db->query('DELETE FROM tbl_nilai_detail WHERE kd_jadwal LIKE "%' . $kodeJadwal . '%"');

		for ($i = 0; $i < count($dataarray); $i++) {
			
			$matakuliah = $this->poin->get_matakuliah($kodeMatakuliah, $kodeJadwal);
			$dataKrs    = $this->poin->get_krs_transaction($dataarray[$i]['npm'].$tahun);
			$nilai      = $dataarray[$i]['nilai_10'];

			$storedPoin[] = array(
				'npm_mahasiswa'      => $dataarray[$i]['npm'],
				'kd_krs'             => $dataKrs->kd_krs,
				'kd_matakuliah'      => $matakuliah->kd_matakuliah,
				'tahun_ajaran'       => $dataKrs->tahunajaran,
				'tipe'               => 10,
				'kd_prodi'           => $dataKrs->kd_jurusan,
				'nid'                => $nid,
				'audit_date'         => date('Y-m-d H:i:s'),
				'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $dataarray[$i]['npm'], //$dataarray[$i]['tran'],
				'nilai'              => $nilai,
				'flag_publikasi'     => 1,
				'kd_jadwal'          => $dataarray[$i]['code'],
			);

		}

		$storedPoin = $this->security->xss_clean($storedPoin);
		$this->db->insert_batch('tbl_nilai_detail', $storedPoin);

		$idjadwal = $this->app_model->getdetail('tbl_jadwal_matkul', 'kd_jadwal', $kodeJadwal, 'kd_jadwal', 'asc')->row()->id_jadwal;
		echo "<script>alert('Sukses'); document.location.href='" . base_url() . "form/formnilai/view/" . $idjadwal . "';</script>";

	}

	function cetak_nilai($id) 
	{
		$this->load->library('Cfpdf');

		$data['id_jadwal']  = $id;
		$data['dataJadwal'] = $this->db->query('SELECT 
													a.kd_matakuliah,
													a.kd_jadwal,
													a.kd_dosen,
													a.kd_tahunajaran,
													a.kelas,
													b.nama_matakuliah,
													b.sks_matakuliah,
													b.semester_matakuliah,
													b.kd_prodi,
													c.nama
												FROM tbl_jadwal_matkul a
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
												JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
												WHERE id_jadwal = "' . $id . '" 
												AND b.kd_prodi = SUBSTR(a.kd_jadwal,1,5)')->row();

		if ($data['dataJadwal']->kd_tahunajaran >= '20171') {
			$tblabsen = 'tbl_absensi_mhs_new_20171';
		} elseif ($data['rows']->kd_tahunajaran == '20162') {
			$tblabsen = 'tbl_absensi_mhs_new';
		} else {
			$tblabsen = 'tbl_absensi_mhs';
		}

		$data['absendosen'] = $this->db->query("SELECT count(distinct pertemuan) AS satu FROM " . $tblabsen . "
												WHERE kd_jadwal = '" . $data['dataJadwal']->kd_jadwal . "'")->row();

		$data['title'] 	= $this->db->query('SELECT * FROM tbl_matakuliah a
											JOIN tbl_jurusan_prodi b ON a.`kd_prodi` = b.`kd_prodi`
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE a.`kd_matakuliah` = "' . $data['dataJadwal']->kd_matakuliah . '"
											AND a.kd_prodi = "' . $data['dataJadwal']->kd_prodi . '"')->row();

		$data['kaprodi'] = $this->db->query('SELECT kaprodi FROM tbl_jurusan_prodi WHERE kd_prodi = "' . $data['dataJadwal']->kd_prodi . '"')->row()->kaprodi;

		$data['peserta'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa mhs
											JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
											WHERE krs.kd_jadwal = "' . $data['dataJadwal']->kd_jadwal . '"')->result();

		if (is_final_assignment($data['title']->nama_matakuliah)) {
			$this->load->view('form/cetak_nilai_matkul_skripsi', $data);

		} else {
			$this->load->view('form/cetak_nilai_matkul', $data);
		}

	}

	function authdosen() 
	{
		if ($this->session->userdata('sess_dosen') != TRUE) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'form_auth_nilai';
			$this->load->view('template/template', $data);
		} else {
			$user = $this->session->userdata('sess_dosen');
			$nik = $user['userid'];
			$tahun = $user['tahun'];
			$bro = $this->session->userdata('sess_login');
			$prodi = $bro['userid'];
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
												WHERE a.`kd_dosen` = "' . $nik . '" AND a.`kd_jadwal` like "' . $prodi . '%" and a.kd_tahunajaran = "' . $tahun . '" and b.kd_prodi = "' . $prodi . '"
												and (a.gabung IS NULL or a.gabung = 0)
												GROUP BY a.id_jadwal')->result();

			$data['page'] = 'form/nilai_view';
			$this->load->view('template/template', $data);
		}
	}

	function authdosenlog() 
	{
		if (!$this->session->userdata('sess_dosen')) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']      = 'form_auth_nilai';
			$this->load->view('template/template', $data);
		} else {
			$user  = $this->session->userdata('sess_dosen');
			$nik   = $user['userid'];
			$tahun = $user['tahun'];
			$data['rows'] 	= $this->db->query('SELECT distinct a.*,b.nama_matakuliah FROM tbl_jadwal_matkul a
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
												WHERE a.`kd_dosen` = "' . $nik . '" 
												and a.kd_tahunajaran = "' . $tahun . '"
												GROUP BY a.id_jadwal')->result();

			$data['page'] = 'form/nilai_view';
			$this->load->view('template/template', $data);
		}
	}

	function load_dosen_autocomplete() 
	{
		$bro = $this->session->userdata('sess_login');
		$prodi = $bro['userid'];
		$this->db->distinct();
		$this->db->select("a.id_kary,a.nid,a.nama");
		$this->db->FROM('tbl_karyawan a');
		$this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');
		$this->db->like('b.kd_jadwal', $prodi, 'after');
		$this->db->like('a.nama', $_GET['term'], 'both');
		$this->db->or_like('a.nid', $_GET['term'], 'both');
		$sql = $this->db->get();
		$data = array();

		foreach ($sql->result() as $row) {
			$data[] = array(
				'id_kary' => $row->id_kary,
				'nid' => $row->nid,
				'value' => $row->nid . ' - ' . $row->nama,
			);
		}

		echo json_encode($data);
	}

	function load_mhs_autocomplete() 
	{

		$sql = $this->db->query('SELECT a.*,b.kd_krs FROM tbl_mahasiswa a
								JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
								JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
								WHERE c.`id_jadwal` = ' . $this->session->userdata('id_jadwal') . ' and (b.npm_mahasiswa like "%' . $_GET['term'] . '%" OR a.`NMMHSMSMHS` like "%' . $_GET['term'] . '%") ORDER BY NIMHSMSMHS');

		$data = array();

		foreach ($sql->result() as $row) {

			$data[] = array(

				'npm' => $row->NIMHSMSMHS,

				'value' => $row->NIMHSMSMHS . '-' . $row->NMMHSMSMHS,

				'kd_krs' => $row->kd_krs,

			);

		}

		echo json_encode($data);

	}

	function histori_nilai() {
		//cek ada atau nggak

		//tab1
		$id = $this->input->post('id_jadwal');

		$data['id_jadwal'] = $id;

		$data['kode_jadwal_bro'] = $this->db->select('kd_jadwal')->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row()->kd_jadwal;

		$data['ding'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul WHERE `id_jadwal` = "' . $id . '"')->row();

		$data['look'] = $this->db->query('SELECT * FROM tbl_mahasiswa a
											JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
											JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
											WHERE c.`id_jadwal` = "' . $id . '"
											AND b.kd_matakuliah = "' . $data['ding']->kd_matakuliah . '"
											ORDER BY NIMHSMSMHS')->result();

		$data['jdl_mk'] = $id;

		$this->session->set_userdata('idj', $id);
		$this->session->set_userdata('kdj', $data['kode_jadwal_bro']);

		//end tab1

		$mhs = explode('-', $this->input->post('npm'));

		$data['npm'] = $mhs[0];
		$data['nama'] = $mhs[1];

		$npm = $this->input->post('npm_mhs');
		$kd_jadwal = $this->input->post('kd_jadwal');

		$cek = $this->db->query("SELECT distinct npm_mahasiswa FROM tbl_nilai_detail where npm_mahasiswa = '" . $npm . "'
    								AND kd_jadwal = '" . $kd_jadwal . "' ")->result();

		$data['ctgs_1'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 5 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_2'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 6 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_3'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 7 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_4'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 8 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_5'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 9 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;

		// $data['pembagi'] = $this->db->query("SELECT COUNT(tipe) AS bill FROM tbl_nilai_detail WHERE tipe IN(5,6,7,8,9) AND kd_jadwal = '".$kd_jadwal."' AND nilai = 0 AND npm_mahasiswa = '".$npm."'")->row()->bill;
		$data['ada'] = $this->db->query("SELECT COUNT(tipe) AS pop FROM tbl_nilai_detail WHERE tipe IN(5,6,7,8,9)
    										AND kd_jadwal = '" . $kd_jadwal . "' AND nilai > 0 AND npm_mahasiswa = '" . $npm . "'")->row()->pop;
		//var_dump($data['ada']);exit();
		$data['byk_mhs'] = $this->db->query('select count(npm_mahasiswa) as banyak FROM tbl_krs where kd_jadwal = "' . $kd_jadwal . '"')->row()->banyak;

		//die($ctgs_1.'---'.$ctgs_2.'---'.$ctgs_3.'---'.$byk_mhs);

		$data['nilai_edit'] = $this->db->query("SELECT DISTINCT a.`NMMHSMSMHS`,a.`NIMHSMSMHS`,b.`kd_jadwal`,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 1 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 2 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS absen,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 3 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS uts,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 4 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS uas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 5 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas1,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 6 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas2,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 7 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas3,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 8 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas4,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 9 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas5,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 10 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS nilai_ahir
				FROM tbl_mahasiswa a
				JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
				WHERE b.`kd_jadwal` = '" . $kd_jadwal . "' AND b.npm_mahasiswa = '" . $npm . "' GROUP BY b.`kd_jadwal`")->row();
		//var_dump($data['nilai_edit']->tugas3);exit();
		$data['task1'] = $data['nilai_edit']->tugas1;
		$data['task2'] = $data['nilai_edit']->tugas2;
		$data['task3'] = $data['nilai_edit']->tugas3;
		$data['task4'] = $data['nilai_edit']->tugas4;
		$data['task5'] = $data['nilai_edit']->tugas5;
		$data['nama'] = $data['nilai_edit']->NMMHSMSMHS;

		$data['absenmhs'] = $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM tbl_absensi_mhs_new_20171
												where kd_jadwal = '" . $kd_jadwal . "' and npm_mahasiswa = '" . $npm . "'
												and (kehadiran IS NULL or kehadiran = 'H')")->row()->dua;

		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171
												where kd_jadwal = '" . $kd_jadwal . "'")->row()->satu;
		//var_dump($data['nilai']);die();
		$data['tayang_nilai'] = 'ada';
		if ($cek == TRUE) {
			$data['page'] = 'form/nilai_lihat';
			$this->load->view('template/template', $data);
		} else {
			echo "<script>alert('Nilai Belum Diinput');history.go(-1);</script>";
		}

	}

	function histori_nilai22() {

		//die($this->input->post('npm_mhs').''.$this->input->post('kd_jadwal'));

		$to = explode('-', $this->input->post('npm'));

		$sa = $to[0];

		$jdl_mk = $this->session->userdata('idj');

		$kdmk = $this->app_model->get_mk_by_jadwal($jdl_mk)->row();

		$hasil_kd = $kdmk->kd_matakuliah;

		$this->db->select('*');

		$this->db->where('id_jadwal', $jdl_mk);

		$qq = $this->db->get('tbl_jadwal_matkul')->row();

		$kd = $qq->kd_jadwal;

		$userr = $this->session->userdata('sess_dosen');

		$tahunn = $userr['tahun'];

		$cari = $this->db->query("SELECT DISTINCT a.`NMMHSMSMHS`,a.`NIMHSMSMHS`,b.`kd_jadwal`,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '1' AND npm_mahasiswa = '" . $sa . "') AS tugas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '2' AND npm_mahasiswa = '" . $sa . "') AS absen,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '3' AND npm_mahasiswa = '" . $sa . "') AS uts,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '4' AND npm_mahasiswa = '" . $sa . "') AS uas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '5' AND npm_mahasiswa = '" . $sa . "') AS tugas1,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '6' AND npm_mahasiswa = '" . $sa . "') AS tugas2,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '7' AND npm_mahasiswa = '" . $sa . "') AS tugas3,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '9' AND npm_mahasiswa = '" . $sa . "') AS tugas5,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '8' AND npm_mahasiswa = '" . $sa . "') AS tugas4,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '10' AND npm_mahasiswa = '" . $sa . "') AS akhir
				FROM tbl_mahasiswa a
				JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
				WHERE b.`kd_jadwal` = '" . $kd . "' AND b.npm_mahasiswa = '" . $sa . "' GROUP BY b.`kd_jadwal`")->result();

		$data = array();

		foreach ($sql->result() as $row) {

			$data[] = array(

				'npm' => $row->NIMHSMSMHS,

				'value' => $row->NIMHSMSMHS . '-' . $row->NMMHSMSMHS,

				'absen' => $row->absen,

				'tugas' => $row->tugas,

				'nilai_t1' => $row->tugas1,

				'nilai_t2' => $row->tugas2,

				'nilai_t4' => $row->tugas4,

				'nilai_t5' => $row->tugas5,

				'nilai_t3' => $row->tugas3,

				'uts' => $row->uts,

				'uas' => $row->uas,

				'akhir' => $row->akhir,

			);

		}

		echo json_encode($data);
	}

	function update_nilai() {

		$npm = $this->input->post('npm2');

		$kd_krs = $this->input->post('kd_krs');

		for ($i = 1; $i < 9; $i++) {
			$a = $this->input->post('tugas[' . $i . ']');
			//var_dump($a);die();

			$dataxx = array(
				'nilai' => $a,
			);

			$this->db->where('npm_mahasiswa', $npm);
			$this->db->where('kd_krs', $kd_krs);
			$this->db->where('tipe', $i);
			$this->db->update('tbl_nilai_detail', $dataxx);
		}

		echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "form/formnilai/view/" . $this->session->userdata('id_jadwal') . "';</script>";
	}

	function update_nilai22() 
	{
		$userr    = $this->session->userdata('sess_dosen');	
		$nikk     = $userr['userid'];
		$tahunn   = $userr['tahun'];
		$npm      = $this->input->post('nim_mhs');
		$nim      = explode('-', $npm);
		$no       = $npm;
		$nilai    = $this->input->post('tugas');
		$jdl_mk   = $this->session->userdata('idj');
		$kdmk     = $this->app_model->get_mk_by_jadwal($jdl_mk)->row();
		$hasil_kd = $kdmk->kd_matakuliah;

		$this->db->where('id_jadwal', $jdl_mk);
		$qq = $this->db->get('tbl_jadwal_matkul')->row();


		$data['lala'] 	= $this->db->query('SELECT * FROM tbl_verifikasi_krs a join tbl_mahasiswa b
	    									on a.`npm_mahasiswa`=b.`NIMHSMSMHS`
	    									where a.`npm_mahasiswa`="' . $no . '"
	    									and tahunajaran = "' . $tahunn . '"')->row();

		for ($z = 1; $z < 11; $z++) {
			$a = $this->input->post('tugas[' . $z . ']');
			//var_dump($a);exit();
			$b = $a[0];
			$c = $a[1];

			$dataxx = array(
				'npm_mahasiswa'      => $no,
				'kd_krs'             => $data['lala']->kd_krs,
				'kd_matakuliah'      => $qq->kd_matakuliah,
				'tahun_ajaran'       => $data['lala']->tahunajaran,
				'tipe'               => $z,
				'kd_prodi'           => $data['lala']->KDPSTMSMHS,
				'nid'                => $nikk,
				'audit_date'         => date('Y-m-d H:i:s'),
				'kd_transaksi_nilai' => $qq->kd_jadwal . 'zzz' . $no,
				'nilai'              => $a,
				'flag_publikasi'     => 1,
				'kd_jadwal'          => $qq->kd_jadwal,
			);

			// var_dump($dataxx);exit();

			$this->db->where('npm_mahasiswa', $no);
			$this->db->where('tipe', $z);
			$this->db->where('kd_jadwal', $qq->kd_jadwal);
			$this->db->where('tahun_ajaran', $tahunn);
			//var_dump($dataxx); exit();
			$this->db->update('tbl_nilai_detail', $dataxx);
		}

		echo "<script>alert('Sukses');
		document.location.href='" . base_url() . "form/formnilai/view/" . $this->session->userdata('id_jadwal') . "';</script>";
	}

}

/* End of file Formnilai.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Formnilai.php */
