<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formcuti extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		// error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(37)->result();
			
			if ($akses != TRUE) {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->library('Cfpdf');
		$this->load->model('form/cuti_model', 'cuti');
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		$asup = $this->session->userdata('sess_login');
		if ($asup['id_user_group'] == 10 || $asup['id_user_group'] == 1) {
			
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'form/cuti_baa';

		} elseif ($asup['id_user_group'] == 8) {
			
			$data['prod']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'form/cuti_prodi';

		} elseif ($asup['id_user_group'] == 9) {
			
			$data['pakultas']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['jur'] = $this->cuti->getJurByFak($asup['userid'])->result();
			$data['page'] = 'form/cuti_fak';

		}elseif ($asup['id_user_group'] == 5) {
			$data['page'] = 'cuti_mhs';
		}else {
			redirect(base_url());
		}
		$this->load->view('template/template', $data);	
	}

	function update_cuti()
	{
		$data = array(
			'pembimbing_akademik' => $this->input->post('pem'),
			'desksripsi' => $this->input->post('deskedit')
		);

		$this->app_model->updatedata('tbl_permohonan_cuti','id_cuti',$this->input->post('iden'),$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formcuti';</script>";
	}

	function simpen_sesi()
	{
		$fakultas = $this->input->post('fakultas');

		$jurusan = $this->input->post('jurusan');

        $tahunajaran = $this->input->post('thnajar');

       
        $this->session->set_userdata('tahunajaran', $tahunajaran);

		$this->session->set_userdata('jurusan', $jurusan);

		$this->session->set_userdata('fakultas', $fakultas);
      
		redirect(base_url('form/formcuti/load_cuti'));
	}

	function load_cuti()
	{
		$asup = $this->session->userdata('sess_login');
		$year = $this->session->userdata('tahunajaran');
		$come = $asup['id_user_group'];
		$mang = $asup['userid'];

		if ($come == 10 || $come == 1)
		{
			$data['holah'] = $this->cuti->loadCuti($come,$year)->result();
		} else {
			$data['holah'] = $this->cuti->loadCuti($come,$year,$mang)->result();
		}

		$data['page'] = 'form/tbl_cuti';
		$this->load->view('template/template', $data);
	}

	function form_cuti($id)
	{
		$data['idd'] = $this->cuti->forForm($id)->row();
		$b = $data['idd']->SMAWLMSMHS;
		$data['semester'] = $this->app_model->get_semester($b);
		$this->load->view('cuti_v', $data);
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function cetak_form_cuti()
	{
		$mhs = explode('-', $this->input->post('npm'));
		$id = $mhs[0];

		$kelas 	= $this->input->post('kelas');
		$hp 	= $this->input->post('hp');
		$alamat = $this->input->post('alamat');
		$alasan = $this->input->post('alasan');

		if ($kelas == 'PG') {
			$data['kelas'] = "Pagi" ;
		}elseif ($kelas == 'SR') {
			$data['kelas'] = "Sore" ;
		}elseif ($kelas == 'KY') {
			$data['kelas'] = "Karyawan" ;
		}

		$tahunajar 	= $this->db->query('SELECT * from tbl_tahunakademik where status = 1')->row();
		$tahun 		= explode(' ', $tahunajar->tahun_akademik);

		$data['npm'] 		= $mhs[0];
		$data['nama'] 		= $mhs[1];
		$data['semester'] 	= $tahun[2];
		$data['akademik'] 	= $tahun[0];
		$data['hp'] 		= $hp;
		$data['alamat'] 	= $alamat;
		$data['alasan'] 	= $alasan;

		$data['cetak'] 	= $this->cuti->printForm($id)->row();
		$data['jurfak'] = $this->cuti->getJurMhs($id)->row();

		$ob = array(
					'npm_mhs' => $id,
					'tgl_permohonan' => date('Y-m-d'),
					'tahun_akademik_cuti' => $tahunajar->kode,
					'alasan' => $alasan,
					'pembimbing_akademik' => $data['cetak']->nid,
					'alamat' => $alamat,
					'hp' => $hp
				);

		$this->db->insert('tbl_permohonan_cuti', $ob);
		$this->load->view('akademik/cetak_form_ajucuti',$data);
	}

	function print_cuti($id)
	{
		$data['cetak'] = $this->cuti->printCuty($id)->row();
		$b = $data['cetak']->SMAWLMSMHS;
		$data['semester'] = $this->app_model->get_semester($b);
		$this->load->view('welcome/print/cuti_print',$data);
	}

	function input_data()
	{
		$jum = count($this->input->post('mhs', TRUE));
		//var_dump($jum);exit();
		for ($i=0; $i < $jum ; $i++) { 
			$v = explode('911', $this->input->post('mhs['.$i.']', TRUE));
			$dx = $this->cuti->updateStatus($v[1],$this->session->userdata('tahunajaran'));
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formcuti/form_cuti';</script>";
	}

	function nyimpen()
	{
		$prodi = substr($this->input->post('kaprodi'), 0,10);
		$new_no = $this->db->query("SELECT kode_cuti from tbl_permohonan_cuti order by id_cuti desc limit 1")->row();
            
            $pecah = explode('/', $new_no->kode_cuti);

            if($pecah[3]<=9){
                $doc_no = "UBJ/CUTI/".date('d-m-Y')."/000".($pecah[3]+1);
            }elseif($pecah[3]<=99){
                $doc_no = "UBJ/CUTI/".date('d-m-Y')."/00".($pecah[3]+1);
            }elseif($pecah[3]<=999){
                $doc_no = "UBJ/CUTI/".date('d-m-Y')."/0".($pecah[3]+1);
            }elseif($pecah[3]<=9999){
                $doc_no = "UBJ/CUTI/".date('d-m-Y')."/".($pecah[3]+1);
            }
            //var_dump($doc_no);exit();
		$data = array(
			'kode_cuti' => $doc_no,
			'npm_mhs' => $this->input->post('npm'),
			'tgl_permohonan' => date('Y-m-d'),
			'status' => 0,
			'tahun_akademik_cuti' => $this->input->post('tahun'),
			'desksripsi' => $this->input->post('desk'),
			'pembimbing_akademik' => $this->input->post('pea'),
			'kaprodi' => $this->input->post('kaprodi')
		);
		//var_dump($data);exit();
		$this->app_model->insertdata('tbl_permohonan_cuti', $data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."form/formcuti/';</script>";
	}

	function aprove()
	{
		$data = array(
			'kode_cuti'				=> $this->input->post('kd_ct'),
			'npm_mhs'				=> $this->input->post('npmhs'),
			'tgl_permohonan'		=> $this->input->post('tanggal'),
			'status'				=> $this->input->post('ketpem'),
			'tahun_akademik_cuti'	=> $this->input->post('thn_akad'),
			'desksripsi'			=> $this->input->post('deskrip'),
			'pembimbing_akademik'	=> $this->input->post('pemakad'),
			'tgl_approve'			=> date('Y-m-d'),
			'keterangan_pembimbing' => $this->input->post('keterangan')
		);
		//var_dump($data);exit();
		$this->app_model->updatedata('tbl_permohonan_cuti','id_cuti',$this->input->post('identity'),$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."form/formcuti/load_cuti';</script>";
	}

	function looad($edc)
	{
		$data['a'] = $this->db->query("SELECT * from tbl_permohonan_cuti where id_cuti = '".$edc."'")->row();
		$this->load->view('form/edit_cuti', $data);
	}

	function print_all()
	{
		$asup = $this->session->userdata('sess_login');
		if ($asup['id_user_group'] == 10 || $asup['id_user_group'] == 1)
		{
			$data['p'] = $this->db->query('SELECT * from tbl_permohonan_cuti a join tbl_mahasiswa b
			on a.`npm_mhs`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`= b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas`
			where a.`tahun_akademik_cuti`="'.$this->session->userdata('tahunajaran').'"')->result();	
		
		} elseif ($asup['id_user_group'] == 9) {
			$data['p'] = $this->db->query('SELECT * from tbl_permohonan_cuti a join tbl_mahasiswa b
			on a.`npm_mhs`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`= b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas`
			where a.`tahun_akademik_cuti`="'.$this->session->userdata('tahunajaran').'"
			AND c.`kd_prodi`="'.$this->session->userdata('jurusan').'"')->result();
		
		} elseif ($asup['id_user_group'] == 8) {
			$data['p'] = $this->db->query('SELECT * from tbl_permohonan_cuti a join tbl_mahasiswa b
			on a.`npm_mhs`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`= b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas`
			where a.`tahun_akademik_cuti`="'.$this->session->userdata('tahunajaran').'"')->result();
		
		} elseif ($asup['id_user_group'] == 7) {
			$data['p'] = $this->db->query('SELECT * from tbl_permohonan_cuti a join tbl_mahasiswa b
			on a.`npm_mhs`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`= b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas`
			where a.`tahun_akademik_cuti`="'.$this->session->userdata('tahunajaran').'"')->result();
		}
		
		$this->load->view('printall_view', $data);
	}

	function load_mhs_autocomplete()
    {

        $sql = $this->db->query('SELECT a.* FROM tbl_mahasiswa a
        						where (a.`NIMHSMSMHS` like "%'.$_GET['term'].'%" OR a.`NMMHSMSMHS` like "%'.$_GET['term'].'%") ORDER BY NIMHSMSMHS');

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                        'value' => $row->NIMHSMSMHS.'-'.$row->NMMHSMSMHS,

                        'npm' => $row->NIMHSMSMHS

                    );

        }

        echo json_encode($data);
    
    }

}

/* End of file Formcuti.php */
/* Location: ./application/modules/form/controllers/Formcuti.php */