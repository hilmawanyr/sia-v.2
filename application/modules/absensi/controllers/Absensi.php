<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(137)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		
	}
	public function index()
	{
		if ($this->session->userdata('sess_jadwal') == '') {

			//var_dump($this->session->userdata('sess_login'));

			redirect('absensi/login','refresh');

			//die('asddsasdasad');

		} else {

			$user = $this->session->userdata('sess_jadwal');
			$kadal = $this->role_model->cek_abs($user['kd_jdl'])->row();
			// var_dump($kadal);exit();
			if ($kadal->open != 1) {
				echo "<script>alert('Jadwal Anda Belum Dapat Diakses. Harap Hubungi Prodi !');history.go(-1);</script>";
			} else { 
				// var_dump($user['id_jdl']);exit();
				// $res = explode('/', $user['kd_jdl']);
				$data['kelas'] = $this->app_model->get_kelas_mahasiswa($user['id_jdl']);


				$tanggal = $this->db->query("SELECT distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$user['kd_jdl']."' order by id_absen desc limit 1")->row();

				$data['tang'] = $tanggal->pertemuan+1;

				$this->db->select('a.nid,a.nama,b.kelas,c.nama_matakuliah')
						->from('tbl_karyawan a')
						->join('tbl_jadwal_matkul b','a.nid = b.kd_dosen')
						->join('tbl_matakuliah c','c.kd_matakuliah = b.kd_matakuliah')
						->where('b.kd_jadwal',$user['kd_jdl']);
				$data['nm'] = $this->db->get()->row();
				// $data['kelas'] = $this->app_model->get_data_jadwal($user['id_jdl'],$res[0])->row();
				$data['max'] = $this->db->query("SELECT count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$user['kelas']."'")->row()->mhs;
				$data['id'] = $user['id_jdl'];

				$data['page'] = "v_abs_dsn";
				$this->load->view('template/template', $data);
			}
		}
	}

	function login()
	{
		$this->load->view('v_login_jdl');
	}

	function verif_kode()
	{
		$user = $this->input->post('kd', TRUE);
		$id = $this->session->userdata('sess_login');
		//$pecah = explode('/', $user);


		$cek = $this->login_model->cek_kode($user,$id['userid'])->result();

		// var_dump($cek);exit();

			if (count($cek) > 0) {

				foreach ($cek as $key) {
					//die('tolol');

					$session['userid'] = $key->kd_dosen;

					$session['kd_jdl'] = $key->kd_jadwal;

					$session['kd_ta'] = $key->kd_tahunajaran;

					$session['kelas'] = $key->kelas;

					$session['kd_mk'] = $key->kd_matakuliah;

					$session['id_jdl'] = $key->id_jadwal;

					$this->session->set_userdata('sess_jadwal',$session);

					redirect(base_url('absensi'));

					// $this->index();

				}

			} else {

				//echo "Gagal Login , <a href=".base_url()."auth> Back >> </a>";

				echo "<script>alert('Akses Ditolak! Kode tidak ditemukan!');history.go(-1);</script>";

			}
	}

	function list_abs()
	{
		$sesi = $this->session->userdata('sess_jadwal');
		$data['code'] = $sesi['kd_jdl'];
		$data['id_jadwal'] = $sesi['id_jdl'];
		// var_dump($rt);exit();
		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "'.$sesi['kd_jdl'].'" GROUP BY pertemuan,tanggal')->result();
		$data['page'] = "v_list_abs";
		$this->load->view('template/template', $data);
	}

	function out()
	{
		$this->session->unset_userdata('sess_jadwal');

        redirect(base_url(), 'refresh');
	}

	function kelas_absen_copy($id)
	{
		date_default_timezone_set('Asia/Jakarta');
		$timekuy = date('H:i:s');

		$jumlahabsen=$this->input->post('jumlah', TRUE);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();
		$cek = $this->db->query("SELECT count(distinct tanggal) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kode->kd_jadwal."'")->row()->jml;
		$tanggal = $this->db->query("SELECT distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kode->kd_jadwal."' order by id_absen desc limit 1")->row();
		$data['tang'] = $tanggal->pertemuan+1;
		//var_dump($cek);exit();
		if ($cek > 0) {
			if ($tanggal->tanggal == $this->input->post('tgl',TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$a = 1;
				for ($i=0; $i < $jumlahabsen-1; $i++) { 
					$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
					$pecah = explode('-', $hasil);
					//var_dump($hasil);
					$datax[] = array(
						'npm_mahasiswa' => $pecah[1],
						'kd_jadwal' => $kode->kd_jadwal,
						'tanggal' => $this->input->post('tgl',TRUE),
						'pertemuan' => $tanggal->pertemuan+1,
						'kehadiran' => $pecah[0]
						// 'bahasan'	=> $this->input->post('bahas')
						);
					if (($tanggal->pertemuan+1) > 16) {
						echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
						document.location.href='".base_url()."absensi/list_abs/';</script>";
						exit();
					}
					$a++;
				}
				// var_dump($datax);exit();
				$this->db->insert_batch('tbl_absensi_mhs_new_20171',$datax);

				// cek sks mk
				$pro = substr($kode->kd_jadwal, 0,5);
				$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$kode->kd_matakuliah."' and kd_prodi = '".$pro."'")->row()->sks_matakuliah;
				
				if ($sks == '2') {
                    $date = new DateTime($timekuy);
                    $date->add(new DateInterval('PT90M'));
				} else {
                    $date = new DateTime($timekuy);
                    $date->add(new DateInterval('PT135M'));
					$fixtime = $date->format('H:i:s');
				}

				$das = array(
					'kd_jadwal'	=> $kode->kd_jadwal,
					'pertemuan'	=> $tanggal->pertemuan+1,
					'tgl'		=> $this->input->post('tgl',TRUE),
					'materi'	=> $this->input->post('bahas', TRUE),
					'jam_masuk'	=> date('H:i:s'),
					'jam_keluar'=> $fixtime
					);
				$this->db->insert('tbl_materi_abs', $das);

				echo "<script>alert('Sukses');
				document.location.href='".base_url()."absensi/list_abs/';</script>";
				
			}
		} else {
			$a = 1;
			for ($i=0; $i < $jumlahabsen-1; $i++) { 
				$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
				$pecah = explode('-', $hasil);
				$datax[] = array(
					'npm_mahasiswa' => $pecah[1],
					'kd_jadwal' => $kode->kd_jadwal,
					'tanggal' => $this->input->post('tgl',TRUE),
					'pertemuan' => 1,
					'kehadiran' => $pecah[0]
					);
				$a++;
			}
			// var_dump($datax);exit();
			$this->db->insert_batch('tbl_absensi_mhs_new_20171',$datax);

			// cek sks mk
			$pro = substr($kode->kd_jadwal, 0,5);
			$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$kode->kd_matakuliah."' and kd_prodi = '".$pro."'")->row()->sks_matakuliah;
			if ($sks == '2') {
                $date = new DateTime($timekuy);
                $date->add(new DateInterval('PT90M'));
			} else {
                $date = new DateTime($timekuy);
                $date->add(new DateInterval('PT135M'));
				$fixtime = $date->format('H:i:s');
			}
			
			$das = array(
					'kd_jadwal'	=> $kode->kd_jadwal,
					'pertemuan'	=> 1,
					'tgl'		=> $this->input->post('tgl',TRUE),
					'materi'	=> $this->input->post('bahas', TRUE),
					'jam_masuk'	=> date('H:i:s'),
					'jam_keluar'=> $fixtime
					);
			$this->db->insert('tbl_materi_abs', $das);

			echo "<script>alert('Sukses');
			document.location.href='".base_url()."absensi/list_abs/';</script>";
		}
		
	}

	function modal_edit_absen_copy($id,$pertemuan)
	{
		clearstatcache();
		$q = $this->db->query('select kd_jadwal from tbl_jadwal_matkul where id_jadwal = '.$id.'')->row()->kd_jadwal;

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q;
		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs_new_20171 WHERE pertemuan = '.$pertemuan.' AND kd_jadwal = "'.$q.'"')->row()->tanggal;
		$data['kelas'] = $this->db->query('SELECT DISTINCT krs.`npm_mahasiswa`,mhs.`NMMHSMSMHS`,
											(SELECT kehadiran FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "'.$q.'" AND pertemuan = '.$pertemuan.' AND npm_mahasiswa = krs.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs krs 
											JOIN tbl_mahasiswa mhs ON krs.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE krs.`kd_jadwal` = "'.$q.'" ORDER BY mhs.NIMHSMSMHS ASC')->result();
 
		$data['h'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$q."' AND pertemuan = '".$pertemuan."' and kehadiran = 'H'")->row();
		$data['a'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$q."' AND pertemuan = '".$pertemuan."' and kehadiran = 'A'")->row();
		$data['i'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$q."' AND pertemuan = '".$pertemuan."' and kehadiran = 'I'")->row();
		$data['s'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$q."' AND pertemuan = '".$pertemuan."' and kehadiran = 'S'")->row();
		$data['id'] = $id;
		$this->load->view('v_list', $data);
	}
	
	function cetak()
	{
		// var_dump($id);exit();
		$sesi = $this->session->userdata('sess_jadwal');
		$data['code'] = $sesi['kd_jdl'];
		$data['look'] = $this->db->query("SELECT a.nama_matakuliah,b.kelas,a.kd_matakuliah from tbl_matakuliah a join tbl_jadwal_matkul b on a.kd_matakuliah = b.kd_matakuliah where b.kd_jadwal = '".$sesi['kd_jdl']."'")->row();
		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "'.$sesi['kd_jdl'].'" GROUP BY pertemuan,tanggal')->result();

		$this->load->view('excel_print', $data);
	}

}

/* End of file Absen.php */
/* Location: ./application/modules/absensi/controllers/Absen.php */