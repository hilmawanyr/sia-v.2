<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
	}

	public function index()
	{
		if ($this->session->userdata('sess_login')) {
			$log = $this->session->userdata('sess_login');
			redirect('home','refresh');
		} else {
			$this->load->view('login02');
		}
	}

	public function login()
	{
		$user 	= $this->security->xss_clean($this->input->post('username', TRUE));
		$pass 	= $this->security->xss_clean($this->input->post('password', TRUE));
		$cek 	= $this->login_model->cekuser($user,$pass)->result();

		if (count($cek) > 0) {

			foreach ($cek as $row) {
				$type = $row->user_type;
			}

			$user_log = $this->login_model->datauser($user,$pass,$type)->result();

			foreach ($user_log as $key) {

				$session['username'] = $key->username;
				$session['userid'] = $key->userid;
				$session['id_user_group'] = $key->id_user_group;
				$session['password_plain'] = $key->password_plain;
				$session['user_type'] = $key->user_type;

				$this->session->set_userdata('sess_login',$session);

				$this->session->set_userdata('userid',$session['userid']);

				helper_log("login", "login siakad");
				$data['last_login'] = date('Y-m-d h:i:s');
				$data['logged'] = 1;
				$this->app_model->updatedata('tbl_user_login','username',$key->username,$data);

				if ($user == $pass) {
					redirect('extra/account/ganti_pass','refresh');

				}else{
					# handle login from direct login from email
					if ($this->session->userdata('sessdirectlogin')) {
						if ($key->id_user_group == 5) {
							redirect('akademik/krs_mhs/jadwal_kuliah','refresh');
						} else {
							$logdir = $this->session->userdata('sessdirectlogin');
							redirect('akademik/bimbingan/viewmhs/'.$logdir['kdkrs'],'refresh');
						}

					} else {
						$this->index();		
					}
				}
			}

		} else {
			echo "<script>alert('Gagal Login');history.go(-1);</script>";
		}

	}

	public function logout()
	{
		$logged = $this->session->userdata('sess_login');
		$data['logged'] = 0;
		$this->app_model->updatedata('tbl_user_login','username',$logged['username'],$data);

		$this->session->unset_userdata('sess_login');
		$this->session->sess_destroy();
        redirect('/auth', 'refresh');
	}

	public function mhs_auth()
	{
		$this->load->model('setting_model');

		$user 	= $this->security->xss_clean($this->input->post('username', TRUE));
		$pass 	= $this->security->xss_clean($this->input->post('password', TRUE));
		$cek 	= $this->login_model->datauser($user,$pass,'2')->result();

		if (count($cek) > 0) {

			foreach ($cek as $key) {

				$session['status'] = '1';
				$session['nimmhs'] = $key->NIMHSMSMHS;
				$session['kelas'] = $this->input->post('kelas');
				$session['strata'] = substr($key->NIMHSMSMHS, 4,1);

				$this->session->set_userdata('sess_mhs',$session);

				$aktif = $this->setting_model->getaktivasi('revisikrs')->result();
				$aktif1 = $this->setting_model->getaktivasi('revisikrss')->result();
				$aktiff = $this->setting_model->getaktivasi('krsfeeder')->result();

				$sp = $this->setting_model->getaktivasi('sp')->result();

				if ((count($aktif) != 0) or (count($aktif1) != 0)) {
					redirect('form/formkrs/reviewform','refresh');
				} elseif (count($sp) != 0) {
					redirect('form/formsp/viewform','refresh');
				} elseif (count($aktiff) != 0) {
					redirect('form/formkrsfeeder/viewform','refresh');
				} else {
					redirect('form/formkrs/viewform','refresh');
				}
			}

		} else {
			echo "<script>alert('Gagal Masuk');history.go(-1);</script>";
		}
	}

	public function mhs_auth_sp()
	{
		$this->load->model('setting_model');

		$user = $this->security->xss_clean($this->input->post('username', TRUE));
		$pass = $this->security->xss_clean($this->input->post('password', TRUE));
		$cek = $this->login_model->datauser($user,$pass,'2')->result();

		if (count($cek) > 0) {

			foreach ($cek as $key) {

				$session['status'] = '1';
				$session['nimmhs'] = $key->NIMHSMSMHS;
				$session['kelas'] = $this->input->post('kelas');
				$session['strata'] = substr($key->NIMHSMSMHS, 4,1);
				$this->session->set_userdata('sess_mhs',$session);
				
				redirect('form/formsp/viewform','refresh');
			}

		} else {
			echo "<script>alert('Gagal Masuk');history.go(-1);</script>";
		}

	}

	public function dosen_auth()
	{
	    $logged = $this->session->userdata('sess_login');
	    $pecah = explode(',', $logged['id_user_group']);
	    $jmlh = count($pecah);
	    for ($i=0; $i < $jmlh; $i++) { 
	    	$grup[] = $pecah[$i];
	    }
		$user = $this->input->post('kd_dosen', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		
		$cek = $this->login_model->loginnilai($user)->result();

		if (count($cek) > 0) {

			foreach ($cek as $key) {

				$session['userid'] = $key->userid;
				$session['tahun'] = $tahun;

				$this->session->set_userdata('sess_dosen',$session);

				if ( (in_array(8, $grup))) {
					redirect('form/formnilai/authdosen','refresh');
				} else {
					redirect('form/formnilai/authdosenlog','refresh');
				}
			}	

		} else {

			echo "<script>alert('Gagal Masuk');history.go(-1);</script>";

		}
	}

	public function forgot_pass()
	{
		$this->load->view('forgot_pass');
	}

}



/* End of file  Auth.php */

/* Location: ./application/controllers/ */