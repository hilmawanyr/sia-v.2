<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

//header
$excel->getActiveSheet()->setCellValue('A2', 'PERSYARATAN BAGI MAHASISWA YANG DINYATAKAN LULUS SELEKSI UJIAN PMB');

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A4', 'NO');
$excel->getActiveSheet()->setCellValue('B4', 'NO REGISTRASI');
$excel->getActiveSheet()->setCellValue('C4', 'NAMA');
$excel->getActiveSheet()->setCellValue('D5', 'AKTE');
$excel->getActiveSheet()->setCellValue('E5', 'KK');
$excel->getActiveSheet()->setCellValue('F5', 'KTP');
$excel->getActiveSheet()->setCellValue('F5', 'RAPOT');
$excel->getActiveSheet()->setCellValue('G5', 'SKHUN');
$excel->getActiveSheet()->setCellValue('H5', 'IJAZAH');
$excel->getActiveSheet()->setCellValue('I5', 'SURAT KELULUSAN');
$excel->getActiveSheet()->setCellValue('J4', 'FOTO');
$excel->getActiveSheet()->setCellValue('K4', 'TAHUN LULUS');
$excel->getActiveSheet()->setCellValue('L4', 'KETERANGAN');
$excel->getActiveSheet()->setCellValue('D4', 'LAMPIRAN KELENGKAPAN');

//database
$xx = 6;$no=1;foreach ($look as $key) {
	$excel->getActiveSheet()->setCellValue('A'.$xx.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$xx.'', $key->noreg);
	$excel->getActiveSheet()->setCellValue('C'.$xx.'', $key->nama);
	if ($this->session->userdata('cuss') == 's1') {
		$tanya = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%AKT%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('D'.$xx.'', $a);
		$tanya2 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%KK%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya2==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('E'.$xx.'', $a);
		$tanya3 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%KTP%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya3==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('F'.$xx.'', $a);
		$tanya4 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%RP%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya4==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('F'.$xx.'', $a);
		$tanya5 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%SKHUN%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya5==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('G'.$xx.'', $a);
		$tanya6 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%IJZ%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya6==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('H'.$xx.'', $a);
		$tanya7 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%SKL%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya7==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('I'.$xx.'', $a);
		$tanya8 = $this->db->query("select status_kelengkapan from tbl_form_camaba where status_kelengkapan LIKE '%FT%' AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya8==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('J'.$xx.'', $a);
		$tanya9 = $this->db->query("select status_kelengkapan from tbl_form_camaba where ((status_kelengkapan LIKE '%FT%' AND status_kelengkapan LIKE '%SKL%' AND status_kelengkapan LIKE '%IJZ%' AND status_kelengkapan LIKE '%SKHUN%' AND status_kelengkapan LIKE '%RP%' AND status_kelengkapan LIKE '%KTP%' AND status_kelengkapan LIKE '%KK%' AND status_kelengkapan LIKE '%AKT%') OR status_kelengkapan LIKE '%LLKP%') AND nomor_registrasi = '".$key->noreg."'")->result();
		if($tanya9==true){$ket = "lengkap";}else{$ket = "belum lengkap";}
		$excel->getActiveSheet()->setCellValue('L'.$xx.'', $ket);
	} else {
		$tanya = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%AKT%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('D'.$xx.'', $a);
		$tanya2 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%KK%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya2==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('E'.$xx.'', $a);
		$tanya3 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%KTP%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya3==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('F'.$xx.'', $a);
		$tanya4 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%RP%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya4==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('F'.$xx.'', $a);
		$tanya5 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%SKHUN%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya5==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('G'.$xx.'', $a);
		$tanya6 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%IJZ%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya6==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('H'.$xx.'', $a);
		$tanya7 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%SKL%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya7==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('I'.$xx.'', $a);
		$tanya8 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%FT%' AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya8==true){$a = "v";}else{$a = "-";}
		$excel->getActiveSheet()->setCellValue('J'.$xx.'', $a);
		$tanya9 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where ((status_kelengkapan LIKE '%FT%' AND status_kelengkapan LIKE '%SKL%' AND status_kelengkapan LIKE '%IJZ%' AND status_kelengkapan LIKE '%SKHUN%' AND status_kelengkapan LIKE '%RP%' AND status_kelengkapan LIKE '%KTP%' AND status_kelengkapan LIKE '%KK%' AND status_kelengkapan LIKE '%AKT%') OR status_kelengkapan LIKE '%LLKP%') AND ID_registrasi = '".$key->noreg."'")->result();
		if($tanya9==true){$ket = "lengkap";}else{$ket = "belum lengkap";}
		$excel->getActiveSheet()->setCellValue('L'.$xx.'', $ket);
	}
	$excel->getActiveSheet()->setCellValue('K'.$xx.'', $key->lls);
	$xx++;$no++;
}
$excel->getActiveSheet()->getStyle('A4:L'.$xx.'')->applyFromArray($BStyle);
$excel->getActiveSheet()->mergeCells('A2:J2');
$excel->getActiveSheet()->mergeCells('A4:A5');
$excel->getActiveSheet()->mergeCells('B4:B5');
$excel->getActiveSheet()->mergeCells('C4:C5');
$excel->getActiveSheet()->mergeCells('D4:I4');
$excel->getActiveSheet()->mergeCells('J4:J5');
$excel->getActiveSheet()->mergeCells('K4:K5');
$excel->getActiveSheet()->mergeCells('L4:L5');

$filename = 'Status_Kelengkapan.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>