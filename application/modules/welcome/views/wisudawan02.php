<?php
//ob_start();
require_once APPPATH.'/libraries/fpdf/rotation.php';

class Rotate extends PDF_Rotate
{
	function RotatedImage($file,$x,$y,$w,$h,$angle)
	{
	    //Image rotated around its upper-left corner
	    $this->Rotate($angle,$x,$y);
	    $this->Image($file,$x,$y,$w,$h);
	    $this->Rotate(0);
	}
}

$pdf = new Rotate("L","mm", array(190,340));
$pdf->AliasNbPages();	

foreach ($q as $val) {

	$pdf->AddPage();
	$pdf->SetMargins(3, 5 ,0);

	$img = base_url().'foto_wisudawan/73201/'.trim($val->npm).'.jpg';
	$img_default = base_url().'foto_wisudawan/poto_default.png';
	// Content type
	// header('Content-type: image/jpeg');

	//$im = imagecreatefromjpeg($img) or die("Cannot Initialize new GD image stream");
	// $rotate = imagerotate($im, 270, 0);
	// imagejpeg($rotate);
	// imagedestroy($rotate);
	// imagedestroy($img);

	// $pdf->image('http://172.16.1.5:801/assets/img/foto_wisudawan/201110225152-Muhammad.jpg',30,50,90);
	// $pdf->image($rotate,10,20,190);

	
	

	if (@getimagesize($img)) {
		$pdf->RotatedImage($img,150,-2,140,190,-90);
		// $pdf->image($img,1,1,125);
	}else{
		$pdf->image($img_default,1,15,150);
	}

	$pdf->SetFont('Arial','B',28);
	$pdf->setXY(0,24);
	$pdf->Cell(480,10,$val->nama,0,10,'C');

	$pdf->setXY(155,22);
	$pdf->Cell(177,15,'',1,1,'C');

	$pdf->Ln();
	$pdf->SetFont('Arial','B',22);
	$pdf->setXY(157,50);
	$pdf->Cell(75,10,'NPM',0,0);
	$pdf->Cell(5,10,':',0,0,'L');
	$pdf->Cell(75,10,$val->npm,0,0);

	$pdf->Ln();
	$pdf->SetFont('Arial','B',22);
	$pdf->setXY(157,80);
	$pdf->Cell(75,10,'Fakultas/Prodi',0,0);
	$pdf->Cell(5,10,':',0,0,'L');
	$pdf->MultiCell(90,10,$val->fak.'/'.$val->jur,0,'L',false);

	$pdf->Ln();
	$pdf->SetFont('Arial','B',22);
	$pdf->setXY(157,110);
	$pdf->Cell(75,10,'Tanggal Lahir',0,0);
	$pdf->Cell(5,10,':',0,0,'L');
	$pdf->MultiCell(90,10,$val->ttl,0,'L',false);


	$pdf->image('http://172.16.1.5:801/assets/logo.gif',220,147,30);
	//exit();
	// $pdf->Output('PPT_WISUDAWAN_MM_'.date('ymd_his').'.PDF','I');
}

$pdf->Output('PPT_WISUDAWAN_MM_'.date('ymd_his').'.PDF','I');

?>

