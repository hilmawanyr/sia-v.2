<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('H3:L4')->applyFromArray($BStyle);

$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Data Nilai ...');
//header
$excel->getActiveSheet()->setCellValue('E1', 'HASIL EVALUASI BELAJAR MENGAJAR');
$excel->getActiveSheet()->setCellValue('E2', ''.$prodi->prodi.' 2016/2017 - PENDEK');
$excel->getActiveSheet()->setCellValue('B3', 'Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C3', $rows->nama_matakuliah);
$excel->getActiveSheet()->setCellValue('B4', 'NID / Nama');
$excel->getActiveSheet()->setCellValue('C4', $rows->kd_dosen.' / '.$rows->nama);
$excel->getActiveSheet()->setCellValue('E3', 'Jumlah MHS');
$excel->getActiveSheet()->setCellValue('F3', $jmlh->jumlah);
$excel->getActiveSheet()->setCellValue('E4', 'Semester / Kelas');
$excel->getActiveSheet()->setCellValue('F4', $rows->semester_kd_matakuliah.' / '.$rows->kelas);
$excel->getActiveSheet()->setCellValue('H3', 'Bobot Nilai');
$excel->getActiveSheet()->setCellValue('I3', 'ABSENSI');
$excel->getActiveSheet()->setCellValue('J3', 'TUGAS');
$excel->getActiveSheet()->setCellValue('K3', 'UTS');
$excel->getActiveSheet()->setCellValue('L3', 'UAS');
$excel->getActiveSheet()->setCellValue('I4', '10%');
$excel->getActiveSheet()->setCellValue('J4', '20%');
$excel->getActiveSheet()->setCellValue('K4', '30%');
$excel->getActiveSheet()->setCellValue('L4', '40%');

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'NO');
$excel->getActiveSheet()->setCellValue('B6', 'NPM');
$excel->getActiveSheet()->setCellValue('C6', 'NAMA');
$excel->getActiveSheet()->setCellValue('D6', 'KEHADIRAN');
$excel->getActiveSheet()->setCellValue('D7', 'DOSEN');
$excel->getActiveSheet()->setCellValue('E7', 'MHS');
$excel->getActiveSheet()->setCellValue('F6', 'ABSEN (%)');
$excel->getActiveSheet()->setCellValue('G6', 'RATA-RATA TUGAS');
$excel->getActiveSheet()->setCellValue('H6', 'UTS');
$excel->getActiveSheet()->setCellValue('I6', 'UAS');
$excel->getActiveSheet()->setCellValue('J6', 'NILAI AKHIR');
$excel->getActiveSheet()->setCellValue('K6', 'NILAI HURUF');

//ISI DATABASE
//mahasiswa
$xx = 8;$no=1;foreach ($ping as $key) {
	$excel->getActiveSheet()->setCellValue('A'.$xx.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$xx.'', $key->NIMHSMSMHS);
	$excel->getActiveSheet()->setCellValue('C'.$xx.'', $key->NMMHSMSMHS);

	//getdata
	//$absendosen = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."'")->row();
	$absenmhs = $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM tbl_absensi_mhs_new where kd_jadwal = '".$rows->kd_jadwal."' and npm_mahasiswa = '".$key->NIMHSMSMHS."' and (kehadiran IS NULL or kehadiran = 'H')")->row();

	//nilai
	$qqq = $this->db->query("SELECT distinct tipe,nilai from tbl_nilai_detail where npm_mahasiswa = '".str_replace(' ', '', $key->NIMHSMSMHS)."' and kd_jadwal = '".$rows->kd_jadwal."' ")->result();
	foreach ($qqq as $key) {
		switch ($key->tipe) {
			case '1':
				$tugas = $key->nilai;
				break;
			case '3':
				$uts = $key->nilai;
				break;
			case '4':
				$uas = $key->nilai;
				break;
			case '5':
				$tugas1 = $key->nilai;
				break;
			case '6':
				$tugas2 = $key->nilai;
				break;
			case '7':
				$tugas3 = $key->nilai;
				break;
			case '8':
				$tugas4 = $key->nilai;
				break;
			case '9':
				$tugas5 = $key->nilai;
				break;
		}
	}

	//ABSENSI
	$excel->getActiveSheet()->setCellValue('D'.$xx.'', $absendosen->satu)
	                      ->setCellValue('E'.$xx.'', $absenmhs->dua)
	                      ->setCellValue('F'.$xx.'', '=(E'.$xx.'/D'.$xx.')*100');
	//TUGAS
	if (is_null($tugas1) or $tugas1 == '') {
		$tugas1 = '-';
	 	//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}
	if (is_null($tugas2) or $tugas2 == '') {
		$tugas2 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}
	if (is_null($tugas3) or $tugas3 == '') {
		$tugas3 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}
	if (is_null($tugas4) or $tugas4 == '') {
		$tugas4 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}
	if (is_null($tugas5) or $tugas5 == '') {
		$tugas5 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}
	if (is_null($tugas) or $tugas == '') {
		// $tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} else {
	 	// $tugasakhir = $tugas;
	}
	
	 
	$excel->getActiveSheet()->setCellValue('G'.$xx.'', '');

	//NILAI AKHIR
	$logged = $this->session->userdata('sess_login');
	if (($logged['userid'] == '74101')) {
		$hrf = '=IF(J'.$xx.'>=80,"B",IF(J'.$xx.'>=75,"B",IF(J'.$xx.'>=70,"B",IF(J'.$xx.'>=65,"B",IF(J'.$xx.'>=60,"B",IF(J'.$xx.'>=55,"C",IF(J'.$xx.'>=40,"D","E")))))))';
	}else{
		$hrf= '=IF(J'.$xx.'>=80,"B",IF(J'.$xx.'>=76,"B",IF(J'.$xx.'>=72,"B",IF(J'.$xx.'>=68,"B",IF(J'.$xx.'>=64,"B-",IF(J'.$xx.'>=60,"C+",IF(J'.$xx.'>=56,"C",IF(J'.$xx.'>=45,"D","E"))))))))';
	}
	$excel->getActiveSheet()->setCellValue('H'.$xx.'', $uts)
	                      ->setCellValue('I'.$xx.'', $uas)
	                      ->setCellValue('J'.$xx.'', '=(0.1*F'.$xx.')+(0.2*G'.$xx.')+(0.3*H'.$xx.')+(0.4*I'.$xx.')')
	                      ->setCellValue('K'.$xx.'',$hrf);
	                      
	$xx++;$no++;
}
$xw = $xx - 1;
$excel->getActiveSheet()->getStyle('A6:K'.$xw.'')->applyFromArray($BStyle);
$xy = $xx + 1;
//footer
// $excel->getActiveSheet()->setCellValue('B'.$xy.'', 'Nilai Maksimal Adalah C');

$logged = $this->session->userdata('sess_login');
if (($logged['userid'] == '74101')) {
	$a = $xy+1; 
	$b = $xy+2;
	$c = $xy+3;
	$d = $xy+4;
	// $excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
	// $excel->getActiveSheet()->setCellValue('B'.$b.'', '75-79.99');
	// $excel->getActiveSheet()->setCellValue('B'.$c.'', '70-74.99');
	// $excel->getActiveSheet()->setCellValue('B'.$d.'', '65-69.99');
	// $excel->getActiveSheet()->setCellValue('D'.$a.'', '60-64.99');
	// $excel->getActiveSheet()->setCellValue('D'.$b.'', '55-59.99');
	// $excel->getActiveSheet()->setCellValue('D'.$c.'', '40-54.99');
	// $excel->getActiveSheet()->setCellValue('D'.$d.'', '0-39.99');
	// $excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
	// $excel->getActiveSheet()->setCellValue('C'.$b.'', 'A-');
	// $excel->getActiveSheet()->setCellValue('C'.$c.'', 'B+');
	// $excel->getActiveSheet()->setCellValue('C'.$d.'', 'B');
	// $excel->getActiveSheet()->setCellValue('E'.$a.'', 'B-');
	// $excel->getActiveSheet()->setCellValue('E'.$b.'', 'C');
	// $excel->getActiveSheet()->setCellValue('E'.$c.'', 'D');
	// $excel->getActiveSheet()->setCellValue('E'.$d.'', 'E');
	// $excel->getActiveSheet()->getStyle('B'.$xy.':E'.$d.'')->applyFromArray($BStyle);
	$excel->getActiveSheet()->setCellValue('L'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
	$excel->getActiveSheet()->setCellValue('L'.$a.'', 'Dosen Yang Bersangkutan');
	$excel->getActiveSheet()->setCellValue('L'.$d.'', $rows->nama);
}else{
	$a = $xy+1; 
	$b = $xy+2;
	$c = $xy+3;
	$d = $xy+4;
	// $excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
	// $excel->getActiveSheet()->setCellValue('B'.$b.'', '65-79.99');
	// $excel->getActiveSheet()->setCellValue('B'.$c.'', '55-64.99');
	// $excel->getActiveSheet()->setCellValue('D'.$a.'', '45-54.99');
	// $excel->getActiveSheet()->setCellValue('D'.$b.'', '0-44.99');
	// $excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
	// $excel->getActiveSheet()->setCellValue('C'.$b.'', 'B');
	// $excel->getActiveSheet()->setCellValue('C'.$c.'', 'C');
	// $excel->getActiveSheet()->setCellValue('E'.$a.'', 'D');
	// $excel->getActiveSheet()->setCellValue('E'.$b.'', 'E');
	// $excel->getActiveSheet()->getStyle('B'.$xy.':E'.$c.'')->applyFromArray($BStyle);
	$excel->getActiveSheet()->setCellValue('L'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
	$excel->getActiveSheet()->setCellValue('L'.$a.'', 'Dosen Yang Bersangkutan');
	$excel->getActiveSheet()->setCellValue('L'.$d.'', $rows->nama);
}
$e = $xy+5;
$r = $xy+4;
$excel->getActiveSheet()->setCellValue('B'.$r.'', '*Harap Memasukkan Nilai Maksimal 2 Angka Di Belakang Koma');
$excel->getActiveSheet()->setCellValue('B'.$e.'', '*Nilai Maksimal Adalah B');
//merge cell
$excel->getActiveSheet()->mergeCells('B'.$xy.':E'.$xy.'');
$excel->getActiveSheet()->mergeCells('L'.$xy.':N'.$xy.'');
$excel->getActiveSheet()->mergeCells('L'.$a.':N'.$a.'');
$excel->getActiveSheet()->mergeCells('L'.$d.':N'.$d.'');
$excel->getActiveSheet()->mergeCells('E1:H1');
$excel->getActiveSheet()->mergeCells('E2:H2');
$excel->getActiveSheet()->mergeCells('H3:H4');
$excel->getActiveSheet()->mergeCells('A6:A7');
$excel->getActiveSheet()->mergeCells('B6:B7');
$excel->getActiveSheet()->mergeCells('C6:C7');
$excel->getActiveSheet()->mergeCells('D6:E6');
$excel->getActiveSheet()->mergeCells('F6:F7');
$excel->getActiveSheet()->mergeCells('G6:G7');
$excel->getActiveSheet()->mergeCells('H6:H7');
$excel->getActiveSheet()->mergeCells('I6:I7');
$excel->getActiveSheet()->mergeCells('K6:K7');
$excel->getActiveSheet()->mergeCells('J6:J7');
//change the font size
$excel->getActiveSheet()->getStyle('E1:E2')->getFont()->setSize(12);
$excel->getActiveSheet()->getStyle()->getFont()->setSize(11);

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("E1:E2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A6:N7")->applyFromArray($style);
//$excel->getDefaultStyle()->applyFromArray($style);

$filename = 'Form_Nilai_'.str_replace(' ', '_', $rows->nama_matakuliah)."_".str_replace(' ', '_', $rows->kelas).'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>