<?php 
ob_start();

class PDF extends FPDF
{
	function Footer()
    {
        // Go to 1.5 cm from bottom
	    $this->SetY(-60);
	    // Select Arial italic 8
	    $this->ln(1);
		$this->SetFont('Arial','',12);
		$this->Cell(135,0,'',0,0,'L');
		$this->Cell(87,0,'........... , .............................'.date('Y').'',0,1);

		$this->ln(8);
		$this->SetFont('Arial','',12);
		$this->Cell(135,0,'Pengawas :',0,1,'L');
		
		$this->ln(10);
		$this->SetFont('Arial','',12);
		$this->Cell(135,0,'1.',0,0,'L');
		$this->Cell(135,0,'(                                      )',0,1,'L');

		$this->ln(30);
		$this->SetFont('Arial','',12);
		$this->Cell(135,0,'2.',0,0,'L');
		$this->Cell(135,0,'(                                      )',0,1,'L');
    }
}


$pdf = new PDF("P","mm", "A4");
$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',18);


$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($garis->prodi),0,0,'L');

$pdf->Ln(6);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(200,5,''.strtoupper($garis->fakultas).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');

$pdf->Ln(5);



$pdf->SetFont('Arial','',9);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(6);



$pdf->SetFont('Arial','',9);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(1);

$pdf->SetFont('Arial','',9);

$pdf->Cell(20,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$line->kd_matakuliah,0,0,'L');

$pdf->Cell(15,5,'Smtr/Kelas',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(40,5,$this->session->userdata('semester').'/'.$line->kelas,0,0,'L');
//$pdf->Cell(40,5,$line->semester_matakuliah.'/'.$line->kelas,0,0,'L');

$pdf->Cell(25,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->MultiCell(50, 5, $line->nama, 0,'L');

//$pdf->ln(4);

$pdf->SetFont('Arial','',9);

$pdf->Cell(20,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 50;
$pdf->MultiCell($cell_width, 5, $line->nama_matakuliah, 0,'L',false);

$x = 50;
$pdf->SetXY($current_x + $x, $current_y);

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(40,5,$line->sks_matakuliah,0,0,'L');

$pdf->Cell(25,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$line->nid,0,1,'L');

$pdf->ln();

$pdf->SetFont('Arial','',8);

$pdf->Cell(25,5,'RUANG/WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(45,5,'',0,'L');

$pdf->Cell(15,5,'TANGGAL',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(40,5,'',0,0,'L');

$pdf->Cell(25,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi/Jakarta',0,0,'L');




$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

if ($tipe == 2) {
	$pdf->Cell(200,8,'DAFTAR HADIR PESERTA UJIAN TENGAH SEMESTER',1,0,'C');
} else {
	$pdf->Cell(200,8,'DAFTAR HADIR PESERTA UJIAN AKHIR SEMESTER',1,0,'C');
}

$pdf->ln(8);



$pdf->SetFont('Arial','',11);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(15,10,'ABSEN','L,T,R,B',0,'C');

$pdf->Cell(67,10,'KEHADIRAN','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NILAI','L,T,R,B',1,'C');
$no=1;
$noo=1;
foreach ($rows as $row) {
	//$pdf->ln(10);
	//$pdf->SetFillColor(128,128,0);
	
	if ((number_format($row->persentaseabsen) >= 60) and ($row->status >= $tipe)) {
		$warna = false;
	} else {
		$warna = true;
	}
	
	
	if ($tipe != 2) {
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$row->npm_mahasiswa,'NIMHSMSMHS','ASC')->row()->NMMHSMSMHS;

		$pdf->SetFont('Arial','',8);
		$pdf->SetFillColor(192,192,192);
		$pdf->Cell(8,10,$no,1,0,'C',$warna);
		$pdf->Cell(25,10,$row->npm_mahasiswa,1,0,'C',$warna);
		$pdf->Cell(60,10,$mhs,1,0,'L',$warna);
		$pdf->Cell(15,10,$row->jumlahabsen.' ('.number_format($row->persentaseabsen).'%)',1,0,'L',$warna);

		$r=$no%2;
		if ($r == 0) {
			$pdf->Cell(34,10,'',1,0,'L',$warna);
			$pdf->Cell(33,10,$no,1,0,'L',$warna);

		} elseif ($r == 1) {
			$pdf->Cell(34,10,$no,1,0,'L',$warna);
			$pdf->Cell(33,10,'',1,0,'L',$warna);

		}

		$pdf->Cell(25,10,'',1,1,'C',$warna);

	} elseif($tipe == 2){  
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$row->npm_mahasiswa,'NIMHSMSMHS','ASC')->row()->NMMHSMSMHS;

		$pdf->SetFont('Arial','',8);
		$pdf->SetFillColor(192,192,192);
		$pdf->Cell(8,10,$no,1,0,'C',$warna);
		$pdf->Cell(25,10,$row->npm_mahasiswa,1,0,'C',$warna);
		$pdf->Cell(60,10,$mhs,1,0,'L',$warna);
		$pdf->Cell(15,10,$row->jumlahabsen.' ('.number_format($row->persentaseabsen).'%)',1,0,'L',$warna);

		$r=$no%2;
		if ($r == 0) {
			$pdf->Cell(34,10,'',1,0,'L',$warna);
			$pdf->Cell(33,10,$no,1,0,'L',$warna);

		} elseif ($r == 1) {
			$pdf->Cell(34,10,$no,1,0,'L',$warna);
			$pdf->Cell(33,10,'',1,0,'L',$warna);

		}
		
		$pdf->Cell(25,10,'',1,1,'C',$warna);
	}
	
	
	if ($noo == 15) {
		$noo = 0;
		$pdf->AliasNbPages();

		$pdf->AddPage();


		$pdf->SetMargins(3, 5 ,0);

		$pdf->SetFont('Arial','',22);


		$pdf->Ln(0);

		$pdf->Cell(200,5,strtoupper($garis->prodi),0,0,'L');

		$pdf->Ln(6);

		$pdf->SetFont('Arial','',12); 

		$pdf->Cell(200,5,''.strtoupper($garis->fakultas).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');

		$pdf->Ln(5);



		$pdf->SetFont('Arial','',6);

		$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

		$pdf->Ln(2);



		$pdf->SetFont('Arial','',6);

		$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

		$pdf->Ln(4);

		$pdf->Cell(290,0,'',1,0,'C');



		$pdf->ln(1);

		$pdf->SetFont('Arial','',9);

		$pdf->Cell(20,5,'KODE MK',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(50,5,$line->kd_matakuliah,0,0,'L');

		$pdf->Cell(15,5,'Smtr/Kelas',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(40,5,$this->session->userdata('semester').'/'.$line->kelas,0,0,'L');
		//$pdf->Cell(40,5,$line->semester_matakuliah.'/'.$line->kelas,0,0,'L');

		$pdf->Cell(25,5,'NAMA DOSEN',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->MultiCell(50, 5, $line->nama, 0,'L');

		//$x = 50;

		//$pdf->ln();

		$pdf->SetFont('Arial','',9);

		$pdf->Cell(20,5,'NAMA MK',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$current_y = $pdf->GetY();
		$current_x = $pdf->GetX();

		$cell_width = 50;
		$pdf->MultiCell($cell_width, 5, $line->nama_matakuliah, 0,'L',false);

		$x = 50;
		$pdf->SetXY($current_x + $x, $current_y);

		$pdf->Cell(15,5,'SKS',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(40,5,$line->sks_matakuliah,0,0,'L');

		$pdf->Cell(25,5,'NID',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(50,5,$line->nid,0,1,'L');

		$pdf->ln();

		$pdf->SetFont('Arial','',8);

		$pdf->Cell(25,5,'RUANG/WAKTU',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(45,5,'',0,'L');

		$pdf->Cell(15,5,'TANGGAL',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(40,5,'',0,0,'L');

		$pdf->Cell(25,5,'KAMPUS',0,0,'L');

		$pdf->Cell(2,5,':',0,0,'L');

		$pdf->Cell(50,5,'Bekasi/Jakarta',0,0,'L');

		$pdf->ln(10);

		$pdf->SetLeftMargin(5);

		$pdf->SetFont('Arial','',12);

		if ($tipe == 2) {
			$pdf->Cell(200,8,'DAFTAR HADIR PESERTA UJIAN TENGAH SEMESTER',1,0,'C');
		} else {
			$pdf->Cell(200,8,'DAFTAR HADIR PESERTA UJIAN AKHIR SEMESTER',1,0,'C');
		}

		$pdf->ln(8);
		$pdf->SetFont('Arial','',11);

		$pdf->Cell(8,15,'NO','L,T,R,B',0,'C');

		$pdf->Cell(25,15,'NPM','L,T,R,B',0,'C');

		$pdf->Cell(60,15,'NAMA','L,T,R,B',0,'C');

		$pdf->Cell(15,15,'ABSEN','L,T,R,B',0,'C');

		$pdf->Cell(67,15,'KEHADIRAN','L,T,R,B',0,'C');

		$pdf->Cell(25,15,'NILAI','L,T,R,B',1,'C');
	}
$noo++;
$no++; 

} 

$pdf->Output('ABSEN_'.str_replace(' ', '_', $line->kelas).'_'.str_replace(' ', '_', $line->nama_matakuliah).'.PDF','I');

?>