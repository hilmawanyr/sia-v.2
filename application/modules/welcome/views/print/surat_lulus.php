<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->image('http://172.16.1.5:801/assets/albino.png',40,34,15);



$pdf->SetMargins(5, 5 ,0);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(80,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');

$pdf->Cell(90,5,'BIRO ADMINISTRASI AKADEMIK',0,1,'C');

$pdf->Ln(1);
$pdf->Cell(5,0,'',0,0,'L');
$pdf->Cell(84,0,'',1,1,'');

$pdf->ln(8);

$pdf->SetFont('Times','BU',14);
$pdf->Ln(8);
$pdf->Cell(5,0,'',0,0,'L');
$pdf->Cell(195,5,'SURAT KETERANGAN',0,1,'C');

$pdf->SetFont('Times','',12);
$pdf->Cell(5,0,'',0,0,'L');

switch (date('m')) {
        case '01':
            $bul = "I";
            break;
        case '02':
            $bul = "II";
            break;
        case '03':
            $bul = "III";
            break;
        case '04':
            $bul = "IV";
            break;
        case '05':
            $bul = "V";
            break;
        case '06':
            $bul = "VI";
            break;
        case '07':
            $bul = "VII";
            break;
        case '08':
            $bul = "VIII";
            break;
        case '09':
            $bul = "IX";
            break;
        case '10':
            $bul = "X";
            break;
        case '11':
            $bul = "XI";
            break;
        case '12':
            $bul = "XII";
            break;
    }

$pdf->Cell(195,5,'Nomor :      /'.$bul.'/'.date('Y').'/BAA-UBJ',0,1,'C');


$pdf->ln(10);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'Universitas Bhayangkara Jakarta Raya dengan ini menerangkan :',0,0,1);

$pdf->ln(10);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(50,5,'Nama ',0,0,1);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(135,5,$wisudawan->NMMHSMSMHS,0,0,1);

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(50,5,'NPM ',0,0,1);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(135,5,$wisudawan->NIMHSMSMHS,0,0,1);

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(50,5,'Fakultas ',0,0,1);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(135,5,$wisudawan->fakultas,0,0,1);

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(50,5,'Program Studi ',0,0,1);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(135,5,$wisudawan->prodi,0,0,1);


$pdf->ln(10);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'Bahwa nama tersebut diatas telah menyelesaikan semua kewajiban dan persyaratan untuk pengambilan',0,1,'L');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'ijazah, antara lain :',0,0,'L');

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(75,5,'a.  Menyerahkan skripsi/tesis dalam bentuk',0,0,'L');
$pdf->SetFont('Times','I',12);
$pdf->Cell(14,5,'soft file',0,0,'L');
$pdf->SetFont('Times','',12);
$pdf->Cell(8,5,'dan',0,0,'L');
$pdf->SetFont('Times','I',12);
$pdf->Cell(18,5,'hard copy',0,0,'L');
$pdf->SetFont('Times','',12);
$pdf->Cell(74,5,'serta sumbangan buku untuk ',0,1,'L');
$pdf->Cell(20,5,'',0,0);
$pdf->Cell(74,5,'fakultas ',0,0,'L');

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(185,5,'c.  Bebas Pinjaman buku dan sumbangan buku di perpustakaan',0,0,'L');

$pdf->ln(7);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(185,5,'b.  Melunasi seluruh biaya kuliah, skripsi/tesis dan Wisuda',0,0,'L');

$pdf->ln(7);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'Untuk  itu  mahasiswa  yang bersangkutan  dapat mengambil  ijazah, transkip,  dan legalisirnya  beserta',0,1,'L');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'SKPI.',0,1,'L');

$pdf->ln(7);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(195,5,'Demikian surat keterangan ini dikeluarkan untuk dapat dipergunakan sebagaimana mestinya.',0,1,'L');




    $pdf->ln(20);

    $pdf->SetFont('Times','',11);
    $pdf->Cell(140,5,'',0,0,'C');

    switch (date('m')) {
        case '01':
            $bln = "Januari";
            break;
        case '02':
            $bln = "Fabruari";
            break;
        case '03':
            $bln = "Maret";
            break;
        case '04':
            $bln = "April";
            break;
        case '05':
            $bln = "Mei";
            break;
        case '06':
            $bln = "Juni";
            break;
        case '07':
            $bln = "Juli";
            break;
        case '08':
            $bln = "Agustus";
            break;
        case '09':
            $bln = "September";
            break;
        case '10':
            $bln = "Oktober";
            break;
        case '11':
            $bln = "November";
            break;
        case '12':
            $bln = "Desember";
            break;
    }
    

    $pdf->Cell(30,5,'JAKARTA , '.date('d').' '.$bln.' '.date('Y'),0,0,'C');
    $pdf->ln();
    $pdf->Cell(140,5,'',0,0,'C');

    $pdf->Cell(30,5,'KEPALA BIRO ADMINISTRASI AKADEMIK',0,0,'C');
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->ln(25);


    $pdf->SetFont('Times','U',11); 
    $pdf->Cell(140,5,'',0,0,'C');

    $pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
    $pdf->ln(3);



    $pdf->image('http://172.16.1.5:801/assets/ttd_baa.png',($x-35),$y+5,30);
    $pdf->Output();


ob_end_flush();
?>

