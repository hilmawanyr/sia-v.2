



<?php



$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',22); 



$pdf->image('http://172.16.1.5:801/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'KARTU RENCANA STUDI MAHASISWA',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'C');



$q=$this->db->query('SELECT a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`TAHUNMSMHS`,a.`SMAWLMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,d.`kd_krs`,d.`kd_matakuliah`,e.`id_pembimbing`,f.`nama`,f.`nid` 

					FROM tbl_mahasiswa a

					LEFT JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

					LEFT JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

					LEFT JOIN tbl_krs d ON d.`npm_mahasiswa` = a.`NIMHSMSMHS`

					LEFT JOIN tbl_verifikasi_krs e ON e.`kd_krs` = d.`kd_krs`

					LEFT JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`

					WHERE a.`NIMHSMSMHS` = '.$npm.'')->row();



$mk_krs=$this->db->query('SELECT DISTINCT (d.`kd_matakuliah`), a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,d.`kd_krs`,d.`kd_matakuliah`,e.`id_pembimbing`,f.`nama`,f.`nid` 

					FROM tbl_mahasiswa a

					LEFT JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

					LEFT JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

					LEFT JOIN tbl_krs d ON d.`npm_mahasiswa` = a.`NIMHSMSMHS`

					LEFT JOIN tbl_verifikasi_krs e ON e.`kd_krs` = d.`kd_krs`

					LEFT JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`

					WHERE a.`NIMHSMSMHS` = '.$npm.'')->result();



$smtr = $this->app_model->get_semester($q->SMAWLMSMHS);



$ipk= $this->db->query('SELECT * FROM tbl_transaksi_nilai a

						LEFT JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`

						WHERE a.`NIMHSTRLNM` = "'.$npm.'"')->result();

// $nilai = 0;

// $nilai2 = 0;

// $nilai3 = 0;

// $jml = count($ipk);

// foreach ($ipk as $key => $value) {

// $nilai = $ipk->BOBOTTRLNM * $ipk->sks_matakuliah;

// $nilai2 = $nilai2 + $nilai;

// }



// $nilai3=$nilai2/$jml;



$pdf->ln(5);

$pdf->SetFont('Arial','B',10); 

$pdf->Cell(1,5,'',0,0,'C');

$pdf->Cell(60,5,'NO KRS : '.$q->kd_krs.'',1,0);

$pdf->Cell(10,5,'',0,0,'C');



$pdf->ln(6);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'Fakultas',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->fakultas,0,0,'L');

$pdf->Cell(40,5,'Semester',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$smtr,0,0,'L');



$pdf->ln(3);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Program Studi',0,0);

$pdf->Cell(10,10,':',0,0,'C');

$pdf->Cell(60,10,$q->prodi,0,0,'L');

$pdf->Cell(40,10,'Tahun Akademik',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(50,10,substr($ta,0,4).'/'.$gg,0,0,'L');



$pdf->ln(10);

$pdf->SetFont('Arial','B',10); 

$pdf->Cell(40,10,'Identitas Mahasiswa',0,0);

$pdf->Cell(10,10,'',0,0,'C');

$pdf->Cell(60,10,'',0,0,'C');

$pdf->Cell(40,10,'Identitas Dosen PA',0,0);

$pdf->Cell(10,10,'',0,0,'L');

$pdf->Cell(60,10,'',0,0,'L');



$pdf->ln(5);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Nama Mahasiswa',0,0);

$pdf->Cell(10,10,':',0,0,'C');

$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

$pdf->Cell(40,10,'Nama Dosen PA',0,0);

$pdf->Cell(5,5,':',0,0,'L');

//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
$pdf->MultiCell(50, 2.5, $q->nama, 0,'L');


$pdf->ln(7);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'NPM',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(40,5,'NIDN',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,2.5,$q->nid,0,0,'L');



$pdf->ln(5);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->SetFont('Arial','',12);

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,1,'L');



// $pdf->ln(5);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'IPS/IPK',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,'3,24',0,0,'C');

// $pdf->SetFont('Arial','B',10);

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(60,5,'',0,0,'L');



// $pdf->ln(5);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'Jumlah SKS Lulus',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,'7',0,0,'C');

// $pdf->SetFont('Arial','B',10);

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(60,5,'',0,1,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(20);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(20,8,'NO',1,0,'C');

$pdf->Cell(50,8,'KODE MATAKULIAH',1,0,'C');

$pdf->Cell(80,8,'NAMA MATAKULIAH',1,0,'C');

$pdf->Cell(20,8,'SKS',1,1,'C');



$no=1;

$total_sks=0;

foreach ($mk_krs as $isi) {

	$pdf->Cell(20,8,$no,1,0,'C');

	$pdf->Cell(50,8,$isi->kd_matakuliah,1,0,'C');



	$mk = $this->db->query('SELECT * from tbl_matakuliah where kd_matakuliah = "'.$isi->kd_matakuliah.'"')->row();



	//$pdf->Cell(80,8,$mk->nama_matakuliah,1,0,'C');
	$pdf->MultiCell(80, 5, $mk->nama_matakuliah, 0,'L');

	$pdf->Cell(20,8,$mk->sks_matakuliah,1,1,'C');



	$total_sks = $total_sks+$mk->sks_matakuliah;

$no++;

}







$pdf->Cell(150,8,'Total SKS',1,0,'C');

$pdf->Cell(20,8,$total_sks,1,1,'C');



$pdf->ln(18);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');



$pdf->ln(8);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(20,5,'',0,0,'C');

$pdf->Cell(30,5,'Dosen PA',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(80,5,'Mahasiswa',0,0,'C');

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,0,'C');



$pdf->ln(35);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(20,5,'',0,0,'C');

$pdf->Cell(30,5,$q->nama,0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(80,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(30,5,$q->NMMHSMSMHS,0,0,'C');

$pdf->Cell(10,5,'',0,0,'L');

$pdf->Cell(60,5,'',0,0,'L');



$pdf->ln(5);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(15,5,'',0,0,'C');

$pdf->Cell(30,5,$q->nid,0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(80,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(30,5,$q->NIMHSMSMHS,0,0,'C');

$pdf->Cell(10,5,'',0,0,'L');

$pdf->Cell(60,5,'',0,0,'L');





$pdf->Output('KRS_'.$q->NIMHSMSMHS.'/'.$q->NMMHSMSMHS.'.PDF','I');



?>

