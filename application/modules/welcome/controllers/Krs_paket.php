<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krs_paket extends CI_Controller {

	function index()
	{
		//exit();
		$id_pembimbing = '051503021';
		$npm_mhs = array('201510515055');
		foreach ($npm_mhs as $value) {
			$data['npm_mahasiswa'] = $value;
			$kodeawal = $data['npm_mahasiswa'].'20161';
			$hitung = strlen($kodeawal);
			$jumlah = 0;
			for ($i=0; $i <= $hitung; $i++) {
				$char = substr($kodeawal,$i,1);
				$jumlah = $jumlah + $char;
			}
			$mod = $jumlah%10;
			$kodebaru = $data['npm_mahasiswa'].'20161'.$mod;
			$data['semester_krs'] = 1;
			$data['kd_krs'] = $kodebaru;
			$kd_matkuliah = array('PSI-2301','PSI-2307','PSI-2310','PSI-2308');
			foreach ($kd_matkuliah as $key => $n) {
				$data['kd_matakuliah'] = $n;
				//echo $data['kd_matakuliah'].'<br>';
				$this->app_model->insertdata('tbl_krs', $data);
			}
			echo $kodebaru;
			//exit();									
			
			
			$datas['id_pembimbing'] = $id_pembimbing;
			$datas['kd_krs'] = $kodebaru;
			$datas['keterangan_krs'] = 'Selamat Datang DI Ubhara Jaya , Semangat Belajar';
			$datas['tgl_bimbingan'] = date('Y-m-d h:i:s');
			$datas['key'] = $this->generateRandomString();
			$datas['tahunajaran'] = '20171';
			$datas['npm_mahasiswa'] = $data['npm_mahasiswa'];
			$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
			$datas['kd_jurusan'] = $jurusan->KDPSTMSMHS;
			$datas['jumlah_sks'] = '';
			$kodver = $kodebaru.$datas['jumlah_sks'].$datas['key'];
			$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);
			
			//qr code
			$this->load->library('ciqrcode');
			$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
			$this->ciqrcode->generate($params);

			$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
			$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
			$datas['slug_url'] = $slug;
			$this->app_model->insertdata('tbl_verifikasi_krs', $datas);	
		}
	}

	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function getkrsall()
	{
		error_reporting(0);
		//$this->load->library('Cfpdf');
		//echo $tahunajaran.' - '.$pembimbing.' - '.$tahun.' - '.$prodi;
		$data['mahasiswa'] = $this->db->query("select distinct npm_mahasiswa,kd_krs from tbl_verifikasi_krs where npm_mahasiswa like '2009%' and kd_krs LIKE CONCAT(npm_mahasiswa,'20151%') and kd_jurusan = '73201' and (kd_krs IS NOT NULL OR kd_krs != 0) ORDER BY id_pembimbing ASC")->result();
		//var_dump($data);exit();
		$this->load->view('welcome/print/krs_pdf_all',$data);
	}

}

/* End of file Krs_paket.php */
/* Location: ./application/modules/welcome/controllers/Krs_paket.php */