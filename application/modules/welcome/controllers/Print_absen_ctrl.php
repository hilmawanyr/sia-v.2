<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Print_absen_ctrl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
	}

	public function index()
	{
		$this->load->view('print/print_absen');
	}

}

/* End of file print_absen_ctrl.php */
/* Location: ./application/controllers/print_absen_ctrl.php */