<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PDClient_v2 extends CI_Controller {

	// error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE ^ E_DEPRECATED);

	protected $url;

	public function __construct()
	{
		parent::__construct();
		// $sessLogin = $this->session->userdata('sess_login');
		// if (!$sessLogin) {
		// 	redirect(base_url('auth/logout'),'refresh');
		// }
		$this->url = 'http://172.16.0.58:8082/ws/live.php?wsdl';
	}

	function runWS($data, $type='json') 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = [];

		if ($type == 'xml') {
			$headers[] = 'Content-Type: application/xml';
		} else {
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			if ($data) {
				if ($type == 'xml') {
					/* contoh xml:
					<?xml
					version="1.0"?><data><act>GetToken</act><username>agus</username><password>abcdef</password>
					</data>
					*/
					$data = stringXML($data);

				} else {
					/* contoh json:
					{"act":"GetToken","username":"agus","password":"abcdef"}
					*/
					$data = json_encode($data);
				}
				
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			}
		
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);
		}
		return $result;
	}

	function stringXML($data) 
	{
		$xml = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
		array_to_xml($data, $xml);
		return $xml->asXML();
	}

	function array_to_xml( $data, &$xml_data ) {
		foreach( $data as $key => $value ) {
			if( is_array($value) ) {
				$subnode = $xml_data->addChild($key);
				array_to_xml($value, $subnode);

			} else {
				//$xml_data->addChild("$key",htmlspecialchars("$value"));
				$xml_data->addChild("$key",$value);
			}
		}
	}

	function run()
	{
		$this->load->library("Nusoap_lib");
        $client = new nusoap_client($url, true);
        $proxy  = $client->getProxy();
        $token = $proxy->GetToken(userfeeder, passwordfeeder);

		$act 	= $_POST['act'];
		$filter = $_POST['filter'];
		$limit	= $_POST['limit'];
		$offset = $_POST['offset'];

		$parseData = [
			'act'		=> $act,
			'token'		=> $token,
			'filter' 	=> $filter,
			'limit'		=> $limit,
			'offset'	=> $offset
		];

		$result = $this->runWS($parseData);

		echo $result;
	}

}

/* End of file PDClient-v2.php */
/* Location: .//tmp/fz3temp-2/PDClient-v2.php */