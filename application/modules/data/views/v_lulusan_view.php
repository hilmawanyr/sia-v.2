<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?php echo base_url();?>form/penugasandosen/load_detail/'+idk);
    }

function status(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status/'+idk);
    }

function status2(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status2/'+idk);
    }

</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Lulusan</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
            <?php $sess = $this->session->userdata('sess_login');
            if ($sess['id_user_group'] == 10) { ?>
             	<a href="<?php echo base_url();?>data/lulusan/form" class="btn btn-primary" ><i class="btn-icon-only icon-plus"> </i> Tambah Data</a>
                <hr>
            <?php } ?>
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Nomor Ijazah</th>
								<th>Judul Skripsi</th>
								<th>IPK</th>
								<?php $sess = $this->session->userdata('sess_login');
            						if ($sess['id_user_group'] == 10) { ?>
	                            <th class="td-actions">Aksi</th>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($rows as $row){?>
	                        <tr>
	                        	<td><?php echo $row->npm_mahasiswa ?></td>
	                            <td><?php echo $row->NMMHSMSMHS ?></td>
	                            <td><?php echo $row->no_ijazah ?></td>
								<td><?php echo $row->jdl_skripsi ?></td>
								<td><?php echo $row->ipk ?></td>
								<?php $sess = $this->session->userdata('sess_login');
            					if ($sess['id_user_group'] == 10) { ?>
	                        	<td class="td-actions">
									<!--a class="btn btn-small btn-success" href="#"><i class="btn-icon-only icon-ok"> </i></a-->
									<?php if ($row->flag_feeder == 2){ ?>
										<button type="button" class="btn btn-warning btn-small edit"><i class="btn-icon-only icon-check"></i></button>	
									<?php }else{ ?>
										<a href="<?php echo base_url();?>data/lulusan/edit/<?php echo $row->id_lulusan; ?>"><button type="button" class="btn btn-primary btn-small edit"><i class="btn-icon-only icon-pencil"></i></button></a>
									<?php } ?>
									
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" 
									href="<?php echo base_url();?>data/lulusan/delete_data/<?php echo $row->id_lulusan;?>"><i class="btn-icon-only icon-remove"> </i></a>
								</td>
								<?php } ?>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>