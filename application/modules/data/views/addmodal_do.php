<script type="text/javascript">
  jQuery(document).ready(function($) {
      $('input[name^=nim]').autocomplete({

          source: '<?php echo base_url('data/dropout/load_mhs_autocomplete');?>',

          minLength: 1,

          select: function (evt, ui) {

              this.form.nim.value = ui.item.value;
          }
      });
  });
</script>

<form id="" action="<?= base_url('data/dropout/saveFromEdit') ?>" method="post" >
                
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Mahasiswa (No. SKEP <?= $get->skep ?>)</h4>
    </div>

    <div class="modal-body">

        <input type="hidden" name="skep" value="<?= $get->skep; ?>" />
        <input type="hidden" name="keyskep" value="<?= $get->key_skep; ?>" />
        <input type="hidden" name="tgl" value="<?= $get->tgl_skep; ?>" />
        <input type="hidden" name="thajar" value="<?= $get->tahunajaran; ?>" />

        <div class="control-group">
            <label class="control-label">NPM</label>
            <div class="controls">
                <input class="form-control span5" name="nim">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Alasan</label>
            <div class="controls">
                <select class="form-control span5" name="alasan">
                    <option disabled="" selected="">--Alasan--</option> 
                    <option value="1">Mengundurkan diri</option>
                    <option value="2">Dikeluarkan</option>
                    <option value="3">Wafat</option>
                </select>
            </div>
        </div>

    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

</form>