<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Alumni</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Angkatan</th>
								<th>Prodi</th>
								<th>Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($mahasiswa as $row){?>
	                        <tr>
	                        	<td><?php echo $row->NIMHSMSMHS ?></td>
	                            <td><?php echo $row->NMMHSMSMHS ?></td>
	                            <td><?php echo $row->TAHUNMSMHS ?></td>
								<td><?php echo get_jur($row->KDPSTMSMHS) ?></td>
								<td>
									<a class="btn btn-sm btn-info" href="<?php echo base_url();?>data/alumni/cetak_kartu/<?php echo $row->NIMHSMSMHS ?>" title="Cetak Kartu Alumni"><i class="icon-print"></i></a>
									<a class="btn btn-warning" onclick="edit(<?php echo $row->NIMHSMSMHS ?>)" href="#editModal" data-toggle="modal" title="Isi Biodata Alumni"><i class=" icon-pencil"></i></a>
								</td>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ubah">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
function edit(id) {
        $("#ubah").load('<?php echo base_url()?>data/alumni/isi_bio/'+id);
    }
</script>