<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Aktifitas Upload Nilai Prodi <?php echo $pengguna->prodi; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>Nama File</th>
                                    <th>Matakuliah</th>
                                    <th>Kelas</th>
                                    <th>Dosen</th>
                                    <th>Waktu</th>
                                    <th width="40">IP/Mac Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($dt as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->nama_file; ?></td>
                                    <td><?php echo $row->nama_matakuliah; ?></td>
                                    <td><?php echo $row->kelas; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->waktu; ?></td>
                                    <td><?php echo $row->ip_addr; ?>/<?php echo $row->mac_addr; ?></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>