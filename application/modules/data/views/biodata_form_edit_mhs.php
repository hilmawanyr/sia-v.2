<?php //var_dump($bio);die(); ?>

<link href="<?php echo base_url();?>assets/css/plugins/bootstrap.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css">

<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<?php // var_dump($bio);die(); ?>

<script type="text/javascript">

$(document).ready(function() {

	$('#alamat_ortu').hide();

$('#tanggal_lahir').datepicker({dateFormat: 'yy-mm-dd',

								yearRange: "1945:2015",

								changeMonth: true,

      							changeYear: true

      							});

$('#tgl_bayar').datepicker({dateFormat: 'yy-mm-dd'});

	

});

</script>



<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Form Biodata</h3>

					</div> <!-- /widget-header -->

						<div class="widget-content">

							<ul class="nav nav-tabs">

							</ul>

							<br>

							<form id="edit-profile" class="form-horizontal" method="POST" action="<?php echo base_url();?>data/biodata/save_data">

								<div class="tab-content ">

									<div class="tab-pane active" id="formcontrols">

										

									<fieldset>

										<h3 style="text-align:center" >IDENTITAS DIRI</h3>

										<br>
										

										<div class="control-group">											

											<label class="control-label" for="npm">NPM</label>

											<div class="controls">

												<input type="text" class="span6 " name="npm" id="npm" value="<?php echo $bio->nomor_pokok; ?>" readonly>

												<p class="help-block">Bila NPM tidak sesuai dengan yang tertera di KTM dapat menghubungi BAA</p>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->												



										

										

										<div class="control-group">											

											<label class="control-label" for="nama">Nama Lengkap</label>

											<div class="controls">

												<input type="text" class="span6" placeholder="Isi dengan Nama Lengkap" id="nama" name="nama" value="<?php echo $bio->nama;  ?>" readonly>

												<p class="help-block">Bila nama tidak sesuai dapat menghubungi BAA dengan membawa fotocopy ijazah</p>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group" id="jekel">

					                        <label class="control-label">Jenis kelamin</label>

					                        <div class="controls">

					                            <input type="radio" name="jekel" value="1" <?php echo ($bio->jenis_kelamin==1)?'checked':'' ?>/> Laki-Laki  &nbsp;&nbsp;

					                            <input type="radio" name="jekel" value="2" <?php echo ($bio->jenis_kelamin==2)?'checked':'' ?> /> Perempuan  

					                        </div>

					                    </div>

										<div class="control-group">											

											<label class="control-label" for="tempat_lahir">Tempat/Tanggal Lahir</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="test" id="danu" name="danu" value="<?php echo $bio->tempat_lahir ?>" > &nbsp;

												<input type="text" class="span2" placeholder="Tanggal Lahir" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $bio->tanggal_lahir ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" >Telepon</label>

											<div class="controls">

												<input type="text" class="span3" id="tlp" placeholder="No Telepon" name="tlp" value="<?php echo $bio->telepon; ?>" >&nbsp;&nbsp; HP 

												<input type="text" class="span3" id="hp" placeholder="No HP" name="hp" value="<?php echo $bio->handphone; ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<?php 
										if (isset($bio->provinsi) && $bio->provinsi != '' && $bio->provinsi != NULL) {
											
										$prov=$this->db->query('select * from tbl_provinsi where provinsi_id = '.$bio->provinsi.'')->row(); 
										$kota=$this->db->query('select * from tbl_kota where kota_id = '.$bio->kota.'')->row(); 
										$kecam=$this->db->query('select * from tbl_kecamatan where kecam_id = '.$bio->kecamatan.'')->row(); 
										}
										?>

										<script>
						                  $(document).ready(function(){
						                    $('#provinsi').change(function(){
						                      $.post('<?php echo base_url()?>data/biodata/get_kota/'+$(this).val(),{},function(get){
						                        $('#kota').html(get);
						                      });
						                    });
						                  });
						                </script>

						                <div class="control-group">
						                  <label class="control-label">Provinsi</label>
						                  <div class="controls">
						                    <select class="form-control span6" name="provinsi" id="provinsi">
						                    	<?php if (isset($bio->provinsi) && $bio->provinsi != '' && $bio->provinsi != NULL) { ?> <option value="<?php echo $prov->provinsi_id;?>"><?php echo $prov->provinsi_nama;?></option> <?php } ?>
						                    	
						                      	<option>--Pilih Provinsi--</option>
						                      	<?php foreach ($provinsi as $row) { ?>
						                      	<option value="<?php echo $row->provinsi_id;?>"><?php echo $row->provinsi_nama;?></option>
						                      <?php } ?>
						                    </select>
						                  </div>
						                </div>

						                <div class="control-group">
						                  <label class="control-label">Kota / Kabupaten</label>
						                  <div class="controls">
						                    <select class="form-control span6" name="kota" id="kota">
						                    	<?php if (isset($bio->provinsi) && $bio->provinsi != '' && $bio->provinsi != NULL) { ?>
						                      	<option value="<?php echo $kota->kota_id;?>"><?php echo $kota->kokab_nama;?></option>
						                      	<?php } ?>
						                    </select>
						                  </div>
						                </div>

						                <script>
						                  $(document).ready(function(){
						                    $('#kota').change(function(){
						                      $.post('<?php echo base_url()?>data/biodata/get_kecamatan/'+$(this).val(),{},function(get){
						                        $('#kecamatan').html(get);
						                      });
						                    });
						                  });
						                </script>

										<div class="control-group">
						                  <label class="control-label">Kecamatan</label>
						                  <div class="controls">
						                    <select class="form-control span6" name="kecamatan" id="kecamatan">
						                    	<?php if (isset($bio->provinsi) && $bio->provinsi != '' && $bio->provinsi != NULL) { ?>
						                      	<option value="<?php echo $kecam->kecam_id;?>"><?php echo $kecam->nama_kecam;?></option>
						                      	<?php } ?>
						                    </select>
						                  </div>
						                </div>

						                <div class="control-group">											

											<label class="control-label" for="pos">Kode Pos</label>

											<div class="controls">

												<input type="text" class="span6" placeholder="Kode Pos" id="pos" name="kode_pos" value="<?php echo $bio->kode_pos; ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group" id="alamat">

											<label class="control-label" for="alamat">Alamat </label>

											<div class="controls">

												
												<textarea class="span6" placeholder="Alamat Tinggal sekarang" id="alamat" name="alamat" rows="4"></textarea>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="email">Email</label>

											<div class="controls">

												<input type="text" class="span6" placeholder="Email" id="email" name="email" value="<?php echo $bio->email; ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

												<br/>

												<h3 style="text-align:center">ORANG TUA</h3>

												<br>

												

		                                        <div class="control-group">											

													<label class="control-label" >Nama</label>

													<div class="controls">

														<input type="text" class="span3" id="ayah" placeholder="Nama Ayah" name="ayah" value="<?php echo $bio->ayah?>" > &nbsp;

														<input type="text" class="span3" id="ibu" placeholder="Nama Ibu" name="ibu" value="<?php echo $bio->ibu?>" >

													</div> <!-- /controls -->				

												</div> <!-- /control-group -->

												<div class="control-group">

													<label class="control-label" >Telepon</label>

													<div class="controls">

														<input type="text" class="span3" id="tlp" placeholder="No Telepon" name="tlp" value="<?php echo $bio->telepon; ?>" >
														
													</div> <!-- /controls -->					

												</div> <!-- /control-group -->

		                                        

		                                        <div class="control-group">											

													<label class="control-label">Alamat</label>

													<div class="controls">

			                                            <input type="radio" id="cek_alamat" name="cek_alamat" onclick="sepertiatas();" value="1" /> Sama dengan diatas

								                        <input type="radio" id="cek_alamat" name="cek_alamat" onclick="hilang();" value="2" /> Lain
		                                           
		                                          	</div><!-- /controls -->		

												</div> <!-- /control-group -->

		                                        <div class="control-group" id="alamat_ortu">

													<label class="control-label" for="sekolah_asal">Alamat Orang Tua</label>

													<div class="controls">

														<!-- <input type="text" class="span4" id="sekolah_asal" name="sekolah_asal" > -->

														<textarea class="span6" placeholder="Alamat orang tua" id="alamat_ortu" name="alamat_ortu" rows="4"></textarea>

													</div> <!-- /controls -->				

												</div> <!-- /control-group -->

											</div> <!-- /control-group -->										

										 <br />

									</div>

										<div class="form-actions">

											<!-- <button type="submit" class="btn btn-primary">UNGGAH</button>

											<button type="submit" class="btn btn-primary">CETAK</button>  -->

											<button type="submit" class="btn btn-primary">SIMPAN</button>

										</div> <!-- /form-actions -->

										

										</fieldset>

									</div>

									</div>

								</div>

							</form>

						</div>
					</div>

				</div>

			</div>

		</div>



<script type="text/javascript">

function sepertiatas(){

		$('#alamat_ortu').hide();

	}



	function hilang(){

		$('#alamat_ortu').show();

	}

</script>