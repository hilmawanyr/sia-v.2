<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Pendaftaran Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                        <a class="btn btn-success btn-large" href="<?php echo base_url(); ?>data/mahasiswapmb/print_dt"><i class="btn-icon-only icon-print"></i>Print</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>Nomor ID</th>
                                    <th>Nama</th>
                                    <th>Asal Sekolah</th>
                                    <th>Pilihan</th>
                                    <th width="80">Gelombang</th>
                                    <th width="90">Status</th>
                                    <th>NPM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($yes as $row) { ?>
                                <tr>
                                    <td><?php echo $row->ID_registrasi; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->asal_skl; ?></td>
                                    <td><?php echo $row->prodi; ?></td>
                                    <td><?php echo substr($row->ID_registrasi, 0,1); ?></td>
                                    <td><?php echo $row->status; ?></td>
                                    <td></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </div>
</div>