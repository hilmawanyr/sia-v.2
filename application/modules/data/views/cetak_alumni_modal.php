<script language="javascript">
	function numeric(e, decimal) { 
	var key;
	var keychar;
	 if (window.event) {
		 key = window.event.keyCode;
	 } else
	 if (e) {
		 key = e.which;
	 } else return true;
	
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		return true;
	} else 
	if ((("0123456789").indexOf(keychar) > -1)) {
		return true; 
	} else 
	if (decimal && (keychar == ".")) {
		return true; 
	} else return false; 
	}
</script> 
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Isi Biodata</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>data/alumni/add_biodata/" method="post" enctype="multipart/form-data">
	<div class="modal-body" style="margin-left: -30px;">    
		<div class="control-group" id="">
			<label class="control-label">NPM</label>
			<div class="controls">
				<input type="text" class="span4" name="npm"class="form-control" value="<?php echo $npm ?>"readonly />
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Nama Lengkap</label>
			<div class="controls">
				<input type="text" class="span4" name="nama"class="form-control" value="<?php echo get_nm_mhs($npm) ?>"readonly />
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Nomor Telepon</label>
			<div class="controls">
				<input type="text" class="span4" name="tlp" class="form-control" onkeypress="return numeric(event, false)" value="<?php if($bio_mhs){ echo $bio_mhs->no_hp; }?>" maxlength="16"/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Pekerjaan</label>
			<div class="controls">
				<input type="text" class="span4" name="pekerjaan" class="form-control" value="<?php if($bio){ echo $bio->pekerjaan; }?>" maxlength="100"/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Email</label>
			<div class="controls">
				<input type="text" class="span4" name="email" class="form-control" value="<?php if($bio_mhs){ echo $bio_mhs->email; }?>"/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Mutasi</label>
			<div class="controls">
				<textarea type="text" class="span4" name="mutasi" class="form-control"><?php if($bio){ echo $bio->mutasi; }?></textarea>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Alamat</label>
			<div class="controls">
				<textarea type="text" class="span4" name="alamat" class="form-control"><?php if($bio){ echo $bio->alamat; }?></textarea>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Foto</label>
			<div class="controls">
				<input type="file" class="span4" name="foto" class="form-control" />
				<small>Gunakan file di bawah 800Kb</small>
			</div>
		</div>
	</div>
	<div class="modal-footer">
	<button type="submit" class="btn btn-success">Simpan</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>