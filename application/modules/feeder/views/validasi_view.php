<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Validasi Sinkronisasi SIA - Feeder PDDIKTI <?php echo $ta->tahun_akademik; ?></h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
         <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#banyak" data-toggle="tab">Kelas Perkuliahan</a></li>
              <li><a href="#satuan1" data-toggle="tab">KRS Mahasiswa <?php echo $ta2->kode; ?></a></li>
              <li><a href="#satuan2" data-toggle="tab">KHS/Nilai Mahasiswa <?php echo $ta->kode; ?></a></li>
              <li><a href="#satuan3" data-toggle="tab">Aktifitas Kuliah Mahasiswa <?php echo $ta->kode; ?></a></li>
              <!-- <li><a href="#satuan4" data-toggle="tab">Status Mahasiswa </a></li> -->
              <li><a href="#satuan5" data-toggle="tab">Ajar Dosen <?php echo $ta->kode; ?></a></li>
            </ul>           
            <br>

            <div class="tab-content">
              <div class="tab-pane active" id="banyak">
                <div class="row">
                  <div class="span11">
                    <table id="example2" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th>No</th>
                          <th>Semester</th>
                          <th>Jumlah Kelas</th>
                          <th width="80">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td>No</td>
                          <td>Semester</td>
                          <td>Jumlah Kelas</td>
                          <td class="td-actions">
                            <a href="#" class="btn btn-primary btn-small" ><i class="btn-icon-only icon-ok"> </i></a>
                            <a href="#" class="btn btn-success btn-small" ><i class="btn-icon-only icon-print"> </i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="satuan1">
                <div class="row">
                  <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th>No</th>
                          <th>Angkatan</th>
                          <th>Jumlah KRS SIA</th>
                          <th>Jumlah KRS Feeder</th>
                          <th width="80">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td>No</td>
                          <td>Angkatan</td>
                          <td>Jumlah</td>
                          <td>Jumlah</td>
                          <td class="td-actions">
                            <a href="#" class="btn btn-primary btn-small" ><i class="btn-icon-only icon-ok"> </i></a>
                            <a href="#" class="btn btn-success btn-small" ><i class="btn-icon-only icon-print"> </i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="satuan2">
                <div class="row">
                  <div class="span11">
                    <table id="example4" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th>No</th>
                          <th>Semester</th>
                          <th>Jumlah Kelas</th>
                          <th width="80">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td>No</td>
                          <td>Semester</td>
                          <td>Jumlah Kelas</td>
                          <td class="td-actions">
                            <a href="#" class="btn btn-primary btn-small" ><i class="btn-icon-only icon-ok"> </i></a>
                            <a href="#" class="btn btn-success btn-small" ><i class="btn-icon-only icon-print"> </i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="satuan3">
                <div class="row">
                  <div class="span11">
                    <table id="example3" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th>No</th>
                          <th>Angkatan</th>
                          <th>Jumlah KRS SIA</th>
                          <th>Jumlah KRS Feeder</th>
                          <th>Jumlah AKM</th>
                          <th>Jumlah Cuti</th>
                          <th>Jumlah Non Aktif</th>
                          <th width="80">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td>No</td>
                          <td>Angkatan</td>
                          <td>Jumlah KRS SIA</td>
                          <td>Jumlah KRS Feeder</td>
                          <td>Jumlah AKM</td>
                          <td>Jumlah Cuti</td>
                          <td>Jumlah Non Aktif</td>
                          <td class="td-actions">
                            <a href="#" class="btn btn-primary btn-small" ><i class="btn-icon-only icon-ok"> </i></a>
                            <a href="#" class="btn btn-success btn-small" ><i class="btn-icon-only icon-print"> </i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- <div class="tab-pane" id="satuan4">
              </div> -->
              <div class="tab-pane" id="satuan5">
                <div class="row">
                  <div class="span11">
                    <table id="example5" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th>No</th>
                          <th>NID</th>
                          <th>NIDN</th>
                          <th>Nama</th>
                          <th>Jumlah SKS</th>
                          <th width="80">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td>No</td>
                          <td>--</td>
                          <td>--</td>
                          <td>--</td>
                          <td>--</td>
                          <td class="td-actions">
                            <a href="#" class="btn btn-primary btn-small" ><i class="btn-icon-only icon-ok"> </i></a>
                            <a href="#" class="btn btn-success btn-small" ><i class="btn-icon-only icon-print"> </i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>