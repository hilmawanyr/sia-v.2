<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infografis extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('info_model');
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Sesi anda telah habis!')</script>";
			redirect(base_url('auth/logout'),'refresh');
		}
	}

	public function index()
	{
		$actyear = date('Y');

		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		/** condition for load query */
		if (in_array(8, $grup) or in_array(9, $grup)) {

			// for student's amount modul
    		$data['jmlmhs'] = $this->info_model->getjumlahmhs($logged['userid']);
			
    	} else {

    		// for student's amount modul
    		$data['jmlmhs'] = $this->info_model->getjumlahmhs(0);

    	}

    	/** condition for title chart*/
    	if (in_array(8, $grup)) { 

    		// for student's amount modul
			$data['textjmlmhs'] = "'Jumlah Mahasiswa UBJ - Prodi ".get_jur($logged['userid'])."'";

		} elseif (in_array(9, $grup)) {

			// for student's amount modul
			$data['textjmlmhs'] = "'Jumlah Mahasiswa UBJ - Fakultas ".get_fak($logged['userid'])."'";

		} else {

			// for student's amount modul
			$data['textjmlmhs'] = "'Jumlah Mahasiswa UBJ per Angkatan'";

		} 

		// load prodi
		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		// if pie chart is enable, uncomment below
		// $data['takd'] = $this->db->query("SELECT * from tbl_tahunakademik where kode <= '".getactyear()."'")->result();

		$data['page'] = "v_reps2";
		$this->load->view('template/template', $data);
	}

	public function index2()
	{
		//akreditas
		$data['akre'] = $this->db->query("SELECT * from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas order by a.kd_fakultas asc")->result();

		/*dosen*/
		$data['getdosen'] = $this->info_model->getdosen();

		/*lulusan*/
		//$data['lulusan'] = $this->info_model->getlulusan(0);

		$data['page'] = "v_reps";
		$this->load->view('template/template', $data);
	}

	function loadPage($rule)
	{
		if ($rule == 'STMHS') {
			redirect(base_url('infografis/stmhs'));
		} elseif ($rule == 'JMLLS') {
			redirect(base_url('infografis/jmlls'));
		}  elseif ($rule == 'IPKGRD') {
			redirect(base_url('infografis/ipkgrd'));
		} elseif ($rule == 'IPKACT') {
			redirect(base_url('infografis/ipkact'));
		} elseif ($rule == 'STSLEC') {
			redirect(base_url('infografis/stslec'));
		} elseif ($rule == 'STSPMB') {
			redirect(base_url('infografis/stspmb'));
		} elseif ($rule == 'AKREDT') {
			redirect(base_url('infografis/akred'));
		}
	}

	function stmhs()
	{
		$actyear = date('Y');

		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {

			// for status mahasiswa
			// active
			$conststsact = "";
			$tahun = 2015; 
			for ($i = $tahun; $i < date('Y'); $i++) { 
        		for ($a = 1; $a < 3; $a++) { 
        			$conststsact = $conststsact.$this->info_model->getstatusaktif($logged['userid'],$i.$a).',';
        		}
        	}
        	$data['mhsact'] = $conststsact;

        	// cuti
        	$constcuti= "";
        	$tahun = 2015; 
        	for ($i=$tahun; $i < date('Y'); $i++) { 
        		for ($a=1; $a < 3; $a++) { 
        			$constcuti = $constcuti.$this->info_model->getstatuscuti($logged['userid'],$i.$a).',';
        		}
        	}
        	$data['mhscut'] = $constcuti;

        	// nonaktif
        	$constnon = "";
        	$tahun = 2015; 
        	for ($i=$tahun; $i < date('Y'); $i++) { 
        		for ($a=1; $a < 3; $a++) { 
        			$constnon = $constnon.$this->info_model->getstatusnonaktif($logged['userid'],$i.$a).',';
        		}
        	}
        	$data['mhsnon'] = $constnon;

		} else {

			// for status mahasiswa
			// active
			$conststsact = "";
			$tahun = 2015; 
			for ($i = $tahun; $i < date('Y'); $i++) { 
        		for ($a = 1; $a < 3; $a++) { 
        			$conststsact = $conststsact.$this->info_model->getstatusaktif(0,$i.$a).',';
        		}
        	}
        	$data['mhsact'] = $conststsact;

        	// cuti
        	$constcuti= "";
        	$tahun = 2015; 
        	for ($i=$tahun; $i < date('Y'); $i++) { 
        		for ($a=1; $a < 3; $a++) { 
        			$constcuti = $constcuti.$this->info_model->getstatuscuti(0,$i.$a).',';
        		}
        	}
        	$data['mhscut'] = $constcuti;

        	// nonaktif
        	$constnon = "";
        	$tahun = 2015; 
        	for ($i=$tahun; $i < date('Y'); $i++) { 
        		for ($a=1; $a < 3; $a++) { 
        			$constnon = $constnon.$this->info_model->getstatusnonaktif(0,$i.$a).',';
        		}
        	}
        	$data['mhsnon'] = $constnon;
		}

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$this->load->view('tab_status_mhs', $data);
	}

	function jmlls()
	{
		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {
			
			// for amount of graduate
    		$data['lulusan'] = $this->info_model->getlulusan($logged['userid']);
    		$data['masas'] = $this->info_model->getmasalulusan($logged['userid']);

    		// for average of graduate
    		$data['data'] = $this->info_model->getmasalulusan($logged['userid']);

		} else {

			// for amount of graduate
    		$data['lulusan'] = $this->info_model->getlulusan(0);
    		$data['masas'] = $this->info_model->getmasalulusan(0);

    		// for average of graduate
    		$data['data'] = $this->info_model->getmasalulusan(0);

		}

		if (in_array(8, $grup)) { 

			// for amount of graduate
			$data['textjmlulus'] = "'Jumlah Lulusan UBJ 5 Tahun Terakhir - Prodi ".get_jur($logged['userid'])."'";

			// for avg of graduate
			$data['textavglulus'] = "'Rata-Rata Masa Studi Lulusan UBJ 5 Tahun Terakhir - Prodi ".get_jur($logged['userid'])."'";

		} elseif (in_array(9, $grup)) {
			
			// for amount of graduate
			$data['textjmlulus'] = "'Jumlah Lulusan UBJ 5 Tahun Terakhir - Prodi ".get_fak($logged['userid'])."'";

			// for avg of graduate
			$data['textavglulus'] = "'Rata-Rata Masa Studi Lulusan UBJ 5 Tahun Terakhir - Prodi ".get_fak($logged['userid'])."'";

		} else {

			// for amount of graduate
			$data['textjmlulus'] = "'Jumlah Lulusan UBJ 5 Tahun Terakhir'";

			// for avg of graduate
			$data['textavglulus'] = "'Rata-Rata Masa Studi Lulusan UBJ 5 Tahun Terakhir'";

		}

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$this->load->view('tab_jml_lulusan', $data);
	}

	function ipkgrd()
	{
		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {

			$actyear = date('Y');
			// for graduate's avg of IPK
			$qw= "";
			for ($i=2010; $i <= $actyear; $i++) {
				$ta = $i-1;
				$qw = $qw.number_format($this->app_model->gotipklulus($i,$logged['userid'],$ta.'2'),2).',';
			}
			$data['ipk_lulusan'] = $qw;
		
		} else {

			$actyear = date('Y');
			// for graduate's avg of IPK
			$qw= "";
			for ($i=2010; $i <= $actyear; $i++) {
				$ta = $i-1;
				$qw = $qw.number_format($this->app_model->gotipklulus($i,0,$ta.'2'),2).',';
			}
			$data['ipk_lulusan'] = $qw;

		}

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$this->load->view('tab_avg_ipklulus', $data);
	}

	function ipkgrd2()
	{
		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		echo "<pre>";
		print_r ($logged);
		echo "</pre>";
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {

			$actyear = date('Y');
			// for graduate's avg of IPK
			$qw= "";
			for ($i=2010; $i <= $actyear; $i++) {
				$ta = $i-1;
				$qw = $qw.number_format($this->app_model->gotipklulus($i,$logged['userid'],$ta.'2'),2).',';
			}
			$data['ipk_lulusan'] = $qw;
		
		} else {

			$actyear = date('Y');
			// for graduate's avg of IPK
			$qw= "";
			for ($i=2010; $i <= $actyear; $i++) {
				$ta = $i-1;
				$qw = $qw.number_format($this->app_model->gotipklulus($i,0,$ta.'2'),2).',';
			}
			$data['ipk_lulusan'] = $qw;

		}

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		echo "<pre>";
		print_r (explode(",", $data['ipk_lulusan']));
		echo "</pre>";
	}

	function ipkact()
	{
		$actyear = date('Y');

		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {

			// for avg active student ipk
			$constavgipk = "";
			for ($i=2014; $i <= $actyear; $i++) { 
	    		for ($n=1; $n < 3; $n++) { 
	    			if ($i.$n == getactyear()) {
	    				# code...
	    			} else {
	    				$constavgipk = $constavgipk.number_format($this->app_model->gotipkon($i.$n,$logged['userid']),2).',';
	    			}
	    		}
	    	}
	    	$data['actstudentipk'] = $constavgipk;

		} else {

			// for avg active student ipk
			$constavgipk = "";
			for ($i=2014; $i <= $actyear; $i++) { 
	    		for ($n=1; $n < 3; $n++) { 
	    			if ($i.$n == getactyear()) {
	    				# code...
	    			} else {
	    				$constavgipk = $constavgipk.number_format($this->app_model->gotipkon($i.$n,0),2).',';
	    			}
	    		}
	    	}
	    	$data['actstudentipk'] = $constavgipk;

		}

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$this->load->view('tab_avg_ipkact', $data);
	}

	function stslec()
	{
		$getdosen = $this->info_model->getdosen();
		$constcat = "";
		foreach ($getdosen as $value) {
        	if ($value->tetap == 1) {
        		$name = "{name: 'Dosen Tetap',";
        	} else {
        		$name = "{name: 'Dosen Tidak Tetap',";
        	}
        	$res = "y: ".$value->jml."},";

        	$constcat = $constcat.$name.$res;
        }
        $data['dosen'] = $constcat;

		$dsn = $this->info_model->getdosenprodi();
		$const = "";	
    	foreach ($dsn as $oz) {
    		$const = $const.$oz->jml.',';
    	}
    	$data['lecture'] = $const;
			
		// for status dosen
    	$constlect = "";
    	foreach ($dsn as $oz) {
    		$constlect = $constlect.$this->info_model->getdosenprodinidn($oz->kd_prodi).',';
    	}
    	$data['lecturenidn'] = $constlect;

    	$this->load->view('tab_lecture', $data);

	}

	function stspmb()
	{
		$actyear = date('Y');

		/** define user group */
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i = 0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		// query
		if (in_array(8, $grup) or in_array(9, $grup)) {

			// for PMB modul
    		$constreg = "";
    		$constes  	= "";
    		$constgrd 	= "";
    		$constre 	= "";

    		for ($i = 2016; $i <= $actyear ; $i ++) { 
    			$reg 		= $this->info_model->getjumlahmabadaftarprodi($i,$logged['userid']);
    			$constreg 	= $constreg.$reg.',';

    			$tes 		= $this->info_model->pesertates($i,$logged['userid']);
    			$constes	= $constes.$tes.',';

    			$grd 		= $this->info_model->getLulusTes($i,$logged['userid']);
    			$constgrd 	= $constgrd.$grd.',';

    			$re 		= $this->info_model->getjumlahmabastatusprodi($i,2,$logged['userid']);
    			$constre 	= $constre.$re.',';
    		}

    		$data['reguser'] = $constreg;
    		$data['tesuser'] = $constes;
    		$data['grduser'] = $constgrd;
    		$data['reregis'] = $constre;

    		// for new student's school category
			$constsmk = "";
			$constsma = "";
			$constma = "";
			$constoth = "";

			for ($i = 2016; $i <= $actyear; $i++) { 
				$smk = $this->info_model->getjumlahmabasklprodi($i,'SMK',$logged['userid']);
				$constsmk = $constsmk.$smk.',';

				$sma = $this->info_model->getjumlahmabasklprodi($i,'SMA',$logged['userid']);
				$constsma = $constma.$sma.',';

				$ma = $this->info_model->getjumlahmabasklprodi($i,'MDA',$logged['userid']);
				$constma = $constma.$ma.',';

				$oth = $this->info_model->getjumlahmabasklprodi($i,'LAIN',$logged['userid']);
				$constoth = $constoth.$oth.',';
			}

			$data['sma'] = $constsma;
			$data['smk'] = $constsmk;
			$data['maa'] = $constma;
			$data['oth'] = $constoth;

		} else {

			// for PMB modul
    		$constreg 	= "";
    		$constes  	= "";
    		$constgrd 	= "";
    		$constre 	= "";

    		for ($i = 2016; $i <= $actyear; $i ++) { 
    			$reg 		= $this->info_model->getjumlahmabadaftar($i,$logged['userid']);
    			$constreg 	= $constreg.$reg.',';

    			$tes = $this->info_model->pesertates($i);
    			$constes = $constes.$tes.',';

    			$grd = $this->info_model->getLulusTes($i);
    			$constgrd = $constgrd.$grd.',';

    			$re = $this->info_model->getjumlahmabastatus($i,2);
    			$constre = $constre.$re.',';
    		}

    		$data['reguser'] = $constreg;
    		$data['tesuser'] = $constes;
    		$data['grduser'] = $constgrd;
    		$data['reregis'] = $constre;

    		// for new student's school category
    		$constsmk = "";
    		$constsma = "";
    		$constma = "";
    		$constoth = "";

    		for ($i = 2016; $i <= $actyear; $i++) { 
    			$smk = $this->info_model->getjumlahmabaskl($i,'SMK');
    			$constsmk = $constsmk.$smk.',';

    			$sma = $this->info_model->getjumlahmabaskl($i,'SMA');
    			$constsma = $constma.$sma.',';

    			$ma = $this->info_model->getjumlahmabaskl($i,'MA');
    			$constma = $constma.$ma.',';

    			$oth = $this->info_model->getjumlahmabaskl($i,'LAIN');
    			$constoth = $constoth.$oth.',';
    		}

    		$data['sma'] = $constsma;
    		$data['smk'] = $constsmk;
    		$data['maa'] = $constma;
    		$data['oth'] = $constoth;

		}

		/** condition for title chart*/
    	if (in_array(8, $grup)) { 

			// for pmb modul
			$data['textpmb'] = 'Jumlah Pendaftar Mahasiswa Baru UBJ - Prodi '.get_jur($logged['userid']).'';

		} elseif (in_array(9, $grup)) {

			// for pmb modul
			$data['textpmb'] = 'Jumlah Pendaftar Mahasiswa Baru UBJ - Fakultas '.get_fak($logged['userid']).'';

		} else {

			// for pmb modul
			$data['textpmb'] = 'Jumlah Pendaftar Mahasiswa Baru UBJ';

		} 

		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$this->load->view('tab_pmb', $data);
	}

	function akred()
	{
		//akreditas
		$data['akre'] = $this->db->query("SELECT * from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas order by a.kd_fakultas asc")->result();

		$this->load->view('tab_akred', $data);
	}

}

/* End of file Report.php */
/* Location: ./application/modules/allreport/controllers/Report.php */