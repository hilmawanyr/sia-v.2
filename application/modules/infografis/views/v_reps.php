<?php
	$logged = $this->session->userdata('sess_login');
	$pecah = explode(',', $logged['id_user_group']);
	$jmlh = count($pecah);
	for ($i=0; $i < $jmlh; $i++) { 
		$grup[] = $pecah[$i];
	}
?>

<script type="text/javascript">
	$(function () {
	    $('#jumlahmhs').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Jumlah Mahasiswa UBJ 10 Tahun Terakhir'
	        },
	        subtitle: {
	            text: 'Source : SIA UBHARAJAYA'
	        },
	        xAxis: {
	            categories: [
	                <?php
	                	if ( (in_array(8, $grup))) {
	                		$data = $this->ig_model->getjumlahmhs($logged['userid']);
	                	} else {
	                		$data = $this->ig_model->getjumlahmhs(0);
	                	}
	                	foreach ($data as $oz) {
	            			echo $oz->TAHUNMSMHS.',';
	            		}
	                ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Jumlah Mahasiswa',
	            data: [
	            	<?php if ( (in_array(8, $grup))) {
	                		$data = $this->ig_model->getjumlahmhs($logged['userid']);
	                	} else {
	                		$data = $this->ig_model->getjumlahmhs(0);
	                	}
	                	foreach ($data as $oz) {
	            		echo $oz->jml.',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
	        }],
	    });
	});
</script>

<!--- PMB -->

<script type="text/javascript">
	$(function () {
	    $('#pmb').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Jumlah Pendaftar Mahasiswa Baru UBJ'
	        },
	        subtitle: {
	            text: 'Source: SIA UBHARAJAYA'
	        },
	        xAxis: {
	            categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
		        name: 'Daftar Mahasiswa Baru',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	if ( (in_array(8, $grup))) {
			        		$daftar = $this->ig_model->getjumlahmabadaftarprodi($i,$logged['userid']); echo $daftar.","; 
			        	} else {
			        		$daftar = $this->ig_model->getjumlahmabadaftar($i); echo $daftar.","; 
			        	}}
		        	?>
		         ]
		    }, {
		        name: 'Lulus Tes PMB',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	if ( (in_array(8, $grup))) {
			        		$daftar = $this->ig_model->getjumlahmabastatusprodi($i,1,$logged['userid']); echo $daftar.","; 
			        	} else {
			        		$daftar = $this->ig_model->getjumlahmabastatus($i,1); echo $daftar.","; 
			        	}}
		        	?>
		         ]
		    }, {
		        name: 'Mahasiswa Baru',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	if ( (in_array(8, $grup))) {
			        		if ($i < 2017) {
			        			$daftar = $this->ig_model->getjumlahmabastatusprodi($i,2,$logged['userid']); echo $daftar.",";
			        		} else {
			        			$daftar = $this->ig_model->getjumlahmabastatusprodi($i,3,$logged['userid']); echo $daftar.",";
			        		}
			        		 
			        	} else {
			        		if ($i < 2017) {
			        			$daftar = $this->ig_model->getjumlahmabastatus($i,2); echo $daftar.",";
			        		} else {
			        			$daftar = $this->ig_model->getjumlahmabastatus($i,2); echo $daftar.",";
			        		}
			        		 
			        	}}
		        	?>
		         ]
		    }],
	    });
	});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#asl_skl').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Kategori Sekolah Asal'
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'SMA',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        	if ( (in_array(8, $grup))) {
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'SMA',$logged['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->ig_model->getjumlahmabaskl($i,'SMA'); echo $daftar.","; 
		        	} }?>
		         ]
		    }, {
		        name: 'SMK',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        	if ( (in_array(8, $grup))) {
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'SMK',$logged['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->ig_model->getjumlahmabaskl($i,'SMK'); echo $daftar.","; 
		        	}}?>
		         ]
		    }, {
		        name: 'MA',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        	if ( (in_array(8, $grup))) {
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'MA',$logged['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->ig_model->getjumlahmabaskl($i,'MA'); echo $daftar.","; 
		        	}}?>
		         ]
		    }, {
		        name: 'LAIN-LAIN',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { if ( (in_array(8, $grup))) {
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'LAIN',$logged['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->ig_model->getjumlahmabaskl($i,'LAIN'); echo $daftar.","; 
		        	}}?>
		         ]
		    }],
		});
	});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#penghasilan').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Penghasilan Orang Tua'
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [
		    <?php for ($i=1; $i < 5; $i++) {
		    	switch ($i) {
		    		case '1':
		    			$hasilan = 'Rp 1,000,000 - 2,000,000';
		    			break;

		    		case '2':
		    			$hasilan = 'Rp 2,100,000 - 4,000,000';
		    			break;

		    		case '3':
		    			$hasilan = 'Rp 4,100,000 - 5,999,000';
		    			break;

		    		case '4':
		    			$hasilan = '>= Rp 6,000,000';
		    			break;
		    	}
		    	echo "{name: '".$hasilan."',";
		    	echo "data: [";
		    	$tahun = date('Y'); for ($a=2016; $a <= $tahun; $a++) {
		    		if ( (in_array(8, $grup))) {
		        		$daftar = $this->ig_model->getjumlahmabapenghasilanprodi($a,$i,$logged['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->ig_model->getjumlahmabapenghasilan($a,$i); echo $daftar.",";
		        	}
		    	}
		    	echo "]},";
		    } ?>
		    ],
		});
	});
</script>

<!--- /PMB -->

<script type="text/javascript">
	$(function () {
	    $('#jml_lulus').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Jumlah Lulusan UBJ 5 Tahun Terakhir'
	        },
	        subtitle: {
	            text: 'Source: SIA UBHARAJAYA'
	        },
	        xAxis: {
	            categories: [
	                <?php 
	                	if ( (in_array(8, $grup))) {
	                		$lulusan = $this->ig_model->getlulusan($logged['userid']);
	                	} else {
	                		$lulusan = $this->ig_model->getlulusan(0);
	                	}
	                	foreach ($lulusan as $oz) {
	            			echo $oz->tahun.',';
	            		} ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Jumlah Lulusan',
	            data: [
	            	<?php 
	            	if ( (in_array(8, $grup))) {
	                		$lulusan = $this->ig_model->getlulusan($logged['userid']);
	                	} else {
	                		$lulusan = $this->ig_model->getlulusan(0);
	                	}
	            	foreach ($lulusan as $oz) {
	            		echo $oz->jml.',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        }]
	    });
	});
</script>

<script type="text/javascript">
	$(function () {
		$('#avgipklulus').highcharts({

		    title: {
		        text: 'Logarithmic axis demo'
		    },

		    xAxis: {
		        tickInterval: 1
		    },

		    yAxis: {
		        type: 'logarithmic',
		        minorTickInterval: 0.1
		    },

		    tooltip: {
		        headerFormat: '<b>{series.name}</b><br />',
		        pointFormat: 'x = {point.x}, y = {point.y}'
		    },

		    series: [{
		        data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
		        pointStart: 1
		    }]
		});
	});
</script>

<script type="text/javascript">
	$(function () {
		$('#avgipkaktif').highcharts({

		    title: {
		        text: 'Logarithmic axis demo'
		    },

		    xAxis: {
		        tickInterval: 1
		    },

		    yAxis: {
		        type: 'logarithmic',
		        minorTickInterval: 0.1
		    },

		    tooltip: {
		        headerFormat: '<b>{series.name}</b><br />',
		        pointFormat: 'x = {point.x}, y = {point.y}'
		    },

		    series: [{
		        data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
		        pointStart: 1
		    }]
		});
	});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#stsmhs').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Jumlah Status Cuti, Non Aktif Mahasiswa UBJ'
		    },

		    subtitle: {
		        text: 'Tahun Akademik 2016/2017 - Ganjil'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
			        <?php 
				        $qw = $this->ig_model->gettahun($q->kode);
				        foreach ($qw as $kue) {
				         	echo "'".trim($kue->tahun_akademik)."',";
				        } 
			        ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Cuti',
		        data: [
		        	<?php 
		        		$dats = $this->ig_model->getStatusMhsCut($q->kode); 
		        		foreach ($dats as $e) {
		        			echo $e->mhs.',';
		        		}
		        	?>
		        ]
		    }, {
		        name: 'Non Aktif',
		        data: [
		        	<?php 
		        		$tahun = 2015;
		        		for ($i=$tahun; $i < date('Y'); $i++) { 
		        			for ($x=1; $x < 3; $x++) { 
		        				if (in_array(8,$grup)) {
			        				$datz = $this->ig_model->getStatusMhsOff2($logged['userid'],$i.$x); echo $datz.',';
			        			} else {
			        				$datz = $this->ig_model->getStatusMhsOff2(0,$i.$x); echo $datz.',';
			        			}
		        			}
		        		}
	        			// foreach ($datz as $val) {
	        			// 	echo $val->juml.',';
	        			// }
		        	?>
		        ]
		    }],
		});

		$('#small').click(function () {
		    chart.setSize(400, 300);
		});

		$('#large').click(function () {
		    chart.setSize(600, 300);
		});
	});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#stsmhson').highcharts({
			colors: ['#55BF3B'],
		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Jumlah Mahasiswa Aktif UBJ'
		    },

		    subtitle: {
		        text: 'per Tahun Akademik 2016/2017 - Ganjil'
		    },

		    xAxis: {
		        categories: [
			        <?php 
				        $qw = $this->ig_model->gettahun($q->kode);
				        foreach ($qw as $kue) {
				         	echo "'".trim($kue->tahun_akademik)."',";
				        } 
			        ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Aktif',
	            data: [
	            	<?php if ( (in_array(8, $grup))) {
	                		$da = $this->ig_model->getStatusMhsOn($logged['userid'],$q->kode);
	                	} else {
	                		$da = $this->ig_model->getStatusMhsOn(0,$q->kode);
	                	}
	                	foreach ($da as $oz) {
	            		echo $oz->jml.',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
	        }],
		});
	});
</script>

<script type="text/javascript">
$(function () {
    $('#dosen').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status Dosen UBJ per 2016/2017'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            <?php foreach ($getdosen as $value) {
            	if ($value->tetap == 1) {
            		echo "{name: 'Dosen Tetap',";
            	} else {
            		echo "{name: 'Dosen Tidak Tetap',";
            	}
            	echo "y: ".$value->jml."},";
            } ?>
            ]
        }]
    });
});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#det_dosen').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Detail Status Dosen UBJ'
		    },

		    subtitle: {
		        text: 'Tahun Akademik 2016/2017'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: ['2015', '2016', '2017', '2018'],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Tetap',
		        data: [1, 4, 3, 10]
		    }, {
		        name: 'Tidak Tetap',
		        data: [6, 4, 2, 11]
		    }],
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-graph"></i>
  				<h3>Service User</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home1">Jumlah Mahasiswa</a></li>
					    <li><a data-toggle="tab" href="#home2">Status Mahasiswa</a></li>
					    <li><a data-toggle="tab" href="#home3">Jumlah Lulusan</a></li>
					    <li><a data-toggle="tab" href="#home4">Rata-rata IPK Lulusan</a></li>
					    <li><a data-toggle="tab" href="#home5">Rata-Rata IPK Mahasiswa Aktif</a></li>
					    <li><a data-toggle="tab" href="#home6">Status Dosen</a></li>
					    <li><a data-toggle="tab" href="#home7">PMB</a></li>
					    <li><a data-toggle="tab" href="#home8">Akreditasi</a></li>
					</ul>
					<div class="tab-content">
					    <div id="home1" class="tab-pane fade in active">
					    	<div id='jumlahmhs' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
					    	<?php 
					    		if ((in_array(8, $grup))) {
					    			# code...
					    		} else { ?>
					    			<hr>
							    	<center>
							    		<h3>Jumlah Mahasiswa Per Prodi</h3>
							    		<small>*pilih fakultas dan tahun akademik pada <i>select box</i></small>
							    	</center>
							    	<br>
							    	<script>
				                        $(document).ready(function(){
				                            $('#fk').change(function(){
				                                $.post('<?php echo base_url()?>infografis/ig02/sess/'+$(this).val(),{},function(get){
				                                    $('#show').html(get);
				                                });
				                            });
				                        }); 
				                    </script>
							    	<div class="pull-right">
				                        <div class="form-group" style="float:left;margin-right">
				                            <select class="form-control" name="prod" id="fk">
				                                <option disabled="" selected="">-- Pilih Prodi --</option>
				                                <?php foreach ($pro as $key) { ?>
				                                <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
				                                <?php } ?>
				                            </select>
				                        </div>
				                    </div>
				                    <br>
				                    <div id="show" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
					    	<?php }
					    		
					    	?>
						</div>
						<div id="home2" class="tab-pane fade in">
							<div id="stsmhs" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>	
							<hr>
							<div id="stsmhson" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>		
						</div>
						<div id="home3" class="tab-pane fade in">
							<div id="jml_lulus" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>				
						</div>
						<div id="home4" class="tab-pane fade in">
							<div id="avgipklulus" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
						</div>
						<div id="home5" class="tab-pane fade in">
							<div id="avgipkaktif" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>						
						</div>
						<div id="home6" class="tab-pane fade in">
							<div id="dosen" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
							<hr>
							<div id="det_dosen" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>			
						</div>
						<div id="home7" class="tab-pane fade in">
							<div id='pmb' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
							<hr>
							<div id='asl_skl' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
							<hr>
							<div id='penghasilan' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
						</div>
						<div id="home8" class="tab-pane fade in">
							<h1>Akreditasi Universitas Bhayangkara Jakarta Raya "B"</h1><br>
							<h3>668/SK/BAN-PT/Akred/PT/VII/2015</h3>
							<hr>
							<table class="table table-bordered table-striped">
								<thead>
							        <tr> 
							        	<th>No</th>
							            <th>Prodi</th>
							            <th>Fakultas</th>
							            <th>Akreditasi</th>
							            <th>No. SK</th>
							            <th>Tahun SK</th>
							            <th>Tanggal Kadaluwarsa</th>
							            <th>Status</th>
							        </tr>
							    </thead>
							    <tbody>
							    	<?php $no = 1; foreach ($akre as $key) { 
							    		if ($key->kd_prodi == $logged['userid']) {
							    			$warna = 'style="background:#FFB200;"';
							    		} else {
							    			$warna = '';
							    		}
							    		
							    		?>
							    		<tr <?php echo $warna; ?>>
								        	<td <?php echo $warna; ?>><?php echo $no; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->prodi; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->fakultas; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->akreditasi; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->sk; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->tahun_akreditasi; ?></td>
								        	<td <?php echo $warna; ?>><?php echo $key->tgl_kadaluarsa; ?></td>
								        	<td <?php echo $warna; ?>>
								        		<?php switch ($key->status_prodi) {
								        			case '1':
								        				echo "Masih Berlaku";
								        				break;
								        			case '2':
								        				echo "Reakreditasi";
								        				break;
								        		} ?>
								        	</td>
								        </tr>
							    	<?php $no++; } ?>
							    </tbody>
							</table>						
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>