<script type="text/javascript">
	$(function () {
		var chart = $('#stsmhs1').highcharts({
			colors: ['#7cb5ec', '#55BF3B','#000000'],
		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Prodi <?= get_jur($stat); ?>'
		    },

		    subtitle: {
		        text: 'Source : SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
		        	<?php $tahun = 2015; for ($i=$tahun; $i < date('Y'); $i++) { 
		        		for ($a=1; $a < 3; $a++) { 
		        			echo $i.$a.',';
		        		}
		        	} ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Aktif',
		        data: [
		        	<?php $tahun = 2015; for ($i=$tahun; $i < date('Y'); $i++) { 
		        		for ($a=1; $a < 3; $a++) { 
		        			$aktif1 = $this->ig_model->getstatusaktif1($stat,$i.$a);
		        			echo $aktif1.',';
		        		}
		        	} ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }, {
		        name: 'Cuti',
		        data: [
		        	<?php $tahun = 2015; for ($i=$tahun; $i < date('Y'); $i++) { 
		        		for ($a=1; $a < 3; $a++) { 
		        			$cuti1 = $this->ig_model->getstatuscuti1($stat,$i.$a);
		        			echo $cuti1.',';
		        		}
		        	} ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }, {
		        name: 'Non Aktif',
		        data: [
		        	<?php $tahun = 2015; for ($i=$tahun; $i < date('Y'); $i++) { 
		        		for ($a=1; $a < 3; $a++) { 
		        			$non1 = $this->ig_model->getstatusnonaktif1($stat,$i.$a);
		        			echo $non1.',';
		        		}
		        	} ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }],
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>
<?php //die($stat); ?>
<div id="stsmhs1" style="min-width: 300px; height: 400px; max-width: 1100px;"></div>
<hr>