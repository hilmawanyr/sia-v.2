<script type="text/javascript">
	$(function () {
		var chart = $('#det_dosen').highcharts({
			colors: ['#7cb5ec', '#55BF3B'],
		    chart: {
		        type: 'column'
		    },

		    title: {
		    	<?php $x = $this->db->query("SELECT tahun_akademik from tbl_tahunakademik where kode = '".$dsn."'")->row()->tahun_akademik; ?>
		        text: 'Detail Status Dosen UBJ Tahun Akademik <?php echo $x; ?>'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
		        	<?php 
	            	$dsn = $this->info_model->getdosenprodi();
	            	foreach ($dsn as $oz) {
	            		echo "'".$oz->prodi."',";
	            	} ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Jumlah Dosen Tetap',
		        data: [
		        <?php 
	                $dsn = $this->info_model->getdosenprodi();	
	            	foreach ($dsn as $oz) {
	            		echo $oz->jml.',';
	            	} ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    },
		    {
		        name: 'Jumlah Dosen NIDN',
		        data: [
		        <?php 
	                $dsn = $this->info_model->getdosenprodi();	
	            	foreach ($dsn as $oz) {
	            		$jmlh = $this->info_model->getdosenprodinidn($oz->kd_prodi);
	            		echo $jmlh.',';
	            	} ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }
		    ],
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='jumlahmhs' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>