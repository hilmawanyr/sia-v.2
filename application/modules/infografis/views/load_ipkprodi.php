<script type="text/javascript">
	$(function () {
		$('#ipklulus1').highcharts({

		    title: {
		    	<?php $x = $this->db->query("SELECT prodi from tbl_jurusan_prodi where kd_prodi = '".$ipk."'")->row()->prodi; ?>
		        text: 'Prodi <?php echo $x; ?>'
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    yAxis: {
		        title: {
		            text: 'IPK'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle'
		    },

		    plotOptions: {
		        series: {
		            pointStart: <?php if ($ipk == '25201' || $ipk == '32201') {
		            	echo "2014";
		            } else {
		            	echo "2010";
		            }
		             ?> 
		        }
		    },

		    series: [{
		        name: 'IPK Rata-rata',
		        data: [
				        <?php $thn = 2016;
		                    $now = date('Y');
		                    if ($ipk == '25201' || $ipk == '32201') {
		                    	for ($i=2014; $i <= $now; $i++) {
			                        $ta = $i-1;
			                        $qw = $this->ig_model->gotipklulus($i,$ipk,$ta.'2'); echo number_format($qw,2).',';
			                    } 
		                    } else {
		                    	for ($i=2010; $i <= $now; $i++) {
			                        $ta = $i-1;
			                        $qw = $this->ig_model->gotipklulus($i,$ipk,$ta.'2'); echo number_format($qw,2).',';
			                    } 
		                    }
		                    
		                    
		                ?>
                    ],
                dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }]
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='ipklulus1' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>