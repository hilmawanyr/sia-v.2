<script type="text/javascript">
	$(function () {
	    $('#loo').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: '<?= get_jur($qry->row()->KDPSTMSMHS); ?>'
	        },
	        subtitle: {
	            text: 'Source : SIA UBHARAJAYA'
	        },
	        xAxis: {
	            categories: [
	                <?php
	                	foreach ($qry->result() as $om) {
	            			echo $om->TAHUNMSMHS.',';
	            		}
	                ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Jumlah Mahasiswa',
	            data: [
	            	<?php
	                	foreach ($qry->result() as $z) {
	            		echo $z->jums.',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
	        }],
	    });
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id="loo" style="min-width: 200px; height: 400px; max-width: 1000px;"></div>