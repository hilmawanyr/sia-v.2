            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Gedung</h4>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $.post('<?php echo base_url()?>organisasi/lokasi/loaddata'+$(this).val(),{},function(get){
                        $("#kd_loks").html(get);
                    });
                });
            </script>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/lokasi/update_gedung" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Gedung</label>
                        <div class="controls">
                            <input type="hidden" name="id_gedung" value="<?php echo $list->id_gedung;?>">
                            <input type="text" class="span4" id="kd_loks" name="gedung" placeholder="Input Gedung" class="form-control" value="<?php echo $list->gedung;?>"required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Lokasi</label>
                        <div class="controls">
                            <select class="form-control" name='loks'>
                                <option>--Pilih Lokasi--</option>
                                <?php foreach ($isako as $row) { ?>
                                    <option value="<?php echo $row->kode_lembaga;?>"><?php echo $row->lembaga;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>