<script type="text/javascript" src="<?php echo base_url();?>assets/jquerymasked/src/jquery.maskedinput.js"></script>
<script type="text/javascript">
    function edit(edr) {
        $("#edit_ruang").load('<?php echo base_url()?>organisasi/ruang/load_ruang/'+edr);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Ruangan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-warning" href="<?php echo base_url();?>organisasi/lokasi/detail_gd/<?php echo $det->id_gedung; ?>" ><i class="icon-chevron-left"></i> Kembali</a>
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode Ruangan</th>
                                <th>Ruang</th>
                                <th>Lantai</th>
                                <th>Kuota</th>
                                <th>Deskripsi</th>
                                <th width="80">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($x as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->kode_ruangan;?></td>
                                <td><?php echo $row->ruangan;?></td>
                                <td><?php echo $row->lantai;?></td>
                                <td><?php echo $row->kuota;?></td>
                                <td><?php echo $row->deskripsi;?></td>
                                <td class="td-actions">
                                    <a onclick="edit(<?php echo $row->id_ruangan;?>)" class="btn btn-primary btn-small" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>organisasi/ruang/del_ruang/<?php echo $row->id_ruangan;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_ruang">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Ruangan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Kode Ruangan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kd_ruang" placeholder="Input Kode Ruangan" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Ruangan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="ruang" placeholder="Input Ruangan" class="form-control" value="" required/>
                            <!-- <input type="hidden" id="gdg" name="building" value="<?php echo $move->id_gedung;?>"/> -->
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <input type="hidden" id="lt" class="span4" name="lants" class="form-control" value="<?php echo $this->uri->segment(4)?>" required/>
                            <input type="text" class="span4" class="form-control" value="<?php echo $lantai->lantai;?>" disabled="disabled"/>
                        </div>
                    </div>
                    <!-- <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <select class="form-control" name='lants'>
                                <option>--Pilih Lantai--</option>
                                <?php foreach ($lantai as $row) { ?>
                                    <option value="<?php echo $row->id_lantai;?>"><?php echo $row->id_lantai;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="control-group" id="">
                        <label class="control-label">Kuota</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kuota" placeholder="Input Kuota" class="form-control" value="" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                            <textarea class="span4" name="deskrip"></textarea>
                        </div>
                    </div>             
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->