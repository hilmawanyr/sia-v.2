<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Karyawan</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>organisasi/karyawan/edit" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -60px;">  
        <div class="control-group" id="">
            <label class="control-label">NIP</label>
            <div class="controls">
                <input type="number" class="span4" name="nip" placeholder="Input NIP" class="form-control" value="<?php echo $kue->nip; ?>" required/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Nama</label>
            <div class="controls">
                <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="<?php echo $kue->nama; ?>" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Unit</label>
            <div class="controls">
                <input type="text" class="span4" name="unit" placeholder="Input Unit" class="form-control" value="<?php echo $kue->unit; ?>" required/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Jabatan</label>
            <div class="controls">
                <input type="text" class="span4" name="jabatan" placeholder="Input Jabatan" class="form-control" value="<?php echo $kue->jabatan; ?>" required/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Status</label>
            <div class="controls">
                <select class="form-control" name='status' required>
                    <option>--Pilih Status--</option>
                    <?php if ($kue->status == '1') { ?>
                        <option value="1" selected="">PHL</option>
                        <option value="2">Tetap</option>
                    <?php } else { ?>
                        <option value="1">PHL</option>
                        <option value="2" selected="">Tetap</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">No. HP</label>
            <div class="controls">
                <input type="number" class="span4" name="hp" placeholder="Input Nomor HP" class="form-control" value="<?php echo $kue->no_hp; ?>" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">No. KTP</label>
            <div class="controls">
                <input type="number" class="span4" name="ktp" placeholder="Input No. KTP" class="form-control" value="<?php echo $kue->no_ktp; ?>" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">E-mail</label>
            <div class="controls">
                <input type="email" class="span4" name="email" placeholder="Input E-mail" class="form-control" value="<?php echo $kue->email; ?>" required/>
            </div>
        </div><div class="control-group" id="">
            <label class="control-label">Alamat</label>
            <div class="controls">
                <textarea type="text" class="span4" name="alamat" placeholder="" class="form-control" value="" required><?php echo $kue->alamat; ?></textarea>
            </div>
        </div>
        <script>
            $(function() {
                $( "#tgll" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm-dd",
                    yearRange: "1950:2030"
                });
            });
        </script>
        <div class="control-group" id="">
            <label class="control-label">Tanggal Masuk</label>
            <div class="controls">
                <input type="text" id="tgll" class="span4" name="thms" placeholder="" class="form-control" value="<?php echo $kue->th_masuk; ?>" required/>
            </div>
        </div>
        <input type="hidden" name="iden" value="<?php echo $kue->id; ?>" placeholder=""> 
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>