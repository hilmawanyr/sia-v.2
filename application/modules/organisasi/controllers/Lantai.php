<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lantai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//$data['gedung']=$this->app_model->getdetail('tbl_gedung','id_gedung',$id,'id_gedung','asc')->row($id);
		$data['gedung'] = $this->app_model->getdata('tbl_gedung','id_gedung','asc')->row();
		//$data['w']=$this->app_model->getdetail('tbl_ruangan','id_ruangan',$id,'id_ruangan','asc')->result();
		$data['w']=$this->app_model->get_lantai($id);
		$data['page']="lantai_view";
		$this->load->view('template/template', $data);
		// $data['gedung']=$this->app_model->getdata('tbl_gedung','id_gedung','asc')->result();
		// $data['lantai']=$this->app_model->get_lantai();
		// $data['page']="lantai_view";
		// $this->load->view('template/template', $data);
	}

	function save_lantai()
	{
		$data=array(
		'lantai'		=> $this->input->post('lantai'),
		'id_gedung'		=> $this->input->post('floor')
		);
		//var_dump($data);die();
		$this->app_model->insertdata('tbl_lantai',$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lokasi/detail_gd/". $this->input->post('floor')."';</script>";
	}

	function update_lantai()
	{
		//$a = $this->input->post('floor');
		$data=array(
		'lantai'	=> $this->input->post('lantai'),
		'id_gedung'	=> $this->input->post('lants')
		);
		$this->app_model->updatedata('tbl_lantai','id_lantai',$this->input->post('id_lantai'),$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lantai/detail_lt/".$this->input->post('lants')."';</script>";
	}

	function load_lantai($edl)
	{
		$data['query'] = $this->db->query("select * from tbl_lantai where id_lantai = '$edl'")->row();
		$data['gedung']=$this->app_model->getdata('tbl_gedung','id_gedung','asc')->result();
		$this->load->view('organisasi/edit_lantai_view', $data);
	}

	function del_lantai($id)
	{
		$this->app_model->deletedata('tbl_lantai','id_lantai',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lokasi/detail_gd/".$id."';</script>";
	}

	function detil_lt($id)
	{
		$data['det'] =  $this->db->query('SELECT * from tbl_lantai a join tbl_gedung b on a.`id_gedung`=b.`id_gedung` where a.`id_lantai`='.$id.'')->row();
		//var_dump($data);exit();
		//$data['gedung']=$this->app_model->getdetail('tbl_gedung','id_gedung',$id,'id_gedung','asc')->row($id);
		$data['din'] = $this->db->query('SELECT * from tbl_lantai a join tbl_ruangan b
			on a.`id_lantai`=b.`id_lantai` where id_ruangan='.$id.'')->row();
		$data['lantai']=$this->app_model->getdetail('tbl_lantai','id_lantai',$id,'id_lantai','asc')->row($id);
		$data['x']=$this->app_model->get_ruang($id);
		$data['page']="ruang_view";
		$this->load->view('template/template', $data);
	}
	// function detail_lt($id)
	// {
	// 	$data['gedung']=$this->app_model->getdetail('tbl_gedung','id_gedung',$id,'id_gedung','asc')->row($id);
	// 	$data['ge'] = $this->app_model->getdata('tbl_gedung','id_gedung','asc')->result();
	// 	$data['w']=$this->app_model->getdetail('tbl_ruangan','id_ruangan',$id,'id_ruangan','asc')->result();
	// 	$data['w']=$this->app_model->get_lantai($id);
	// 	$data['page']="lantai_view";
	// 	$this->load->view('template/template', $data);
	// }



}

/* End of file  */
/* Location: ./application/controllers/ */