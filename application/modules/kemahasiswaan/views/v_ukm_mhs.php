<script type="text/javascript">

</script>

<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-group"></i>
        <h3>Daftar Organisasi Mahasiswa</h3>
      </div> <!-- /widget-header -->
    
      <div class="widget-content">
        <form action="<?php echo base_url('akademik/perbaikan/addkrs'); ?>" method="post" accept-charset="utf-8">
          <div class="span11">
            <a href="#info" class="btn btn-primary" data-toggle="modal" title=""><i class="icon icon-plus"></i> Tambah Data</a>
            <br><br>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr style="text-align:center;"> 
                  <th style="text-align:center;vertical-align:middle">No</th>
                  <th style="text-align:center;vertical-align:middle">Kode Organisasi</th>
                  <th style="text-align:center;vertical-align:middle">Nama Organisasi</th>
                  <th style="text-align:center;vertical-align:middle">Kategori</th>
                  <th width="122" style="text-align:center;vertical-align:middle">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>BEM-UN</td>
                  <td>Badan Eksekutif Mahasiawa Universitas</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>BEM-FT</td>
                  <td>Badan Eksekutif Mahasiawa Fakultas Teknik</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>BEM-FE</td>
                  <td>Badan Eksekutif Mahasiawa Fakultas Ekonomi</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>BEM-FK</td>
                  <td>Badan Eksekutif Mahasiawa Fakultas Komunikasi</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>BEM-FP</td>
                  <td>Badan Eksekutif Mahasiawa Fakultas Psikologi</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>BEM-FH</td>
                  <td>Badan Eksekutif Mahasiawa Fakultas Hukum</td>
                  <td>Organisasi Non UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>UKM-SN</td>
                  <td>Unit Kegiatan Mahasiswa Kesenian</td>
                  <td>Organisasi UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>8</td>
                  <td>UKM-KB</td>
                  <td>Unit Kegiatan Mahasiswa Kapal Baja</td>
                  <td>Organisasi UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>9</td>
                  <td>UKM-OG</td>
                  <td>Unit Kegiatan Mahasiswa Olah Raga</td>
                  <td>Organisasi UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>10</td>
                  <td>UKM-PM</td>
                  <td>Unit Kegiatan Mahasiswa Pena Muda</td>
                  <td>Organisasi UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>11</td>
                  <td>UKM-MW</td>
                  <td>Unit Kegiatan Mahasiswa Menwa</td>
                  <td>Organisasi UKM</td>
                  <td>
                    <a href="" class="btn btn-success" title="Lihat Organisasi"><i class="icon-eye-open"></i></a>
                    <a href="" class="btn btn-primary" title="Edit Organisasi"><i class="icon-pencil"></i></a>
                    <a href="" class="btn btn-danger" title="Hapus Organisasi"><i class="icon-remove"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="kontenmodal">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Tambah Data Organisasi Mahasiswa</h4>
          </div>
          <div class="modal-body">  
            
          </div> 
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
