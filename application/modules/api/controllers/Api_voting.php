<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_voting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-voting-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	public function attemp_login()
	{
		$payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);

		$check 	= $this->login_model->cekuser($decodePayload->npm,$decodePayload->password)->result();

		if (count($check) > 0) {
			// prepare user response
			$this->_prepare_response($check);

		} else {
			$response = ['status' => 60, 'message' => 'user not found' ];
			$this->output
				->set_status_header(404)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	private function _prepare_response($user)
	{
		foreach ($user as $row) {
			$user = $row->username;
			$pass = $row->password_plain;
			$type = $row->user_type;
		}

		$user_log = $this->login_model->datauser($user,$pass,$type)->result();

		foreach ($user_log as $key) {
			$session['userid']= $key->userid;
			$session['nama']  = $key->NMMHSMSMHS;
			$session['prodi'] = $key->KDPSTMSMHS;

			$response = ['status' => 1, 'message' => 'authentication success', 'data' => $session];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	public function amount_of_active_student()
	{
		$activeYear = getactyear();
		$totalActiveStudent = $this->db->where('tahunajaran', $activeYear)->get('tbl_verifikasi_krs')->num_rows();

		$response = ['status' => 1, 'message' => 'get amount of active student success', 'result' => $totalActiveStudent];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	public function student_name($npm)
	{
		$studentName = $this->db->where('NIMHSMSMHS', $npm)->get('tbl_mahasiswa')->row()->NMMHSMSMHS;

		$response = ['status' => 1, 'message' => 'get student name success', 'result' => $studentName];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

}

/* End of file Api_voting.php */
/* Location: ./application/modules/api/controllers/Api-voting.php */