<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_sarpras extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-sarpras-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	public function attemp_login()
	{
		$payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);

		$check 	= $this->login_model->cekuser($decodePayload->username,$decodePayload->password)->result();

		if (count($check) > 0) {

			// if user not available for sarpras
			$this->_is_sarpras_user($check);

			// prepare user response
			$this->_prepare_response($check);
		} else {
			$response = ['status' => 60, 'message' => 'user not found'];
			$this->output
				->set_status_header(404)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	private function _is_sarpras_user($user)
	{
		foreach ($user as $val) {
			if (is_null($val->is_sarpras)) {
				$response = ['status' => 401, 'message' => 'invalid access' ];
				$this->output
					->set_status_header(401)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
			return;
		}
	}

	private function _prepare_response($user)
	{
		foreach ($user as $row) {
			$user = $row->username;
			$pass = $row->password_plain;
			$type = $row->user_type;
		}

		$user_log = $this->login_model->datauser($user, $pass, $type)->result();

		foreach ($user_log as $key) {
			$session['id']		 		= $key->id_user;
			$session['userid']		 	= $key->userid;
			$session['divisi'] 			= $key->divisi;
			$session['group_id']		= $key->id_user_group;
			$session['group_name']		= get_group_user($key->id_user_group);
			$session['is_sarpras_user']	= $key->is_sarpras;
			$session['is_active']		= $key->status;

			$response = ['status' => 201, 'message' => 'login success', 'data' => $session];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}
}

/* End of file Api_sarpras.php */
/* Location: ./application/modules/api/controllers/Api_sarpras.php */
