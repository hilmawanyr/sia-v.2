<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_perpus extends CI_Controller {

	private $id_student;

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-app-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		$this->id_student = $this->input->get_request_header('param-key');
	}

	/**
	 * Get all students
	 * @param int $limit
	 * @return JSON
	 */
	public function getAllStudents($limit = '')
	{
		$HTTP_METHOD = $this->input->method(TRUE);
		if ($HTTP_METHOD != 'GET') {
			$response = ['status' => 66, 'message' => 'method not allowed.', 'data' => ''];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		# minimum angkatan
		$angkatanTerkecil = date('Y') - 7;

		$this->db->select('a.id_mhs, a.NIMHSMSMHS, a.NMMHSMSMHS, a.TGLHRMSMHS, b.no_hp, b.email, b.alamat, a.KDJEKMSMHS, c.fakultas, d.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_bio_mhs b', 'a.NIMHSMSMHS = b.npm', 'left');
		$this->db->join('tbl_jurusan_prodi d', 'd.kd_prodi = a.KDPSTMSMHS');
		$this->db->join('tbl_fakultas c', 'c.kd_fakultas = d.kd_fakultas');
		$this->db->where('a.TAHUNMSMHS >', $angkatanTerkecil);
		$this->db->order_by('a.TAHUNMSMHS', 'asc');

		if (!empty($limit)) {
			$this->db->limit($limit);
		}

		$students = $this->db->get();

		# if response true
		if ($students->num_rows() > 0) {
			foreach ($students->result() as $student) {
				$data[] = [
					'id' => $student->id_mhs,
					'npm' => $student->NIMHSMSMHS,
					'nama' => $student->NMMHSMSMHS,
					'tanggal_lahir' => $student->TGLHRMSMHS,
					'telepon' => $student->no_hp,
					'email' => $student->email,
					'alamat' => $student->alamat,
					'jenis_kelamin' => $student->KDJEKMSMHS,
					'fakultas' => $student->fakultas,
					'prodi' => $student->prodi
				];
			}
			$response = ['status' => 1, 'message' => 'success', 'data' => $data];

		# if response false
		} else {
			$response = ['status' => 88, 'message' => 'error process', 'data' => ''];
		}

		$this->output
			->set_status_header(400)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}
	
	/**
	 * Get specify student
	 * 
	 * @return JSON
	 */
	public function getDetailStudent()
	{
		$HTTP_METHOD = $this->input->method(TRUE);
		if ($HTTP_METHOD != 'GET') {
			$response = ['status' => 66, 'message' => 'method not allowed.', 'data' => ''];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		# minimun tahun angkatan
		$angkatanTerkecil = date('Y') - 7;

		$this->db->select('a.id_mhs, a.NIMHSMSMHS, a.NMMHSMSMHS, a.TGLHRMSMHS, b.no_hp, b.email, b.alamat, a.KDJEKMSMHS, c.fakultas, d.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_bio_mhs b', 'a.NIMHSMSMHS = b.npm', 'left');
		$this->db->join('tbl_jurusan_prodi d', 'd.kd_prodi = a.KDPSTMSMHS');
		$this->db->join('tbl_fakultas c', 'c.kd_fakultas = d.kd_fakultas');
		$this->db->where('a.id_mhs', $this->id_student);
		$this->db->where('a.TAHUNMSMHS >', $angkatanTerkecil);
		$this->db->order_by('a.TAHUNMSMHS', 'asc');
		$students = $this->db->get();

		# if response true
		if ($students->num_rows() > 0) {
			foreach ($students->result() as $student) {
				$data[] = [
					'id' => $student->id_mhs,
					'npm' => $student->NIMHSMSMHS,
					'nama' => $student->NMMHSMSMHS,
					'tanggal_lahir' => $student->TGLHRMSMHS,
					'telepon' => $student->no_hp,
					'email' => $student->email,
					'alamat' => $student->alamat,
					'jenis_kelamin' => $student->KDJEKMSMHS,
					'fakultas' => $student->fakultas,
					'prodi' => $student->prodi
				];
			}
			$response = ['status' => 1, 'message' => 'success', 'data' => $data];

		# if response false
		} else {
			$response = ['status' => 60, 'message' => 'data not found', 'data' => ''];
		}

		$this->output
			->set_status_header(400)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	public function getMahasiswa()
	{
		$datamhs = $this->db->where_in('NIMHSMSMHS', ['201210225054','201210225045'])->get('tbl_mahasiswa')->result();

		foreach ($datamhs as $value) {
			$datastudent[] = [
				'npm' => $value->NIMHSMSMHS,
				'nama' => $value->NMMHSMSMHS,
				'krs' => $this->getKrs($value->NIMHSMSMHS)
			];
		}

		$response = ['status' => 1, 'data' => $datastudent];
		$this->output
			->set_status_header(400)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();

	}

	function getKrs($npm)
	{
		$getkrs = $this->db->where('npm_mahasiswa', $npm)->limit(3)->get('tbl_krs')->result();

		foreach ($getkrs as $key) {
			$datakrs[] = $key->kd_krs;
		}

		return $datakrs;
	}
	

}

/* End of file Api_perpus.php */
/* Location: .//tmp/fz3temp-2/Api_perpus.php */