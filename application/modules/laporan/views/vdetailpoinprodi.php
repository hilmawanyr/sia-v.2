<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Evaluasi Prodi <?php echo get_jur($prodi); ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="tabbable">
	                <div class="span11">
		                    
	                    <a href="<?= base_url(); ?>laporan/evaluasi/excelPoin/<?= $prodi; ?>" class="btn btn-success">
	                    	<i class="btn-icon-only icon-print"> </i> Print Excel
	                    </a>

	                    <br><hr>

						<table id="example1" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
	                                <th>Parameter</th>
	                                <th>Nilai</th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                            <?php $no = 1; foreach ($getData as $value) { ?>
		                        <tr>
		                        	<td><?php echo number_format($no); ?></td>
		                        	<td><?php echo $value->parameter; ?></td>
		                        	<?php 	
		                        		$this->db2 = $this->load->database('eval', TRUE); 
	                        			
	                        			if ($sesi < '20171') {
	                        				$avg = $this->db2->query("SELECT AVG(nilai) as nilaibro 
	                        										from tbl_nilai_parameter 
	                        										where parameter_id = ".$value->id_parameter." 
	                        										AND kd_jadwal like '".$prodi."%'")->row()->nilaibro;
	                        			} else {
	                        				$avg = $this->db2->query("SELECT AVG(nilai) as nilaibro 
	                        										from tbl_nilai_parameter_".$sesi."
	                        										where parameter_id = ".$value->id_parameter."
	                        										AND kd_jadwal like '".$prodi."%'")->row()->nilaibro;
	                        			}

	                        			if ($sesi < 20172) {
	                        				
	                        				$aveg = number_format(($avg/20),2);

	                        			} else {
									                        				
	                        				$aveg = number_format($avg*1.25,2);

	                        			}
	                        			

		                        	?>

		                        	<td><?= $aveg; ?></td>
		                        </tr>
	                            <?php $no++; } ?>
		                    </tbody>
		               	</table>
	                </div>
	            </div>				
			</div>
		</div>
	</div>
</div>