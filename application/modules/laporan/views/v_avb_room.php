<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-list"></i>
        <h3>Ketersediaan Ruangan</h3>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>laporan/ruangkosong/save_sess">
            <fieldset>

              <script type="text/javascript">
                $(document).ready(function(){
                  $('#jams').timepicker();
                });
              </script>

              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span4" name="ta" id="" required>
                    <option>--Pilih Tahun Akademik--</option>
                    <?php foreach ($tahunajar as $row) { ?>
                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Hari</label>
                <div class="controls">
                  <select class="form-control span4" name="day" id="" required>
                    <option>--Pilih Tahun Akademik--</option>
                    <?php for ($i = 1; $i < 8; $i++) { ?>
                    <option value="<?php echo $i;?>"><?php echo notohari($i);?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Jam Mulai</label>
                <div class="controls">
                  <input type="text" name="jam" value="" class="form-control span4" id="jams" placeholder="Masukan Jam Masuk" required>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">SKS Matakuliah</label>
                <div class="controls">
                  <input type="number" name="sks" value="" class="form-control span4" placeholder="Masukan SKS Mata Kuliah" required>
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
            
        </div>
      </div>
    </div>
  </div>
</div>
