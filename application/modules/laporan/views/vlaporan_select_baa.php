<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Rekap Data Akademik</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>laporan/save_session" target="_blank">
          <fieldset>

            <div class="control-group">
              <label class="control-label">Laporan</label>
              <div class="controls">
                <select id="show" class="form-control span6" onchange="change(this)" name="laporan" required>
                  <option disabled selected>--Pilih Laporan--</option>
                    <option value="1"><?php echo 'Jumlah Mahasiswa' ?></option>
                    <option value="2"><?php echo 'Jumlah kelas Prodi' ?></option>
                    <option value="3"><?php echo 'Beban Dosen' ?></option>
                    <option value="4"><?php echo 'IPS & IPK' ?></option>
                </select>
              </div>
            </div> 

              <script>
                $(document).ready(function(){
                  $('#faks').change(function(){
                    $.post('<?php echo base_url()?>laporan/get_jurusan/'+$(this).val(),{},function(get){
                      $('#jurs').html(get);
                    });
                  });
                });
                </script>
                 <div id="fakultas" class="control-group">
                    <label class="control-label">Fakultas</label>
                    <div class="controls">
                      <select class="form-control span6" name="fakultas" id="faks" required>
                        <option>--Pilih Fakultas--</option>
                        <?php foreach ($fakultas as $row) { ?>
                        <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div id="jurusan" class="control-group">
                    <label class="control-label">Jurusan</label>
                    <div class="controls">
                      <select class="form-control span6" name="jurusan" id="jurs" required>
                        <option>--Pilih Jurusan--</option>
                      </select>
                    </div>
                  </div>
                <div id="tahun" class="control-group">
                  <label class="control-label">Tahun Ajaran</label>
                  <div class="controls">
                    <select class="form-control span6" name="tahun" id="thn" required>
                      <option disabled selected>--Pilih Tahun Akademik--</option>
                      <?php foreach ($akademik as $key) { ?>
                        <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>    
              <br />
                    <div class="form-actions">
                        <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                    </div> <!-- /form-actions -->
                </fieldset>
            </form>
          
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#fakultas').hide();
$('#jurusan').hide();
$('#tahun').hide();

function change() {
    var selectBox = document.getElementById("show");
    var selected = selectBox.options[selectBox.selectedIndex].value;


    $('#jurusan option').prop('selected', function() {
        return this.defaultSelected;
    });

    $('#tahun option').prop('selected', function() {
        return this.defaultSelected;
    });

    if(selected === '1'){
      $('#fakultas').show();
        $('#jurusan').show();
        $('#tahun').show();
    }else if(selected === '2'){
      $('#fakultas').show();
        $('#jurusan').show();
        $('#tahun').show();
    }else if(selected === '3'){
      $('#fakultas').show();
        $('#jurusan').show();
        $('#tahun').show();
    }else if(selected === '4'){
      $('#fakultas').show();
        $('#jurusan').show();
        $('#tahun').show();
    }
    
}
</script>