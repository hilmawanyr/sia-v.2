<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Evaluasi Fakultas <?= get_fak($arry).' '.get_thajar($sesi); ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url(); ?>laporan/evaluasi/excelListProdi/<?= $arry; ?>" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
                    <br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Prodi</th>
                                <th>NILAI AKUMULATIF</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($prod as $value) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $value->prodi; ?></td>
	                        	<?php 
		                            $this->db2 = $this->load->database('eval', TRUE);
		                            if ($sesi < '20171') {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
		                            								tbl_pengisian_kuisioner 
		                            								WHERE kd_jadwal like '".$value->kd_prodi."%' 
		                            								AND tahunajaran = '".$sesi."'")->row()->akhir;
		                            } else {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
		                            								tbl_pengisian_kuisioner_".$sesi." 
		                            								WHERE kd_jadwal like '".$value->kd_prodi."%' 
		                            								AND tahunajaran = '".$sesi."'")->row()->akhir;	
		                            } 
	                        	?>

	                        	<!-- nilai rata-rata -->
	                        	<?php if ($sesi < '20172') {
	                        		$average = number_format(($rata2/20),2);
	                        	} else {
	                        		$average = number_format((($rata2/20)*10),2);
	                        	}
	                        	 ?>

	                        	<td><?php echo $average; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-primary btn-small" target="_blank" href="<?= base_url(); ?>laporan/evaluasi/perPoin/<?= $value->kd_prodi; ?>" ><i class="btn-icon-only icon-ok"> </i></a>
									<!-- <a class="btn btn-success btn-small" href="#" ><i class="btn-icon-only icon-print"> </i></a> -->
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>