<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Sinkronisasi PDPT</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="#" class="btn btn-primary"> Submit Valid </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="40">No</th>
	                            <th>Data</th>
	                            <th width="80">Lihat</th>
	                            <th width="40">Valid</th>
	                            <th width="40">Sync</th>
	                            <th width="40">Laporan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php //$no = 1; foreach($jabatan as $row){?>
	                        <tr>
	                        	<td>1</td>
	                        	<td>Kartu Rencana Studi Mahasiswa (2016/2017 - Ganjil). [20161]</td>
	                        	<td><a href="#">Lihat Data</a></td>
	                        	<td><input type="checkbox" name="" value=""></td>
	                        	<td class="td-actions">
									<!--a class="btn btn-small btn-success" href="#"><i class="btn-icon-only icon-ok"> </i></a-->
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-primary btn-small" href="#"><i class="btn-icon-only icon-rss"> </i></a></td>
								<td class="td-actions">
									<a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-print"> </i></a>
								</td>
	                        </tr>
	                        <tr>
	                        	<td>2</td>
	                        	<td>Kartu Hasil Studi Mahasiswa (2015/2016 - Genap). [20152]</td>
	                        	<td><a href="#">Lihat Data</a></td>
	                        	<td><input type="checkbox" name="" value=""></td>
	                        	<td class="td-actions">
									<!--a class="btn btn-small btn-success" href="#"><i class="btn-icon-only icon-ok"> </i></a-->
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-primary btn-small" href="#"><i class="btn-icon-only icon-rss"> </i></a></td>
								<td class="td-actions">
									<a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-print"> </i></a>
								</td>
	                        </tr>
							<?php //$no++;} ?>
	                      
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>