<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Status Mahasiswa</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>laporan/statusmhs/export_data">

                        <fieldset>

                              <?php 
                              $logged = $this->session->userdata('sess_login');
                              $pecah = explode(',', $logged['id_user_group']);
                              $jmlh = count($pecah);
                              for ($i=0; $i < $jmlh; $i++) { 
                                $grup[] = $pecah[$i];
                              }
                              if ( (in_array(10, $grup))) {
                              ?>

                              <script>

                              $(document).ready(function(){

                                $('#faks').change(function(){

                                  $.post('<?php echo base_url()?>laporan/statusmhs/get_jurusan/'+$(this).val(),{},function(get){

                                    $('#jurs').html(get);

                                  });

                                });

                              });

                              </script>

                              <div class="control-group">

                                <label class="control-label">Fakultas</label>

                                <div class="controls">

                                  <select class="form-control span6" name="fakultas" id="faks" required>

                                    <option disabled selected>--Pilih Fakultas--</option>

                                    <?php foreach ($fakultas as $row) { ?>

                                    <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              
                              <div class="control-group">

                                <label class="control-label">Program Studi</label>

                                <div class="controls">

                                  <select class="form-control span6" name="jurusan" id="jurs" required>

                                    <option disabled selected>--Pilih Program Studi--</option>

                                  </select>

                                </div>

                              </div>
                              <?php } else { ?>
                              <input type="hidden" name="jurusan" value="<?php echo $logged['userid']; ?>">
                              <?php } ?>

                              <div class="control-group">

                                <label class="control-label">Tahun Masuk</label>

                                <div class="controls">

                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>

                                    <option disabled selected>--Pilih Tahun Masuk--</option>

                                    <?php $now = date('Y'); for ($i=$now-8; $i <= $now; $i++) { 
                                      echo "<option value='".$i."'>".$i."</option>";
                                    } ?>

                                  </select>

                                </div>

                              </div>  

                            <br/>

                              

                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 

                            </div> <!-- /form-actions -->

                        </fieldset>

                    </form>

        </div>

      </div>

    </div>

  </div>

</div>

