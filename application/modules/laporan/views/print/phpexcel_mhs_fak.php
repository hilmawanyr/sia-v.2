<?php 

//var_dump($cuti);

$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$shit=0;

    $ts = $this->db->where('kd_prodi', $jur)->get('tbl_jurusan_prodi')->row();    
    
    //ACTIVE SHEET
    $excel->setActiveSheetIndex($shit);
    //name the worksheet
    $excel->getActiveSheet()->setTitle($ts->prodi);

    //border
    //$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
    //$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
    //$excel->getActiveSheet()->getStyle('H3:L4')->applyFromArray($BStyle);

    //tittle
    $excel->getActiveSheet()->setCellValue('C1', 'DATA JUMLAH MAHASISWA');
    $excel->getActiveSheet()->mergeCells('C1:F1');
    $excel->getActiveSheet()->setCellValue('C2', 'PROGRAM STUDI '.$ts->prodi.'');
    $excel->getActiveSheet()->mergeCells('C2:F2');

    //semester
    $h1=4;
    $hy=$h1+1;
    $excel->getActiveSheet()->setCellValue('A'.$h1, 'NO');
    $excel->getActiveSheet()->mergeCells('A4:A5');
    $excel->getActiveSheet()->setCellValue('B'.$h1, 'PRODI');
    $excel->getActiveSheet()->mergeCells('B4:B5');
    $excel->getActiveSheet()->setCellValue('C'.$h1, 'ANGKATAN');
    $excel->getActiveSheet()->mergeCells('C4:C5');
    $excel->getActiveSheet()->setCellValue('D'.$h1, 'AKTIF');
    $excel->getActiveSheet()->mergeCells('D4:F4');
    $excel->getActiveSheet()->setCellValue('D'.$hy, 'PAGI');
    $excel->getActiveSheet()->setCellValue('E'.$hy, 'SORE');
    $excel->getActiveSheet()->setCellValue('F'.$hy, 'P2K');
    $excel->getActiveSheet()->setCellValue('G'.$h1, 'CUTI');
    $excel->getActiveSheet()->mergeCells('G4:G5');
    $excel->getActiveSheet()->setCellValue('H'.$h1, 'NON AKTIF');
    $excel->getActiveSheet()->mergeCells('H4:H5');
    //isi semester
    $hy++;

    $no=1;
    $c_apg = 0;
    $c_asr = 0;
    $c_apk = 0;
    $c_cuti = 0;
    $c_non = 0;

    for ($i=0; $i < 8 ; $i++) { 

        $aktif=$this->db->query("SELECT krs.`kelas`,COUNT(krs.`npm_mahasiswa`) AS jml_mhs FROM tbl_mahasiswa mhs
                                                JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
                                                WHERE krs.`tahunajaran` = '".$thn."' AND mhs.`KDPSTMSMHS` = '".$jur."' 
                                                AND mhs.`TAHUNMSMHS` = '".$angkatan."'
                                                GROUP BY krs.`kelas`
                                                ")->result();

        $cuti=$this->db->query("SELECT COUNT(mhs.`NIMHSMSMHS`) AS jml_mhs FROM tbl_mahasiswa mhs
                                                    JOIN tbl_status_mahasiswa sts ON sts.`npm` = mhs.`NIMHSMSMHS`
                                                    WHERE mhs.`KDPSTMSMHS` = '".$jur."' AND sts.`status` = 'C' 
                                                    AND sts.`tahunajaran` = '".$thn."' AND mhs.`TAHUNMSMHS` = '".$angkatan."'")->row();

        $non=$this->db->query("SELECT DISTINCT mhs.`KDPSTMSMHS`, COUNT(mhs.`NIMHSMSMHS`) AS jml_mhs FROM tbl_mahasiswa mhs WHERE mhs.`NIMHSMSMHS` 
                                NOT IN (SELECT vkrs.`npm_mahasiswa` FROM tbl_verifikasi_krs vkrs WHERE  vkrs.`tahunajaran` = '".$angkatan."') AND 
                                mhs.`NIMHSMSMHS` NOT IN (SELECT sts.`npm` FROM tbl_status_mahasiswa sts WHERE sts.`status` = 'C' AND sts.`tahunajaran` = '".$thn."')
                                AND (mhs.`STMHSMSMHS` NOT IN ('L','K','D')) AND mhs.`TAHUNMSMHS` != (".$angkatan."+1) AND mhs.`KDPSTMSMHS` = '".$jur."'
                                AND mhs.`TAHUNMSMHS` >= (".$angkatan."+1)  AND mhs.`TAHUNMSMHS` = '".$angkatan."'
                                ")->row();
        
    
        $excel->getActiveSheet()->setCellValue('A'.$hy, $no);
        $excel->getActiveSheet()->mergeCells('A4:A5');
        $excel->getActiveSheet()->setCellValue('B'.$hy, $ts->prodi);
        $excel->getActiveSheet()->mergeCells('B4:B5');
        $excel->getActiveSheet()->setCellValue('C'.$hy, $angkatan);
        $excel->getActiveSheet()->mergeCells('C4:C5');

        foreach ($aktif as $key) {

            if ($key->kelas == 'PG') {
                $excel->getActiveSheet()->setCellValue('D'.$hy, $key->jml_mhs);
                $c_apg = $c_apg + $key->jml_mhs;
            }elseif ($key->kelas == 'SR') {
                $excel->getActiveSheet()->setCellValue('E'.$hy, $key->jml_mhs);
                $c_asr = $c_asr + $key->jml_mhs;
            }elseif ($key->kelas == 'PK') {
                $excel->getActiveSheet()->setCellValue('F'.$hy, $key->jml_mhs);
                $c_apk = $c_apk + $key->jml_mhs;
            }
        }
        $excel->getActiveSheet()->setCellValue('G'.$hy, $cuti->jml_mhs);
        $excel->getActiveSheet()->setCellValue('H'.$hy, $non->jml_mhs);

        $c_cuti = $c_cuti + $cuti->jml_mhs;
        $c_non = $c_non + $cuti->jml_mhs;

        $angkatan++;
        $hy++;
        $no++;

    } //end for angkatan
    $excel->getActiveSheet()->setCellValue('A'.$hy, 'JUMLAH');
    $excel->getActiveSheet()->mergeCells('A'.$hy.':C'.$hy);
    $excel->getActiveSheet()->setCellValue('D'.$hy, $c_apg);
    $excel->getActiveSheet()->setCellValue('E'.$hy, $c_asr);
    $excel->getActiveSheet()->setCellValue('F'.$hy, $c_apk);
    $excel->getActiveSheet()->setCellValue('G'.$hy, $c_cuti);
    $excel->getActiveSheet()->setCellValue('H'.$hy, $c_non);

    $excel->getActiveSheet()->getStyle('A4:H'.$hy)->applyFromArray($BStyle);
   

    //align
    $cstyle = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $excel->getActiveSheet()->getStyle("A4")->applyFromArray($cstyle);
    $excel->getActiveSheet()->getStyle("C4:H5")->applyFromArray($cstyle);
    $excel->getActiveSheet()->getStyle("C4:E4")->applyFromArray($cstyle);
    $excel->createSheet();

$shit++;

$filename = 'Data_Jumlah_kelas_'.$ts->prodi.'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>