<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_dosen_".$kry->nama.".xls'");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<td colspan="8" style="text-align:center;vertical-align:middle;background:#8FBC8B;">
				<h3>Data Evaluasi Dosen <?php echo $kry->nama; ?> <?php $tahunajar = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row()->tahun_akademik; echo $tahunajar; ?></h3>
			</td>
		</tr>	
        <tr> 
        	<th style="backgruond:yellow">No</th>
            <th style="backgruond:yellow">KELAS</th>
            <th style="backgruond:yellow">KODE MK</th>
            <th style="backgruond:yellow">MATAKULIAH</th>
            <th style="backgruond:yellow">NILAI</th>
            <th style="backgruond:yellow">TOTAL INPUT</th>
            <th style="backgruond:yellow">JUMLAH MHS</th>
            <th style="backgruond:yellow">SKS</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($eval as $value) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
        	<td><?php echo $value->kelas; ?></td>
        	<td><?php echo $value->kd_matakuliah; ?></td>
        	<td><?php echo $value->nama_matakuliah; ?></td>
        	<?php 
                $this->db2 = $this->load->database('eval', TRUE);
                if ($this->session->userdata('ta') < '20171') {
                    $rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                    $jmlmhs = $this->db2->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_pengisian_kuisioner_20171 WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                    $jmlkrs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_krs WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                } else {
                    $rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner_".$tahunakad." WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                    $jmlmhs = $this->db2->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_pengisian_kuisioner_".$tahunakad." WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                    $jmlkrs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_krs WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                }
        	?>

            <!-- nilai rata-rata -->
            <?php if ($this->session->userdata('ta') < '20172') {
                $average = number_format(($rata2/20),2);
            } else {
                $average = number_format((($rata2/20)*10),2);
            }
             ?>
            
        	<td><?php echo $average; ?></td>
        	<td><?php echo number_format($jmlmhs); ?></td>
        	<td><?php echo number_format($jmlkrs); ?></td>
        	<td><?php echo $value->sks_matakuliah; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>