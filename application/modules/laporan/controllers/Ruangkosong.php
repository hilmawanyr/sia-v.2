<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangkosong extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
	}

	public function index()
	{
		$data['tahunajar'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page'] = "v_kosong_select";
		$this->load->view('template/template', $data);
	}

	function save_sess()
	{
		$array = array(
			'tahun' => $this->input->post('ta'),
			'hari' => $this->input->post('day'),
			'jam' => $this->input->post('jam'),
			'sks' => $this->input->post('sks')
		);
		
		$this->session->set_userdata('sess_room',$array);
		redirect(base_url('laporan/ruangkosong/list_ruang_kosong'),'refresh');
	}

	function list_ruang_kosong(){
		$waktu = $this->session->userdata('sess_room');

		$h = $waktu['jam'].':00';
		$jam = new DateTime($h);
		
		if ($waktu['sks'] == 2) {
			$jam->modify('+100 minutes');
			$jam_keluar = $jam->format('H:i:s');
		}elseif ($waktu['sks'] == 3) {
			$jam->modify('+150 minutes');
			$jam_keluar = $jam->format('H:i:s');
		}

		$data['jam_keluar'] = $jam_keluar;

		$data['kelas'] = $this->temph_model->emptyroom($h,$jam_keluar,$waktu['tahun'],$waktu['hari'])->result();

		$data['page'] = "v_kosong_list";
		$this->load->view('template/template', $data);
	}


	function destroy_sess()
	{
		$this->session->unset_userdata('sess_room');
		redirect(base_url('laporan/ruangkosong'),'refresh');
	}

	// function load_room()
	// {
	// 	$room = $this->session->userdata('sessroom');
	// 	if ($room['sks'] == '2') {
	// 		$skstime = '01:40';
	// 	} else {
	// 		$skstime = '02:30';
	// 	}
		
	// 	$pecah 	= explode(':', $skstime);
	// 	$jam	= $pecah[0];
	// 	$mnt 	= $pecah[1];

	// 	$jaminput 	= $room['jam'];
	// 	$pecah2		= explode(':', $jaminput);
	// 	$jam2		= $pecah2[0];
	// 	$mnt2		= $pecah2[1];

	// 	$summinute	= $mnt2+$mnt;
	// 	$sumhour	= $jam2+$jam;

	// 	if ($sumhour > 23) {
	// 		$sumhour = '00';
	// 	}

	// 	if ($summinute > 59) {
	// 		$menit = '00';
	// 		$jamxx = $sumhour+01;
	// 	} else {
	// 		$menit = $summinute;
	// 		$jamxx = $sumhour;
	// 	}
		
	// 	$jamfix = strval($jamxx).':'.strval($menit);
	// 	echo $jamfix;
	// }

}

/* End of file Availability_room.php */
/* Location: ./application/modules/laporan/controllers/Availability_room.php */