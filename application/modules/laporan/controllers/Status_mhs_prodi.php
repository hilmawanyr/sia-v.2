<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_mhs_prodi extends CI_Controller {

	public function index()
	{
		
	}

	function load_stmhs($usr,$thn)
	{
		$data['actv'] = $this->temph_model->load_stmhs_prd_actv($usr,$thn)->result();
		$data['nact'] = $this->temph_model->load_stmhs_prd_nctv();
		$data['cuti'] = $this->temph_model->load_stmhs_prd_cuti();
		$data['page'] = "v_sts_mhs_prd";
		$this->load->view('template/template', $data);
	}

}

/* End of file Status_mhs_prodi.php */
/* Location: ./application/modules/laporan/controllers/Status_mhs_prodi.php */