<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporansemester extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		//$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(115)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'laporan/laporan_select';
	    $this->load->view('template/template', $data);
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');

		$jurusan = $this->input->post('jurusan');

        $tahunajaran = $this->input->post('tahunajaran');

		$angkatan = $this->input->post('angkatan');  
		//var_dump($jurusan);exit();
        $this->session->set_userdata('tahunajaran', $tahunajaran);

		$this->session->set_userdata('jurusan', $jurusan);

		$this->session->set_userdata('fakultas', $fakultas);

		$this->session->set_userdata('angkatan', $angkatan);
      
		redirect(base_url('laporan/laporansemester/load_laporan'));
	}

	function load_laporan()
	{
		$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$this->session->userdata('jurusan'),'kd_prodi','asc')->row();
		$data['ta'] = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('tahunajaran'),'kode','asc')->row();
		$data['jumlahkrs'] = $this->app_model->getjumlahkrsperprodi($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['totalkrs'] = $this->app_model->gettotalkrsperprodi($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['getabcde'] = $this->app_model->getabcdeperprodi($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['getips1'] = $this->app_model->getipsperprodi1($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['getips2'] = $this->app_model->getipsperprodi2($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['getips3'] = $this->app_model->getipsperprodi3($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		$data['kelasin'] = $this->db->query('SELECT DISTINCT jdl.`waktu_kelas`, COUNT(DISTINCT jdl.`kd_jadwal`) AS jml FROM tbl_jadwal_matkul  jdl
												JOIN tbl_krs krs ON jdl.`kd_jadwal` = krs.`kd_jadwal`
												WHERE jdl.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'" AND jdl.`kd_jadwal` LIKE "'.$this->session->userdata('jurusan').'%"
												GROUP BY jdl.`waktu_kelas`')->result();
		$data['cuti'] = $this->app_model->get_laporan_cuti($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));

		$data['dosen_tidak'] = $this->app_model->get_dostitap($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));

		$data['dosen_tetap'] = $this->app_model->get_dostap($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));
		//var_dump($data['kelas']);die();

		$data['page'] = 'laporan/laporan_view';
	    $this->load->view('template/template', $data);
	}

}

/* End of file LaporanSemester.php */
/* Location: ./application/modules/laporan/controllers/LaporanSemester.php */