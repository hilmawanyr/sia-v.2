<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transkrip_adjustment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$data = [
			'title' => 'Edit Transkrip',
			'page' => 'transkrip-adjustment/view_transkrip',
		];


		$this->load->view('template/template', $data);
		/*$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();
		$data['q'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		$data['q_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai_konversi nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$this->load->library('Cfpdf');

		$data['npm'] = $npm;
		$data['output'] = 'F';
		$filename = "transcript-" . $npm . ".pdf";

		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename)) {
			$data['pathPdf'] = $_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename;
			$this->load->view('akademik/transkrip_pdf3', $data);
		}

		$response = ['status' => 1, 'data' => base_url() . 'downloaded/transkrip/' . $filename];
		$this->output
			->set_status_header(200)
			// ->set_header('Content-Disposition: attachment; filename="transcript-'.$npm.'.pdf"')
			->set_content_type('application/json', 'utf-8') // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
			// ->set_output(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename))
			->set_output(json_encode($response))
			->_display();
		exit();*/
	}

	public function findNpm($npm='')
	{
		$this->db->distinct();
        $this->db->select("NIMHSMSMHS,NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa');
        $this->db->like('NIMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('NMMHSMSMHS', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                    'nama'          => $row->NMMHSMSMHS,
                    'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS,
                    'nid'          => $row->NIMHSMSMHS,
                    );
        }
        echo json_encode($data);
	}

	function viewTranskrip() {
		$npm = $_GET['npm'];

		$data['head'] = $this->db->query('SELECT NIMHSMSMHS as npm_mhs , NMMHSMSMHS as nama_mhs, prodi, jenjang, SMAWLMSMHS from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();

		
		$data['transkrip'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM, 
										deletedAt FROM tbl_transaksi_nilai nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '" and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC')->result();


		/*
			Mahasiswa Sebelum ada SIA, angkatan lama sebelum 2015 semester ganjil
			ada matakuliah yang nilainya ga ada di tbl_transaksi_nilai
		*/
		$data['transkrip_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM, 
										deletedAt FROM tbl_transaksi_nilai_konversi nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '" and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC')->result();

		
		$this->load->view('transkrip-adjustment/tbl_transkrip-adjustment', $data);


	}

	// Remove matakuliah from tbl_transaksi_nilai
	function rm()
	{
		if(@$_POST['KDKMKTRLNM']) {
			// table name init
			$table 		= 'tbl_transaksi_nilai';
			// string konversi
			$konversi = $_POST['t'];
			// konversi table init
			if(!empty($konversi) || $konversi = '') 
				$table = $table .'_'.$konversi; 
			
			// Get UserName Login 
			$userLogin 	= $this->session->userdata('sess_login')['username'];

			// Declare var & array
			$condition 	= [];
			$data 		= [];
			$rowId 		= 0;

			// Condition 
			$condition = [
				'KDKMKTRLNM' => $_POST['KDKMKTRLNM'],
				'NIMHSTRLNM' => $_POST['NIMHSTRLNM']
			];

			// Data Source for update
			$data = [
				'deletedAt' => date('Y-m-d H:i:s'),
				'deletedBy' => $userLogin
			];

			// Update information table
			$this->db->update($table, $data, $condition);
			$res = $this->db->affected_rows();

			// If Update Success 
			// 0 => Failed Or No Changed
			// 1 => Success 
			if ($res > 0) {
				echo json_encode(['status' => true, 'msg' => 'Berhasil hapus mata kuliah']);
			} else {
				echo json_encode(['status' => false, 'msg' => 'Gagal hapus mata kuliah']);
			}
		}else{
			echo json_encode(['status' => false, 'msg' => 'Gagal hapus mata kuliah']);
		}
		
	}
}

/* End of file Transkrip.php */
/* Location: ./application/controllers/Transkrip.php */