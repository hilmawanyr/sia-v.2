<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'), 'refresh');
		}
	}

	function load_mhs_autocomplete()
	{
		$this->db->distinct();
		$this->db->select("a.NMMHSMSMHS,a.NIMHSMSMHS");
		$this->db->from('tbl_mahasiswa a');
		$this->db->where('KDPSTMSMHS',$this->session->userdata('sess_login')['userid']);
		$this->db->group_start();
		$this->db->like('a.NMMHSMSMHS', $_GET['term'], 'both');
		$this->db->or_like('a.NIMHSMSMHS', $_GET['term'], 'both');
		$this->db->group_end();
		$sql  = $this->db->get();
		$data = array();
		foreach ($sql->result() as $row) {
			$data[] = array(
				'nama'     => $row->NMMHSMSMHS,
				'npm'     => $row->NIMHSMSMHS,
				'value'   => $row->NIMHSMSMHS . ' - ' . $row->NMMHSMSMHS
			);
		}

		echo json_encode($data);
	}

	function load_mhs_kls()
	{
		$this->db->distinct();
		$this->db->select("a.NMMHSMSMHS,b.npm_mahasiswa,b.kelas_mhs");
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_pa b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('b.npm_mahasiswa', $_POST['npm']);
		$sql  = $this->db->get();
		if ($sql->num_rows() > 0) {
			$data = array(
				'npm'     => $sql->row()->npm_mahasiswa,
				'nama'     => $sql->row()->NMMHSMSMHS,
				'kelas'   => $sql->row()->kelas_mhs
			);
			echo $this->load->view('change_class_v', $data);
		} else {
			echo '<script>alert("Data tidak ditemukan")</script>';
		}
	}
	function save()
	{
		$data = array(
			'kelas_mhs'     => $this->input->post('kls')
		);
		$this->app_model->updatedata('tbl_pa', 'npm_mahasiswa', $this->input->post('npm'), $data);

		// also update on tbl_verifikasi KRS in active academic year
		$yearactive = getactyear();
		$this->db->like('kd_krs', $this->input->post('npm').$yearactive, 'AFTER');
		$this->db->update('tbl_verifikasi_krs', ['kelas' => $this->input->post('kls')]);

		echo "<script>alert('Berhasil Update Data');window.location = '" . base_url('akademik/dospa/') . "';</script>";
	}
}

/* End of file Kelas.php */
/* Location: ./application/modules/akademik/controllers/Kelas.php */
