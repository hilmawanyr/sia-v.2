<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perbaikan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		$this->load->model('temph_model');
		$this->load->library('Cfpdf');
		// $log = $this->session->userdata('sess_login');
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$user = $this->session->userdata('sess_login');
			$akses = $this->role_model->cekakses(151)->result();
			//$aktif = $this->setting_model->getaktivasi('nilai')->result();
			//if ((count($aktif) != 0) or ($user['userid'] == '61201' or $user['userid'] == '62201')) {
			//if ((count($aktif) != 0)) {
			//die('mati');
			if ($akses != TRUE) {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='" . base_url() . "home';</script>";
				//echo "gabisa";
			}
			//} else {
			//echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			//echo "gabisa";
			//}
		} else {
			redirect('auth', 'refresh');
		}
	}

	public function index()
	{
		$data['select'] = $this->db->where('kode =', getactyear())->get('tbl_tahunakademik')->result();

		$data['page'] = 'validasi_select';
		$this->load->view('template/template', $data);
	}
	function session_create()
	{
		$tahun = $this->input->post('tahunajaran');

		$this->session->set_userdata('ta', $tahun);

		redirect(base_url() . 'akademik/perbaikan/kls_perbaikan', 'refresh');
	}
	public function kls_perbaikan()
	{
		$actyear 	=  $this->session->userdata('ta');
		$log = $this->session->userdata('sess_login');
		$isactive 	= $this->db->query("SELECT * from tbl_verifikasi_krs_sp
										where kd_krs like '" . $log['userid'] . $actyear . "%'")->num_rows();
		if ($isactive < 1) {
			echo "<script>alert('Anda tidak melakukan pengisian KRS di semester ini!');history.go(-1);</script>";
			exit();
		}

		$kdkrs = $log['userid'] . $actyear;
		$cek_available = $this->temph_model->cek_verif_krs($kdkrs)->row();
		if (count($cek_available) == 0 or is_null($cek_available)) {
			$data['filter'] = $this->temph_model->get_mksp($log['userid'])->result(); //var_dump($data['filter']);exit();
			$data['hitung'] = count($data['filter']); //var_dump($data['hitung']);exit();
			$data['nim'] = $log['userid']; // var_dump($data['nim']);exit();
			$data['page'] = "v_mhs_mksp";
			$this->load->view('template/template', $data);
		} else {
			redirect(base_url('akademik/perbaikan/show_krs_sp'));
		}
	}

	function addkrs()
	{
		$log = $this->session->userdata('sess_login');
		$npm	= $log['userid'];
		$kdjd	= $this->input->post('kdjdl');
		$this->load->model('Model_krs_sp');
		$mk = $this->Model_krs_sp->cekmk($kdjd);
		foreach ($mk as $row) {
			$ini[] = $row->kd_matakuliah;
		}
		if (count($ini) != count(array_unique($ini))) {
			echo "<script>alert('Silahkan pilih 1 jadwal dari 1 matakuliah');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$this->temph_model->add_krs($npm, $kdjd);
			redirect(base_url('akademik/perbaikan/show_krs_sp'));
		}
	}

	function show_krs_sp()
	{
		$yearimprove = $this->session->userdata('ta');

		$log = $this->session->userdata('sess_login');
		$data['filter'] = $this->temph_model->list_krs_sp($log['userid'])->result();
		$data['krs'] = $this->temph_model->list_krs_sp($log['userid'])->row();

		if (is_null($data['krs'])) {
			echo "<script>alert('Tidak Ada Data Terinput !');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$select = $this->db->where('status', 1)->get('tbl_tahunakademik')->result();
			foreach ($select as $key) {
				$ta = substr($key->kode, -1);
				if ($ta == '1') {
					$ta = substr($key->kode, 0, -1);
					$ta_sp = $ta . '3';
				} elseif ($ta == '2') {
					$ta = substr($key->kode, 0, -1);
					$ta_sp = $ta . '4';
				}
			}
			$thn = $this->temph_model->cek_thsp($log['userid']);
			$data['bayar'] = $this->db->query("SELECT COUNT(npm_mahasiswa) AS byr FROM tbl_sinkronisasi_renkeu WHERE npm_mahasiswa = '" . $log['userid'] . "' AND tahunajaran = '" . $yearimprove . "' and status = '1'")->row()->byr;

			$data['page'] = "v_krs_sp";
			$this->load->view('template/template', $data);
		}
	}

	function ubah($krs, $id)
	{
		$log = $this->session->userdata('sess_login');
		$data['nim'] = $log['userid'];
		$data['filter'] = $this->temph_model->change_krs($krs)->result();

		foreach ($data['filter'] as $gigs) {
			$dats[] = $gigs->id_jadwal;
		}

		$data['unselectedmk'] = $this->temph_model->getMkSpWhenEdit($log['userid'], $dats)->result();
		// var_dump($data['unselectedmk']);

		if (count($data['filter']) == 0) {
			echo "<script>alert('Data KRS Tidak Ditemukan!');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$data['krs'] = $krs;
			$data['idjd'] = get_kd_jdl_remid($id);
			$data['page'] = "v_mhs_mksp_edit";
			$this->load->view('template/template', $data);
		}
	}

	function del_mksp($kd)
	{
		$this->db->where('status', 1);
		$hey = $this->db->get('tbl_tahunakademik')->row();
		$fixyear = $hey->kode + 2;

		$this->db->where('kd_krs', $kd)->delete('tbl_verifikasi_krs_sp');
		$this->db->where('kd_krs', $kd)->delete('tbl_krs_sp');

		$this->db->where('npm_mahasiswa', substr($kd, 0, 12));
		$this->db->where('tahunajaran', $fixyear);
		$huu = $this->db->get('tbl_sinkronisasi_renkeu')->result();

		if ($huu) {
			$this->db->where('npm_mahasiswa', substr($kd, 0, 12));
			$this->db->where('tahunajaran', $fixyear);
			$this->db->delete('tbl_sinkronisasi_renkeu');
		}

		echo "<script>alert('Data Matakuliah Terhapus!');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
	}

	##CEtak 

	function print_formulir($kode)
	{
		$this->load->library('Cfpdf');
		$log = $this->session->userdata('sess_login');


		$data['prodi'] = get_mhs_jur($log['userid']);
		$ta = $this->db->where('status', 1)->get('tbl_tahunakademik')->row();

		$data['tahun'] = substr($ta->tahun_akademik, 0, 9);
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', substr($kode, 0, 12), 'NIMHSMSMHS', 'asc')->row();

		$npm = substr($kode, 0, 12);

		$a = substr($kode, 16, 1);

		if ($a = 3) {
			$data['ganjilgenap'] = 'Ganjil';
		} elseif ($a = 4) {
			$data['ganjilgenap'] = 'Genap';
		}

		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		//$this->db->join('tbl_fakultas c', 'c.kd_fakultas = b.kd_fakultas');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$this->db->where('npm_mahasiswa', $npm);
		$this->db->order_by('id_verifikasi', 'desc');
		$dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

		$this->db->where('nid', $dsn);
		$data['dosen'] = $this->db->get('tbl_karyawan', 1)->row();

		$qq = $this->app_model->get_KDPSTMSMHS($npm)->row();

		$data['matkul'] = $this->db->query('select distinct * from tbl_krs_sp b join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
        join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
        join tbl_jadwal_matkul_sp d on d.kd_jadwal = b.kd_jadwal
        join tbl_karyawan e on e.nid = d.kd_dosen 
        where b.kd_krs = "' . $kode . '" AND d.`open` = 1 AND c.kd_prodi = "' . $qq->KDPSTMSMHS . '"')->result();

		//var_dump($data['matkul']);die();

		$this->load->view('akademik/espe_formulir', $data);
	}

	function print_sp($kode)
	{

		$this->load->library('Cfpdf');
		$log = $this->session->userdata('sess_login');

		$data['prodi'] = get_mhs_jur($log['userid']);

		$data['tahun'] = getactyear();
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', substr($kode, 0, 12), 'NIMHSMSMHS', 'asc')->row();
		$a = substr($kode, 16, 1);

		if ($a = 3) {
			$data['ganjilgenap'] = 'Ganjil';
		} elseif ($a = 4) {
			$data['ganjilgenap'] = 'Genap';
		}


		$npm = substr($kode, 0, 12);

		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.KDPSTMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$this->db->where('npm_mahasiswa', $npm);
		$this->db->order_by('id_verifikasi', 'desc');
		$dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

		$this->db->where('nid', $dsn);
		$data['dosen'] = $this->db->get('tbl_karyawan', 1)->row();

		$data['matkul'] = $this->db->query('SELECT distinct * from tbl_krs_sp b 
											join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
											join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
											join tbl_jadwal_matkul_sp d on d.kd_jadwal = b.kd_jadwal
											join tbl_karyawan e on e.nid = d.kd_dosen 
											where b.kd_krs = "' . $kode . '" 
											AND d.`open` = 1 
											AND c.kd_prodi = "' . get_mhs_jur($log['userid']) . '"')->result();

		//var_dump($data['matkul']);die();

		$this->load->view('akademik/espe', $data);
	}

	function khs_sp()
	{
		$logged = $this->session->userdata('sess_login');
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logged['userid'], 'NIMHSMSMHS', 'asc')->row();
		$data['detail_khs'] = $this->temph_model->get_all_khs_mahasiswa($logged['userid'])->result();
		$data['page'] = "v_khs_sp";
		$this->load->view('template/template', $data);
	}

	function detailkhs($npm, $id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $npm;
		}
		$data['npm'] = $nim;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $nim, 'NIMHSMSMHS', 'asc')->row();
		$data['semester'] = $a = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS, $id);

		$data['tahunakademik'] = $id;

		if ($id < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT distinct b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah from tbl_transaksi_nilai_sp a join tbl_matakuliah_copy b 
													on a.KDKMKTRLNM = b.kd_matakuliah 
													where a.THSMSTRLNM = '" . $id . "' and b.tahunakademik = '" . $id . "' and a.NIMHSTRLNM = '" . $nim . "' 
													and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();

			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '" . $nim . $id . "%'")->row();
		} else {
			$data['detail_khs'] = $this->db->query("SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs_sp a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
													where a.kd_krs like '" . $nim . $id . "%' and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();

			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs_sp where kd_krs like '" . $nim . $id . "%'")->row();
		}

		$logged = $this->session->userdata('sess_login');

		$data['page'] = 'akademik/detail_khs_sp';
		$this->load->view('template/template', $data);
	}
}

/* End of file Perbaikan.php */
/* Location: ./application/modules/akademik/controllers/Perbaikan.php */
