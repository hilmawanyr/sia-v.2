<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viewkrs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '512M');
		error_reporting(0);
		$this->load->library('slug');
		$this->load->model('temph_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(64)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		//201510235005
		$this->load->library('user_agent');
	}

	function index()
	{
		$loginSession 	= $this->session->userdata('sess_login');
	 	$idUserGroup 	= explode(',', $loginSession['id_user_group']);
        $numberOfGroup 	= count($idUserGroup);
        
        for ($i=0; $i < $numberOfGroup; $i++) { 
            $grup[] = $idUserGroup[$i];
        }

		# if login as prodi or staff prodi
		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['getData']=$this->app_model->getkrsmhsbyprodi($loginSession['userid'])->result();
			$data['page'] = 'akademik/viewkrs_select';

		# if login as mahasiswa
		} elseif ((in_array(5, $grup))) {
			$data['krs'] = $this->app_model->get_all_krs_mahasiswa($loginSession['userid'])->result();
			$data['page'] = 'akademik/viewkrs_view';

		# if login as fakultas
		} elseif ((in_array(9, $grup))) {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$loginSession['userid'],'prodi','asc')->result();
			$data['page'] = 'akademik/viewkrs_fak';

		# if login as baa / admin
		} else {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['page'] = 'akademik/viewkrs_fak';
		}
		$this->load->view('template/template',$data);
	}

	function view($id)
	{
		$tahunakademik = getactyear();
				
		$logged = $this->session->userdata('sess_login');
		if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
			$primary = $idkd->kd_krs;
		}

		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup)) || (in_array(10, $grup)) || (in_array(9, $grup)) || (in_array(19, $grup))) {
			$data['pembimbing']	= $this->app_model->get_pembimbing_krs($primary)->row_array();
			$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($primary)->result();
			$data['kd_krs'] 	= $primary;
			$data['page'] 		= 'akademik/viewkrs_detail';
			$this->load->view('template/template',$data);

		} elseif ((in_array(5, $grup))) {
			$data['pembimbing'] = $this->app_model->get_pembimbing_krs($primary)->row_array();
			$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($primary)->result();
			$data['bimbingan'] 	= $this->db->where('kd_krs',$primary)->get('tbl_bimbingan_krs')->result();
			$data['status_krs'] = $this->db->where('kd_krs', $primary)->get('tbl_verifikasi_krs')->row();
			$data['npm']		= $logged['userid'];
			$data['ta']			= $tahunakademik;
			$data['kd_krs'] 	= $primary;
			$data['cek'] 		= $this->app_model->validjadwal($primary)->result();
			$data['page'] 		= 'akademik/viewkrs_detail_bymhs';
			$this->load->view('template/template',$data);
		}	
	}
	function view2($id)
	{
		$tahunakademik = getactyear();
				
		$logged = $this->session->userdata('sess_login');
		if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
			$primary = $idkd->kd_krs;
		}

		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup)) or (in_array(10, $grup)) or (in_array(9, $grup)) or (in_array(19, $grup))) {
			$data['pembimbing']=$this->app_model->get_pembimbing_krs($primary)->row_array();
			$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($primary)->result();
			//var_dump($data['detail_krs']);die();

			$data['kd_krs'] = $primary;
			$data['page'] = 'akademik/viewkrs_detail';
			$this->load->view('template/template',$data);
		} elseif ((in_array(5, $grup))) {
			//var_dump($idkd->npm_mahasiswa.' == '.substr($logged['userid'], 0,12));exit();
			if (($idkd->npm_mahasiswa != (substr($logged['userid'], 0,12))) and (substr($id, 0,12) != (substr($logged['userid'], 0,12)))) {
				echo "love you";
			} else {
				$data['pembimbing']= $this->app_model->get_pembimbing_krs($primary)->row_array();
				$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($primary)->result();
				$data['bimbingan'] = $this->db->where('kd_krs',$primary)->get('tbl_bimbingan_krs')->result();
				$data['status_krs'] = $this->db->where('kd_krs', $primary)->get('tbl_verifikasi_krs')->row();
				//var_dump($data['detail_krs']);die($primary);
				$data['npm']= $logged['userid'];
				$data['ta']			= $tahunakademik;
				$data['kd_krs'] = $primary;
				$data['cek'] = $this->app_model->validjadwal($primary)->result();
				$data['page'] = 'akademik/viewkrs_detail_bymhs';
				$this->load->view('template/template',$data);
			}
		}	
	}

	function view_tes($id)
	{
		$logged = $this->session->userdata('sess_login');
		if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
			$primary = $idkd->kd_krs;
		}

		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup)) or (in_array(10, $grup)) or (in_array(9, $grup)) or (in_array(19, $grup))) {
			$data['pembimbing']=$this->app_model->get_pembimbing_krs_tes($primary)->row_array();
			$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa_tes($primary)->result();
			//var_dump($data['detail_krs']);die();

			$data['kd_krs'] = $primary;
			$data['page'] = 'akademik/viewkrs_detail';
			$this->load->view('template/template',$data);
		} elseif ((in_array(5, $grup))) {
			//var_dump($idkd->npm_mahasiswa.' == '.substr($logged['userid'], 0,12));exit();
			if (($idkd->npm_mahasiswa != (substr($logged['userid'], 0,12))) and (substr($id, 0,12) != (substr($logged['userid'], 0,12)))) {
				echo "love you";
			} else {
				$data['pembimbing']= $this->app_model->get_pembimbing_krs_tes($primary)->row_array();
				$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa_tes($primary)->result();
				$data['bimbingan'] = $this->db->where('kd_krs',$primary)->get('tbl_bimbingan_krs')->result();
				$data['status_krs'] = $this->db->where('kd_krs', $primary)->get('tbl_verifikasi_krs_tes')->row();
				//var_dump($data['detail_krs']);die($primary);

				$data['kd_krs'] = $primary;
				$data['cek'] = $this->app_model->validjadwal_tes($primary)->result();
				$data['page'] = 'akademik/viewkrs_detail_bymhs_tes';
				$this->load->view('template/template',$data);
			}
		}	
	}

	function kosongkan_jadwal($id){
		//die('test'.$id.'');
		//$logged = $this->session->userdata('sess_login');
		//$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
		$data = array('kd_jadwal' => null );

		$this->db->where('id_krs', $id);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs')
						->where('id_krs',$id)
						->get()->row();

		redirect(base_url('akademik/viewkrs/view/'.$q->kd_krs.''));

	}

	function viewkrsmhs($id)
	{
		if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetail('tbl_verifikasi_krs_tes','slug_url',$id,'npm_mahasiswa','asc')->row();
			$primary = $idkd->npm_mahasiswa;
			$kdkrs = $idkd->kd_krs;
		}
		$data['slugcoy'] = $this->app_model->getdetail('tbl_verifikasi_krs','npm_mahasiswa',$primary,'npm_mahasiswa','asc')->row();
		$data['krs'] = $this->app_model->get_all_krs_mahasiswa($primary)->result();
		// $data['krs'] = $this->temph_model->get_krs_mahasiswa($id)->result(); //var_dump($data['krs']);exit();
		$data['npm'] = $primary;
		$mhs = $this->app_model->get_detail('tbl_mahasiswa','NIMHSMSMHS',$id,'NIMHSMSMHS','asc')->row();
		$data['smtr'] = $this->app_model->get_semester($mhs->SMAWLMSMHS);
		// $data['page'] = 'akademik/v_krs_mahasiswa';
		$data['page'] = 'akademik/viewkrs_view';
		$this->load->view('template/template',$data);
	}

	function viewkrsmhs_2($kd)
	{
		$user = $this->session->userdata('sess_login');
		$data['usergroup'] = $user['id_user_group'];
		// gunakan untuk tes
		// $data['pembimbing'] = $this->app_model->get_pembimbing_krs_tes($kd)->row_array();

		// gunakan untuk live
		$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kd)->row_array();

		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$data['nama'] = $this->db->query("SELECT NMMHSMSMHS,NIMHSMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".substr($kd, 0,12)."'")->row_array();
		$data['smt'] = $this->db->query("SELECT * from tbl_krs where kd_krs = '".$kd."' limit 1")->row_array();
		$data['akad'] = get_thnajar(substr($kd, 12,5));

		$data['kd_krs'] = $kd;

		// gunakan untuk tes
		// $data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_tes($kd)->result();

		// gunakan untuk live
		$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa($kd)->result();

		$data['page'] = 'akademik/v_krs_mahasiswa';
		$this->load->view('template/template',$data);
	}

	function viewkrsprodi()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah 	= explode(',', $logged['id_user_group']);
		$jmlh 	= count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		# login as prodi or staff prodi
		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$prodi = $logged['userid'];

		# login as admin / baa
		} elseif ((in_array(10, $grup)) || (in_array(1, $grup))) {
			$prodi = $this->input->post('jurusan', TRUE);
		}

		$tahun 					= $this->input->post('tahun', TRUE);
		$akad 					= $this->input->post('akad');
		$data['tahunakademik'] 	= $akad;
		$data['getData']		= $this->app_model->getkrsmhsbyprodi($prodi,$tahun,$akad)->result();
		$data['page'] 			= 'akademik/viewkrs_prodi';

		$this->load->view('template/template',$data);
	}

	function viewview($id)
	{
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($id)->result();
		//var_dump($data['detail_krs']);die();

		$data['kd_krs'] = $id;
		$data['page'] = 'akademik/viewkrs_detail';
		$this->load->view('template/template',$data);
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}
	
	function get_jadwal(){
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$user = $this->session->userdata('sess_login');

		$prodi  = $user['userid'];

		$mhs  = $this->app_model->get_jurusan_mhs($npm)->row();
		
		// $data = $this->db->query('SELECT * 
		// 					FROM tbl_jadwal_matkul a
		// 					LEFT JOIN tbl_ruangan b ON a.`kelas` = b.`id_ruangan`
		// 					LEFT JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 					LEFT JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 					WHERE  a.`kd_jadwal` LIKE "'.$mhs->KDPSTMSMHS.'%" AND a.`kd_matakuliah` = "'.$kd_matakuliah.'" AND d.`kd_prodi` LIKE "'.$mhs->KDPSTMSMHS.'"');

		// if ($prodi == 61201 || $prodi == 55201) {
		// 	$data =  $this->db->query("SELECT a.*,b.`kuota`,b.`ruangan`,c.`nama`,b.`kode_ruangan`,(SELECT COUNT(z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal`) AS jumlah FROM tbl_jadwal_matkul a
		// 						left JOIN tbl_ruangan b ON a.`kd_ruangan` = b.`id_ruangan`
		// 						JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 						JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 						WHERE d.`kd_prodi` = '".$prodi."' AND a.`kd_matakuliah` = '".$kd_matakuliah."' AND a.`kd_jadwal` LIKE '".$prodi."%' and a.kd_tahunajaran IN (20161,20152)
		// 						")->result();
		// }else{
			$data = $this->app_model->get_pilih_jadwal($kd_matakuliah,$prodi,$tahunakademik->kode)->result();	
		//}
		
		
                
		$js = json_encode($data);
		echo $js;
	}

	function get_jumlah(){
		$kd_jadwal = $_POST['kd_jadwal'];

		$data = $this->db->query("SELECT COUNT(npm_mahasiswa) as jumlah FROM tbl_krs WHERE kd_jadwal = '".$kd_jadwal."'")->result();
                
		$js = json_encode($data);
		echo $js;
	}
	
	function save_jadwal(){
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');
		
		$this->app_model->updatedata_krs($kd_matakuliah, $kd_krs, $data);
		redirect($this->agent->referrer());
	}

	function printkrs($id)
	{
		//$data['npm'] = $logged = $this->session->userdata('sess_login');
		$data['kd_krs'] = $id;
		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		$nim		= substr($id, 0,12);
		$tah		= substr($id, 12,5);
		$renkeu = $this->app_model->renkeu_printKRS($nim,$tah)->row();
		if ($renkeu == false)
		{
			echo "<script>alert('Maaf, Silahkan menyelesaikan administrasi terlebih dahulu');
			document.location.href='".base_url()."';</script>";
		} 
		elseif ($renkeu == true)
		{
		
			// $data['prodi']= $this->db->where('NIMHSMSMHS',$data['npm'])
			// 					 	 ->get('tbl_mahasiswa')->row();

			//$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$data['npm'].'"')->row();
			$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE kd_krs = "'.$id.'" order by kd_krs_verifikasi desc limit 1')->row();

			$data['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
									->from('tbl_verifikasi_krs')
									->like('npm_mahasiswa', $data['npm'],'both')
									->get()->row();
			
			$a=substr($id, 16,1);

			if ($a == 1) {
				$b = 'Ganjil';
			} else {
				$b = 'Genap';
			}

			$data['gg']  = $b;

			$this->load->view('welcome/print/krs_pdf',$data);
		}
	}

	function printkrs_satuan()
	{
		$id='201210225265201528';
		//$data['npm'] = $logged = $this->session->userdata('sess_login');
		$data['kd_krs'] = $id;
		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		// $data['prodi']= $this->db->where('NIMHSMSMHS',$data['npm'])
		// 					 	 ->get('tbl_mahasiswa')->row();

		//$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$data['npm'].'"')->row();
		$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$npm.'" and tahunajaran = "'.$ta.'" order by kd_krs_verifikasi desc limit 1')->row();

		$data['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
								->from('tbl_verifikasi_krs')
								->like('npm_mahasiswa', $data['npm'],'both')
								->get()->row();
		
		$a=substr($id, 16,1);

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}

		$data['gg']  = $b;

		$this->load->view('welcome/print/krs_pdf_tyas',$data);
	}

	function newss($id)
	{
		$sql = 'SELECT a.*,b.`NIMHSMSMHS`,b.`NMMHSMSMHS`,c.`nama_matakuliah`,c.`sks_matakuliah` FROM tbl_krs a
				JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
				JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
				WHERE b.`KDPSTMSMHS` = "'.$id.'" AND c.kd_prodi = "'.$id.'" ORDER BY a.`npm_mahasiswa` ASC';
		$data['mhs'] = $this->db->query($sql)->result();
		//var_dump($data);
		$this->load->view('print_excel_krs_mhs',$data);
	}

	function create_sess()
	{
		$this->session->set_userdata('krssatuan',$this->input->post('npm'));
		redirect(base_url('akademik/viewkrs/loadkrssatuan'));
	}

	function loadkrssatuan()
	{
		$log = $this->session->userdata('sess_login');
		$nama = $this->session->userdata('krssatuan');
		$data['load'] = $this->temph_model->loadkrssatuan($nama,$log['userid'])->result();
		$data['page'] = 'v_krs_satuan';
		$this->load->view('template/template', $data);
	}

	function loadkrsmhs($npm)
	{
		$data['krs'] = $this->app_model->get_all_krs_mahasiswa($npm)->result();
		$data['nama'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row_array();
		$data['page'] = 'akademik/v_all_krs_mhs';
		$this->load->view('template/template', $data);
	}

	function loadjadwaluji($id)
	{
		$this->load->model('ujian_model');
		$data['uts'] = $this->ujian_model->getDetailJdl($id,1);
    	$data['uas'] = $this->ujian_model->getDetailJdl($id,2);
    	$this->load->view('modal_dtljdl', $data);
	}

	public function removeCourse($kdkrs, $kdmk)
	{
		$user = $this->session->userdata('sess_login');

		$get_krs = $this->db->where('kd_krs', $kdkrs)->where('kd_matakuliah', $kdmk)->get('tbl_krs')->row();
		
		$storeData = [
			'npm_mahasiswa' => $get_krs->npm_mahasiswa,
			'kd_matakuliah' => $get_krs->kd_matakuliah,
			'semester_krs' 	=> $get_krs->semester_krs,
			'kd_krs'		=> $get_krs->kd_krs,
			'kd_jadwal'		=> $get_krs->kd_jadwal,
			'deleted_at'	=> date('Y-m-d'),
			'deleted_by'	=> $user['userid']
		];
		$this->db->insert('tbl_deleted_krs', $storeData);

		$this->db->where('kd_krs', $kdkrs);
		$this->db->where('kd_matakuliah', $kdmk);
		$this->db->delete('tbl_krs');

		echo "<script>alert('Data berhasil dihapus!');history.go(-1);</script>";
	}

}

/* End of file viewkrs.php */
/* Location: .//C/Users/danum246/Desktop/viewkrs.php */