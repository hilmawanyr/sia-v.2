<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_soal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(77)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(1, $grup)) or (in_array(10, $grup))) {
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/upload_select';
		} elseif (in_array(9, $grup)) {
			$data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $nik, 'id_prodi' , 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/upload_select';
		} elseif (in_array(8, $grup)) {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/upload_select';
		} else {
			//var_dump($this->session->userdata('tahunajaran'));
			if ($this->session->userdata('tahunajaran') != '' or $this->session->userdata('tahunajaran') != NULL) {
				$data['rows'] = $this->db->query('SELECT  a.`kd_dosen`,a.`kd_matakuliah`,a.`kd_tahunajaran`,(SELECT COUNT(c.`kd_jadwal`) FROM tbl_krs c WHERE c.`kd_jadwal` = a.`kd_jadwal`) AS jml,a.`kd_jadwal`,b.`kd_matakuliah`,a.`kelas`,b.`nama_matakuliah`,b.`sks_matakuliah`,a.`approve_uts`,a.`approve_uas`,a.`id_jadwal`,a.`status_uts`,a.`status_uas`,a.`komentar_uts`,a.`komentar_uas`
											FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											WHERE a.`kd_dosen` = '.$nik.' AND a.`kd_tahunajaran` = "'.$this->session->userdata("tahunajaran").'"')->result();						
				/* $data['mhs']  = $this->db->query('SELECT COUNT(c.`kd_jadwal`),a.`kd_matakuliah`,b.`nama_matakuliah`,a.`kelas`
													FROM tbl_jadwal_matkul a
													JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
													JOIN tbl_krs c ON a.`kd_jadwal` = c.`kd_jadwal` 
													GROUP BY a.`kd_jadwal`')->result(); */
				$data['nm_dosen']  = $this->db->where('nid', $nik)->get('tbl_karyawan')->row();
				$data['nik_dosen'] = $nik;
				$data['page'] = 'akademik/upload_view';
				
											
			} else {
				$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
				$data['page'] = 'akademik/upload_select';
			}
			
			// $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
			// 								LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
			// 								WHERE a.`kd_dosen` = '.$nik.'')->result();
			// $data['nm_dosen']  = $this->db->where('nid', $nik)->get('tbl_karyawan')->row();
			// $data['nik_dosen'] = $nik;
			// $data['page'] = 'akademik/upload_view';
			
		}
		$this->load->view('template/template',$data);	
	}
	function app_uts($id)
	{
		$data = array(
			'approve_uts'			=> 1
		);
		$this->app_model->updatedata('tbl_jadwal_matkul','id_jadwal',$id,$data);
		redirect(base_url('akademik/upload_soal'));
	}
	function app_uas($id)
	{
		$data = array(
			'approve_uas'			=> 1
		);
		$this->app_model->updatedata('tbl_jadwal_matkul','id_jadwal',$id,$data);
		redirect(base_url('akademik/upload_soal'));
	}
	function save_session(){

		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if (in_array(9, $grup)) {
			$fakultas = $nik;

			$jurusan = $this->input->post('jurusan');

	        $tahunajaran = $this->input->post('tahunajaran');

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $nik);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);
			redirect(base_url('akademik/upload_soal/load_view'));
		} elseif (in_array(8, $grup)) {
			$jurusan = $nik;

			$namajur = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$nik,'kd_prodi','asc')->row();	

			$tahunajaran = $this->input->post('tahunajaran');

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $namajur->kd_fakultas);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);		
			redirect(base_url('akademik/upload_soal/load_view'));
		} elseif ((in_array(1, $grup)) or (in_array(10, $grup))) { 

			$fakultas = $this->input->post('fakultas');

			$jurusan = $this->input->post('jurusan');

	        $tahunajaran = $this->input->post('tahunajaran');

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $fakultas);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);
			redirect(base_url('akademik/upload_soal/load_view'));
		} else {
			$tahunajaran = $this->input->post('tahunajaran');

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$data['rows'] = $this->db->query('SELECT  a.`kd_dosen`,a.`kd_matakuliah`,a.`kd_tahunajaran`,(SELECT COUNT(c.`kd_jadwal`) FROM tbl_krs c WHERE c.`kd_jadwal` = a.`kd_jadwal`) AS jml,a.`kd_jadwal`,b.`kd_matakuliah`,a.`kelas`,b.`nama_matakuliah`,b.`sks_matakuliah`,a.`approve_uts`,a.`approve_uas`,a.`id_jadwal`,a.`status_uts`,a.`status_uas`,a.`komentar_uts`,a.`komentar_uas`
											FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE a.`kd_dosen` = '.$nik.' AND a.`kd_tahunajaran` = "'.$this->session->userdata("tahunajaran").'"')->result();	
			$data['nm_dosen']  = $this->db->where('nid', $nik)->get('tbl_karyawan')->row();
			$data['nik_dosen'] = $nik;
			$data['page'] = 'akademik/upload_view';
			$this->load->view('template/template',$data);
		}

	}

	function back(){
		$this->session->sess_destroy();
		$this->index();
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function view_soal($id)
	{
		$data['id'] = $id;
		$this->load->view('akademik/upload_soal',$data);
	}
	function status_soal_uts($id)
	{
		$data['row']  = $this->db->query('SELECT * from tbl_jadwal_matkul where id_jadwal = "'.$id.'"')->row();
		$data['id'] = $id;
		$this->load->view('status_upload_uts',$data);
	}

	function status_soal_uas($id)
	{
		$data['row']  = $this->db->query('SELECT * from tbl_jadwal_matkul where id_jadwal = "'.$id.'"')->row();
		$data['id'] = $id;
		$this->load->view('akademik/status_upload_uas',$data);
	}

	function save_uts($id)
	{
		$status = $this->input->post('status_uts');
		if ($status == 'Terima'){
			$data = array(
				'status_uts'			=> $status,
				'komentar_uts'			=> $this->input->post('komentar_uts')
			);
		} elseif ($status == 'Revisi'){
			$data = array(
				'status_uts'			=> $status,
				'approve_uts'			=> 0,
				'komentar_uts'			=> $this->input->post('komentar_uts')
			);
		}
		$this->app_model->updatedata('tbl_jadwal_matkul','id_jadwal',$id,$data);
		echo "<script>alert('Sukses');
		history.go(-1);</script>";
	}
	function save_uas($id)
	{
		$status = $this->input->post('status_uas');
		if ($status == 'Terima'){
			$data = array(
				'status_uas'			=> $status,
				'komentar_uas'			=> $this->input->post('komentar_uas')
			);
		} elseif ($status == 'Revisi'){
			$data = array(
				'status_uas'			=> $status,
				'approve_uas'			=> 0,
				'komentar_uas'			=> $this->input->post('komentar_uas')
			);
		}
		$this->app_model->updatedata('tbl_jadwal_matkul','id_jadwal',$id,$data);
		echo "<script>alert('Sukses');
		history.go(-1);</script>";
	}
	function load_view()
	{
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];

		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		
		if ( (in_array(8, $grup)) ) {
			$data['dosen'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
                                                            JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                                            WHERE jdl.`kd_dosen` = kry.`nid` AND (mk.nama_matakuliah not like "tesis%" and mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and mk.kd_prodi = "'.$nik.'" AND jdl.kd_tahunajaran = '.$this->session->userdata('tahunajaran').') AS sks 
								FROM tbl_jadwal_matkul jdl
								JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
								RIGHT JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
								WHERE mk.`kd_prodi` = "'.$nik.'" AND NOT (jdl.`kd_dosen` <=> NULL) AND NOT (jdl.`kd_dosen` <=> "") and jdl.kd_tahunajaran = '.$this->session->userdata('tahunajaran').'')->result();

		}elseif ((in_array(10, $grup))) {
			$data['dosen'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
                                                            JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                                            WHERE jdl.`kd_dosen` = kry.`nid` AND (mk.nama_matakuliah not like "tesis%" and mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and mk.kd_prodi = "'.$nik.'" AND jdl.kd_tahunajaran = '.$this->session->userdata('tahunajaran').') AS sks 
								FROM tbl_jadwal_matkul jdl
								JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
								RIGHT JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
								WHERE NOT (jdl.`kd_dosen` <=> NULL) AND NOT (jdl.`kd_dosen` <=> "") AND jdl.kd_tahunajaran = '.$this->session->userdata('tahunajaran').'')->result();
		}
		$data['page'] = 'akademik/upload_list';
		$this->load->view('template/template',$data);
	}

	function show_dokumen_dosen($id)
	{
		$data['rows'] = $this->db->query('SELECT  a.`kd_dosen`,a.`kd_matakuliah`,a.`kd_tahunajaran`,(SELECT COUNT(c.`kd_jadwal`) FROM tbl_krs c WHERE c.`kd_jadwal` = a.`kd_jadwal`) AS jml,a.`kd_jadwal`,b.`kd_matakuliah`,a.`kelas`,b.`nama_matakuliah`,b.`sks_matakuliah`,a.`approve_uts`,a.`approve_uas`,a.`id_jadwal`,a.`status_uts`,a.`status_uas`,a.`komentar_uts`,a.`komentar_uas`
											FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`									
											WHERE a.`kd_dosen` = '.$id.' and b.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'" AND a.kd_tahunajaran = '.$this->session->userdata('tahunajaran').' AND (b.nama_matakuliah not like "tesis%" and b.nama_matakuliah not like "skripsi%" and b.nama_matakuliah not like "kerja praktek%" and b.nama_matakuliah not like "magang%" and b.nama_matakuliah not like "kuliah kerja%")')->result();
		$data['nm_dosen']  = $this->db->where('nid', $id)->get('tbl_karyawan')->row();
		$data['nik_dosen'] = $id;
		$data['page'] = 'akademik/upload_view';
		$this->load->view('template/template',$data);
	}

	function save_data($id)
	{
		$pecah = explode('911', $id);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $pecah[1], 'id_jadwal', 'ASC')->row();
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
        $cek = $this->db->query("select * from tbl_dokumen_ujian where userid = '".$nik."' and kd_jadwal = '".$kode->kd_jadwal."' and tipe = ".$pecah[0]." ")->row();
		$data['kd_jadwal'] = $kode->kd_jadwal;	
		$data['tanggal_upload'] = date('Y-m-d h:i:s');
		$data['tipe'] = $pecah[0];
		$data['userid'] = $nik;
		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$nama = underscore($_FILES['userfile']['name']);
            $config['allowed_types'] = 'pdf|PDF';
            $config['max_size'] = '1000000';
            $config['file_name'] = $nama;
            $config['upload_path'] = './upload/course/';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("userfile")) {
                $error = array('error' => $this->upload->display_errors());
                //die("gagal");
				echo "<script>alert('Gagal');
				document.location.href='".base_url()."akademik/upload_soal';</script>";
            } else {
                $aa = array('upload_data' => $this->upload->data());
                //die("sukses");
            $data['nama_dokumen'] = $config['file_name'];
        if ($cek == TRUE) {
        	$this->app_model->updatedata('tbl_dokumen_ujian','id_dokumen',$cek->id_dokumen,$data);
        } else {
	        $this->app_model->insertdata('tbl_dokumen_ujian',$data);
        }
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."akademik/upload_soal';</script>";
            }
        }
		/* echo '<pre>';
		print_r($error);exit(); */



	}


}

/* End of file Upload_soal.php */
/* Location: ./application/modules/akademik/controllers/Upload_soal.php */
