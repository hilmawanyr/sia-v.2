<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenDosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth','refresh');
		}
		$this->load->library('Cfpdf');
	}

	function event($id)
	{
		$user = $this->session->userdata('sess_login');

		$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = '.$id.'')->row();

		$kodeprodi = substr($data['rows']->kd_jadwal, 0,5);

		$data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE b.`kd_prodi` = "'.$kodeprodi.'"')->row();

		if ($data['rows']->gabung > 0) {
			$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" OR kd_jadwal = "'.$data['rows']->referensi.'"
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->referensi.'")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		} else {
			$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" 
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->kd_jadwal.'")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}

		$this->load->view('beritacara', $data);
	}

}

/* End of file AbsenDosen.php */
/* Location: ./application/modules/akademik/controllers/AbsenDosen.php */