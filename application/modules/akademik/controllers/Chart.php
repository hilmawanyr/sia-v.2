<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(116)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['tek'] = $this->db->query('SELECT c.`fakultas`,COUNT(DISTINCT a.npm_mahasiswa) AS jmlh FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
										ON b.`kd_prodi` = a.`kd_jurusan` JOIN  tbl_fakultas c
										ON c.`kd_fakultas`=b.`kd_fakultas` WHERE a.`tahunajaran` = "20151" GROUP BY c.`kd_fakultas`')->result();

		$data['nol'] = $this->db->query('SELECT c.prodi,COUNT(npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi c
										ON c.kd_prodi=a.kd_jurusan GROUP BY c.kd_prodi')->result();

		$data['status'] = $this->db->query('SELECT b.`prodi`,COUNT(npm) as jml from tbl_status_mahasiswa a join tbl_mahasiswa g on a.`npm`=g.`NIMHSMSMHS`
											join tbl_jurusan_prodi b on b.`kd_prodi`=g.`KDPSTMSMHS` WHERE (a.status = "C" OR a.status = "N" OR a.status = "CA")
											group by b.`kd_prodi`')->result();

		$data['kelas'] = $this->db->query("SELECT CASE WHEN waktu_kelas = 'PG' THEN 'PAGI' WHEN waktu_kelas = 'SR' THEN 'SORE' ELSE 'P2K' END AS wkt_kls,
											COUNT(DISTINCT kelas) AS jumjum FROM tbl_jadwal_matkul a JOIN tbl_krs b
											ON a.kd_jadwal=b.kd_jadwal
											WHERE waktu_kelas IN ('PG','SR','PK') and kd_tahunajaran = '20151' AND b.kd_krs LIKE CONCAT(npm_mahasiswa,'20151%')
											GROUP BY waktu_kelas")->result();
		
		$data['beban'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
									(SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                    WHERE jdl.`kd_dosen` = kry.`nid` and (mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'" and jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%") AS jumlah 
                                    FROM tbl_jadwal_matkul jdl

									JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
									JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
									where jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'"
									and jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%"
									ORDER BY kry.`nama` ASC ')->result();

		$data['dosen_tetap'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS jumlh_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) != '00' and SUBSTRING(kd_dosen,2,2) != '0' and kd_tahunajaran = '20151'")->row();

		$data['dosen_tidak'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS juml_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) = '00' and kd_tahunajaran = '20151'")->row();

		$data['prodi'] = $this->app_model->getdata('tbl_jurusan_prodi', 'kd_prodi', 'ASC')->result();

		$data['tahun']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

		$data['dos']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

		$data['kelasflt']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

		$data['page'] = "v_chart";
		$this->load->view('template/template', $data);
	}

	function sesi()
	{
		$a = $this->input->post('bbn');
		$this->session->set_userdata('sess',$a);
		redirect(base_url('akademik/chart/load'));
	}

	function get_jurusan($id){

		$this->session->set_userdata('prodi', $id);
        $data['beban'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
									(SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                    WHERE jdl.`kd_dosen` = kry.`nid` and
                                    (mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%"
                                    	and mk.nama_matakuliah not like "kuliah kerja%") and mk.kd_prodi = "'.$id.'" and jdl.kd_jadwal like "'.$id.'%") AS jumlah 
                                    FROM tbl_jadwal_matkul jdl
									JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
									JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
									where mk.kd_prodi = "'.$id.'"
									ORDER BY kry.`nama` ASC ')->result();
        $this->load->view('v_beban', $data);
	}

	function get_beban_thajaran($thn)
	{
		$this->session->set_userdata('beban_tahunajar',$thn );
		$data['beban'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
									(SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                    WHERE jdl.`kd_dosen` = kry.`nid` and (mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%"
                                    	and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$thn.'" and mk.kd_prodi = "'.$this->session->userdata('prodi').'"
										and jdl.kd_jadwal like "'.$this->session->userdata('prodi').'%") AS jumlah 
                                    FROM tbl_jadwal_matkul jdl

									JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
									JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
									where jdl.kd_tahunajaran = "'.$thn.'" and mk.kd_prodi = "'.$this->session->userdata('prodi').'"
									and jdl.kd_jadwal like "'.$this->session->userdata('prodi').'%"
									ORDER BY kry.`nama` ASC ')->result();
		$this->load->view('v_beban_thn', $data);
	}

	function get_jml_mhs($id)
	{
		$data['jumlah'] = $this->db->query('SELECT c.`fakultas`,COUNT(DISTINCT a.npm_mahasiswa) AS jmlh FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
											ON b.`kd_prodi` = a.kd_jurusan JOIN  tbl_fakultas c
											ON c.`kd_fakultas`=b.`kd_fakultas` WHERE a.`tahunajaran` = "'.$id.'" GROUP BY c.`kd_fakultas`')->result();
		$data['summ'] = $id;

		$this->session->set_userdata('tahunakademik',$id);
		//var_dump($id);exit();
		if ($data['jumlah'] == '' or $data['jumlah'] == NULL) {
			echo "<script>alert('Data Jumlah Mahasiswa ".$id." Tidak Ditemukan');</script><h2 style='color:grey;'>Tidak ada data</h2>";
		} else {
			
			$this->load->view('v_jml_mhs', $data);
		}
		
		// 
	}

	function tahun_cuti($ide)
	{
		// echo $id;
		$data['status'] = $this->db->query('SELECT  b.`kd_prodi`,b.`prodi`,COUNT(npm) AS jml FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa g ON a.`npm`=g.`NIMHSMSMHS`
											JOIN tbl_jurusan_prodi b ON b.`kd_prodi`=g.`KDPSTMSMHS` WHERE (a.status = "C" OR a.status = "N" OR a.status = "CA")
											AND a.tahunajaran = "'.$ide.'" GROUP BY b.`kd_prodi`')->result();
		
		$data['thu'] = $ide;

		$this->session->set_userdata('tahuncutsay',$ide);

		if ($data['status'] == '' or $data['status'] == NULL) {
			echo "<script>alert('Data Jumlah Status Mahasiswa ".$ide." Tidak Ditemukan');</script><h2 style='color:grey;'>Tidak ada data</h2>";
		} else {
			$this->load->view('v_tahun_cuti', $data);
		}
	}

	function jml_kelas($id)
	{
		$sesikelas = $this->session->set_userdata('sesi_kelas',$id);
		$data['tahunkelas'] = $id;
		$data['kelas'] = $this->db->query("SELECT CASE WHEN waktu_kelas = 'PG' THEN 'PAGI' WHEN waktu_kelas = 'SR' THEN 'SORE' ELSE 'P2K' END AS wkt_kls,
											COUNT(DISTINCT a.kelas) AS jumjum FROM tbl_jadwal_matkul a JOIN tbl_krs b
											ON a.kd_jadwal=b.kd_jadwal
											WHERE waktu_kelas IN ('PG','SR','PK') AND a.kd_tahunajaran = '".$id."' AND b.kd_krs LIKE CONCAT(npm_mahasiswa,'".$id."%')
											GROUP BY waktu_kelas")->result();
		if ($data['kelas'] == '' or $data['kelas'] == NULL) {
			echo "<script>alert('Data Jumlah Status Mahasiswa ".$id." Tidak Ditemukan');</script><h2 style='color:grey;'>Tidak ada data</h2>";
		} else {
			$this->load->view('v_filter_kls', $data);
		}
	}

	function dosen($id)
	{
		$data['tahundosen'] = $id;
		$this->session->set_userdata('sesi_dosen',$id);
		$data['dosen_tetap'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS jumlh_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) != '00' and SUBSTRING(kd_dosen,2,2) != '0' and kd_tahunajaran = '".$id."'")->row();

		$data['dosen_tidak'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS juml_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) = '00' and kd_tahunajaran = '".$id."'")->row();

		if ($data['dosen_tetap'] == '' and $data['dosen_tidak'] == NULL) {
			echo "<script>alert('Data Status Dosen ".$id." Tidak Ditemukan');</script><h2 style='color:grey;'>Tidak ada data</h2>";
		} else {
			$this->load->view('v_dosen', $data);
		}
	}

	function jml($qr)
	{
		$data['nik'] = $this->db->query('SELECT b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` where c.`kd_fakultas`="'.$qr.'" group by b.`kd_prodi`')->result();
		//var_dump($data['tek']);exit();
	}

	function export_excel_sts()
	{
		$tahun = $this->session->userdata('tahunakademik');
		$data['tahun'] = $tahun;
		// var_dump($tahun);exit();
		$data['jumlah'] = $this->db->query('SELECT a.`fakultas`,COUNT(b.`kd_prodi`) AS juml FROM tbl_fakultas a
											JOIN tbl_jurusan_prodi b ON a.`kd_fakultas`=b.`kd_fakultas` GROUP BY a.`kd_fakultas`')->result();

		$data['angkt'] = $this->db->query("SELECT SUBSTR(npm_mahasiswa,1,4) AS tahun FROM tbl_verifikasi_krs WHERE tahunajaran = '".$tahun."' GROUP BY SUBSTR(npm_mahasiswa,1,4)")->result();
		$data['hit'] = count($data['angkt']);

		$data['status'] = $this->db->query('SELECT b.kd_prodi,b.`prodi`,COUNT(DISTINCT a.`npm_mahasiswa`) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
											ON b.`kd_prodi` = a.`kd_jurusan` JOIN  tbl_fakultas c
											ON c.`kd_fakultas`= b.`kd_fakultas` WHERE a.`tahunajaran` = "'.$tahun.'"
											GROUP BY b.`kd_prodi` ORDER BY c.kd_fakultas')->result();
		$this->load->view('excel_sts', $data);
	}

	function excel_kelas()
	{
		$sess_kelas = $this->session->userdata('sesi_kelas');
		// var_dump($sess_kelas);exit();
		$data['kelas'] = $this->db->query("SELECT CASE WHEN waktu_kelas = 'PG' THEN 'PAGI' WHEN waktu_kelas = 'SR' THEN 'SORE' ELSE 'P2K' END AS wkt_kls,
											COUNT(DISTINCT kelas) AS jumjum FROM tbl_jadwal_matkul a JOIN tbl_krs b
											ON a.kd_jadwal=b.kd_jadwal
											WHERE waktu_kelas IN ('PG','SR','PK') and kd_tahunajaran = '".$sess_kelas."' AND b.kd_krs LIKE CONCAT(npm_mahasiswa,'".$sess_kelas."%')
											GROUP BY waktu_kelas")->result();
		$this->load->view('excel_kelas', $data);
	}

	function excel_status()
	{
		$sesi_dosen = $this->session->userdata('sesi_dosen');
		// var_dump($sesi_dosen);exit();
		$data['dosen_tetap'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS jumlh_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) != '00' and SUBSTRING(kd_dosen,2,2) != '0' and kd_tahunajaran = '".$sesi_dosen."'")->row();
		$data['dosen_tidak'] = $this->db->query("SELECT COUNT(DISTINCT kd_dosen) AS juml_dsn FROM tbl_jadwal_matkul WHERE SUBSTRING(kd_dosen,1,2) = '00' and kd_tahunajaran = '".$sesi_dosen."'")->row();
		$this->load->view('excel_status', $data);
	}

	function excel_beban()
	{
		var_dump($this->session->userdata('prodi'),$this->session->userdata('beban_tahunajar'));exit();
		$data['weight'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
											(SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
		                                    JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
		                                    WHERE jdl.`kd_dosen` = kry.`nid` and (mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%"
	                                    	and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('beban_tahunajar').'"
											and mk.kd_prodi = "'.$this->session->userdata('prodi').'"
											and jdl.kd_jadwal like "'.$this->session->userdata('prodi').'%") AS jumlah 
		                                    FROM tbl_jadwal_matkul jdl

											JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
											JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
											where jdl.kd_tahunajaran = "'.$this->session->userdata('beban_tahunajar').'" and mk.kd_prodi = "'.$this->session->userdata('prodi').'"
											and jdl.kd_jadwal like "'.$this->session->userdata('prodi').'%"
											ORDER BY kry.`nama` ASC ')->result();
		$this->load->view('excel_beban', $data);
	}

	function excel_cuti()
	{
		$tahuncuti = $this->session->userdata('tahuncutsay');
		// var_dump($tahuncuti);exit();
		$data['cuty'] = $this->db->query("SELECT c.`prodi`,COUNT(CASE WHEN a.`status` = 'C' THEN 'CUTI' END) AS 'CUTI', COUNT(CASE WHEN a.`status` = 'CA' THEN 'CUTI AKTIF' END) AS 'CUTAK',
											COUNT(CASE WHEN a.`status` = 'N' THEN 'NON AKTIF' END) AS 'NON',
											COUNT(*) AS 'TOTAL'
											FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
											JOIN tbl_jurusan_prodi c ON c.`kd_prodi`=b.`KDPSTMSMHS`
											WHERE a.`status` IN ('C','CA','N')  and a.`tahunajaran` = '".$tahuncuti."'
											GROUP BY c.`kd_prodi`")->result();
		$this->load->view('excel_cuti', $data);
	}
}

/* End of file Chart.php */
/* Location: .//tmp/fz3temp-1/Chart.php */