<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_ujian extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo '<script>alert("Sesi anda telah habis!")</script>';
			redirect(base_url('auth/logout'),'refresh');
		}
		$this->load->model('ujian_model');
		$this->load->model('hilman_model');
        error_reporting(0);
	}

	public function index()
	{
		$sess = $this->session->userdata('sess_login');
		$data['grup'] = $sess['id_user_group'];

		// if user as BAA
		if ($sess['id_user_group'] == 10) {
			$data['prod'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();	
		}
		// end if user as BAA

		$data['date'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = "v_jadwalujian";
		$this->load->view('template/template', $data);
	}

	function save_sess()
	{
		$userlog = $this->session->userdata('sess_login');

		// if login as BAA
		if ($userlog['id_user_group'] == 10) {
			$prod = $this->input->post('prodi', TRUE);
			$this->session->set_userdata('sessjdlbaa',$prod);	
		}

		$date = $this->input->post('year', TRUE);

		$array = array(
			'akad' => $date
		);
		
		$this->session->set_userdata('sessjadwaluji',$array);

        // if login as lecture
        if ($userlog['id_user_group'] != 10 and $userlog['id_user_group'] != 8) {
            redirect(base_url('akademik/jadwal_ujian/loaddatadosen'));
        } else {
            redirect(base_url('akademik/jadwal_ujian/loaddata'));
        }
	}

    function loaddatadosen()
    {
        $log = $this->session->userdata('sess_login');
        $uid = $log['userid'];
        $sesijadwal   = $this->session->userdata('sessjadwaluji');
        $data['year'] = $sesijadwal['akad'];
        $data['page'] = "v_loadjadwaluji";
        $data['list'] = $this->ujian_model->loadByDosen($sesijadwal['akad'],$uid);
        $this->load->view('template/template', $data);
    }

	function loaddata()
	{
        $sess = $this->session->userdata('sess_login');

        if ($sess['id_user_group'] == 10) {
            $uid = $this->session->userdata('sessjdlbaa');
        } else {
            $uid = $sess['userid'];    
        }
        
        $data['uid'] = $uid;
		$sesijadwal   = $this->session->userdata('sessjadwaluji');
		$data['year'] = $sesijadwal['akad'];
		$data['page'] = "v_loadjadwaluji";
        $data['list'] = $this->ujian_model->loaduji($sesijadwal['akad'],$uid);
		$this->load->view('template/template', $data);
	}

	function getdata()
	{
		$userlog = $this->session->userdata('sess_login');

		if ($userlog['id_user_group'] == 6 || $userlog['id_user_group'] == 7 || $userlog['id_user_group'] == 8) {
			$uid = $userlog['userid'];
		} else {
			$baa = $this->session->userdata('sessjdlbaa');
			$uid = $baa;
		}

		$sesjdl = $this->session->userdata('sessjadwaluji');

		$start 	= $_POST['start'];
		$length	= $_POST['length'];
        $search = $_POST['search']['value'];
            
        if(empty($search)) {            
            $list   = $this->ujian_model->loaduji($sesjdl['akad'],$uid,$start,$length);
        } else {
            $list   = $this->ujian_model->loaduji($sesjdl['akad'],$uid,$start,$length,$search);
        }

        // var_dump($list->result());exit();
		// $list 	= $this->ujian_model->loaduji($sesjdl['akad'],$uid,$start,$length);

        $data 	= array();
        $no 	= 1;
        foreach ($list->result() as $field) {
            $row = array();
            $row[] = $no;
            $row[] = $field->kelas;
            $row[] = $field->kd_matakuliah;
            $row[] = get_nama_mk($field->kd_matakuliah, $uid);
            $row[] = $field->kd_dosen;
            $row[] = '<button data-target="#addmod" data-toggle="modal" onclick="loadaddmod('.$field->id_jadwal.')" class="btn btn-primary">
            			<i class="icon icon-plus"></i>
            		  </button>';
           	$row[] = '<button data-target="#seemod" data-toggle="modal" onclick="loaddetmod('.$field->id_jadwal.')" class="btn btn-success">
            			<i class="icon icon-eye-open"></i>
            		  </button>';
            $row[] = '<button data-target="#edtmod" data-toggle="modal" onclick="loadedtmod('.$field->id_jadwal.')" class="btn btn-warning">
            			<i class="icon icon-pencil"></i>
            		  </button>';
 
            $data[] = $row;
            $no++;
        }

        $arrs = array('kd_jadwal' => $userlog['userid'],'kd_tahunajaran' => $sesjdl['akad']);
        $totalRecs = $this->hilman_model->moreLike($arrs,'tbl_jadwal_matkul', 'after')->num_rows();
 
        $output = array(
            "draw" 				=> intval($_POST['draw']),
            "recordsTotal" 		=> $totalRecs,
            "recordsFiltered"	=> $list->num_rows(),
            "data" 				=> $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	function loaddosen()
	{
		$this->db->distinct();

        $this->db->select("*");

        $this->db->from('tbl_karyawan');

        $this->db->like('nama', $_GET['term'], 'both');

        $this->db->or_like('nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

	            'dosen'		=> $row->nid,

	            'value' 	=> $row->nid.' - '.$row->nama

            );

        }

        echo json_encode($data);

    }

    function loadroom()
	{
		$this->db->distinct();

        $this->db->select("*");

        $this->db->from('tbl_ruangan');

        $this->db->like('ruangan', $_GET['term'], 'both');

        $this->db->or_like('kode_ruangan', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

            	'idroom' 	=> $row->id_ruangan,

	            'ruang'		=> $row->kode_ruangan,

	            'value' 	=> $row->kode_ruangan.' - '.$row->ruangan

            );

        }

        echo json_encode($data);

    }

    function addmodal($id)
    {
    	$jdl = $this->session->userdata('sessjadwaluji');
    	$data['data'] = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();

        // kondisi untuk cek ketersediaan data uts atau uas
        $uts = array('id_jadwal' => $id, 'tipe_uji' => 1);
        $uas = array('id_jadwal' => $id, 'tipe_uji' => 2);
        $data['cek1'] = $this->hilman_model->moreLike($uts, 'tbl_jadwaluji', 'after')->num_rows();
        $data['cek2'] = $this->hilman_model->moreLike($uas, 'tbl_jadwaluji', 'after')->num_rows();
    	
    	$data['akad'] = $jdl['akad'];
    	$this->load->view('modal_addjdluji', $data);
    }

    function addjadwal()
    {
    	extract(PopulateForm());

    	$prods = substr($prodi, 0,5);

    	$uid = $this->session->userdata('sess_login');

    	$strdate = ymdDate($start);

        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime($strdate);
        $date->add(new DateInterval('P5D'));
        
        $endate	= $date->format('Y-m-d');

        $arr['id_jadwal'] 		= $id;
        $arr['kd_jadwal']       = $prodi;
        $arr['prodi']			= $prods;
        $arr['start_date']		= $strdate;
        $arr['end_date']		= $endate;
        $arr['tipe_uji']		= $tipe;
        $arr['tahunakademik'] 	= $tahun;
        $arr['userinput']		= $uid['userid'];
        $arr['kd_ruang']		= $hiddenroom;
        // $arr['pengawas']		= $hiddenguide;
        $arr['dateinput']		= date('Y-m-d');
        $arr['start_time']      = $starttime;

        $this->app_model->insertdata('tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');
    }

    function detmodal($id)
    {
    	$data['uts'] = $this->ujian_model->getDetailJdl($id,1);
    	$data['uas'] = $this->ujian_model->getDetailJdl($id,2);
    	$this->load->view('modal_dtljdl', $data);
    }

    function edtmodal($id)
    {
    	$data['uts'] = $this->ujian_model->getDetailJdl($id,1);
    	$data['uas'] = $this->ujian_model->getDetailJdl($id,2);
    	$this->load->view('modal_edtjdl', $data);
    }

    function changeDataUts()
    {
    	extract(PopulateForm());

    	$uid = $this->session->userdata('sess_login');

    	$strdate = $tgl;

        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime($strdate);
        $date->add(new DateInterval('P5D'));
        
        $endate	= $date->format('Y-m-d');

        $data = array('id_jadwal' => $id, 'tipe_uji' => $typ);

        $arr['start_date']		= $strdate;
        $arr['end_date']		= $endate;
        $arr['userinput']		= $uid['userid'];
        $arr['kd_ruang']		= $hiddenrooms;
        $arr['start_time']      = $starttimeuts;
        // $arr['pengawas']		= $hiddenwatch;
        $arr['dateinput']		= date('Y-m-d');

        // var_dump($arr);exit();
        $this->hilman_model->updateMoreParams($data, 'tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');
    }

    function changeDataUas()
    {
    	extract(PopulateForm());

    	$uid = $this->session->userdata('sess_login');

    	$strdate = $uastgl;

        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime($strdate);
        $date->add(new DateInterval('P5D'));
        
        $endate	= $date->format('Y-m-d');

        $data = array('id_jadwal' => $uasid, 'tipe_uji' => $uastyp);

        $arr['start_date']		= $strdate;
        $arr['end_date']		= $endate;
        $arr['userinput']		= $uid['userid'];
        $arr['kd_ruang']		= $uashiddenroom;
        $arr['start_time']      = $starttimeuas;
        $arr['pengawas']		= $uashiddenwatch;
        $arr['dateinput']		= date('Y-m-d');

        // var_dump($arr);exit();
        $this->hilman_model->updateMoreParams($data, 'tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');

    }

    function exportxls()
    {
        $year   = $this->session->userdata('sessjadwaluji');
        $uid    = $this->session->userdata('sess_login');
        $type   = $this->input->post('tipe', TRUE);
        $data['tipe'] = $type;
        $data['akad'] = $year['akad'];
        $data['user'] = $uid['userid'];

        if ($uid['id_user_group'] == 10 or $uid['id_user_group'] == 8) {
            $data['load'] = $this->ujian_model->export($year['akad'],$uid['userid'],$type);
        } else {
            $data['load'] = $this->ujian_model->export_byDosen($year['akad'],$uid['userid'],$type);
        }
        
        $this->load->view('xls_jadwaluji', $data);
    }

    function whoseHavent()
    {
        $data['user'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();
        $data['page'] = "v_belumupload";
        $this->load->view('template/template', $data);
    }

}

/* End of file Jadwal_ujian.php */
/* Location: .//tmp/fz3temp-1/Jadwal_ujian.php */