<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dospa extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	 $akses = $this->role_model->cekakses(6 || 7)->result();
		// 	 if ($akses != TRUE) {
		// 	 	redirect('home','refresh');
		// 	 }
		// } else {
		// 	redirect('auth','refresh');
		// }
	}

	public function index()
	{
		//$user = $this->session->userdata('sess_login');	
		//$data['tahun_masuk'] = $this->db->query("SELECT DISTINCT mhs.`TAHUNMSMHS` FROM tbl_mahasiswa mhs WHERE mhs.`KDPSTMSMHS` = ".$user['userid']." ORDER BY mhs.`TAHUNMSMHS`")->result();

		$data['page'] = 'v_dospa_select';
		$this->load->view('template/template', $data);
	}

	function save_sess()
	{
		$dsn_now = $this->input->post('kd_dosen');
		$dsn_new = $this->input->post('kd_dosen_baru');

		$this->session->set_userdata('dsn_now', $dsn_now);
		$this->session->set_userdata('dsn_new', $dsn_new);

		redirect(base_url('akademik/dospa/list_mhs_pa'), 'refresh');
	}

	function save_sess2()
	{
		$dsn_now = $this->input->post('kd_dosen');
		$dsn_new = $this->input->post('kd_dosen_baru');

		$this->session->set_userdata('dsn_now', $dsn_now);
		$this->session->set_userdata('dsn_new', $dsn_new);

		redirect(base_url('akademik/dospa/list_mhs_pa2'), 'refresh');
	}

	function list_mhs_pa()
	{

		$data['mhs'] = $this->db->distinct()
			->select('pa.kd_dosen,kry.nama,mhs.NIMHSMSMHS,mhs.NMMHSMSMHS')->from('tbl_pa pa')
			->join('tbl_mahasiswa mhs', 'pa.npm_mahasiswa = mhs.NIMHSMSMHS', 'left')
			->join('tbl_karyawan kry', 'kry.nid = pa.kd_dosen', 'left')
			->where('pa.kd_dosen', $this->session->userdata('dsn_now'))
			->where('mhs.STMHSMSMHS !=', 'L')
			->where('mhs.STMHSMSMHS !=', 'D')
			->order_by('mhs.NIMHSMSMHS', 'asc')
			->get()->result();

		$data['page'] = 'v_dospa_list_mhs';
		$this->load->view('template/template', $data);
	}

	function list_mhs_pa2()
	{

		$data['mhs'] = $this->db->distinct()
			->select('pa.id_pembimbing,kry.nama,mhs.NIMHSMSMHS,mhs.NMMHSMSMHS')->from('tbl_verifikasi_krs pa')
			->join('tbl_mahasiswa mhs', 'pa.npm_mahasiswa = mhs.NIMHSMSMHS', 'left')
			->join('tbl_karyawan kry', 'kry.nid = pa.id_pembimbing', 'left')
			->where('pa.id_pembimbing', $this->session->userdata('dsn_now'))
			->where('mhs.STMHSMSMHS !=', 'L')
			->where('mhs.STMHSMSMHS !=', 'D')
			->order_by('mhs.NIMHSMSMHS', 'asc')
			->get()->result();

		$data['page'] = 'v_dospa_list_mhs2';
		$this->load->view('template/template', $data);
	}

	function save()
	{
		$user = $this->session->userdata('sess_login');
		$jml_mhs = count($this->input->post('npm'));


		$show = $this->input->post('show_mhs');
		$this->session->set_userdata('show_mhs', $show);

		$q = $this->db->where_in('npm_mahasiswa', $show)
			->get('tbl_pa')->result();


		for ($i = 0; $i < $jml_mhs; $i++) {
			$npm = $this->input->post('npm[' . $i . ']', TRUE);

			//echo $npm;exit();

			$object = array('kd_dosen' => $this->session->userdata('dsn_new'), 'audit_time' => date('Y-m-d H:i:s'), 'audit_user' => $user['userid']);

			$q = $this->db->where('npm_mahasiswa', $npm)
				->update('tbl_pa', $object);
		}

		//redirect('akademik/dospa/show_change','refresh');
		echo "<script>alert('Berhasil Update Data');window.location = '" . base_url('akademik/dospa/show_change') . "';</script>";
	}
	function save2()
	{
		$actyear = getactyear();

		$user = $this->session->userdata('sess_login');
		$jml_mhs = count($this->input->post('npm'));


		$show = $this->input->post('show_mhs');
		$this->session->set_userdata('show_mhs', $show);

		$q = $this->db->where_in('npm_mahasiswa', $show)
			->get('tbl_verifikasi_krs')->result();


		for ($i = 0; $i < $jml_mhs; $i++) {
			$npm = $this->input->post('npm[' . $i . ']', TRUE);

			//echo $npm;exit();

			$object = array('id_pembimbing' => $this->session->userdata('dsn_new'));

			$q = $this->db->like('kd_krs', $npm . $actyear, 'AFTER')
				->update('tbl_verifikasi_krs', $object);

			$this->db->where('npm_mahasiswa', $npm)->update('tbl_pa', ['kd_dosen' => $this->session->userdata('dsn_new')]);
		}

		//redirect('akademik/dospa/show_change','refresh');
		echo "<script>alert('Berhasil Update Data');window.location = '" . base_url('akademik/dospa/') . "';</script>";
	}

	function show_change()
	{
		$data['mhs'] = $this->db->select('pa.kd_dosen,kry.nama,mhs.NIMHSMSMHS,mhs.NMMHSMSMHS')->from('tbl_pa pa')
			->join('tbl_mahasiswa mhs', 'pa.npm_mahasiswa = mhs.NIMHSMSMHS', 'left')
			->join('tbl_karyawan kry', 'kry.nid = pa.kd_dosen', 'left')
			->where('mhs.STMHSMSMHS !=', 'L')
			->where('mhs.STMHSMSMHS !=', 'D')
			->order_by('mhs.NMMHSMSMHS', 'asc')
			->where_in('npm_mahasiswa', $this->session->userdata('show_mhs'))
			->get()->result();

		$data['page'] = 'v_dospa_list_mhs';
		$this->load->view('template/template', $data);
	}
	function show_change2()
	{
		$data['mhs'] = $this->db->select('pa.id_pembimbing,kry.nama,mhs.NIMHSMSMHS,mhs.NMMHSMSMHS')->from('tbl_verifikasi_krs pa')
			->join('tbl_mahasiswa mhs', 'pa.npm_mahasiswa = mhs.NIMHSMSMHS', 'left')
			->join('tbl_karyawan kry', 'kry.nid = pa.id_pembimbing', 'left')
			->where('mhs.STMHSMSMHS !=', 'L')
			->where('mhs.STMHSMSMHS !=', 'D')
			->order_by('mhs.NMMHSMSMHS', 'asc')
			->where_in('npm_mahasiswa', $this->session->userdata('show_mhs'))
			->get()->result();

		$data['page'] = 'v_dospa_list_mhs2';
		$this->load->view('template/template', $data);
	}

	function load_dosen_autocomplete()
	{

		$bro = $this->session->userdata('sess_login');

		$prodi   = $bro['userid'];

		$this->db->distinct();

		$this->db->select("a.id_kary,a.nid,a.nama");

		$this->db->from('tbl_karyawan a');

		// $this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');

		// $this->db->like('b.kd_jadwal', $prodi, 'after');

		$this->db->like('a.nama', $_GET['term'], 'both');

		$this->db->or_like('a.nid', $_GET['term'], 'both');

		$sql  = $this->db->get();

		$data = array();

		foreach ($sql->result() as $row) {

			$data[] = array(

				'id_kary'       => $row->id_kary,

				'nid'           => $row->nid,

				'value'         => $row->nid . ' - ' . $row->nama

			);
		}

		echo json_encode($data);
	}
}

/* End of file dosen_pembimbing.php */
/* Location: ./application/controllers/dosen_pembimbing.php */
