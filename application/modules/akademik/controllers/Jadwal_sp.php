<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Jadwal_sp extends CI_Controller
{



    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sess_login') == TRUE) {
            $user = $this->session->userdata('sess_login');
            $akses = $this->role_model->cekakses(153)->result();

            if ($akses != TRUE) {
                echo "<script>alert('Akses Tidak Diizinkan');document.location.href='" . base_url() . "home';</script>";
            }
        } else {
            redirect('auth', 'refresh');
        }
    }



    public function index()

    {
        $this->session->unset_userdata('tahunajaran');
        $this->session->unset_userdata('id_fakultas_prasyarat');
        $this->session->unset_userdata('nama_fakultas_prasyarat');
        $this->session->unset_userdata('id_jurusan_prasyarat');
        $this->session->unset_userdata('nama_jurusan_prasyarat');



        $logged = $this->session->userdata('sess_login');
        //var_dump($logged['id_user_group']);exit();
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);
        for ($i = 0; $i < $jmlh; $i++) {
            $grup[] = $pecah[$i];
        }

        if ((in_array(1, $grup)) || (in_array(10, $grup))) {

            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

            //$data['tahunajar']=$this->db->where('kode >=', 20161)->get('tbl_tahunakademik')->result();
            $data['select'] = $this->db->where('kode =', getactyear())->get('tbl_tahunakademik')->result();

            $data['page'] = 'jdl_sp_select';

            $this->load->view('template/template', $data);
        } elseif ((in_array(8, $grup))) {

            $q = $this->db->query('

                            SELECT * FROM tbl_jurusan_prodi

                            JOIN tbl_fakultas ON tbl_fakultas.`kd_fakultas` = tbl_jurusan_prodi.`kd_fakultas`

                            WHERE tbl_jurusan_prodi.`kd_prodi`="' . $logged['userid'] . '"')->row();

            $this->db->select('kode');
            $this->db->from('tbl_tahunakademik');
            $this->db->where('status', 1);
            $thn_ak = $this->db->get()->row();

            $data['tahunajar'] = $this->db->get('tbl_tahunakademik')->result();


            $tahunajaran = substr($thn_ak->kode, 0, 4);

            $this->session->set_userdata('id_fakultas_prasyarat', $q->kd_fakultas);

            $this->session->set_userdata('nama_fakultas_prasyarat', $q->fakultas);

            $this->session->set_userdata('id_jurusan_prasyarat', $q->kd_prodi);

            $this->session->set_userdata('nama_jurusan_prasyarat', $q->prodi);

            $data['page'] = 'v_jdl_kuliah_select';

            $this->load->view('template/template', $data);
        } elseif ((in_array(9, $grup))) {
            $data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
            $data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $logged['userid'], 'kd_fakultas', 'ASC')->result();
            $data['page'] = 'jdl_kuliah_fak';

            $this->load->view('template/template', $data);
        } elseif ((in_array(5, $grup))) {
            $this->session->set_userdata('tahunajaran', '20162');

            $logged = $this->session->userdata('sess_login');

            $getprd = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logged['userid'], 'NIMHSMSMHS', 'asc')->row()->KDPSTMSMHS;

            $getjur = $this->db->query("select a.*,b.fakultas from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas where a.kd_prodi = '" . $getprd . "'")->row();

            $this->session->set_userdata('id_fakultas_prasyarat', $getjur->kd_fakultas);

            $this->session->set_userdata('nama_fakultas_prasyarat', $getjur->fakultas);

            $this->session->set_userdata('id_jurusan_prasyarat', $getjur->kd_prodi);

            $this->session->set_userdata('nama_jurusan_prasyarat', $getjur->prodi);

            redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));
        } else {
            $data['page'] = 'jdl_kuliah_mhs';
            $this->load->view('template/template', $data);
        }
    }



    function get_jurusan($id)
    {

        $jrs = explode('-', $id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

        $out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='" . $row->kd_prodi . "-" . $row->prodi . "'>" . $row->prodi . "</option>";
        }

        $out .= "</select>";

        echo $out;
    }



    function load_dosen($idk)
    {
        $data['jadwal'] = $this->db->where('id_jadwal', $idk)->get('tbl_jadwal_matkul')->row();
        $data['dosen'] = $this->db->get('tbl_karyawan')->result();
        $this->load->view('penugasan_dosen', $data);
    }

    function load_dosen_autocomplete()
    {
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        $this->db->like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = [

                'id_kary'       => $row->id_kary,

                'nid'           => $row->nid,

                'value'         => $row->nama
            ];
        }

        echo json_encode($data);
    }

    function save_session_prodi()
    {
        $tahunajaran = $this->input->post('tahunajaran');
        $this->session->set_userdata('tahunajaran', $tahunajaran);
        redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));
    }



    function save_session()
    {
        $fakultas = explode('-', $this->input->post('fakultas'));
        $jurusan = explode('-', $this->input->post('jurusan'));
        $tahunajaran = $this->input->post('tahunajaran');

        $this->session->set_userdata('tahunajaran', $tahunajaran);
        $this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);
        $this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);
        $this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);
        $this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);

        redirect(base_url('akademik/jadwal_sp/view_matakuliah'));
    }

    function view_matakuliah()
    {
        $sesi = $this->session->userdata('sess_login');
        $data['sess'] = $sesi['id_user_group'];
        $data['tabs'] = $this->db->query('SELECT DISTINCT semester_kd_matakuliah as semester_matakuliah FROM tbl_kurikulum_matkul
                                        WHERE semester_kd_matakuliah IS NOT NULL 
                                        AND semester_kd_matakuliah != "" ORDER BY semester_kd_matakuliah ASC')->result();

        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $data['page'] = 'jdl_sp_detail';
        $this->load->view('template/template', $data);
    }

    function get_detail_matakuliah_by_semester($id)
    {
        $user = $this->session->userdata('sess_login');
        $kode = $this->session->userdata("id_jurusan_prasyarat");
        $data = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
                                    join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah
                                    WHERE a.kd_prodi = ' . $kode . ' AND b.semester_kd_matakuliah = ' . $id . ' 
                                    AND b.`kd_kurikulum` LIKE "%' . $kode . '%"
                                    order by nama_matakuliah asc')->result();

        $list = "<option> -- </option>";

        foreach ($data as $row) {

            $list .= "<option value='" . $row->id_matakuliah . "'>" . $row->kd_matakuliah . " - " . $row->nama_matakuliah . "</option>";
        }

        die($list);
    }

    function get_kode_mk($id)
    {

        $data = $this->db->query('select * from tbl_matakuliah where id_matakuliah = ' . $id . '')->row();

        echo $data->kd_matakuliah;
    }



    function get_lantai($id)
    {

        $data = $this->app_model->get_lantai($id);

        $list = "<option> -- </option>";

        foreach ($data as $row) {

            $list .= "<option value='" . $row->id_lantai . "'>" . $row->lantai . "</option>";
        }

        die($list);
    }



    function get_ruangan($id)
    {

        $data = $this->app_model->get_ruang($id);

        $list = "<option> -- </option>";

        foreach ($data as $row) {

            $list .= "<option value='" . $row->id_ruangan . "'>" . $row->kode_ruangan . ' - ' . $row->ruangan . "</option>";
        }



        die($list);
    }



    function getdosen()

    {
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        //$this->db->where('jabatan_id', 8);
        $this->db->like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();
        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = array(

                'nid'      => $row->nid,
                'value'  => $row->nid . ' - ' . $row->nama,

            );
        }
        echo json_encode($data);
    }

    function penugasan($id)
    {
        $data['id_jadwal'] = $id;
        $this->load->view('penugasan', $data);
    }

    function save_dosen()
    {
        $nid  = $this->input->post('kd_dosen');
        $jdl = $this->input->post('id_jadwal');
        $data = array('kd_dosen' => $nid);

        $this->db->where('id_jadwal', $jdl)->update('tbl_jadwal_matkul_sp', $data);
        redirect('akademik/jadwal_sp/view_matakuliah', 'refresh');
    }

    function save_data()
    {
        date_default_timezone_set("Asia/Jakarta");
        extract(PopulateForm());

        $q = $this->db->where('id_matakuliah', $nama_matakuliah)
            ->get('tbl_matakuliah')->row();

        $w = ($q->sks_matakuliah) * 50;

        $ambil_kd = $this->db->query('select * from tbl_matakuliah where id_matakuliah = ' . $nama_matakuliah . '')->row();
        $kd_mk = $ambil_kd->kd_matakuliah;

        //$id_jur = $this->session->userdata('id_jurusan_prasyarat');
        $session = $this->session->userdata('sess_login');

        $prodi = $this->session->userdata('id_jurusan_prasyarat');

        $gpass = NULL;
        $n = 6; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";

        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $gpass .= substr($chr, $rIdx, 1);
        }

        $cc = $this->db->query('SELECT count(kd_jadwal) as jml FROM tbl_jadwal_matkul_sp WHERE kd_jadwal LIKE "' . $prodi . '%" and kd_tahunajaran = ' . $this->session->userdata('tahunajaran') . '')->row();
        $hasil = $cc->jml;
        //var_dump($cc);
        //die($hasil);

        if ($hasil == 0) {
            $hasilakhir = "" . $prodi . "/" . $gpass . "/001";
        } elseif ($hasil < 10) {
            $hasilakhir = "" . $prodi . "/" . $gpass . "/00" . ($hasil + 1);
        } elseif ($hasil < 100) {
            $hasilakhir = "" . $prodi . "/" . $gpass . "/0" . ($hasil + 1);
        } elseif ($hasil < 1000) {
            $hasilakhir = "" . $prodi . "/" . $gpass . "/" . ($hasil + 1);
        } elseif ($hasil < 10000) {
            $hasilakhir = "" . $prodi . "/" . $gpass . "/" . ($hasil + 1);
        }
        //edit danu
        $time2 = strtotime($jam_masuk) + $w * 60;

        $jumlah_time = date('H:i', $time2);

        $session = $this->session->userdata('sess_login');

        $user = $session['username'];

        //Cek Pemakaian Ruangan
        //$cekpemakaianruangan = $this->db->query("select * from tbl_jadwal_matkul_sp where (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."'")->result();

        //if (count($cekpemakaianruangan) > 0) {
        // echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";
        //} else {
        $data = array(

            'kelas'         => $kelas,

            'waktu_kelas'   => $st_kelas,

            'kd_jadwal'     => $hasilakhir,

            'hari'          => $hari_kuliah,

            'kd_matakuliah' => $kd_mk,

            'kd_ruangan'    => $ruangan,

            'waktu_mulai'   => $jam_masuk,

            'waktu_selesai' => $jumlah_time,

            'kd_tahunajaran' => $this->session->userdata('tahunajaran'),

            'audit_date'    => date('Y-m-d H:i:s'),

            'audit_user'    => $user,

            'open'    => 0

        );

        $this->db->insert('tbl_jadwal_matkul_sp', $data);
        redirect('akademik/jadwal_sp/view_matakuliah', 'refresh');
        //}
    }

    function update_dosen()
    {

        $dosen = $this->input->post('kd_dosen');
        $id    = $this->input->post('id_jadwal');
        $jadwal = $this->db->where('id_jadwal', $id)
            ->get('tbl_jadwal_matkul_sp')->row();

        $jadwal_dosen = $this->db->query('SELECT * FROM tbl_jadwal_matkul_sp jdl 
                                          WHERE jdl.`kd_tahunajaran` = "' . $this->session->userdata('tahunajaran') . '" AND 
                                          jdl.`kd_dosen` = "' . $dosen . '" AND jdl.`hari` = ' . $jadwal->hari . ' AND jdl.`id_jadwal` != ' . $id . '')->result();

        $jadwal_masuk1 = $jadwal->waktu_mulai;
        $jadwal_keluar1 = $jadwal->waktu_selesai;

        $msk1 = explode(":", $jadwal_masuk1);
        $menit_masuk1 = $msk1[0] * 60 + $msk1[1];

        $klr1 = explode(":", $jadwal_keluar1);
        $menit_keluar1 = $klr1[0] * 60 + $klr1[1];

        foreach ($jadwal_dosen as $key) {

            $jadwal_masuk2 = $key->waktu_mulai;
            $jadwal_keluar2 = $key->waktu_selesai;

            $msk2 = explode(":", $jadwal_masuk2);
            $menit_masuk2 = $msk2[0] * 60 + $msk2[1];

            $klr2 = explode(":", $jadwal_keluar2);
            $menit_keluar2 = $klr2[0] * 60 + $klr2[1];


            //Cek Jadwal Dosen
            //if (($menit_masuk1 > $menit_masuk2 && $menit_masuk1 < $menit_keluar2) || ($menit_keluar1 > $menit_masuk2 && $menit_keluar1 < $menit_keluar2) || ($menit_masuk1 == $menit_masuk2)) {
            //    echo "<script>alert('Jadwal Dosen Bentrok');
            //    document.location.href='".base_url()."perkuliahan/jdl_kuliah/view_matakuliah';</script>";
            //    exit();
            //}


        }


        $data = array('kd_dosen' => $dosen);

        $this->db->where('id_jadwal', $id)

            ->update('tbl_jadwal_matkul_sp', $data);

        redirect('perkuliahan/jdl_kuliah/view_matakuliah', 'refresh');
    }

    function delete($id)
    {
        $kd = get_kd_jdl_remid($id);
        $hasil = $this->db->where('kd_jadwal', $kd)
            ->get('tbl_krs_sp');
        if ($hasil->num_rows() > 0) {
            echo "<script>alert('Tidak dapat menghapus jadwal, Jadwal telah digunakan');</script>";
        } else {
            $this->db->query('delete from tbl_jadwal_matkul_sp where id_jadwal = ' . $id . '');
            echo "<script>alert('Jadwal berhasil di hapus');</script>";
        }
        redirect('akademik/jadwal_sp/view_matakuliah', 'refresh');
    }

    function edit_jadwal($id)
    {
        //die($id);
        $session = $this->session->userdata('sess_login');

        $user = $session['username'];

        if (
            $user == '24201' || $user == '25201' || $user == '26201' || $user == '32201' || $user == '55201' || $user == '61201'
            || $user == '62201' || $user == '70201' || $user == '73201' || $user == '74201'
        ) {

            $data['rows'] = $this->db->query('SELECT DISTINCT * FROM tbl_jadwal_matkul_sp mk
                                LEFT JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nik` = mk.`kd_dosen`
                                WHERE mk.`id_jadwal` = ' . $id . ' AND SUBSTR(mk.kd_jadwal,1,5) = a.kd_prodi')->row();
        } else {

            $data['rows'] = $this->db->query('SELECT DISTINCT * FROM tbl_jadwal_matkul_sp mk
                                LEFT JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nik` = mk.`kd_dosen`
                                WHERE mk.`id_jadwal` = ' . $id . ' AND SUBSTR(mk.kd_jadwal,1,5) = a.kd_prodi')->row();
        }

        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $this->load->view('jdl_sp_edit', $data);
    }


    function update_data_matakuliah()
    {
        date_default_timezone_set("Asia/Jakarta");

        extract(PopulateForm());

        $q = $this->db->where('id_matakuliah', $nama_matakuliah)

            ->get('tbl_matakuliah')->row();

        $w = ($q->sks_matakuliah) * 50;

        $ambil_kd = $this->db->query('select * from tbl_matakuliah where id_matakuliah = ' . $nama_matakuliah . '')->row();
        $kd_mk = $ambil_kd->kd_matakuliah;


        $id_jur = $this->session->userdata('id_jurusan_prasyarat');

        $q_jur = $this->db->where('kd_prodi', $id_jur)

            ->get('tbl_jurusan_prodi')->row();


        $time2 = strtotime($jam_masuk) + $w * 60;

        $jumlah_time = date('H:i', $time2);

        //$cekpemakaianruangan = $this->db->query("select * from tbl_jadwal_matkul_sp where id_jadwal != ".$id_jadwal." and (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."' and id_jadwal != ".$id_jadwal." ")->result();

        //if (count($cekpemakaianruangan) > 0) {
        // var_dump("select * from tbl_jadwal_matkul_sp where (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."' and id_jadwal != ".$id_jadwal." ");exit();
        //  echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";
        //} else {

        $data = array(

            'kelas'         => $kelas,

            'hari'          => $hari_kuliah,

            'kd_matakuliah' => $kd_mk,

            'kd_ruangan'    => $ruangan,

            'waktu_mulai'   => $jam_masuk,

            'waktu_selesai' => $jumlah_time,

            'waktu_kelas'   => $st_kelas


        );

        $this->db->where('id_jadwal', $id_jadwal)
            ->update('tbl_jadwal_matkul_sp', $data);

        redirect(base_url('akademik/jadwal_sp/view_matakuliah'));
        //}

    }

    function update_data_matakuliah_prodi()
    {
        date_default_timezone_set("Asia/Jakarta");

        $data = array(

            'kelas'         => $this->input->post('kelas'),

            'kd_ruangan'    => $this->input->post('ruangan')

        );

        $this->db->where('id_jadwal', $this->input->post('idjad'))
            ->update('tbl_jadwal_matkul', $data);

        redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));
    }

    function view_matakuliah_test()
    {



        // die($this->session->userdata('id_fakultas_prasyarat').'-'.

        // $this->session->userdata('nama_fakultas_prasyarat').'-'.

        // $this->session->userdata('id_jurusan_prasyarat').'-'.

        // $this->session->userdata('nama_jurusan_prasyarat'));



        //$data['matakuliah'] = $this->app_model->get_detail_jdl_matakuliah($this->session->userdata('id_fakultas_prasyarat'), $this->session->userdata('id_jurusan_prasyarat'));



        $data['matakuliah'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul mk
                                LEFT JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                LEFT JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                WHERE mk.`kd_jadwal` LIKE "%' . $this->session->userdata('id_jurusan_prasyarat') . '%"
                                AND a.`kd_prodi` = "' . $this->session->userdata('id_jurusan_prasyarat') . '"')->result();


        $data['rows'] = $this->db->get('tbl_jadwal_matkul')->result();



        $data['matkul'] = $this->app_model->get_detail_matakuliah($this->session->userdata('id_fakultas_prasyarat'), $this->session->userdata('id_jurusan_prasyarat'));

        $data['gedung'] = $this->db->get('tbl_gedung')->result();

        $data['rows']   = $this->db->select('*')

            ->from('tbl_jadwal_matkul a')

            ->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah')

            ->join('tbl_ruangan c', 'a.kd_ruangan = c.id_ruangan')

            ->join('tbl_karyawan d', 'a.kd_dosen = d.nik', 'left')

            ->get()->result();



        //var_dump($data['matakuliah']);die();



        $data['page'] = 'v_jdl_kuliah_detail_test';

        $this->load->view('template/template', $data);
    }

    function cetak_jadwal_all()
    {
        $logged = $this->session->userdata('sess_login');
        $userid = $logged['userid'];

        //$data['prodi'] = $this->db->query('select * from tbl_jurusan_prodi where kd_prodi = '.$this->session->userdata('id_jurusan_prasyarat').'')->row()->kd_prodi;

        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_sp mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN tbl_kurikulum kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE mk.`kd_jadwal` LIKE "' . $userid . '%"
                                AND a.`kd_prodi` = "' . $userid . '"
                                AND kur.`kd_prodi` = "' . $userid . '"
                                AND mk.`kd_tahunajaran` LIKE "' . $this->session->userdata('tahunajaran') . '%"
                                GROUP BY mk.`kd_jadwal`
                                ORDER BY km.`semester_kd_matakuliah`')->result();

        $this->load->view('print_cetak_jadwal', $data);
    }

    function cetak_absensi($id)
    {
        $this->load->library('Cfpdf');
        $nik = $this->session->userdata('id_jurusan_prasyarat');

        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_sp a
                                            JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
                                            JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
                                            LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
                                            WHERE a.`id_jadwal` = ' . $id . '')->row();

        $kodeprodi = substr($data['rows']->kd_jadwal, 0, 5);

        $data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
                                            JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
                                            WHERE b.`kd_prodi` = "' . $kodeprodi . '"')->row();

        $data['mhs'] = $this->db->query('SELECT DISTINCT mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.kd_jadwal FROM tbl_krs_sp b
                                            JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
                                            JOIN tbl_sinkronisasi_renkeu renkeu ON renkeu.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
                                            WHERE b.kd_jadwal = "' . $data['rows']->kd_jadwal . '" AND renkeu.`tahunajaran` = SUBSTRING(b.`kd_krs` FROM 13 FOR 5) ORDER BY mhs.NIMHSMSMHS ASC;')->result();

        $this->load->view('jdl_sp_cetak_absen', $data);
    }
}



/* End of file jdl_kuliah.php */

/* Location: ./application/controllers/jdl_kuliah.php */
