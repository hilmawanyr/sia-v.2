<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publikasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(66)->result();
			$aktif    = $this->setting_model->getaktivasi('publikasi')->result();
			$logged   = $this->session->userdata('sess_login');
			if (!$cekakses || !$aktif) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->model('akademik/publikasi_model', 'publikasi');
	}

	function index()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}	

		if ((in_array(10, $grup)) or (in_array(1, $grup))) {
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/publikasi_view';
		} elseif ((in_array(9, $grup))) {
			$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$logged['userid'],'kd_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/publikasi_view';
		} elseif ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/publikasi_view';
		} else {
			redirect('auth','refresh');
		}

		

		$this->load->view('template/template',$data);
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('prodi');
        $tahunajaran = $this->input->post('tahunajaran');
		$semester = $this->input->post('semester');


        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('prodi', $jurusan);
		$this->session->set_userdata('fakultas', $fakultas);
		$this->session->set_userdata('semester', $semester);
      
		redirect(base_url('akademik/publikasi/view'));
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function view()
	{
		$data['aktif'] = $this->setting_model->getaktivasi('publikasi')->result();
		$logged        = $this->session->userdata('sess_login');
		$prodi         = $logged['userid'];
		$akademik      = $this->session->userdata('tahunajaran');
		$semester      = $this->session->userdata('semester');
		$pecah         = explode(',', $logged['id_user_group']);
		$jmlh          = count($pecah);

		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		$data['group'] 	= $grup;

		$data['crud'] 	= $this->db->query('SELECT * from tbl_role_access 
											WHERE menu_id = 66 
											AND user_group_id = '.$logged['id_user_group'].' ')->row();
		
		if (in_array(10, $grup) || in_array(1, $grup)) {
			$data['rows'] = $this->publikasi->publikasi_from_baa($prodi,$akademik,$semester);

		} elseif (in_array(9, $grup)) {
			$data['rows']      = $this->publikasi->publikasi_from_fakultas($prodi,$akademik,$semester);
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			// $data['page']      = 'akademik/publikasi_view';
		} elseif (in_array(8, $grup) || in_array(19, $grup)) {
			$data['rows'] = $this->publikasi->publikasi_from_staff($prodi,$akademik,$semester);

		} else {
			redirect('auth','refresh');
		}

		$data['page'] = 'akademik/publikasi_detail';
		$this->load->view('template/template',$data);	
	}

	

	function publikasi_all($id){
		$data['id_jadwal']=$id;

		$data['look'] = $this->db->query('SELECT * FROM tbl_mahasiswa a
										JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
										JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
										WHERE c.`id_jadwal` = "'.$id.'"')->result();
		$data['ding'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul 
										WHERE `id_jadwal` = "'.$id.'"')->row();


		$pub = $this->app_model->get_kelas_mahasiswa_nilai($id,3)->row();

		//var_dump($data['kelas']);die();

		if ($pub->flag_publikasi == 1) {
			$data['pub'] = 1;
		} elseif($pub->flag_publikasi == 2) {
			$data['pub'] = 1;
		}

		$data['jdl_mk'] = $id;

		$data['page']='akademik/publikasi_all';
		$this->load->view('template/template', $data);
	}

	function pub_all($id)
	{
		$logged = $this->session->userdata('sess_login');
		$kodejd = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();

		$this->db->query('DELETE from tbl_transaksi_nilai where kd_transaksi_nilai like "'.$kodejd->kd_jadwal.'zzz%"');

		// check publication type UTS/UAS
		// buat range tanggal selama 1 bulan untuk pengecekan tipe ujian
		$makeDateRange = strtotime(date('Y-m-d').'+ 4 week');
		$dateRange = date('Y-m-d',$makeDateRange);

		// cek tipe nilai ujian (UTS/UAS)
		$checkType 	= $this->db->query("SELECT * from tbl_jadwaluji where kd_jadwal = '".$kodejd->kd_jadwal."'
									 	AND start_date <= '".$dateRange."'
									 	order by tipe_uji DESC limit 1");

		if ($checkType->num_rows() > 0) {
			// if type UTS
			if ($checkType->row()->tipe_uji == 1) {
				// set UTS value
				$tipe = 3;
			// if type UAS
			} else {
				// set last poin
				$tipe = 10;
			}
		} else {
			$tipe = 10;
		}
		
		$kelas 	= $this->db->query('SELECT a.`npm_mahasiswa`,a.`nilai`,a.`kd_matakuliah`,a.`kd_transaksi_nilai`
									FROM tbl_nilai_detail a WHERE a.`tipe` = "'.$tipe.'" 
									AND a.`kd_jadwal` = "'.$kodejd->kd_jadwal.'" ')->result();
		
		foreach ($kelas as $isi) {

			$kd_nilai = $isi->kd_transaksi_nilai;
			$rtnew = number_format($isi->nilai,2);
			if (($rtnew >= 80) and ($rtnew <= 100)) {
				$rw = "A";
				$nilai_hrf= 4;
			} elseif (($rtnew >= 76) and ($rtnew <= 79.99)) {
				$rw = "A-"; 
				$nilai_hrf= 3.67; //3.70
			} elseif (($rtnew >= 72) and ($rtnew <= 75.99)) {
				$rw = "B+";
				$nilai_hrf= 3.33; //3.30
			} elseif (($rtnew >= 68) and ($rtnew <= 71.99)) {
				$rw = "B";
				$nilai_hrf= 3;
			} elseif (($rtnew >= 64) and ($rtnew <= 67.99)) {
				$rw = "B-";
				$nilai_hrf= 2.67; //2.70
			} elseif (($rtnew >= 60) and ($rtnew <= 63.99)) {
				$rw = "C+";
				$nilai_hrf= 2.33;
			} elseif (($rtnew >= 56) and ($rtnew <= 59.99)) {
				$rw = "C";
				$nilai_hrf= 2;
			} elseif (($rtnew >= 45) and ($rtnew <= 55.99)) {
				$rw = "D";
				$nilai_hrf= 1;
			} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
				$rw = "E";
				$nilai_hrf= 0;
			} elseif (($rtnew > 100)) {
				$rw = "T";
				$nilai_hrf= 0;
			}

			$kodeprd = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$isi->npm_mahasiswa,'NIMHSMSMHS','asc')->row()->KDPSTMSMHS;

			$data[] = array(
					'THSMSTRLNM' => $this->session->userdata('tahunajaran'), 
					'KDPTITRLNM' => '031036',
					'KDJENTRLNM' => 'C',
					'KDPSTTRLNM' => $kodeprd,
					'NIMHSTRLNM' => $isi->npm_mahasiswa,
					'KDKMKTRLNM' => $isi->kd_matakuliah,
					'NLAKHTRLNM' => $rw,
					'BOBOTTRLNM' => $nilai_hrf,
					'KELASTRLNM' => 17,
					'nilai_akhir' => $rtnew,
					'kd_transaksi_nilai' => $isi->kd_transaksi_nilai
				);

		}
				
    	$this->db->insert_batch('tbl_transaksi_nilai', $data);
    	
    	$datax['flag_publikasi'] = 2;
		$datax['publis_date'] = date('Y-m-d h:i:s');
    	$this->db->like('kd_transaksi_nilai', $kodejd->kd_jadwal.'zzz','after');
		$this->db->update('tbl_nilai_detail', $datax);

		echo "<script>alert('Publikasi Sukses');history.go(-1);</script>";
		
	}

	// ini untuk jika beberapa data ada yg hilang
	function sausBelibis()
	{
		$load = $this->db->query("SELECT * from tbl_krs where kd_jadwal = '62201/HzZJgv/092'")->result();

		foreach ($load as $key) {
			$data['kd_krs'] = $key->kd_krs;
			$data['tahun_ajaran'] = '20172';
			$data['kd_prodi'] = '62201';
			$data['nid'] = '031509040';

			$this->db->where('kd_jadwal', '62201/HzZJgv/092');
			$this->db->where('npm_mahasiswa', $key->npm_mahasiswa);
			$this->db->update('tbl_nilai_detail', $data);
		}
	}

	function publishIps()
	{
		$usr = $this->session->userdata('sess_login');
		$act = getactyear();
		$data['get'] = $this->db->query("SELECT substring(npm_mahasiswa, 1, 4) as akt from 
										tbl_verifikasi_krs
										where npm_mahasiswa NOT IN 
										(SELECT npm_mahasiswa from tbl_ips_copy where tahunajaran = '20172')
										AND tahunajaran = '".$act."' and kd_jurusan = '".$usr['userid']."'
										group by akt order by akt asc")->result();
		$data['page'] = 'v_publisips';
		$this->load->view('template/template', $data);
	}

	function generateIps()
	{
		$log 	= $this->session->userdata('sess_login');
		$prodi 	= $log['userid'];
		$year 	= $this->input->post('tahun', TRUE);
		$before = yearBefore();
		$mhs 	= $this->db->query("SELECT DISTINCT a.kd_krs,b.npm_mahasiswa FROM tbl_verifikasi_krs a 
									JOIN tbl_krs b ON a.kd_krs = b.kd_krs 
									WHERE a.kd_jurusan = '".$prodi."' 
									AND b.kd_krs LIKE CONCAT(b.npm_mahasiswa,'".$before."%')
									AND b.npm_mahasiswa like '".$year."%'")->result();
		
		
		foreach ($mhs as $key) {
			if (!empty($key->kd_krs)){
				$count 	= $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,
										a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
	                    				JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
	                    				WHERE a.`kd_transaksi_nilai` IS NOT NULL 
	                    				AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$key->npm_mahasiswa.'" 
	                    				AND THSMSTRLNM = "'.substr($key->kd_krs, 12,5).'" ')->result();
										
				$st = 0;
				$ht = 0;
				foreach ($count as $iso) {
					$h = 0;

					$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
					$ht= $ht + $h;

					$st= $st+$iso->sks_matakuliah;
				}

				$ips_nr = $ht/$st;

				$this->db->distinct();
				$this->db->select('semester_krs');
				$this->db->from('tbl_krs');
				$this->db->where('kd_krs', $key->kd_krs);
				$smt = $this->db->get()->row()->semester_krs;

				$object[] = [
					'kd_krs' => $key->kd_krs,
					'tahunajaran' => substr($key->kd_krs, 12,5),
					'npm_mahasiswa' => $key->npm_mahasiswa,
					'ips' => number_format($ips_nr,2),
					'semester' => $smt,
					'sks' => $st
				];
			}
		}
			
		$this->db->insert_batch('tbl_ips_copy', $object);
		
		echo "<script>alert('Publikasi Sukses');
		document.location.href='".base_url()."akademik/publikasi/publiships';</script>";
		
	    
	    // echo "<pre>";
	    // print_r ($object);exit();
	    // echo "</pre>";
	    
	}

	function unitPublish()
	{
		$log 	= $this->session->userdata('sess_login');
		$prodi 	= $log['userid'];
		$npm 	= substr($this->input->post('npm'),0,12);
		$before = yearBefore();
		$mhs 	= $this->db->query("SELECT DISTINCT a.kd_krs,b.npm_mahasiswa FROM tbl_verifikasi_krs a 
									JOIN tbl_krs b ON a.kd_krs = b.kd_krs 
									WHERE b.kd_krs LIKE CONCAT('".$npm."','".$before."%')")->row();
		
		if (!empty($mhs)){
			$count 	= $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,
										a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
	                    				JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
	                    				WHERE a.`kd_transaksi_nilai` IS NOT NULL 
	                    				AND NIMHSTRLNM = "'.$npm.'" 
	                    				AND THSMSTRLNM = "'.$before.'" ')->result();

															
										
	        $st = 0;
	        $ht = 0;
	        foreach ($count as $iso) {
	            $h = 0;

	            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
	            $ht= $ht + $h;

	            $st= $st+$iso->sks_matakuliah;
	        }

	        $ips_nr = $ht/$st;

	        $this->db->distinct();
	        $this->db->select('semester_krs');
	        $this->db->from('tbl_krs');
	        $this->db->where('kd_krs', $mhs->kd_krs);
	        $smt = $this->db->get()->row()->semester_krs;
			
			
	        $object = array(
	        	'kd_krs' => $mhs->kd_krs,
	        	'tahunajaran' => $before,
	        	'npm_mahasiswa' => $npm,
	        	'ips' => number_format($ips_nr,2),
	        	'semester' => $smt,
	        	'sks' => $st
	        );
			$available = $this->db->select('*')
						->from('tbl_ips_copy a')
						->join('tbl_mahasiswa b','a.npm_mahasiswa = b.NIMHSMSMHS')
						->where('a.tahunajaran',$before)
						->where('b.KDPSTMSMHS',$prodi)
						->where('a.npm_mahasiswa',$npm)
						->get()->row(); 
						
						
			if (!empty($available)){
				echo "<script>alert('Sudah di Publikasi sebelumnya');
				document.location.href='".base_url()."akademik/publikasi/publiships';</script>";
			}else{	
			 /* echo "<pre>";
	    print_r ($available);exit(); */
				$this->db->insert('tbl_ips_copy', $object);
				
				echo "<script>alert('Publikasi Sukses');
				document.location.href='".base_url()."akademik/publikasi/publiships';</script>";
			}
		}else{		
			echo "<script>alert('Tidak ada data');
				document.location.href='".base_url()."akademik/publikasi/publiships';</script>";				
		}
	}

	function ipsHistory()
	{
		$npm = $this->input->post('npm', TRUE);
		$before = yearBefore();
		$kdkrs = $npm.$before;

		$this->db->like('kd_krs', $kdkrs, 'after');
		$data['data'] = $this->db->get('tbl_ips_copy')->row();
		$this->load->view('table_edit_ips', $data);
	}

	function updateIps()
	{
		$npm = $this->input->post('npm', TRUE);
		$bfr = yearBefore();
		$kdk = $npm.$bfr;

		$ips = $this->input->post('ips');
		$sks = $this->input->post('sks');

		$obj = ['ips' => $ips, 'sks' => $sks];
		$this->db->like('kd_krs', $kdk, 'after');
		$this->db->update('tbl_ips_copy', $obj);		
			echo "<script>alert('Berhasil di ubah');
				document.location.href='".base_url()."akademik/publikasi/publiships';</script>";
	}

	function load_mhs_autocomplete()
	{
		$log = $this->session->userdata('sess_login');
		$uid = $log['userid'];

		$this->db->distinct();
        $this->db->select("NIMHSMSMHS,NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa');
        $this->db->where('KDPSTMSMHS', $uid);
        $this->db->group_start();
        $this->db->like('NMMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('NIMHSMSMHS', $_GET['term'], 'both');
        $this->db->group_end();
        $sql  = $this->db->get();

        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = array(
	            'value' => $row->NIMHSMSMHS.'-'.$row->NMMHSMSMHS
            );
        }

        if (count($data) == 0) {
        	$result = ['Tidak ada data'];
        } else {
        	$result = $data;
        }
        
        echo json_encode($result);
	}

	function loadtabrevisi($param)
	{
		$exp 	= explode('-', $param);
		$year 	= yearBefore();
		$kdkrs 	= $exp[0].$year;
		$this->db->like('kd_krs', $kdkrs, 'after');
		$data['get'] = $this->db->get('tbl_ips_copy')->row();
		/* var_dump($data['get']);exit(); */
		$this->load->view('tab_revisi_ips', $data);
	}

	function jumlahpeserta($idjdl)
	{
		$kdjdl = get_kd_jdl($idjdl);
		$amount = $this->db->query("SELECT kd_matakuliah, count(kd_krs) as jum from tbl_krs where kd_jadwal = '".$kdjdl."'")->row();
		echo json_encode($amount);
	}

	function statuspublish($idjdl)
	{
		error_reporting(0);
		$kdjdl = get_kd_jdl($idjdl);

		// detail jadwal
		$detailJadwal = $this->db->select('kd_matakuliah')
								->from('tbl_jadwal_matkul')
								->where('id_jadwal',$idjdl)
								->get()->row();

		$kd_matakuliah = $detailJadwal->kd_matakuliah;
		$kd_prodi = substr($kdjdl, 0, 5);

		// get nama matakuliah
		$namaMatakuliah = $this->db->select('nama_matakuliah')
									->from('tbl_matakuliah')
									->where('kd_matakuliah', $kd_matakuliah)
									->where('kd_prodi', $kd_prodi)
									->get()->row()->nama_matakuliah;
		$setCourseToUppercase = strtoupper($namaMatakuliah);

		// Get Kelas (Untuk Kebutuhan Kelas Khusus)
		$kelas = getNamaKelas($idjdl);
		$findString = 'KH';
		$kelasKhusus = stripos($kelas, $findString);
		
		$all = $this->db->query('SELECT DISTINCT flag_publikasi, 
										date(max(publis_date)) as pubdate,
										SUM(flag_publikasi) AS itung_publis,
										COUNT(DISTINCT kd_transaksi_nilai) AS mhs 
								FROM tbl_nilai_detail 
								WHERE`kd_jadwal` LIKE "'.$kdjdl.'%"
								AND `nilai` IS NOT NULL AND tipe = 10')->row();

		// buat range tanggal selama 1 bulan untuk pengecekan tipe ujian
		$makeDateRange = strtotime(date('Y-m-d').'+ 4 week');
		$dateRange = date('Y-m-d',$makeDateRange);

		// cek tipe nilai ujian (UTS/UAS)
		$cekType 	= $this->db->query("SELECT * from tbl_jadwaluji where kd_jadwal = '".$kdjdl."'
									 	AND start_date <= '".$dateRange."'
									 	order by tipe_uji DESC limit 1")->row();
		
		// jika nilai UTS
		if ($cekType->tipe_uji == 1 and date('Y-m-d') >= $cekType->start_date) {
			
			// belum publikasi
			if (($all->itung_publis < ($all->mhs*2)) and ($all->mhs != 0)) {
				$button = '<a class="btn btn-warning btn-small" href="'.base_url().'akademik/publikasi/pub_all/'.$idjdl.'">';
				$button .= 'UTS</a>';

			// sudah publikasi
			} elseif (($all->flag_publikasi == 2) and ($all->itung_publis == ($all->mhs*2)) and $all->pubdate> $cekType->start_date) {
				$button = '<a class="btn btn-success btn-small" onClick="alert(\'Sudah Dipublish!\')">';
		    	$button .= '<i class="btn-icon-only icon-ok"></i></a>';

		    // nilai belum di-upload
			} else {
				$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
				$button .= '<i class="btn-icon-only icon-ok"> </i></a>';
	    	}

	    // jika nilai UAS
		} elseif ($cekType->tipe_uji == 2 and date('Y-m-d') >= $cekType->start_date) {

			// belum publikasi
			if (($all->itung_publis < ($all->mhs*2)) and ($all->mhs != 0)) {
				$button = '<a class="btn btn-warning btn-small" href="'.base_url().'akademik/publikasi/pub_all/'.$idjdl.'">';
				$button .= 'UAS</a>';

			// sudah publikasi
			} elseif (($all->flag_publikasi == 2) and ($all->itung_publis == ($all->mhs*2)) and $all->pubdate > $cekType->start_date) {
				$button = '<a class="btn btn-success btn-small" onClick="alert(\'Sudah Dipublish!\')">';
		    	$button .= '<i class="btn-icon-only icon-ok"></i></a>';

		    // nilai belum di-upload
			} else {
				$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
				$button .= '<i class="btn-icon-only icon-ok"> </i></a>';
	    	}

	    // jika nilai UTS dan belum diinput
		} elseif ($cekType->tipe_uji == 1 and date('Y-m-d') < $cekType->start_date) {
			$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
			$button .= '<i class="btn-icon-only icon-ok"> </i></a>';

		// jika nilai UAS dan belum diinput
		} elseif ($cekType->tipe_uji == 2 and date('Y-m-d') < $cekType->start_date) {
			$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
			$button .= '<i class="btn-icon-only icon-ok"> </i></a>';

		// untuk kelas khusus
		} elseif($kelasKhusus !== false){

			// jika nilai belum dipublikasi
			if (($all->itung_publis < ($all->mhs*2)) and ($all->mhs != 0)) {
				$button = '<a class="btn btn-warning btn-small" href="'.base_url().'akademik/publikasi/pub_all/'.$idjdl.'">Publikasi</a>';

			// sudah publikasi
			} elseif (($all->flag_publikasi == 2) and ($all->itung_publis == ($all->mhs*2))) {
				$button = '<a class="btn btn-success btn-small" onClick="alert(\'Sudah Dipublish!\')">';
		    	$button .= '<i class="btn-icon-only icon-ok"></i></a>';

		    // belum upload nilai
			} else {
				$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
				$button .= '<i class="btn-icon-only icon-ok"> </i></a>';
			}
		
		// jika nilai matakuliah tugas akhir
		} elseif (is_final_assignment($namaMatakuliah)) {

			// jika nilai belum dipublikasi
			if (($all->itung_publis < ($all->mhs*2)) and ($all->mhs != 0)) {
				$button = '<a class="btn btn-warning btn-small" href="'.base_url().'akademik/publikasi/pub_all/'.$idjdl.'">Publikasi</a>';

			// sudah publikasi
			} elseif (($all->flag_publikasi == 2) and ($all->itung_publis == ($all->mhs*2))) {
				$button = '<a class="btn btn-success btn-small" onClick="alert(\'Sudah Dipublish!\')">';
		    	$button .= '<i class="btn-icon-only icon-ok"></i></a>';

		    // belum upload nilai
			} else {
				$button = '<a class="btn btn-danger btn-small" onClick="alert(\'Nilai Belum Diinput!\')">';
				$button .= '<i class="btn-icon-only icon-ok"> </i></a>';
			}
		
		} else {
			$button = '<a href="javascript:void(0);" class="btn" onclick="alert(\'Jadwal matakuliah tidak ditemukan!\')">';
			$button .= '<i class="icon-remove"></i></a>';
		}

		echo $button;
	}
}

/* End of file Publikasi.php */
/* Location: ./application/modules/akademik/controllers/Publikasi.php */
