<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bimbingan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(63)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'akademik/cus_ta';
		$this->load->view('template/template', $data);
	}

	function index2()
	{	
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'akademik/cus_ta_tes';
		$this->load->view('template/template', $data);
	}

	function bimsalabim()
	{
		$tahun = $this->input->post('tahunajaran');
		$this->session->set_userdata('yearacademic',$tahun);
		redirect(base_url('akademik/bimbingan/list_bimbingan'));
	}

	function bimsalabim_tes()
	{
		$tahun = $this->input->post('tahunajaran');
		$this->session->set_userdata('yearacademic',$tahun);
		redirect(base_url('akademik/bimbingan/list_bimbingan_tes'));
	}

	function list_bimbingan()
	{
		$logged = $this->session->userdata('sess_login');

		// var_dump($logged['userid'].'----------'.$this->session->userdata('yearacademic'));
		//var_dump($logged['id_user_group']);exit();
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ( (in_array(6, $grup)) || (in_array(7, $grup)) ) {
			//$data['getData'] = $this->app_model->getpembimbingall($logged['userid'])->result();
			/*$data['getData'] = $this->db->query('SELECT a.*,c.perihal as perihalan from tbl_penugasan_dosen a 
				join tbl_karyawan b on b.`nik` = a.`nik` 
				join tbl_perihal c on a.`perihal` = c.`kd_perihal` where a.nik='.$logged['userid'].'')->result();
			*/
			//var_dump($data['getData']); die();
			
			$data['getData'] = $this->db->query('SELECT distinct a.*,c.* FROM tbl_verifikasi_krs a 
												join tbl_mahasiswa c on c.NIMHSMSMHS = SUBSTR(a.kd_krs, 1,12)
												WHERE a.id_pembimbing="'.$logged['userid'].'" 
												AND a.tahunajaran = "'.$this->session->userdata('yearacademic').'"
												AND a.status_verifikasi in ("4","1")
												ORDER BY c.NIMHSMSMHS')->result();
			
			$data['page'] = 'akademik/bimbingan_v_pados';
		} else {
			$data['getData'] = $this->app_model->getpembimbingall($logged['userid'])->result();
			$data['page'] = 'akademik/bimbingan_view';
		}
		$this->load->view('template/template',$data);
	}

	function list_bimbingan_tes()
	{
		$logged = $this->session->userdata('sess_login');
		//var_dump($logged['id_user_group']);exit();
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ( (in_array(6, $grup)) || (in_array(7, $grup)) ) {
			//$data['getData'] = $this->app_model->getpembimbingall($logged['userid'])->result();
			/*$data['getData'] = $this->db->query('SELECT a.*,c.perihal as perihalan from tbl_penugasan_dosen a 
				join tbl_karyawan b on b.`nik` = a.`nik` 
				join tbl_perihal c on a.`perihal` = c.`kd_perihal` where a.nik='.$logged['userid'].'')->result();
			*/
			//var_dump($data['getData']); die();
			
			$data['getData'] = $this->db->query('SELECT a.*,c.* FROM tbl_verifikasi_krs a 
												join tbl_karyawan b on((a.id_pembimbing=b.nid)) 
												join tbl_mahasiswa c on((c.NIMHSMSMHS=SUBSTR(a.kd_krs, 1,12)))
												WHERE a.id_pembimbing="'.$logged['userid'].'" AND a.tahunajaran = "'.$this->session->userdata('yearacademic').'" ORDER BY c.NIMHSMSMHS')->result();
			$data['page'] = 'akademik/bimbingan_v_pados_tes';
		} else {
			$data['getData'] = $this->app_model->getpembimbingall($logged['userid'])->result();
			$data['page'] = 'akademik/bimbingan_view';
		}
		$this->load->view('template/template',$data);
	}

	function viewmhs($id)
	{
		$npm = substr($id, 0,12);
		//die($npm);

		$this->session->set_userdata('npm',$npm);
		$data['semester'] = $this->db->query("select distinct semester_krs from tbl_krs where kd_krs = '".$id."' ")->row();
		
		$this->session->set_userdata('kode_krs',$id);
		$data['sts'] = $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs = '".$id."'")->row();
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($id)->result();
		$data['detail_krs_row'] = $this->app_model->get_detail_krs_mahasiswa($id)->row();

		$data['histori'] = $this->db->query("SELECT * from tbl_bimbingan_krs where kd_krs = '".$id."'")->result();
		
		$data['kd_krs'] = $id;
		
		$data['page'] = 'akademik/viewkrs_detail_bimbingan';
		//$data['page'] = 'akademik/anak_dibimbing';
		$this->load->view('template/template', $data);
	}

	// function viewmhs_tes($id)
	// {
	// 	$npm = substr($id, 0,12);

	// 	$this->session->set_userdata('npm',$npm);
	// 	$data['semester'] = $this->db->query("select distinct semester_krs from tbl_krs where kd_krs = '".$id."' ")->row();
		
	// 	$this->session->set_userdata('kode_krs',$id);
	// 	$data['sts'] = $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs = '".$id."'")->row();
	// 	$data['pembimbing']=$this->app_model->get_pembimbing_krs_tes($id)->row_array();
	// 	$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa_tes($id)->result();
	// 	$data['detail_krs_row'] = $this->app_model->get_detail_krs_mahasiswa_tes($id)->row();
	// 	$data['nmmhs'] = $this->db->query("SELECT NMMHSMSMHS,NIMHSMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".$npm."'")->row();
	// 	$data['histori'] = $this->db->query("SELECT * from tbl_bimbingan_krs where kd_krs = '".$id."'")->result();
		
	// 	$data['kd_krs'] = $id;
		
	// 	$data['page'] = 'akademik/viewkrs_detail_bimbingan_tes';
	// 	$this->load->view('template/template', $data);
	// }

	function comein($id)
	{
		//$aktif = $this->setting_model->getaktivasi('krs')->result();
		$aktif = TRUE;
		if ($aktif == TRUE) {
			// $data = array(
			// 	'kd_krs'	=> $this->input->post('kode'),
			// 	'note'		=> $this->input->post('note'),
			// 	'status'	=> $this->input->post('sts'),
			// 	'tgl'		=> date('Y-m-d H:i:s')
			// 	);
			// $this->app_model->insertdata('tbl_bimbingan_krs',$data);

			$datax = array(
				// 'keterangan_krs'	=> $this->input->post('note'),
				// 'status_verifikasi'	=> $this->input->post('sts')
				 'status_verifikasi'	=> 1
				);
			
			$this->app_model->updatedata('tbl_verifikasi_krs','id_verifikasi',$id,$datax);

			// $this->notifMail($id,1);

			// $this->notifMessage($id);

			echo "<script>alert('Berhasil!');history.go(-1);</script>";
		}
	}

	function comeback($id)
	{
		//$aktif = $this->setting_model->getaktivasi('krs')->result();
		$aktif = TRUE;
		if ($aktif == TRUE) {
			// $data = array(
			// 	'kd_krs'	=> $this->input->post('kode'),
			// 	'note'		=> $this->input->post('note'),
			// 	'status'	=> $this->input->post('sts'),
			// 	'tgl'		=> date('Y-m-d H:i:s')
			// 	);
			// $this->app_model->insertdata('tbl_bimbingan_krs',$data);

			$datax = array(
				// 'keterangan_krs'	=> $this->input->post('note'),
				// 'status_verifikasi'	=> $this->input->post('sts')
				 'status_verifikasi'	=> 2
				);
			
			$this->app_model->updatedata('tbl_verifikasi_krs','id_verifikasi',$id,$datax);

			$this->notifMail($id,2);

			echo "<script>alert('Berhasil!');history.go(-1);</script>";
		}
	}

	function notifMessage($id)
	{
		$npm = $this->app_model->getdetail('tbl_verifikasi_krs','id_verifikasi',$id,'id_verifikasi','asc')->row()->npm_mahasiswa;

		$bio = $this->app_model->getdetail('tbl_bio_mhs','npm',$npm,'npm','asc')->row();

		$sub = substr($bio->no_hp, 1, 13);

		// var_dump('62'.$subs);exit();

		$curl = curl_init();
				curl_setopt_array($curl, array(
			    CURLOPT_RETURNTRANSFER => 1,
			    CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
			    CURLOPT_POST => true,
			    CURLOPT_POSTFIELDS => array(
			        'user' => 'ubharajaya2_api',
			        'password' => 'U6psYjL',
			        // 'user' => 'ubharajaya1_api',
			        // 'password' => 'P5Mqr6R',
			        'SMSText' => get_nm_mhs($npm).', KRS anda telah mendapatkan respon dari dari dosen PA anda. Terimakasih. --SIA-UBJ',
			        'GSM' => '62'.$sub
			    )
			));

		curl_exec($curl);
		curl_close($curl);
	}

	function notifMail($id,$sts)
	{
		// get some information base on kodekrs
		$datakrs 	= $this->app_model->getdetail('tbl_verifikasi_krs','id_verifikasi',$id,'id_verifikasi','asc')->row();

		// get key of kode jadwal in tbl_keykrs
		$keykrs 	= $this->app_model->getdetail('tbl_keykrs','kdkrs',$datakrs->kd_krs,'kdkrs','asc',1)->row();

        // get email mhs
        $get_mail 	= $this->app_model->getdetail('tbl_bio_mhs','npm',$datakrs->npm_mahasiswa,'npm','asc')->row()->email;

        // make condition base on status
        if ($sts == 1) {
        	$strStatus = "telah disetujui pada ".date('Y-m-d H:i:s');
        } else {
        	$strStatus = "perlu direvisi";
        }
        

		$isi = '<p>Yang terhormat '.get_nm_mhs($datakrs->npm_mahasiswa).',</p>
				<p>KRS anda '.$strStatus.'</p>
				<p>Untuk melihat KRS anda silahkan masuk ke akun SIA Anda. </p>
				</br>
				<p>Untuk informasi, silahkan hubungi Direktorat Pengembangan Teknologi dan Informasi Ubharajaya di email berikut <em>puskom@ubharajaya.ac.id</em></p>
				<p>Terima Kasih</p>
				</br>
				</br>
				<p><em>Universitas Bhayangkara Jakarta Raya</em></p>';

		//$isi = $this->load->view('forgot_mail');
		
		$judul = 'Verifikasi KRS Mahasiswa';

		$config = array(
	        'protocol' 	=> 'smtp',
	        'smtp_host' => 'ssl://smtp.gmail.com',
	        'smtp_port' => 465,
	        'smtp_user' => 'puskom@ubharajaya.ac.id', //email id
	        'smtp_pass' => 'Puskom787566',
	        'mailtype'  => 'html', 
	        'charset'   => 'iso-8859-1'
	    );
	    
	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");

	    $this->email->from('puskom@ubharajaya.ac.id','Universitas Bhayangkara Jakarta Raya');
	    $this->email->to($get_mail); // email array
	    $this->email->subject($judul);   
	    $this->email->message($isi);

	    $result = $this->email->send();
	}

	function lihatkrs($primary)
	{
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($primary)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($primary)->result();
		//var_dump($data['detail_krs']);die();

		$data['kd_krs'] = $primary;
		$data['page'] = 'akademik/viewkrs_detail';
		$this->load->view('template/template',$data);
	}

	function viewkrsmhs($id)
	{
		$data['krs'] = $this->app_model->get_all_krs_mahasiswa($id)->result();
		$data['npm'] = $id;
		$data['page'] = 'akademik/bimbingankrs_view';
		$this->load->view('template/template',$data);
	}

	function view($id)
	{
		$logged = $this->session->userdata('sess_login');
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($id)->result();
		$data['kd_krs'] = $id;
		$data['page'] = 'akademik/bimbingankrs_detail';
		$this->load->view('template/template',$data);	
	}

	function printbimbingan()
	{
		$logged = $this->session->userdata('sess_login');
		$data['getData'] = $this->db->query('SELECT distinct b.nid,b.nama,c.NIMHSMSMHS,c.NMMHSMSMHS,c.TAHUNMSMHS FROM tbl_verifikasi_krs a 
												join tbl_karyawan b on((a.id_pembimbing=b.nid)) 
												join tbl_mahasiswa c on((c.NIMHSMSMHS=SUBSTR(a.kd_krs, 1,12)))
												WHERE b.nid="'.$logged['userid'].'" order by c.NIMHSMSMHS asc')->result();
		$this->load->view('print_excel_bimbingan', $data);
	}

	function printdaftarbimbingan()
	{
		$logged = $this->session->userdata('sess_login');
		$data['getData'] = $this->db->query('SELECT DISTINCT b.nama,b.nid,c.NIMHSMSMHS,c.NMMHSMSMHS,c.TAHUNMSMHS FROM tbl_verifikasi_krs a 
												join tbl_karyawan b on((a.id_pembimbing=b.nid)) 
												join tbl_mahasiswa c on((c.NIMHSMSMHS=SUBSTR(a.kd_krs, 1,12)))
												WHERE b.nid="'.$logged['userid'].'" and c.STMHSMSMHS = "A" order by c.NIMHSMSMHS asc')->result();
		$this->load->view('print_excel_daftar_bimbingan', $data);
	}

	function kosongkan_jadwal($id){
		//die('test'.$id.'');
		//$logged = $this->session->userdata('sess_login');
		//$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
		$data = array('kd_jadwal' => null );

		$this->db->where('id_krs', $id);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs')
						->where('id_krs',$id)
						->get()->row();

		redirect(base_url('akademik/bimbingan/viewmhs/'.$q->kd_krs.''));

	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}
	
	function get_jadwal(){
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$user = $this->session->userdata('npm');

		//$npm  = $user['username'];

		$mhs  = $this->app_model->get_jurusan_mhs($user)->row();
		
		// $data = $this->db->query('SELECT * 
		// 					FROM tbl_jadwal_matkul a
		// 					LEFT JOIN tbl_ruangan b ON a.`kelas` = b.`id_ruangan`
		// 					LEFT JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 					LEFT JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 					WHERE  a.`kd_jadwal` LIKE "'.$mhs->KDPSTMSMHS.'%" AND a.`kd_matakuliah` = "'.$kd_matakuliah.'" AND d.`kd_prodi` LIKE "'.$mhs->KDPSTMSMHS.'"');



		$data = $this->app_model->get_pilih_jadwal($kd_matakuliah,$mhs->KDPSTMSMHS,$tahunakademik->kode)->result();
                //var_dump($data);die();
		$js = json_encode($data);
		echo $js;
	}

	function get_jumlah(){
		$kd_jadwal = $_POST['kd_jadwal'];

		$data = $this->db->query("SELECT COUNT(npm_mahasiswa) as jumlah FROM tbl_krs WHERE kd_jadwal = '".$kd_jadwal."'")->result();
                
		$js = json_encode($data);
		echo $js;
	}
	
	function save_jadwal(){
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');
		
		$this->app_model->updatedata_krs($kd_matakuliah, $kd_krs, $data);
		redirect($this->agent->referrer());
	}

	function dp_view($npm)
	{
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row();
		$data['detail_khs'] = $this->app_model->get_all_khs_mahasiswa($npm)->result();
		//$data['ipk'] = $this->app_model->get_ipk_mahasiswa($logged['userid'])->row()->ipk;
		$data['page'] = 'akademik/khs_view';
		$this->load->view('template/template', $data);
	}

	function detailkhs($npm, $id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $npm;
		}
		$data['npm'] = $nim;
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['semester'] = $a = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS,$id);
		$data['tahunakademik'] = $id;
		$data['kode'] = $this->db->query("select distinct kd_krs from tbl_krs where kd_krs like '".$nim.$id."%'")->row();
		if ($id < 20151) {
			$data['eror'] = error_reporting(0);
		} 
		
		if ($id < '20151') {
			$data['detail_khs'] = $this->db->query("select distinct b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah from tbl_transaksi_nilai a join tbl_matakuliah_copy b on a.KDKMKTRLNM = b.kd_matakuliah 
													where a.THSMSTRLNM = '".$id."' and b.tahunakademik = '".$id."' and a.NIMHSTRLNM = '".$nim."' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		} else {
				$data['detail_khs'] = $this->db->query("select distinct a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
													where a.kd_krs like '".$nim.$id."%' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		}
		$logged = $this->session->userdata('sess_login');
		$data['page'] = 'akademik/khs_detail';
	    $this->load->view('template/template', $data);
	}

	function detailKhs_konversi($npm,$id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $npm;
		}
		$data['npm'] = $nim;
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		if ($id < '20151') {
			$data['detail_khs'] = $this->db->query("select distinct b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah from tbl_transaksi_nilai_konversi a join tbl_matakuliah_copy b on a.KDKMKTRLNM = b.kd_matakuliah where a.THSMSTRLNM = '".$id."' and b.tahunakademik = '".$id."' and a.NIMHSTRLNM = '".$nim."' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		} else {
			$data['detail_khs'] = $this->db->query("select distinct b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah from tbl_transaksi_nilai_konversi a join tbl_matakuliah b on a.KDKMKTRLNM = b.kd_matakuliah where a.THSMSTRLNM = '".$id."'  and a.NIMHSTRLNM = '".$nim."' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		}
		$data['tahunakademik'] = $id;
		$data['page'] = 'akademik/khs_detail_konversi';
	    $this->load->view('template/template', $data);
	}

	function cetak_bimb($id)
	{
		$thn = $this->db->query("SELECT kode from tbl_tahunakademik where status = '1'")->row()->kode;
		$data['query'] = $this->db->query("SELECT distinct a.npm_mahasiswa,b.NMMHSMSMHS,b.TAHUNMSMHS,a.tahunajaran from tbl_verifikasi_krs a join tbl_mahasiswa b on a.npm_mahasiswa = b.NIMHSMSMHS where id_pembimbing = '".$id."' and status_verifikasi = '1' and tahunajaran = '".$thn."' GROUP BY NIMHSMSMHS order by TAHUNMSMHS,NIMHSMSMHS")->result();
		$this->load->view('excel_list_bimb', $data);
	}

	function cetak_bimb_byprodi()
	{
		$sesi  = $this->session->userdata('sess_login');
		$data['prodi'] = $this->db->query("SELECT prodi from tbl_jurusan_prodi where kd_prodi = '".$sesi['userid']."'")->row();
		$data['query'] = $this->db->query("SELECT distinct a.npm_mahasiswa,b.NMMHSMSMHS,b.TAHUNMSMHS,a.tahunajaran,c.nama from tbl_verifikasi_krs a join tbl_mahasiswa b 
											on a.npm_mahasiswa = b.NIMHSMSMHS join tbl_karyawan c 
											on c.nid = a.id_pembimbing where id_pembimbing = '".$this->input->post('kd_dosen')."' and kd_jurusan = '".$this->input->post('prodi')."'
											GROUP BY NIMHSMSMHS order by TAHUNMSMHS,NIMHSMSMHS")->result();
		$this->load->view('excel_bimb_byprodi', $data);
	}

}

/* End of file bimbingan.php */
/* Location: ./application/modules/akademik/controllers/bimbingan.php */