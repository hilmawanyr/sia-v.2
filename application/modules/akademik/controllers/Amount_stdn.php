<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amount_stdn extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan log-in kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		} else {
			$this->sess = $this->session->userdata('sess_login');
			$this->load->model('akademik/amountstd_model', 'amount');
		}
	}

	public function index()
	{
		$data['year'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['user'] = $this->sess['userid'];
		$data['page'] = 'vselect_year';
		$this->load->view('template/template', $data);	
	}

	function saveses()
	{
		$tahun = $this->input->post('tahunajaran', TRUE);
		$this->session->set_userdata('sess_tahunajar', $tahun);
		redirect(base_url('akademik/Amount_stdn/listed'));
	}

	function listed()
	{
		$data['sess'] = $this->session->userdata('sess_tahunajar');
		$data['data'] = $this->amount->getList()->result();
		$data['page'] = 'vlist_stdn';
		$this->load->view('template/template', $data);
	}

	function export()
	{
		$data['sess'] = $this->session->userdata('sess_tahunajar');
		$data['data'] = $this->amount->getList()->result();
		$this->load->view('excel_amount_mhs_reg', $data);
	}

	function detail($id,$cls)
	{
		$data['clss'] = $cls;
		$data['prod'] = $id;
		$data['data'] = $this->amount->getDetailClass($id,$cls,$this->session->userdata('sess_tahunajar'))->result();
		$data['page'] = 'vdetail_class';
		$this->load->view('template/template', $data);
	}

	function export_dtl($id,$cls)
	{
		$data['clss'] = classType($cls);
		$data['prod'] = get_jur($id);
		$data['data'] = $this->amount->getDetailClass($id,$cls,$this->session->userdata('sess_tahunajar'))->result();
		$this->load->view('export_detail_mhs_reg', $data);
	}

}

/* End of file Amount_stdn.php */
/* Location: .//tmp/fz3temp-1/Amount_stdn.php */