<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_krs_sp extends CI_Model
{


	function cekmk($id)
	{
		$this->db->select("kd_matakuliah");
		$this->db->from('tbl_jadwal_matkul_sp');
		$this->db->where_in('kd_jadwal', $id);
		return $this->db->get()->result();
	}
}

/* End of file Ajar_model.php */
/* Location: ./application/models/Ajar_model.php */
