<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajar_model extends CI_Model
{

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	function list_dosen($sess, $smt, $ta)
	{
		$hey = 'b.id_jadwal,b.kd_jadwal,b.kd_matakuliah,b.kd_dosen,b.kelas,b.open,b.hari,b.waktu_mulai,b.waktu_selesai';
		$this->db->distinct();
		$this->db->select($hey);
		$this->db->from('tbl_jadwal_matkul b');
		$this->db->join('tbl_kurikulum_matkul_new e', 'b.kd_matakuliah = e.kd_matakuliah');
		$this->db->where('e.semester_kd_matakuliah', $smt);
		$this->db->where('b.kd_tahunajaran', $ta);
		$this->db->like('b.kd_jadwal', $sess, 'after');
		$this->db->group_by('b.kd_jadwal');
		$this->db->order_by('b.kd_matakuliah', 'asc');
		return $this->db->get()->result();
	}

	function list_dosen_fak($ses)
	{
		return $this->db->query("SELECT DISTINCT jdl.kd_dosen ,kry.nama,kry.nid,
								(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul jdl
                                JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
                                WHERE jdl.kd_dosen = kry.nid AND mk.kd_prodi = " . $ses . ") AS sks 
								FROM tbl_jadwal_matkul jdl
								JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								JOIN tbl_karyawan kry ON jdl.kd_dosen = kry.nid
								WHERE mk.kd_prodi = " . $ses . " AND NOT (jdl.kd_dosen <> NULL) AND NOT (jdl.kd_dosen <=> '')")->result();
	}

	function list_dosen_baa()
	{
		return $this->db->query("SELECT DISTINCT jdl.kd_dosen ,kry.nama,kry.nid,
								(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul jdl
                                LEFT JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
                                WHERE jdl.kd_dosen = kry.nid) AS sks 
								FROM tbl_jadwal_matkul jdl
								LEFT JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								RIGHT JOIN tbl_karyawan kry ON jdl.kd_dosen = kry.nid
								WHERE NOT (jdl.kd_dosen <=> NULL) AND NOT (jdl.kd_dosen <=> '')")->result();
	}

	function meetingAmount($year, $kdjdl)
	{
		if (($year == '20152') or ($year == '20161')) {

			$this->db->select('count(distinct pertemuan) as maksimal');
			$this->db->from('tbl_absensi_mhs');
			$this->db->where('kd_jadwal', $kdjdl);
			return $this->db->get()->row()->maksimal;
		} elseif ($year == '20162') {

			$this->db->select('count(distinct pertemuan) as maksimal');
			$this->db->from('tbl_absensi_mhs_new');
			$this->db->where('kd_jadwal', $kdjdl);
			return $this->db->get()->row()->maksimal;
		} else {

			$this->db->select('count(distinct pertemuan) as maksimal');
			$this->db->from('tbl_absensi_mhs_new_20171');
			$this->db->where('kd_jadwal', $kdjdl);
			return $this->db->get()->row()->maksimal;
		}
	}

	function meetingAmountForPrint($year, $kdjdl)
	{
		if (($year == '20152') or ($year == '20161')) {

			$this->db->distinct();
			$this->db->select('pertemuan');
			$this->db->from('tbl_absensi_mhs');
			$this->db->where('kd_jadwal', $kdjdl);
			$this->db->order_by('pertemuan', 'desc');
			$this->db->limit(1);
			return $this->db->get()->row()->pertemuan;
		} elseif ($year == '20162') {

			$this->db->distinct();
			$this->db->select('pertemuan');
			$this->db->from('tbl_absensi_mhs_new');
			$this->db->where('kd_jadwal', $kdjdl);
			$this->db->order_by('pertemuan', 'desc');
			$this->db->limit(1);
			return $this->db->get()->row()->pertemuan;
		} else {

			$this->db->distinct();
			$this->db->select('pertemuan');
			$this->db->from('tbl_absensi_mhs_new_20171');
			$this->db->where_in('kd_jadwal', $kdjdl);
			$this->db->order_by('pertemuan', 'desc');
			$this->db->limit(1);
			return $this->db->get()->row()->pertemuan;
		}
	}

	function loadAbsen($id)
	{
		$actyear = getactyear();
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $id, 'id_jadwal', 'asc')->row();
	
		if ($kode->gabung > 0) {
			$q 	= $this->db->query('SELECT distinct 
										mhs.NIMHSMSMHS,
										mhs.NMMHSMSMHS,
										b.`kd_jadwal` 
									FROM tbl_krs b
									JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
									JOIN tbl_mahasiswa mhs ON b.npm_mahasiswa = mhs.NIMHSMSMHS
									WHERE v.status_verifikasi = 1
									AND b.kd_jadwal = "' . $kode->kd_jadwal . '" 
									OR kd_jadwal = "' . $kode->referensi . '"
									OR kd_jadwal IN 
										(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul 
										WHERE referensi = "' . $kode->referensi . '")
									ORDER BY mhs.NIMHSMSMHS ASC')->result();
		} else {
			$q 	= $this->db->query('SELECT distinct 
										mhs.NIMHSMSMHS,
										mhs.NMMHSMSMHS,
										b.`kd_jadwal` 
									FROM tbl_krs b
									JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
									JOIN tbl_mahasiswa mhs ON b.npm_mahasiswa = mhs.NIMHSMSMHS
									WHERE v.status_verifikasi = 1
									AND b.kd_jadwal = "' . $kode->kd_jadwal . '" 
									ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}
		return $q;
	}

	/**	
	 * loadPertemuan : mendapatkan seluruh pertemuan berdasarkan id_jadwal
	 * @param id <id_jadwal>
	 * @return Array : array pertemuan yang terisi di database
	 */
	function loadPertemuan($id)
	{
		$tableAbsensi = 'tbl_absensi_mhs_new_20171';

		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $id, 'id_jadwal', 'asc')->row();
		$sql = "SELECT DISTINCT pertemuan FROM ".$tableAbsensi." WHERE kd_jadwal = '" . $kode->kd_jadwal . "'";

		$data = $this->db->query($sql);
		$arr = [];
		foreach ($data->result_array() as $key => $value) {
			$arr[] = $value['pertemuan'];
		}
		return $arr;
	}

	/**
	 * Get list lecture course
	 * 
	 * @return array
	 */
	function course_list($nid, $year)
	{
		$courses = $this->db->query('SELECT DISTINCT 
										a.kd_matakuliah,
										a.kd_jadwal,
										a.id_jadwal,
										a.hari,
										a.kd_ruangan,
										a.waktu_mulai,
										a.waktu_selesai,
										a.gabung,
										a.kelas,
										b.nama_matakuliah,
										b.sks_matakuliah,
										IF(a.gabung > 0, 
											(SELECT COUNT(npm_mahasiswa) FROM tbl_krs 
											WHERE kd_jadwal = a.kd_jadwal 
											OR kd_jadwal = a.referensi),
											(SELECT COUNT(npm_mahasiswa) FROM tbl_krs 
											WHERE kd_jadwal = a.kd_jadwal)) AS jumlah_peserta
									FROM tbl_jadwal_matkul a
									JOIN tbl_matakuliah b ON a.`id_matakuliah` = b.`id_matakuliah` 
									WHERE a.`kd_dosen` = "' . $nid . '" 
									AND a.kd_tahunajaran = "' . $year. '" ')->result();
		return $courses;
	}

	/**
	 * Get lecturer course list
	 * @param string $tahunakademik
	 * @param string $prodi
	 * @return array
	 */
	public function lecturer_course_list(string $tahunakademik, string $prodi) : array
	{
		$courses = $this->db->query("SELECT DISTINCT 
										jdl.`kd_dosen`,
										kry.`nama`,
										kry.`nid`,
										(SELECT SUM(mk.`sks_matakuliah`) AS sks 
										FROM tbl_jadwal_matkul jdl
			                    		JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
			                    		WHERE jdl.`kd_dosen` = kry.`nid` 
			                			AND (mk.nama_matakuliah NOT LIKE 'tesis%' 
			                				OR mk.nama_matakuliah NOT LIKE 'skripsi%'
			                				OR mk.nama_matakuliah NOT LIKE 'kerja praktek%' 
			                				OR mk.nama_matakuliah NOT LIKE 'magang%'
			                				OR mk.nama_matakuliah NOT LIKE 'kuliah kerja%') 
			                			AND jdl.kd_tahunajaran = '$tahunakademik' 
			                			AND mk.kd_prodi = '$prodi' 
			                			AND jdl.kd_jadwal LIKE '$prodi%') AS sks 
				            		FROM tbl_jadwal_matkul jdl
									JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
									JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
									where jdl.kd_tahunajaran = '$tahunakademik' 
									AND mk.kd_prodi = '$prodi'
									AND jdl.kd_jadwal LIKE '$prodi%' 
									AND (jdl.`kd_dosen` <> '' OR kry.`nama` != '' OR kry.`nid` <> '')
									ORDER BY kry.`nama` ASC ")->result();
		return $courses;
	}
	
}

/* End of file Ajar_model.php */
/* Location: ./application/models/Ajar_model.php */
