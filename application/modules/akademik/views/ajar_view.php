<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script>
    function edit(id){
        $('#absen').load('<?= base_url();?>akademik/ajar/view_absen/'+id);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Kegiatan Mengajar</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a href="<?=  base_url('akademik/ajar/back') ?>" class="btn btn-warning">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Hari/Ruang</th>
                                <th>Waktu</th>
                                <th>Kelas</th>
                                <th width="80">Jumlah Peserta</th>
                                <th width="40">Absen Dosen</th>
                                <th width="80">Lihat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($rows as $isi): ?>
                                <tr>
                                    <td><?= $isi->kd_matakuliah; ?></td>
                                    <td><?= $isi->nama_matakuliah; ?></td>
                                    <td><?= $isi->sks_matakuliah; ?></td>
                                    <td><?= notohari($isi->hari).' / '.get_room($isi->kd_ruangan); ?></td>
                                    <td><?= del_ms($isi->waktu_mulai).' - '.del_ms($isi->waktu_selesai); ?></td>
                                    <td><?= $isi->kelas;?></td>
                                    <td><?= $isi->jumlah_peserta; ?></td>
                                    <td>
                                        <a 
                                            href="<?= base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
                                            target="_blank" class="btn btn-info btn-small" 
                                            title="Berita Acara"><i class="icon icon-file"></i>
                                        </a>
                                    </td>
                                    <td class="td-actions">
                                        <a 
                                            href="<?= base_url();?>akademik/ajar/cetak_absensi/<?= $isi->id_jadwal; ?>" 
                                            target='_blank' 
                                            class="btn btn-primary btn-small"><i class="btn-icon-only icon-print"></i>
                                        </a>

                                        <?php if ($log['id_user_group'] == 8 OR $log['id_user_group'] == 19) { ?>
                                            
                                            <a 
                                                href="<?= base_url('akademik/ajar/cetak_absensi_polos/').$isi->id_jadwal;?>" 
                                                target='_blank' 
                                                class="btn btn-warning btn-small"><i class="btn-icon-only icon-print"></i>
                                            </a>

                                        <?php } ?>
                                    </td>
                                </tr>    
                            <?php $no++; endforeach ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->