<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	
	var table_jadwal = $('#tabel_jadwal');
	
	var oTable_jadwal = table_jadwal.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [4,5] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		oTable.on('click', 'tbody tr .edit', function() {
			var nRow = $(this).parents('tr')[0];
			var aData = oTable.fnGetData(nRow);
			var kd_matakuliah = aData[1];
			$.ajax({
				url: "<?php echo base_url('akademik/viewkrs/get_jadwal'); ?>",
				type: "post",
				data: {kd_matakuliah: kd_matakuliah},
				success: function(d) {
					var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable_jadwal.fnClearTable();
                    oTable_jadwal.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_jadwal.fnAddData([arr[i]['hari'],arr[i]['waktu_mulai'].substring(0, 5)+'/'+arr[i]['waktu_selesai'].substring(0, 5), arr[i]['kode_ruangan'],'',arr[i]['kd_jadwal'],arr[i]['kd_matakuliah']]);
                    }
					
					$('#jadwal').modal('show');
				}
			});	
		});
		
		oTable_jadwal.on( 'click', 'tr', function () {
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
        }
        else {
            table_jadwal.$('tr.active').removeClass('active');
            $(this).addClass('active');
			var aData = oTable_jadwal.fnGetData(this);
			$('#kd_jadwal').val(aData[4]);
			$('#kd_matakuliah').val(aData[5]);
        }
    } );
	});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>akademik/bimbingan" class="btn btn-success"><< Kembali</a>
					<a href="<?php echo base_url(); ?>akademik/viewkrs/printkrs" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KRS</a>
					<hr>
					<table>
						<!--tr>
							<td>Status</td>
							<td> : <?php //if($pembimbing['status_verifikasi'] == 0) { echo 'Belum Disetujui'; } ?></td>
							<td></td>
						</tr-->
						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Catatan</td>
							<td> : <?php echo $pembimbing['keterangan_krs']; ?></td>
							<td></td>
						</tr>
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>