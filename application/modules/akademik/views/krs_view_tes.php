<?php// var_dump($kode_krs);die(); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/plugins/TableTools/js/dataTables.tableTools.min.js"></script>

<script type="text/javascript">
	function loadforkhs(edl) {
        $("#khshere").load('<?php echo base_url()?>akademik/krs_mhs/load_khs/'+edl);
    }
</script>

<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	
	var table_tambahan = $('#tabel_tambahan');
	
	var oTable_tambahan = table_tambahan.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
	});
	
	var table_rekam = $('#tabel_rekam');
	
	var oTable_rekam = table_rekam.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	
	$("#semester").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		var prodi = <?php echo $prodi; ?>;
		$.ajax({
                url: "<?php echo base_url('akademik/krs_mhs/get_matkul'); ?>",
                type: "post",
                data: {semester: semester, npm:npm, prodi:prodi},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable.fnClearTable();
                    oTable.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'],'<input type="checkbox" name="kd_matkuliah[]" value="'+arr[i]['kd_matakuliah']+'"/><input type="hidden" name="semester_matakuliah[]" value="'+arr[i]['semester_matakuliah']+'"/>']);
                    }

                }
            });
	});
	
	$("#semester_rekam").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		$.ajax({
                url: "<?php echo base_url('akademik/krs_mhs/get_krs'); ?>",
                type: "post",
                data: {semester: semester, npm:npm},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable_rekam.fnClearTable();
                    oTable_rekam.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_rekam.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'],'<input type="checkbox" name="kd_matkuliah[]" value="'+arr[i]['kd_matakuliah']+'"/><input type="hidden" name="semester_matakuliah[]" value="'+arr[i]['semester_matakuliah']+'"/>']);
                    }

                }
            });
	});
	
	$("#semester_tambahan").change(function(){
		var semester = $(this).val();
		var npm = <?php echo $npm; ?>;
		var prodi = <?php echo $prodi; ?>;
		$.ajax({
                url: "<?php echo base_url('akademik/krs_mhs/get_matkul'); ?>",
                type: "post",
                data: {semester: semester, npm:npm, prodi:prodi},
                success: function(d) {
                    var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }

                    oTable_tambahan.fnClearTable();
                    oTable_tambahan.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_tambahan.fnAddData([arr[i]['kd_matakuliah'], arr[i]['nama_matakuliah'], arr[i]['sks_matakuliah'], arr[i]['prasyarat_matakuliah'],'<button type="button" class="btn btn-primary btn-small edit pluss"><i class="btn-icon-only icon-plus"> </i></button>',arr[i]['semester_kd_matakuliah']]);
                    }

                }
            });
	});
	
	oTable_tambahan.on('click', 'tbody tr .pluss', function() {
			var nRow = $(this).parents('tr')[0];
			var aData = oTable_tambahan.fnGetData(nRow);
			var kd_mk = aData[0];
			var nama_mk = aData[1];
			var sks_mk = aData[2];
			var prasyarat = aData[3];
			var sms_mk = aData[5];
			
			if(prasyarat!='[]'){
				$.ajax({
					url: "<?php echo base_url('akademik/krs_mhs/cek_prasyarat'); ?>",
					type: "post",
					data: {prasyarat: prasyarat},
					success: function(d) {
						if(d>0){
							var a = oTable.fnAddData([kd_mk, nama_mk, sks_mk, '<button type="button" class="btn btn-danger btn-small edit delete"><i class="btn-icon-only icon-remove"> </i></button><input type="hidden" name="kd_matkuliah[]" value="'+kd_mk+'"/><input type="hidden" name="semester_matakuliah[]" value="'+sms_mk+'"/>']);
							var nTr = oTable.fnSettings().aoData[ a[0] ].nTr;
							nTr.className = "cek";
							oTable_tambahan.fnDeleteRow(nRow);
						}else{
							alert('Prasyarat matakuliah tidak terpenuhi');
						}
					}
				});
			}else{
				var a = oTable.fnAddData([kd_mk, nama_mk, sks_mk, '<button type="button" class="btn btn-danger btn-small edit delete"><i class="btn-icon-only icon-remove"> </i></button><input type="hidden" name="kd_matkuliah[]" value="'+kd_mk+'"/><input type="hidden" name="semester_matakuliah[]" value="'+sms_mk+'"/>']);
				var nTr = oTable.fnSettings().aoData[ a[0] ].nTr;
				nTr.className = "cek";
				
				oTable_tambahan.fnDeleteRow(nRow);
			}	
		sum_sks();
    });
	
	oTable.on('click', 'tbody tr .delete', function() {
		var nRow = $(this).parents('tr')[0];
		oTable.fnDeleteRow(nRow);
		sum_sks();
	});
	
	function sum_row(oTable, nRow) {
		var intVal = function(i) {
        return typeof i === 'string' ?
			i.replace(/[\$,]/g, '') * 1 :
            typeof i === 'number' ?
            i : 0;
        };

        var api = oTable.api(), data;

        var total = api
			.cells(nRow,2)
            .data()
            .reduce(function(a, b) {
				return intVal(a) + intVal(b);
			});
        return total;
	}
	
	function sum_sks(){
		$.ajax({
			url: "<?php echo base_url('akademik/krs_mhs/cekjumlahsks'); ?>",
			type: "post",
			data: {sks: sum_row(oTable, oTable.api().rows('.cek').indexes())},
			success: function(d) {
				if(d>0){
					document.getElementById("simpan").disabled = false; //harus falefalse
					$('#total').html('Total SKS : '+sum_row(oTable, oTable.api().rows('.cek').indexes()));
					$("#total_sks").val(sum_row(oTable, oTable.api().rows('.cek').indexes()));
				}else{
					alert('Jumlah SKS Melebihi Ketentuan');
					document.getElementById("simpan").disabled = true;
					$('#total').html('Total SKS : '+sum_row(oTable, oTable.api().rows('.cek').indexes()));
					$("#total_sks").val(sum_row(oTable, oTable.api().rows('.cek').indexes()));
				}
			}
		});
	}
	
	oTable.on('click', 'tbody tr .td-actions .checks', function() {
		var nRow = $(this).parents('tr')[0];
		var aData = oTable.fnGetData(nRow);
		if($(this).is(":checked")){
			$(this).parents('tr').addClass('cek');
		 }else{
			$(this).parents('tr').removeClass('cek');
		 }
		//alert(aData[2]);
		sum_sks();
	});
	
	sum_sks();
});
</script>

<?php // var_dump($semester);exit(); ?>
<div class="row">
  <!-- /span6 -->
  <div class="span12">
     <div class="widget widget-nopad" style="margin-top:30px">
      <div class="widget-header"> <i class="icon-edit"></i>
        <h3>Form KRS <?php echo $nama_mahasiswa.' ( '.$npm.' )'; ?></h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content" style="padding:30px;">
            <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#form_pendaftar" data-toggle="tab">Form KRS</a>
              </li>
            </ul>           
            <br>            
              <div class="tab-content">
                <div class="tab-pane active" id="form_pendaftar">
                <form id="edit-profile" class="form-horizontal" action="<?php echo base_url(); ?>akademik/krs_mhs/saving" method="post" onsubmit="simpan.disabled = true; simpan.value='Please wait ..'; return true;">
                  <fieldset>
                  	<?php 
                  		$q = $this->temph_model->get_ipk($npm)->result();
						$tskss = 0;
						$tnl=0;
						foreach ($q as $row) {
							$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
							$tskss=$tskss+$row->sks_matakuliah;
							$tnl = $tnl+$nk;
						}
						$ipk = number_format($tnl/$tskss,2);
                  	 ?>
                  	<b style="background:#00FF7F;padding:2px;border-radius:3px;">IPS terakhir : <?php echo number_format($ipsmhs_before,2); ?></b>&nbsp&nbsp
                  	<b style="background:#9ACD32;padding:2px;border-radius:3px;">IPK terakhir : <?php echo $ipk; ?></b>
                  	<br><br>

					<a data-toggle="modal" href="#myModal" class="btn btn-primary"><i class="icon icon-plus"></i> Tambah Matakuliah </a>
					<a href="#btnkhs" onclick="loadforkhs('<?php echo $seekhs; ?>')" class="btn btn-warning" data-toggle="modal" title=""><i class="icon icon-eye-open"></i>	Lihat Khs</a>
					<a href="<?php echo base_url(); ?>akademik/khs/transkrip/<?php echo $npm; ?>" class="btn btn-success" target="blank"><i class="icon icon-print"></i> Cetak Transkrip </a>
					<a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah" class="btn btn-info" target="_blank"><i class="icon icon-table"></i> Lihat Jadwal </a>
					<a data-toggle="modal" href="#myModal1" class="btn btn-danger"><i class="icon icon-ok"></i> Ketentuan SKS </a>
                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
					
					<table id="tabel_krs" class="table table-bordered table-striped">
                    <thead>
                          <tr> 
                            <th>Kode MKkk</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
						<?php $no = 1; foreach ($data_matakuliah as $row) { ?>
						<tr class="<?php if($status_krs==1) echo 'cek'; ?>">
							<input type="hidden" name="kode_krs" value="<?php if($status_krs==1) { echo $row->kd_krs; } ?>"/>
                        	<td><?php echo $row->kd_matakuliah;?></td>
                        	<td><?php echo $row->nama_matakuliah;?></td>
                            <td><?php echo $row->sks_matakuliah; ?></td>
                        	<td class="td-actions">
                        		<?php if ($row->prasyarat_matakuliah == '[]') {  ?>
									<input type="hidden" name="semester_matakuliah[]" value="<?php echo $row->semester_kd_matakuliah; ?>"/>
									<input type="checkbox" class="checks" name="kd_matkuliah[]" value="<?php echo $row->kd_matakuliah; ?>" <?php if($status_krs==1) echo 'checked'; ?>/>
								<?php } else { 
									$exp = explode(',', $row->prasyarat_matakuliah);
									$hit = array();
									for ($i=0; $i < count($exp); $i++) { 
										$arr = array('[',']');
										$rep = str_replace($arr, '', $exp[$i]);
										$kv = $this->db->where('kd_baru',$rep)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
											if (count($kv) > 0) {
												$hit[] = $kv->kd_lama;
												$kv2 = $this->db->where('kd_baru',$kv->kd_lama)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
													if (count($kv2) > 0) {
														$hit[] = $kv2->kd_lama;
														$kv3 = $this->db->where('kd_baru',$kv2->kd_lama)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
															if (count($kv3) > 0) {
																$hit[] = $kv3->kd_lama;
															}
													}
											}
										$hit[] = $rep;
									}
									
									$cek = $this->temph_model->cek_prasyarat($hit,$npm)->result();
									
									if (count($cek) == 0) { $cekmk = $this->temph_model->getdetail('tbl_matakuliah','kd_matakuliah',$hit,'id_matakuliah','asc')->row()->nama_matakuliah;?>
										
										<a href="javascript:void(0);" onclick="alert('Anda Belum Menyelesaikan Matakuliah <?php echo $cekmk.' '; ?> Sebagai Matakuliah Prasyarat!')"><i class="icon icon-remove"></i></a>
									<?php } else { 
										?>
										<input type="hidden" name="semester_matakuliah[]" value="<?php echo $row->semester_kd_matakuliah; ?>"/>
										<input type="checkbox" class="checks" name="kd_matkuliah[]" value="<?php echo $row->kd_matakuliah; ?>" <?php if($status_krs==1) echo 'checked'; ?>/>
									<?php } ?>
								<?php } ?>
							</td>
                        </tr>
	                    <?php $no++; } ?>
	                    <?php $no1 = 1; foreach ($data_ngulang as $row1) { ?>
	                    <tr style="background:#ff3333;">
	                    	<td style="background:#ff3333;"><?php echo $row1->kd_matakuliah;?></td>
                        	<td style="background:#ff3333;"><?php echo $row1->nama_matakuliah;?></td>
                            <td style="background:#ff3333;"><?php echo $row1->sks_matakuliah; ?></td>
                        	<td class="td-actions" style="background:#ff3333;">
                        		<?php if ($row1->prasyarat_matakuliah == '[]') {  ?>
									<input type="hidden" name="semester_matakuliah[]" value="<?php echo $row1->semester_kd_matakuliah; ?>"/>
									<input type="checkbox" class="checks" name="kd_matkuliah[]" value="<?php echo $row1->kd_matakuliah; ?>" <?php if($status_krs==1) echo 'checked'; ?>/>
								<?php } else { 
									$exp = explode(',', $row1->prasyarat_matakuliah);
									$hit = array();
									for ($i=0; $i < count($exp); $i++) { 
										$arr = array('[',']');
										$rep = str_replace($arr, '', $exp[$i]);
										$kv = $this->db->where('kd_baru',$rep)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
											if (count($kv) > 0) {
												$hit[] = $kv->kd_lama;
												$kv2 = $this->db->where('kd_baru',$kv->kd_lama)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
													if (count($kv2) > 0) {
														$hit[] = $kv2->kd_lama;
														$kv3 = $this->db->where('kd_baru',$kv2->kd_lama)->where('flag',1)->get('tbl_konversi_matkul_temp')->row();
															if (count($kv3) > 0) {
																$hit[] = $kv3->kd_lama;
															}
													}
											}
										$hit[] = $rep;
									}
									
									$cek = $this->temph_model->cek_prasyarat($hit,$npm)->result();
									
									if (count($cek) == 0) { 
											$cekmk = $this->temph_model->getdetail('tbl_matakuliah','kd_matakuliah',$hit,'id_matakuliah','asc')->row();
										?>
										<a href="javascript:void(0);" onclick="alert('Anda belum Menyelesaikan Matakuliah <?php echo $cekmk->nama_matakuliah.' '.$cekmk->kd_matakuliah; ?> Sebagai Matakuliah Prasyarat!')"><i class="icon icon-remove"></i></a>
									<?php } else { 
										
										?>
										<input type="hidden" name="semester_matakuliah[]" value="<?php echo $row1->semester_kd_matakuliah; ?>"/>
										<input type="checkbox" class="checks" name="kd_matkuliah[]" value="<?php echo $row1->kd_matakuliah; ?>" <?php if($status_krs==1) echo 'checked'; ?>/>
									<?php } ?>
								<?php } ?>
							</td>
	                    </tr>
	                    <?php $no1++; } ?>
                      </tbody>
                  </table>
				  <div id="total" style="font-weight: bolder;font-size: 14px;text-align: right;"></div>
				  <!-- <input type="hidden" name="kode_krs" value="<?php if($status_krs==1) { echo $kode_krs; } ?>"/> -->
				  <input type="hidden" name="jumlah_sks" value="<?php echo $row->sks_matakuliah; ?>" id="total_sks"/>
				  <input type="hidden" name="kodos" value="<?php echo $dospem; ?>" placeholder="">
                  <!-- <label class="control-label" for="firstname">Catatan Pembimbing</label>
				  <textarea name="keterangan_krs" class="form-control span10" rows="4" required><?php if($status_krs==1) { echo $catatan_pembimbing; } ?></textarea> -->
                      <div class="form-actions">
						<input type="hidden" name="npm_mahasiswa" value="<?php echo $npm; ?>" />
                        <button type="submit" id="simpan" name="simpan" class="btn btn-primary">Simpan</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
			</div>
          </div> <!-- /widget-content -->
		</div>  
	</div>
</div>        


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Matakuliah</h4>
            </div>
            <div class="modal-body">    
            
                <select class="form-control span2" id="semester_tambahan">
						<option value="">--Pilih Semester--</option>
						<?php $kdprd = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row()->KDPSTMSMHS; if($kdprd == '74101' or $kdprd == '61101') {?>
							<?php //if(($semester %2) == 1 ){ $mulai = 1; } else { $mulai = 2; }
							for ($i=1; $i <= 4 ; $i++) { 
								if ($i != $semester) {
									echo '<option value="'.$i.'">Semester '.$i.'</option>';	
								}
							 } 
							
						 	?>
						<?php } else { ?>
							<?php //$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row()->tahun_akademik; ?>
							<?php if(($semester %2) == 1 ){ $mulai = 1; } else { $mulai = 2; }
							for ($i=$mulai; $i <= 8 ; $i++) { 
								// if ($i != $semester) {
									echo '<option value="'.$i.'">Semester '.$i.'</option>';	
								// }
								$i=$i+1;
							 } 
							 if ($mulai == 1) {
								echo '<option value="8">Semester 8</option>';
							} else {
								echo '<option value="7">Semester 7</option>';
							}
							
						 	} ?>
                </select>
                <table id="tabel_tambahan" class="table table-bordered table-striped">
                  <thead>
                        <tr> 
                          <th>Kode MK</th>
                          <th>Mata Kuliah</th>
                          <th>SKS</th>
						  <th>Prasyarat</th>
                          <th>Aksi</th>
						  <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Selesai</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ketentuan SKS Berdasarkan IPS Semester Sebelumnya</h4>
            </div>
            <div class="modal-body">    
                <table class="table table-bordered table-striped">
                  <thead>
                        <tr> 
                          <th>No</th>
                          <th>IPS</th>
                          <th>SKS</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr> 
                          <td>1</td>
                          <td> > 3.00</td>
                          <td>21-24 SKS</td>
                        </tr>
                        <tr> 
                          <td>2</td>
                          <td>2.50-2.99</td>
                          <td>18-21 SKS</td>
                        </tr>
                        <tr> 
                          <td>3</td>
                          <td>2.00-2.49</td>
                          <td>15-18 SKS</td>
                        </tr>
                        <tr> 
                          <td>4</td>
                          <td>1.40-1.99</td>
                          <td>12-15 SKS</td>
                        </tr>
                        <tr> 
                          <td>5</td>
                          <td> < 1.40 </td>
                          <td>12 SKS KEBAWAH </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="btnkhs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">KHS Semester Sebelumnya</h4>
            </div>
            <div class="modal-body" id="khshere">    
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

