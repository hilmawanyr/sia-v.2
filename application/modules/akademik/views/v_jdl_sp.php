<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">PILIH JADWAL MATAKULIAH PERBAIKAN</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/perbaikan/save_jadwal" method="post">
    <div class="modal-body">    
		<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>
		<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>
		<input type="hidden" id="kd_krs" name="kd_krs" value="<?php echo $kd_krs; ?>"/>
        <table id="tabel_jadwal" class="table table-bordered table-striped">
          <thead>
                <tr> 
                  <th>Hari</th>
                  <th>Kelas</th>
                  <th>Jam</th>
                  <th>Ruang</th>
        				  <th>Dosen</th>
        				  <th>Kuota</th>
        				  <th width="40">Aksi</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($getjdl as $key) { ?>
            		<tr>
	            		<td><?php echo notohari($key->hari); ?></td>
	            		<td><?php echo $key->kelas; ?></td>
	            		<td><?php echo $key->waktu_mulai; ?></td>
	            		<td><?php echo $key->ruangan; ?></td>
	            		<td><?php echo $key->kd_dosen; ?></td>
	            		<td><?php ?></td>
	            		<td><a href="<?php echo base_url('akademik/perbaikan/add_jdl/'.$key->id_jadwal); ?>" title="Tambah Jadwal" class="btn btn-success"><i class="icon icon-plus"></i></a></td>
	            	</tr>
            	<?php } ?>
            </tbody>
        </table>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>