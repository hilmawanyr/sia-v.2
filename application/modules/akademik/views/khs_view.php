<?php error_reporting(0); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Hasil Studi</h3>
			</div> <!-- /widget-header -->
			
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<!-- <a href="<?php //echo base_url(); ?>akademik/khs/transkrip" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a> -->
					<?php $statusVerifikasi 	= $this->db->query("SELECT status_verifikasi from tbl_verifikasi_krs where kd_krs 
												LIKE '".$mhs->NIMHSMSMHS."%' and status_verifikasi = 10")->row()->status_verifikasi;
						  if ($statusVerifikasi != 10) {
                                 //if (substr($mhs->NIMHSMSMHS, 0,4) > 2014) { ?>
						<a href="<?php echo base_url(); ?>akademik/khs/transkrip/<?php echo $mhs->NIMHSMSMHS; ?>" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a>
					<?php } ?>
					
					<hr>
					<table style="overflow-x:auto !important;">

                        <tr>

                            <td>NPM</td>

                            <td>:</td>

                            <td><?php echo $mhs->NIMHSMSMHS;?></td>

                        </tr>

                        <tr>

                            <td>Nama</td>

                            <td>:</td>

                            <td><?php echo $mhs->NMMHSMSMHS;?></td>

                        </tr>
                        <?php
                        $logged = $this->session->userdata('sess_login');
	                    $pecah = explode(',', $logged['id_user_group']);
						$jmlh = count($pecah);
						for ($i=0; $i < $jmlh; $i++) { 
							$grup[] = $pecah[$i];
						}
	                    if ((in_array(5, $grup)) or (in_array(7, $grup))) {
							$prodi = $mhs->KDPSTMSMHS;
						} else {
							$prodi = $logged['userid'];
						}
						$tskss = 0;
						$tnl=0;
						if (count($detail_khs_konversi) > 0) {
							$q = $this->db->query('SELECT nl.`KDKMKTRLNM`,
									IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
										(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
									IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
										(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
									MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,nl.`THSMSTRLNM`,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai_konversi nl 
									WHERE nl.`NIMHSTRLNM` = "'.$mhs->NIMHSMSMHS.'" and NLAKHTRLNM != "T"
									AND nl.deletedAt IS NULL
									GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC
									')->result();
							$tskss = 0;
							$tnl=0;
							foreach ($q as $row) {
								$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
								$tskss=$tskss+$row->sks_matakuliah;
								$tnl = $tnl+$nk;
							}
						}
						$hitung_ips = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
										WHERE nl.`NIMHSTRLNM` = "'.$mhs->NIMHSMSMHS.'" and NLAKHTRLNM != "T"
										AND nl.deletedAt IS NULL
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
						// get ips
						// $ips = $this->krs_model->countips($prodi, $mhs->NIMHSMSMHS, $year);

						$st=0;
						$ht=0;
						foreach ($hitung_ips as $iso) {
							$h = 0;


							$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
							$ht=  $ht + $h;

							$st=$st+$iso->sks_matakuliah;
						}

						$ipk = $ht/$st;
						$ipk = number_format($ipk,2);
						
						$totalksks=$tskss+$st;
						
						//}
						$jkon=$tnl+$ht;
						$ipk_kon=$jkon/$totalksks;
						$ipk_kon = number_format($ipk_kon,2);
                        ?>
                        <tr>
                            <td>IPK</td>

                            <td>:</td>

                            <?php if (count($detail_khs_konversi) > 0) {?>
                            <td><?php echo $ipk_kon; ?></td>
							<?php }else{ ?>
                            <td><?php echo $ipk; ?></td>
							<?php } ?>

                        </tr>

                        <tr>

                            <td>SKS</td>

                            <td>:</td>
							<?php if (count($detail_khs_konversi) > 0) {?>
                            <td><?php echo $tskss+$st; ?></td>
							<?php }else{ ?>
                            <td><?php echo $st; ?></td>
							<?php } ?>
                        </tr>


                    </table>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                        	<th>IPS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; if (count($detail_khs_konversi) > 0) { foreach ($detail_khs_konversi as $row2) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td>KONVERSI</td>
	                        	<?php
	                        		$q = $this->db->query("select distinct KDKMKTRLNM from tbl_transaksi_nilai_konversi where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' and THSMSTRLNM = '".$row2->THSMSTRLNM."'")->result();
		                        	$sumsks = 0;
		                        	if ($row->THSMSTRLNM < '20151') {
		                        		foreach ($q as $value1) {
			                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah_copy where kd_matakuliah = '".$value1->KDKMKTRLNM."' AND kd_prodi = ".trim($row2->KDPSTTRLNM)." and tahunakademik = '".$row2->THSMSTRLNM."'")->row()->sks_matakuliah; 
			                        		$sumsks = $sumsks + $sksmk;
			                        	}
		                        	} else {
		                        		foreach ($q as $value1) {
			                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$value1->KDKMKTRLNM."' AND kd_prodi = ".trim($row2->KDPSTTRLNM)."")->row()->sks_matakuliah; 
			                        		$sumsks = $sumsks + $sksmk;
			                        	}
		                        	}
		                        	
	                        	?>
                                <td><?php echo $sumsks; ?></td>
                                <td>-</td>
	                        	<td class="td-actions">
	                        		<?php if ((in_array(7, $grup)) || (in_array(6, $grup))) { ?>
										<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/bimbingan/detailKhs_konversi/<?php echo $mhs->NIMHSMSMHS; ?>/<?php echo $row2->THSMSTRLNM; ?>"><i class="btn-icon-only icon-ok"> </i></a>
										<!-- <a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-ok"> </i></a> -->
									<?php } else { ?>
										<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/khs/detailKhs_konversi/<?php echo $mhs->NIMHSMSMHS; ?>/<?php echo $row2->THSMSTRLNM; ?>"><i class="btn-icon-only icon-ok"> </i></a>
										<!--  <a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-ok"> </i></a-->
									<?php } ?>
									
									<?php //} ?>
								</td>
	                        </tr>
							<?php $no++; } } ?>
							<?php foreach ($detail_khs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,$row->THSMSTRLNM); ?>
	                        	<td><?php echo $a; ?></td>
	                        	<?php
	                        	if ($row->THSMSTRLNM < '20151') {
	                        		$q = $this->db->query("select distinct KDKMKTRLNM from tbl_transaksi_nilai where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' and THSMSTRLNM = '".$row->THSMSTRLNM."'")->result();
		                        	$sumsks = 0;
		                        	foreach ($q as $value1) {
		                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah_copy where kd_matakuliah = '".$value1->KDKMKTRLNM."' AND kd_prodi = ".trim($row->KDPSTTRLNM)."")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	} else {
	                        		$q = $this->db->query("select distinct kd_matakuliah from tbl_krs where kd_krs LIKE CONCAT('".$mhs->NIMHSMSMHS."','".$row->THSMSTRLNM."%')")->result();
		                        	$sumsks = 0;
		                        	foreach ($q as $value1) {
		                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$value1->kd_matakuliah."' AND kd_prodi = ".trim($row->KDPSTTRLNM)."")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	}
	                        	?>
                                <td><?php echo $sumsks; ?></td>
                                <!-- <td><?php //echo $row->jum_sks; ?></td> -->
                                <?php 
                                $logged = $this->session->userdata('sess_login');
			                    $pecah = explode(',', $logged['id_user_group']);
								$jmlh = count($pecah);
								for ($i=0; $i < $jmlh; $i++) { 
									$grup[] = $pecah[$i];
								}
			                    if ((in_array(5, $grup)) || (in_array(7, $grup))) {
									$prodi = $mhs->KDPSTMSMHS;
								} elseif (in_array(10, $grup)) {
									$prodi = $mhs->KDPSTMSMHS;
								} else {
									$prodi = $logged['userid'];
								}

								if ($row->THSMSTRLNM < '20151') {
			                        $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                        JOIN tbl_matakuliah_copy b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                        WHERE b.`tahunakademik` = "'.$row->THSMSTRLNM.'" AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$mhs->NIMHSMSMHS.'" and THSMSTRLNM = "'.$row->THSMSTRLNM.'" ')->result();
			                    } else {
			                        $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                        WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$mhs->NIMHSMSMHS.'" and THSMSTRLNM = "'.$row->THSMSTRLNM.'" and NLAKHTRLNM <> "T" ')->result();
			                    }

			                    $st=0;
			                    $ht=0;
			                    foreach ($hitung_ips as $iso) {
			                        $h = 0;

			                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			                        $ht=  $ht + $h;

			                        $st=$st+$iso->sks_matakuliah;
			                    }

			                    $ips_nr = $ht/$st;
                                ?>

                                <?php $statusVerifikasi = $this->db->query("SELECT status_verifikasi from tbl_verifikasi_krs where kd_krs 
                                											LIKE CONCAT('".$mhs->NIMHSMSMHS."','".$row->THSMSTRLNM."%')")->row()->status_verifikasi; 

                                if ($statusVerifikasi == 10) {
                                	$jadinya = '-';
                                } else {
                                	$jadinya = number_format($ips_nr, 2);
                                }
                                
                                ?>

                                <td><?php echo $jadinya; ?></td>
	                        	<td class="td-actions">
	                        		<?php if ((in_array(7, $grup)) || (in_array(6, $grup))) { ?>
										<a 
											class="btn btn-success btn-small" 
											href="<?= base_url('akademik/bimbingan/detailKhs/'.$mhs->NIMHSMSMHS.'/'.$row->THSMSTRLNM); ?>"><i class="btn-icon-only icon-ok"> </i>
										</a>

									<?php } else { ?>
										<a 
											class="btn btn-success btn-small" 
											href="<?= base_url('akademik/khs/detailKhs/'.$mhs->NIMHSMSMHS.'/'.$row->THSMSTRLNM); ?>">
											<i class="btn-icon-only icon-ok"> </i>
										</a>
									<?php } ?>
									
									<?php //} ?>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>