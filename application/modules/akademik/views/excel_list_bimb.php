<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=DAFTAR_MHS_BIMBINGAN.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <?php 
            $cek = $this->db->query("SELECT kode from tbl_tahunakademik where status = 1")->row()->kode;
            $ta = $this->db->query("SELECT * from tbl_tahunakademik where kode <= '".$cek."'")->result();
            $spans = count($ta);
        ?>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">NPM</th>
            <th rowspan="2">NAMA MAHASISWA</th>
            <th rowspan="2">Angkatan</th>
            <th colspan="<?php echo $spans; ?>">Tahun Akademik</th>
        </tr>
        
        <tr>
            <?php foreach ($ta as $val) { ?>
                <th><?php echo $val->kode; ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($query as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->npm_mahasiswa; ?></td>
            <td><?php echo $key->NMMHSMSMHS; ?></td>
            <td><?php echo $key->TAHUNMSMHS; ?></td>
            <?php foreach ($ta as $val) { 
                $cex = $this->db->query("SELECT * from tbl_verifikasi_krs where tahunajaran = '".$val->kode."' and npm_mahasiswa = '".$key->npm_mahasiswa."'")->row_array();
                if (count($cex['npm_mahasiswa']) > 0) { ?>
                     <td>v</td>
                <?php } else { ?>
                     <td> - </td>
                <?php }  ?>
            <?php } ?>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>