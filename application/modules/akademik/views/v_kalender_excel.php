<?php 
$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Data Kalender Akademik');

//Title
$excel->getActiveSheet()->setCellValue('A1', 'JADWAL KALENDER AKADEMIK');
$excel->getActiveSheet()->setCellValue('A2', 'UNIVERSITAS BHAYANGKARA JAKARTA RAYA');
$excel->getActiveSheet()->setCellValue('A3', 'SEMESTER GENAP TAHUN AKADEMIK 2016/2017');

#ambil cells


#Table 1
#Header
$y = 5;
$yh1 = $y;
$excel->getActiveSheet()->setCellValue('A'.$y, 'KEGIATAN PRA SEMESTER');
$y++;
$excel->getActiveSheet()->setCellValue('A'.$y, 'NO');
$excel->getActiveSheet()->setCellValue('B'.$y, 'KEGIATAN');
$excel->getActiveSheet()->setCellValue('C'.$y, 'WAKTU  PELAKSANAAN');
$excel->getActiveSheet()->setCellValue('D'.$y, 'KETERANGAN');

#isi - td
$y++;
$no = 1;
foreach ($pra as $isi) {

	
    if ($isi->mulai == $isi->ahir) {
        $waktu = TanggalIndo($isi->mulai);
    }else{
        $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
    }

	$excel->getActiveSheet()->setCellValue('A'.$y, $no);
	$excel->getActiveSheet()->setCellValue('B'.$y, $isi->kegiatan);
	$excel->getActiveSheet()->setCellValue('C'.$y, $waktu);
	$excel->getActiveSheet()->setCellValue('D'.$y, $isi->keterangan);
	$no++;
	$y++;
}
$yf1 = $y - 1;




#Table 2
#Header
$yh2 = $y++;
$excel->getActiveSheet()->setCellValue('A'.$y, 'KEGIATAN SEMESTER PERKULIAHAN');
$y++;
$excel->getActiveSheet()->setCellValue('A'.$y, 'NO');
$excel->getActiveSheet()->setCellValue('B'.$y, 'KEGIATAN');
$excel->getActiveSheet()->setCellValue('C'.$y, 'WAKTU  PELAKSANAAN');
$excel->getActiveSheet()->setCellValue('D'.$y, 'KETERANGAN');

#isi - td
$y++;
$no = 1;
foreach ($acara as $isi) {

	
    if ($isi->mulai == $isi->ahir) {
        $waktu = TanggalIndo($isi->mulai);
    }else{
        $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
    }

	$excel->getActiveSheet()->setCellValue('A'.$y, $no);
	$excel->getActiveSheet()->setCellValue('B'.$y, $isi->kegiatan);
	$excel->getActiveSheet()->setCellValue('C'.$y, $waktu);
	$excel->getActiveSheet()->setCellValue('D'.$y, $isi->keterangan);
	$no++;
	$y++;
}
$yf2 = $y - 1;



#Table 3
#Header
$yh3 = $y++;
$excel->getActiveSheet()->setCellValue('A'.$y, 'KEGIATAN PEMBAYARAN PERKULIAHAN');
$y++;
$excel->getActiveSheet()->setCellValue('A'.$y, 'NO');
$excel->getActiveSheet()->setCellValue('B'.$y, 'KEGIATAN');
$excel->getActiveSheet()->setCellValue('C'.$y, 'WAKTU  PELAKSANAAN');
$excel->getActiveSheet()->setCellValue('D'.$y, 'KETERANGAN');

#isi - td
$y++;
$no = 1;
foreach ($biaya as $isi) {

    if ($isi->mulai == $isi->ahir) {
        $waktu = TanggalIndo($isi->mulai);
    }else{
        $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
    }
	$excel->getActiveSheet()->setCellValue('A'.$y, $no);
	$excel->getActiveSheet()->setCellValue('B'.$y, $isi->kegiatan);
	$excel->getActiveSheet()->setCellValue('C'.$y, $waktu);
	$excel->getActiveSheet()->setCellValue('D'.$y, $isi->keterangan);
	$no++;
	$y++;
}
$yf3 = $y - 1;




//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

#merge cells
#header
$excel->getActiveSheet()->mergeCells("A1:F1");
$excel->getActiveSheet()->mergeCells("A2:F2");
$excel->getActiveSheet()->mergeCells("A3:F3");

#table head
#th
$excel->getActiveSheet()->mergeCells("A".$yh1.":D".$yh1);
$excel->getActiveSheet()->mergeCells("A".$yh2.":D".$yh2);
$excel->getActiveSheet()->mergeCells("A".$yh3.":D".$yh3);

#align center
$excel->getActiveSheet()->getStyle("A1:F3")->applyFromArray($style);

//change the font style
#header
$excel->getActiveSheet()->getStyle('A1:F3')->getFont()->setBold(true);
#table
$a = $yh1 + 1;
$b = $yh2 + 2;
$c = $yh3 + 2;
$excel->getActiveSheet()->getStyle('A'.$yh1.':D'.$a)->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A'.$yh2.':D'.$b)->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A'.$yh3.':D'.$c)->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle()->getFont()->setSize(11);

//border
$yh2 = $yh2-1;
$yh3 = $yh3-1;
$excel->getActiveSheet()->getStyle("A".$yh1.":D".$yf1)->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle("A".$yh2.":D".$yf2)->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle("A".$yh3.":D".$yf3)->applyFromArray($BStyle);


$filename = 'KALENDER_AKADEMIK.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>