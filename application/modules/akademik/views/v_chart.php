<style type="text/css">
${demo.css}
</style>
<script type="text/javascript">
// function hey(kd)
// {
//     $('#TEKNIK').load('<?php echo base_url ?>akademik/chart/jml'+kd);
// }
</script>
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Mahasiswa Aktif UBJ per 20151'
        },
        subtitle: {
            text: 'Klik grafik untuk melihat jumlah per prodi'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} mhs'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            // events: {
            //         click : function() {
            //             $('#TEKNIK').load('<?php echo base_url() ?>akademik/chart/jml/$ds->kd_fakultas');
            //         }
            //     }, 
            colorByPoint: true,
            data: [{
                <?php 
                    $baris = count($tek);
                    $no = 0;
                    foreach ($tek as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "name: '".$row->fakultas."', y:".$row->jmlh.", drilldown:'".$row->fakultas."'}]";
                        } else {
                            echo "name: '".$row->fakultas."', y:".$row->jmlh.", drilldown:'".$row->fakultas."'}, {";
                        };
                    }
                ?>
        }],
        drilldown: {
            series: [{
                name: 'teknik',
                id: 'TEKNIK',
                data: [
                        <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "2" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ekonomi',
                id: 'EKONOMI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "1" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'hukum',
                id: 'HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "3" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'fikom',
                id: 'ILMU KOMUNIKASI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "4" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'psikologi',
                id: 'PSIKOLOGI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "5" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'pasca',
                id: 'PASCASARJANA',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "6" AND a.`tahunajaran` = "20151" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }]
        }
    });
});
</script>
<style type="text/css">
${demo.css}
        </style>
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#contain').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Mahasiswa Cuti/Non Aktif, dan Aktif Kembali UBJ per Prodi per 20151'
        },
        subtitle: {
            text: 'Klik grafik untuk melihat jumlah per prodi'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} mhs'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            // events: {
            //         click : function() {
            //             $('#TEKNIK').load('<?php echo base_url() ?>akademik/chart/jml/$ds->kd_fakultas');
            //         }
            //     }, 
            colorByPoint: true,
            data: [{
                <?php 
                    $baris = count($status);
                    $no = 0;
                    foreach ($status as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "name: '".$row->prodi."', y:".$row->jml.", drilldown:'".$row->prodi."'}]";
                        } else {
                            echo "name: '".$row->prodi."', y:".$row->jml.", drilldown:'".$row->prodi."'}, {";
                        }
                    }
                ?>
        }],
        drilldown: {
            series: [{
                name: 'TEKNIK INFORMATIKA',
                id: 'TEKNIK INFORMATIKA',
                data: [
                        <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "55201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'TEKNIK LINGKUNGAN',
                id: 'TEKNIK LINGKUNGAN',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "25201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'TEKNIK INDUSTRI',
                id: 'TEKNIK INDUSTRI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "26201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'TEKNIK KIMIA',
                id: 'TEKNIK KIMIA',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "24201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'TEKNIK PERMINYAKAN',
                id: 'TEKNIK PERMINYAKAN',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "32201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            if ($q == TRUE) {
                                foreach ($q as $raw) {
                                    $no++;
                                    if ($no == $idih) {
                                        echo "['".$raw->status."',".$raw->jum."]]";
                                    } else {
                                        echo "['".$raw->status."',".$raw->jum."],";
                                    };
                                }
                            } else { ?>
                                ['C',0]]
                            <?php }
                            
                            
                        ?>
            }, {
                name: 'MAGISTER MANAJEMEN',
                id: 'MAGISTER MANAJEMEN',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "55201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            if ($q == TRUE) {
                                foreach ($q as $raw) {
                                    $no++;
                                    if ($no == $idih) {
                                        echo "['".$raw->status."',".$raw->jum."]]";
                                    } else {
                                        echo "['".$raw->status."',".$raw->jum."],";
                                    };
                                }
                            } else { ?>
                                ['C',0]]
                            <?php }
                        ?>
            }, {
                name: 'MAGISTER HUKUM',
                id: 'MAGISTER HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "55201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            if ($q == TRUE) {
                                foreach ($q as $raw) {
                                    $no++;
                                    if ($no == $idih) {
                                        echo "['".$raw->status."',".$raw->jum."]]";
                                    } else {
                                        echo "['".$raw->status."',".$raw->jum."],";
                                    };
                                }
                            } else { ?>
                                ['C',0]]
                            <?php }
                        ?>
            }, {
                name: 'ILMU HUKUM',
                id: 'ILMU HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "74201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'MANAJEMEN',
                id: 'MANAJEMEN',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "61201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'AKUNTANSI',
                id: 'AKUNTANSI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "62201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ILMU KOMUNIKASI',
                id: 'ILMU KOMUNIKASI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "70201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ILMU PSIKOLOGI',
                id: 'ILMU PSIKOLOGI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                        on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=f.`KDPSTMSMHS`
                                        where b.`kd_prodi`= "73201" group by a.`status`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->status."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->status."',".$raw->jum."],";
                                };
                            }
                        ?>
            }]
        }
    });
});
</script>

<script type="text/javascript">
$(function () {
    // Create the chart
    $('#kelas').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Kelas Mahasiswa UBJ per 20151'
        },
        subtitle: {
            text: 'Sumber: <a href="http://sia.ubharajaya.ac.id">SIA UBHARAJAYA</a>'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} kelas'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            // events: {
            //         click : function() {
            //             $('#TEKNIK').load('<?php echo base_url() ?>akademik/chart/jml/$ds->kd_fakultas');
            //         }
            //     }, 
            colorByPoint: true,
            data: [{
                <?php 
                    $baris = count($kelas);
                    $no = 0;
                    foreach ($kelas as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "name: '".$row->wkt_kls."', y:".$row->jumjum.", drilldown:'".$row->wkt_kls."'}]";
                        } else {
                            echo "name: '".$row->wkt_kls."', y:".$row->jumjum.", drilldown:'".$row->wkt_kls."'}, {";
                        };
                    }
                ?>
        }],
        drilldown: {
            series: [{
                name: 'teknik',
                id: 'TEKNIK',
                data: [
                        <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS`
                                        where c.`kd_fakultas`= "2" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ekonomi',
                id: 'EKONOMI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "1" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'hukum',
                id: 'HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "3" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'fikom',
                id: 'ILMU KOMUNIKASI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "4" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'psikologi',
                id: 'PSIKOLOGI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "5" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'pasca',
                id: 'PASCASARJANA',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "6" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }]
        }
    });
});
</script>

<script type="text/javascript">
$(function () {
    $('#dosen').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status Dosen UBJ per 20151'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Dosen Tetap', 
                y: <?php echo $dosen_tetap->jumlh_dsn; ?>
            }, {
                name: 'Dosen Tidak Tetap',
                y: <?php echo $dosen_tidak->juml_dsn; ?>
            }]
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#beban').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Beban Mengajar Dosen Tetap UBJ'
        },
        subtitle: {
            text: 'Source: SIA UBHARAJAYA'
        },
        xAxis: {
            categories: [
                <?php 
                        foreach ($beban as $row) {
                            $a = substr($row->nama, 0,10);
                            echo '"'.$a.'",';
                            
                        }
                    ?>
                // 'Jan',
                // 'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Jumlah SKS',
            data: [<?php
                        foreach ($beban as $row) {
                            echo $row->jumlah.",";
                        }
                    ?>
                ]

        }]
    });
    
});
</script>


<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>

<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div class="container">
    <div class="row">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon-bar-chart"></i>
                    <h3>
                        Mahasiswa Aktif</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                	<a href="<?php echo base_url(); ?>akademik/chart/export_excel_sts" class="btn btn-primary">Download Excel</a>
                    <script>
                        $(document).ready(function(){
                            $('#tahun').change(function(){
                                $.post('<?php echo base_url()?>akademik/chart/get_jml_mhs/'+$(this).val(),{},function(get){
                                    $('#container').html(get);
                                });
                            });
                        }); 
                    </script>
                    <div class="pull-right">
                        <div class="form-group" style="float:left;margin-right">
                            <select class="form-control" name="thn" id="tahun">
                                <option disabled="" selected="">-- Pilih Tahun Akademik --</option>
                                <?php foreach ($tahun as $key) { ?>
                                <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                	<hr>
                    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <!-- /widget-content -->
            </div>
        </div>
        
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon-bar-chart"></i>
                    <h3>
                        Status Mahasiswa</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                	<a href="<?php echo base_url(); ?>akademik/chart/excel_cuti" class="btn btn-primary">Download Excel</a>
                    <script>
                        $(document).ready(function(){
                            $('#tahuncuti').change(function(){
                                $.post('<?php echo base_url()?>akademik/chart/tahun_cuti/'+$(this).val(),{},function(get){
                                    $('#contain').html(get);
                                });
                            });
                        }); 
                    </script>
                    <div class="pull-right">
                        <div class="form-group" style="float:left;margin-right">
                            <select class="form-control" name="thn" id="tahuncuti">
                                <option disabled="" selected="">-- Pilih Tahun Akademik --</option>
                                <?php foreach ($tahun as $key) { ?>
                                <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                	<hr>
                    <div id="contain" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                    <!-- /pie-chart -->
                </div>
                <!-- /widget-content -->
            </div>
        </div>

        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon-bar-chart"></i>
                    <h3>
                        Jumlah Kelas</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                	<a href="<?php echo base_url(); ?>akademik/chart/excel_kelas" class="btn btn-primary">Download Excel</a>
                    <script>
                        $(document).ready(function(){
                            $('#tahunkls').change(function(){
                                $.post('<?php echo base_url()?>akademik/chart/jml_kelas/'+$(this).val(),{},function(get){
                                    $('#kelas').html(get);
                                });
                            });
                        }); 
                    </script>
                    <div class="pull-right">
                        <div class="form-group" style="float:left;margin-right">
                            <select class="form-control" name="thn" id="tahunkls">
                                <option disabled="" selected="">-- Pilih Tahun Akademik --</option>
                                <?php foreach ($tahun as $key) { ?>
                                <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                	<hr>
                    <div id="kelas" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <!-- /widget-content -->
            </div>
        </div>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon-bar-chart"></i>
                    <h3>
                        Status Dosen</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                	<a href="<?php echo base_url(); ?>akademik/chart/excel_status" class="btn btn-primary">Download Excel</a>
                    <script>
                        $(document).ready(function(){
                            $('#tahu').change(function(){
                                $.post('<?php echo base_url()?>akademik/chart/dosen/'+$(this).val(),{},function(get){
                                    $('#dosen').html(get);
                                });
                            });
                        }); 
                    </script>
                    <div class="pull-right">
                        <div class="form-group" style="float:left;margin-right">
                            <select class="form-control" name="thn" id="tahu">
                                <option disabled="" selected="">-- Pilih Tahun Akademik --</option>
                                <?php foreach ($dos as $key) { ?>
                                <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                	<hr>
                    <div id="dosen" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <!-- /widget-content -->
            </div>
        </div>
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon-bar-chart"></i>
                    <h3>
                        Beban Ajar</h3>
                </div>
                <!-- /widget-header -->
                <script>
                    $(document).ready(function(){
                        $('#faks').change(function(){
                            $.post('<?php echo base_url()?>akademik/chart/get_jurusan/'+$(this).val(),{},function(get){
                                $('#beban').html(get);
                            });
                        });
                    });
                    $(document).ready(function(){
                        $('#tahunajaran').change(function(){
                            $.post('<?php echo base_url()?>akademik/chart/get_beban_thajaran/'+$(this).val(),{},function(get){
                                $('#beban').html(get);
                            });
                        });
                    }); 
                </script>
                <div class="widget-content">
                	<a href="<?php echo base_url(); ?>akademik/chart/excel_beban" class="btn btn-primary">Download Excel</a>
                	<div class="pull-right">
                		<form action="<?php echo base_url(); ?>akademik/chart/get_jurusan" method="post">
                			<div class="form-group" style="float:left;margin-right">
                				<select class="form-control" name="bbn" id="faks">
                					<option disabled="" selected="">-- Pilih Prodi --</option>
                                    <?php foreach ($prodi as $key) { ?>
                                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                                    <?php } ?>
                				</select>
                                <select class="form-control" name="tahunajaran" id="tahunajaran" required>
                                    <option disabled selected>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahun as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                </select>
                			</div>
                			<!-- <button type="submit" class="btn btn-primary" style="margin-left:10px;">Submit</button> -->
                		</form>
                	</div>
                	<hr>
                    
                        <div id='beban' style='min-width: 300px; height: 400px; max-width: 1500px; margin: 0 auto'></div>
                    
                    
                    <!-- /pie-chart -->
                </div>
                <!-- /widget-content -->
            </div>
        </div>
    </div>
</div>
