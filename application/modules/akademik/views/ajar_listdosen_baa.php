<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dosen Mengajar</h3>

            </div> <!-- /widget-header -->

            <div class="widget-content">

                <div class="span11">

                    <a href="<?= base_url(); ?>akademik/ajar" class="btn btn-warning ">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>  
                    <a href="<?= base_url(); ?>akademik/ajar/export_excel" class="btn btn-success ">
                        <i class="icon-download"></i> Export Data
                    </a>
                    <p style="margin-top: 10px; color: red;">
                        <i>*Total SKS yang dihitung tidak termasuk skripsi, tesis, ataupun kerja praktek/magang</i>
                    </p>
                    <hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>NID</th>

                                <th>Nama Dosen</th>

                                <th>Total Sks</th>

                                <th width="80">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($dosen as $isi): ?>

                                <tr>

                                    <td><?= $no; ?></td>

                                    <td><?= $isi->nid; ?></td>

                                    <td><?= $isi->nama; ?></td>

                                    <td><?= $isi->sks; ?></td>

                                    <td class="td-actions">

                                       <a href="<?= base_url();?>akademik/ajar/show_dosen_by_prodi/<?= $isi->nid; ?>" class="btn btn-primary btn-small"><i class="btn-icon-only icon-list"> </i></a>

                                       <a href="<?= base_url();?>akademik/ajar/print_surat_tugas/<?= $isi->nid; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



