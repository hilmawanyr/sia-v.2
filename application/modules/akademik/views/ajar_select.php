<div class="row">
  <div class="span12">                
    <div class="widget ">

      <div class="widget-header">
        <i class="icon-archive"></i>
        <h3>Kegiatan Perkuliahan</h3>
      </div> <!-- /widget-header -->      

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>akademik/ajar/save_session">
            <fieldset>
              <script>
                $(document).ready(function(){
                  $('#faks').change(function(){
                    $.get('<?= base_url()?>perkuliahan/jdl_kuliah/get_jurusan/'+$(this).val(),{},function(res){
                      $('#jurs').html(res);
                    });
                  });
                });
              </script>

              <div class="control-group">
                <label class="control-label">Fakultas</label>
                <div class="controls">
                  <select class="form-control span6" name="fakultas" id="faks" required="">
                    <option value="" disabled="" selected="">--Pilih Fakultas--</option>
                    <?php foreach ($fakultas as $row) { ?>
                    <option value="<?= $row->kd_fakultas.'-'.$row->fakultas;?>"><?= $row->fakultas;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
  
              <div class="control-group">
                <label class="control-label">Program Studi</label>
                <div class="controls">
                  <select class="form-control span6" name="jurusan" id="jurs">
                    <option>--Pilih Program Studi--</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                    <option value="" disabled="" selected="">--Pilih Tahun Akademik--</option>
                    <?php foreach ($tahunajar as $row) { ?>
                    <option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>  

              <br/> 

              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

