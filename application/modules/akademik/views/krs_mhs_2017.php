<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('akademik/krs_mhs/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;

        }

    });

});

</script>
<?php $sesi = $this->session->userdata('sess_login');?>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Form KRS</h3>
      </div> <!-- /widget-header -->
      <?php $logged = $this->session->userdata('sess_login'); ?>
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM KRS</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>akademik/krs_mhs_2017/create_session">
            <fieldset>

              <div class="control-group">                     
                <label class="control-label">Penasehat Akademik Anda</label>
                <div class="controls">
                  <input type="text" class="span3" name="dosen" placeholder="Ketikan Nama Dosen" value="<?php echo get_nm_pa($mhs->kd_dosen)?>" id="dosen" 
                  <?php if(substr($logged['userid'], 0,4) < 2017){ echo "readonly";} ?>>
                  <input type="hidden" id='kd_dosen' name="kd_dosen" value="<?php echo $mhs->kd_dosen; ?>">
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <div class="control-group">                     
                <label class="control-label">Kelompok Kelas</label>
                <div class="controls">
                  <select name="kelas" class="span3" required>
                    <option disabled>-- Pilih kelompok Kelas --</option>
                    <option value="PG" <?php if($mhs->kelas_mhs == 'PG'){echo 'selected';} ?> >Reguler Pagi (A) </option>
                    <option value="SR" <?php if($mhs->kelas_mhs == 'SR'){echo 'selected';} ?> >Reguler Sore (B) </option>
                    <option value="PK" <?php if($mhs->kelas_mhs == 'PK'){echo 'selected';} ?> >P2K / Karyawan (C) </option>
                  </select>
                  <p class="help-block">*Bila terdapat ketidaksesuaian pembimbing akademik/kelompok kelas mohon hubungi program studi</p>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <input type="hidden" name="mhs" value="<?php echo $sesi['userid']; ?>" placeholder="">
              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>