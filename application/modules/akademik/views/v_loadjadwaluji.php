<!-- <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/datatables/serverside'); ?>/jquery.dataTables.min.css"> -->
<!-- <script src="<?= base_url('assets/js/datatables/serverside'); ?>/jquery.dataTables.min.js" type="text/javascript"></script> -->
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"/>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<?php $sess = $this->session->userdata('sess_login'); ?>

<script type="text/javascript">
    function loadaddmod(id)
    {
        $('#contaddmod').load('<?= base_url('akademik/jadwal_ujian/addmodal/'); ?>'+id);
    }

    function loaddetmod(id)
    {
        $('#contdetmod').load('<?= base_url('akademik/jadwal_ujian/detmodal/'); ?>'+id);
    }

    function loadedtmod(id)
    {
        $('#contedtmod').load('<?= base_url('akademik/jadwal_ujian/edtmodal/'); ?>'+id);
    }
</script>

<!-- datatable serverside -->
<script type="text/javascript">
    // var table;
    // $(document).ready(function() {
    //     table = $('#table').DataTable({ 
    //         processing: true, 
    //         serverSide: true,
    //         order: [],
    //         ajax: {
    //             url: "<?= base_url('akademik/jadwal_ujian/getdata')?>",
    //             type: "POST"
    //         }
    //     });
    // });
</script>


<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Jadwal Ujian Tahun Akademik <?php echo get_thajar($year); ?></h3>

            </div> <!-- /widget-header -->


            <div class="widget-content">

                <button class="btn btn-primary" data-target="#exmod" data-toggle="modal">
                    <i class="icon icon-download"></i> Ekspor Data
                </button>
                

                <div class="pull-right">
                    <button class="btn btn-danger" data-target="#inmod" data-toggle="modal">
                        <i class="icon icon-info"></i> Informasi
                    </button>
                </div>

                <hr>

                <div class="span11">

                    <table id="example1" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelas</th>
                                <th>Kode Mata Kuliah</th>
                                <th>Mata Kuliah</th>
                                <th>Dosen</th>

                                <?php if ($sess['id_user_group'] == 8) { ?>
                                    <th>Tambah Jadwal</th>
                                <?php } ?>
                                
                                <th>Lihat Jadwal</th>

                                <?php if ($sess['id_user_group'] == 8) { ?>
                                    <th>Rubah Jadwal</th>
                                <?php } ?>
                                
                            </tr>
                        </thead>
                        <tbody>
						
                            <?php $no = 1; foreach ($list->result() as $key) { ?>

                                <!-- script for color information -->
                                <?php 
                                    $uts = getDataUjian($key->id_jadwal,1)->num_rows();
                                    $uas = getDataUjian($key->id_jadwal,2)->num_rows();

                                    if ($uts == 0 && $uas == 0) {
                                        $color = 'style="background-color: #F08080"';
                                    } elseif ($uts == 1 && $uas == 0) {
                                        $color = 'style="background-color: #FFA500"';
                                    } elseif ($uts == 0 && $uas == 1) {
                                        $color = 'style="background-color: #48D1CC"';
                                    } elseif ($uts == 1 && $uas == 1) {
                                        $color = 'style="background-color: #00FF7F"';
                                    }
                                    
                                 ?>

                                <tr>
                                    <td <?= $color; ?> ><?= $no; ?></td>
                                    <td <?= $color; ?> ><?= $key->kelas; ?></td>
                                    <td <?= $color; ?> ><?= $key->kd_matakuliah; ?></td>

                                    <?php 
                                    // jika login sebagai BAA / Prodi
                                    if ($sess['id_user_group'] == 10 || $sess['id_user_group'] == 8) {
                                        $user = $uid;

                                    // jika login sebagai dosen
                                    } else {
                                        $getusr = substr($key->kd_jadwal, 0, 5);
                                        $user   = $getusr;
                                    } ?>

                                    <td <?= $color; ?> ><?= get_nama_mk($key->kd_matakuliah,$user); ?></td>
                                    <td <?= $color; ?> ><?= nama_dsn($key->kd_dosen); ?></td>
                                    
                                    <!-- if baa -->
                                    <?php if ($sess['id_user_group'] == 8) { ?>
                                        <td <?= $color; ?> >
                                            <button data-target="#addmod" data-toggle="modal" onclick="loadaddmod(<?= $key->id_jadwal; ?>)" class="btn btn-primary">
                                                <i class="icon icon-plus"></i>
                                            </button>
                                        </td>    
                                    <?php } ?>
                                    
                                    <td <?= $color; ?> >
                                        <button data-target="#seemod" data-toggle="modal" onclick="loaddetmod(<?= $key->id_jadwal; ?>)" class="btn btn-success">
                                            <i class="icon icon-eye-open"></i>
                                        </button>
                                    </td>

                                    <?php if ($sess['id_user_group'] == 8) { ?>
                                        <td <?= $color; ?> >
                                            <button data-target="#edtmod" data-toggle="modal" onclick="loadedtmod(<?= $key->id_jadwal; ?>)" class="btn btn-warning">
                                                <i class="icon icon-pencil"></i>
                                            </button>
                                        </td>    
                                    <?php } ?>
                                    
                                </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>


<!-- add modal -->
<div class="modal fade" id="addmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="contaddmod">

            

        </div>

    </div>

</div>


<!-- detail modal -->
<div class="modal fade" id="seemod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="contdetmod">

            

        </div>

    </div>

</div>


<!-- edit modal -->
<div class="modal fade" id="edtmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="contedtmod">

            

        </div>

    </div>

</div>

<!-- export modal -->
<div class="modal fade" id="exmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="<?= base_url('akademik/jadwal_ujian/exportxls'); ?>" method="post" >
                
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title">Ekspor Data Jadwal (excel)</h4>

                </div>

                <div class="modal-body">

                    <div class="control-group">
                        <label class="control-label">Tipe Ujian</label>
                        <div class="controls">
                            <select class="form-control span5" name="tipe">
                                <option disabled="" selected="">-- Pilih Tipe Ujian --</option>
                                <option value="1">UTS</option>
                                <option value="2">UAS</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- info modal -->
<div class="modal fade" id="inmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">
                
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title">Informasi Keterangan Warna</h4>

            </div>

            <div class="modal-body">

                <button class="btn btn-danger btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Kedua jadwal belum di unggah (UTS & UAS)</b>
                <br><br>

                <button class="btn btn-warning btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Hanya jadwal <i>UTS</i> yang telah diunggah</b>
                <br><br>

                <button class="btn btn-primary btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Hanya jadwal <i>UAS</i> yang telah diunggah</b>
                <br><br>

                <button class="btn btn-success btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Kedua jadwal telah diunggah (UTS & UAS)</b>
                <br><br>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>

        </div>

    </div>

</div>