<?php $sess = $this->session->userdata('sess_login'); ?>
<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal">&times;</button>

    <h4 class="modal-title">Detail Jadwal Ujian</h4>

</div>

<div class="modal-body">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Jadwal UTS</a></li>
        <li><a data-toggle="tab" href="#menu1">Jadwal UAS</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">

            <!-- data uts start-->
            <?php if ((bool)$uts->result()) {
                $kelas  = $uts->row()->kelas;
                $kdmk   = $uts->row()->kd_matakuliah;
                $nmmk   = get_nama_mk($uts->row()->kd_matakuliah,$uts->row()->prodi);
                $dosen  = nama_dsn($uts->row()->kd_dosen);
                $tgl    = TanggalIndo($uts->row()->start_date);
                $room   = get_room($uts->row()->kd_ruang);
                // $watch  = nama_dsn($uts->row()->pengawas);
                $timex  = $uts->row()->start_time;
                $endate = TanggalIndo($uts->row()->end_date); ?>

                <div class="control-group">
                    <label class="control-label">Nama Kelas</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $kelas; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Mata Kuliah</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $kdmk.' - '.$nmmk; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Dosen</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $dosen; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Tanggal UTS</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $tgl; ?>" disabled=""/>
                    </div>
                </div>

                <?php if ($sess['id_user_group'] != 5) { ?>
                        <div class="control-group">
                            <label class="control-label">Batas Upload Nilai</label>
                            <div class="controls">
                                <input class="form-control span5" value="<?= $endate; ?>" disabled=""/>
                            </div>
                        </div>
                <?php } ?>

                <div class="control-group">
                    <label class="control-label">Ruangan</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $room; ?>" disabled=""/>
                    </div>
                </div>

                <!-- <div class="control-group">
                    <label class="control-label">Pengawas</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $watch; ?>" disabled=""/>
                    </div>
                </div> -->

                <div class="control-group">
                    <label class="control-label">Jam Mulai</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $timex; ?>" disabled="" />
                    </div>
                </div>

            <?php } else { ?>

                <h3>Belum ada jadwal.</h3>

            <?php } ?>
            <!-- data uas end -->
            
        </div>
        <div id="menu1" class="tab-pane fade">
            
            <!-- data uts start-->
            <?php if ((bool)$uas->result()) {
                $kelas  = $uas->row()->kelas;
                $kdmk   = $uas->row()->kd_matakuliah;
                $nmmk   = get_nama_mk($uas->row()->kd_matakuliah,$uas->row()->prodi);
                $dosen  = nama_dsn($uas->row()->kd_dosen);
                $tgl    = TanggalIndo($uas->row()->start_date);
                $room   = get_room($uas->row()->kd_ruang);
                // $watch  = nama_dsn($uas->row()->pengawas); 
                $times  = $uas->row()->start_time;
                $endate = TanggalIndo($uas->row()->end_date); ?>

                <div class="control-group">
                    <label class="control-label">Nama Kelas</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $kelas; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Mata Kuliah</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $kdmk.' - '.$nmmk; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Dosen</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $dosen; ?>" disabled=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Tanggal UAS</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $tgl; ?>" disabled=""/>
                    </div>
                </div>

                <?php if ($sess['id_user_group'] != 5) { ?>
                        <div class="control-group">
                            <label class="control-label">Batas Upload Nilai</label>
                            <div class="controls">
                                <input class="form-control span5" value="<?= $endate; ?>" disabled=""/>
                            </div>
                        </div>
                <?php } ?>

                <div class="control-group">
                    <label class="control-label">Ruangan</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $room; ?>" disabled=""/>
                    </div>
                </div>

                <!-- <div class="control-group">
                    <label class="control-label">Pengawas</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $watch; ?>" disabled=""/>
                    </div>
                </div> -->

                <div class="control-group">
                    <label class="control-label">Jam Mulai</label>
                    <div class="controls">
                        <input class="form-control span5" value="<?= $times; ?>" disabled="" />
                    </div>
                </div>

            <?php } else { ?>

                <h3>Belum ada jadwal.</h3>

            <?php } ?>
            <!-- data uas end -->

        </div>
    </div>

</div>

<div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

</div>