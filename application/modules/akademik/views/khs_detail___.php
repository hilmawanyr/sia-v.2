<?php // var_dump($detail_khs);die();  ?>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
    function edit(idj) {
        $("#edit").load('<?php echo base_url()?>akademik/khs/view_edit/'+idj);
    }
</script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Kartu Hasil Studi</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="span11">

					<a href="<?php echo base_url(); ?>akademik/khs" class="btn btn-success"><< Kembali</a>

                    <?php 
                    $user = $this->session->userdata('sess_login');
                    $pecah = explode(',', $user['id_user_group']);
                    $jmlh = count($pecah);
                    
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }

                    if ( (in_array(8, $grup)) ) { ?>
                        
                        <a href="<?php echo base_url(); ?>akademik/khs/printkhs/<?php echo $npm ?>/<?php echo $semester;?>" target="_blank" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KHS</a>
                     
                    <?php } ?>

					<hr>

					<table>

                        <tr>

                            <td>NPM</td>

                            <td>:</td>

                            <td><?php echo $mhs->NIMHSMSMHS;?></td>

                        </tr>

                        <tr>

                            <td>Nama</td>

                            <td>:</td>

                            <td><?php echo $mhs->NMMHSMSMHS;?></td>

                            <td width="100"></td>

                            <td>Semester</td>

                            <td>:</td>

                            <td><?php echo $semester;?></td>

                        </tr>

                        <tr>

                            <td>IPS</td>

                            <td>:</td>

                            <td><?php echo number_format($ips, 2); ?></td>

                        </tr>

                    </table>

                    <hr>

					<table id="example1" class="table table-bordered table-striped">

	                	<thead>

	                        <tr> 

	                        	<th>No</th>

	                        	<th>Kode MK</th>

	                        	<th>Mata Kuliah</th>

	                        	<th>SKS</th>

	                        	<!--th>Dosen</th-->

	                        	<th>Nilai</th>

	                        	<th width="40">Detail Nilai</th>

	                        </tr>

	                    </thead>

	                    <tbody>

							<?php $no = 1; foreach ($detail_khs as $row) { ?>

	                        <tr>

	                        	<td><?php echo $no; ?></td>

	                        	<td><?php echo $row->kd_matakuliah; ?></td>

                                <td><?php echo $row->nama_matakuliah; ?></td>

                                <td><?php echo $row->sks_matakuliah ?></td>

                                <!--td></td-->

                                <td><?php echo $row->NLAKHTRLNM; ?></td>

                                <td class="td-actions">

                                	<a onclick="edit(<?php echo $row->id;?>)" data-toggle="modal" class="btn btn-success btn-small" href="#detil"><i class="btn-icon-only icon-ok"> </i></a>

                                </td>

	                        </tr>

							<?php $no++; } ?>

	                    </tbody>

	               	</table>

				</div>

			</div>

		</div>

	</div>

</div>



<div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->