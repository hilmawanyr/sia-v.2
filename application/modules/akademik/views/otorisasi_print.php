<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }

    function myFunction(){
        window.location.reload();
    }

</script>



<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Print Kartu UTS</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>akademik/otorisasi/cetak" target="_blank"  method="post">

                        <a class="btn btn-cancel btn" href="<?php echo base_url(); ?>akademik/otorisasi/load_mhs"> Kembali</i></a>
                        <input type="submit" class="btn btn-primary btn" value="Print">
                        <!-- <button type="submit" onClick="myFunction()" class="btn btn-primary btn" >Submit</button> -->
                        <hr>
                        <table  class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($rows as $row) { ?>
                                 
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
                                    <td><?php echo $row->TAHUNMSMHS; ?></td>                              
                                </tr>
                                <input type="hidden"  name="kaskus[]" value="<?php echo $row->NIMHSMSMHS ?>"/>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>
