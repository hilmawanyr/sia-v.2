<?php $actyear = getactyear(); ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>Rubah Jadwal MKDU Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <table class="table table-striped">
                        <tr>
                            <td colspan="3" style="text-align: center;"><b>Jadwal Awal</b></td>
                            <td colspan="3" style="text-align: center;"><b>Jadwal Baru</b></td>
                        </tr>
                        <tr>
                            <td>Kode Matakuliah</td>
                            <td>:</td>
                            <td><?= $kd_mk ?></td>
                            <td>Kode Matakuliah</td>
                            <td>:</td>
                            <td><?= $kd_mk2 ?></td>
                        </tr>
                        <tr>
                            <td>Nama Matakuliah</td>
                            <td>:</td>
                            <td><?= $nm_mk ?></td>
                            <td>Nama Matakuliah</td>
                            <td>:</td>
                            <td><?= $nm_mk2 ?></td>
                        </tr>
                        <tr>
                            <td>Kelas</td>
                            <td>:</td>
                            <td><?= $kelas ?></td>
                            <td>Kelas</td>
                            <td>:</td>
                            <td><?= $kelas2 ?></td>
                        </tr>
                        <tr>
                            <td>Dosen</td>
                            <td>:</td>
                            <td><?= $dosen ?></td>
                            <td>Dosen</td>
                            <td>:</td>
                            <td><?= $dosen2 ?></td>
                        </tr>
                    </table>
                    <a href="<?= base_url('akademik/change_mkdu_schedule/backmkdu')  ?>" class="btn btn-warning">< Kembali</a>
                    <hr>
                    <form action="<?= base_url('akademik/change_mkdu_schedule/updateJadwalMkdu') ?>" method="post">
    					<table id="example1" class="table table-bordered table-striped">
    	                	<thead>
    	                        <tr> 
    	                        	<th>No</th>
    	                        	<th>NPM</th>
    	                        	<th>Nama Mahasiswa</th>
    	                            <th width="120">Aksi</th>
    	                        </tr>
    	                    </thead>
    	                    <tbody>
                                <?php $no = 1; foreach ($mhs as $row) { ?>
    	                        <tr>
    	                        	<td><?= $no; ?></td>
    	                        	<td><?= $row->npm_mahasiswa; ?></td>
                                    <td><?= get_nm_mhs($row->npm_mahasiswa); ?></td>
    	                        	<td class="td-actions">
                                        <input type="checkbox" name="npm[]" value="<?= $row->npm_mahasiswa.$actyear ?>">
    								</td>
    	                        </tr>
    	                        <?php $no++; } ?>
    	                    </tbody>
    	               	</table>

                        <hr>
                        <button type="submit" class="btn btn-primary">Rubah Jadwal</button>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>