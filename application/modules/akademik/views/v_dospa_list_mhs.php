    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>List Mahasiswa Bimbingan</h3><a href="#myModal" data-toggle="modal" class="icon-question-sign"></a>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <!-- <button class="btn btn-success" id="select_all"><i class="icon icon-ok"></i> Ceklis Semua Mahasiswa</button> -->
                    <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/dospa/save" method="post" >
                        <div class="control-group" id="">
                            <label class="control-label">Dosen Pembimbing Sekarang</label>
                            <div class="controls">
                                <input type="text" class="span4 form-control" value="<?php echo nama_dsn($this->session->userdata('dsn_now')) ?>" name="dosen" readonly/>
                            </div>
                        </div>
                        <div class="control-group" id="">
                            <label class="control-label">Dosen Pembimbing Baru</label>
                            <div class="controls">
                                <input type="text" class="span4 form-control" value="<?php echo nama_dsn($this->session->userdata('dsn_new')); ?>" readonly/>
                            </div>
                        </div>
                        <div class="control-group" id="">
                            
                            <button type="submit" class="btn btn-primary">Pindahkan</button>
                        </div>

                    <hr>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama Mahasiswa</th>
                                <th>Pembimbing</th>
                                <th width="18%"> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1; foreach ($mhs as $row) { 

                                if($this->session->userdata('dsn_new') == $row->kd_dosen){ 
                                    $act = '<i class="icon icon-ok"></i>';
                                }else{
                                    $act = '<input type="checkbox" class="checks" name="npm[]" id="checkBoxClass" value="'.$row->NIMHSMSMHS.'"/>';
                                    
                                } 
                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $row->NIMHSMSMHS ?></td>
                                <td><?php echo $row->NMMHSMSMHS ?></td>
                                <td><?php echo $row->nama ?></td>
                                <td width="18%">
                                    <?php echo $act; ?>
                                    <!-- <i class="icon icon-ok"></i> -->
                                </td>
                                <input type="hidden" name="show_mhs[]" value="<?php echo $row->NIMHSMSMHS; ?>">
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
$('#select_all').click(function(event) {
  if(this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
          this.checked = true;
      });
  }
  else {
    $(':checkbox').each(function() {
          this.checked = false;
      });
  }
});
</script>




<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

							
					<button type="button" class="close" data-dismiss="modal">&times;</button>

				<div class="modal-body">


					<div>
						Update Dosen Akademik di saat mahasiswa belum melakukan verifikasi KRS
					</div>

				</div>




        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->