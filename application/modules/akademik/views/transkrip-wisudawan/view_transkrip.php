
<div class="row">
	<div class="span12">      		  		
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3><?php echo $title ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">

				<form class="form-horizontal" id="trans-wisuda" method="post">
					<!-- Nomor -->
					<div class="control-group">
						<label class="control-label">Nomor</label>
						<div class="controls">
							<input class="form-control span5" type="text" placeholder="Nomor TA" name="nta" id="nta">
						</div>
					</div>
					<!-- Nomor -->
					<!-- Nomor Induk Ijazah -->
					<div class="control-group">
						<label class="control-label">Nomor Induk Ijazah</label>
						<div class="controls">
							<input class="form-control span5" type="text" placeholder="Nomor Induk Ijazah" name="ncn" id="ncn">
						</div>
					</div>
					<!-- Nomor Induk Ijazah -->
					<!-- Tanggal Kelulusan -->


					<div class="control-group">
						<label class="control-label">Tanggal Kelulusan</label>
						<div class="controls">
							<input class="form-control span2" type="text" placeholder="Tanggal Kelulusan"  name="dog" id="dog" value="">
						</div>
					</div>
					<!-- Tanggal Kelulusan -->
					<!-- Predikat Kelulusan -->
					<div class="control-group">
						<label class="control-label">Predikat Kelulusan</label>
						<div class="controls">
							<input class="form-control span5" type="text" placeholder="Predikat Kelulusan" name="pog" id="pog">
						</div>
					</div>
					<!-- Predikat Kelulusan -->
					<hr>
					<div class="control-group controls">
						<button class="btn btn-success" id="throw">
							<i class="icon icon-search"></i> Lihat Transkrip
						</button>
					</div>
				</form>
				
				<div class="col">
					<div id="load_table"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(function() {
		$( "#dog" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-50:+0",
			dateFormat: 'yy-mm-dd'
		});

		$("#throw").on('click', function (e) {
			e.preventDefault();
			window.open("/akademik/transkrip-wisudawan/view-transkrip")
		})
	});
</script>