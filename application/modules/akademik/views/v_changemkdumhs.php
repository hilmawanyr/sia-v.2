<script>
  jQuery(document).ready(function($) {
    $('input[name^=jdl_before]').autocomplete({
      source: '<?= base_url('akademik/change-mkdu-schedule/autocomplete_mkdu');?>',
      minLength: 3,
      select: function (evt, ui) {
          this.form.jdl_before.value = ui.item.value;
      }
    });

    $('input[name^=jdl_after]').autocomplete({
      source: '<?= base_url('akademik/change-mkdu-schedule/autocomplete_mkdu');?>',
      minLength: 3,
      select: function (evt, ui) {
          this.form.jdl_after.value = ui.item.value;
      }
    });
  });
</script>

<div class="row">

  <div class="span12">                

    <div class="widget ">

      <div class="widget-header">

        <i class="icon-calendar"></i>

        <h3>Rubah Jadwal MKDU</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

          <form method="post" class="form-horizontal" action="<?= base_url('akademik/change_mkdu_schedule/create_sess_schedule'); ?>">

            <fieldset>

              <div class="control-group">

                <label class="control-label">Jadwal Lama</label>

                <div class="controls">

                  <input type="text" name="jdl_before" id="jdl_before" class="form-control">

                </div>

              </div>

              <div class="control-group">

                <label class="control-label">Jadwal Baru</label>

                <div class="controls">

                  <input type="text" name="jdl_after" id="jdl_after">

                </div>

              </div>

              <div class="form-actions">

                  <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

              </div> <!-- /form-actions -->

            </fieldset>

          </form>

        </div>

      </div>

    </div>

  </div>

</div>

