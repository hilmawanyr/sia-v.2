<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=detail_jumlah_mahasiswa_reg_dan_nonreg_".$prod.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th colspan="3">Prodi <?= $prod ?></th>
        </tr>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($data as $isi): ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $isi->npm_mahasiswa; ?></td>
                <td><?= get_nm_mhs($isi->npm_mahasiswa) ?></td>
            </tr>    
        <?php $no++; endforeach ?>
    </tbody>
</table>