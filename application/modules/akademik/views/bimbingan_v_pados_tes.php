<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Mahasiswa Bimbingan</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a href="<?php echo base_url(); ?>akademik/bimbingan/printbimbingan" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
                    <a href="<?php echo base_url(); ?>akademik/bimbingan/printdaftarbimbingan" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print Daftar</a><hr>

                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NIM</th>
                                <th>Mahasiswa</th>
                                <th>Angkatan</th>
                                <th>Semester KRS</th>
                                <th width="40">Lihat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach ($getData as $row) { ?>
                            <?php if ($row->status_verifikasi == NULL) { 
                                $kelas = 'style="background:#F08080"';
                            } elseif ($row->status_verifikasi == 2 || $row->status_verifikasi == 3) {
                                $kelas = 'style="background:#FFD700"';
                            } else {
                                $kelas = '';
                            } ?>
                            <tr>
                                <td <?php echo $kelas; ?>><?php echo number_format($no); ?></td>
                                <td <?php echo $kelas; ?>><?php echo $row->NIMHSMSMHS; ?></td>
                                <td <?php echo $kelas; ?>><?php echo $row->NMMHSMSMHS; ?></td>
                                <td <?php echo $kelas; ?>><?php echo $row->TAHUNMSMHS; ?></td>
                                <?php $semester = $this->db->query("SELECT distinct semester_krs from tbl_krs_tes where kd_krs = '".$row->kd_krs."' ")->row(); ?>
                                <td <?php echo $kelas; ?>><?php echo $semester->semester_krs; ?></td>
                                <td <?php echo $kelas; ?> class="td-actions">
                                    <a class="btn btn-success btn-small" href="<?php echo base_url();?>akademik/bimbingan/viewmhs_tes/<?php echo $row->kd_krs; ?>"><i class="btn-icon-only icon-ok"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>