<?php// echo $id_jadwal;die(); ?>

<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>data/divisi/view_edit/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<?php 
  				$mk=$this->app_model->get_mk_by_jadwal($jdl_mk)->row();
  				 ?>
  				<h3>Data Nilai Mahasiswa ( '<?php echo $mk->nama_matakuliah ?>' ) (2015/2016)</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>akademik/publikasi/view" class="btn btn-warning"> << Kembali </a>
					<?php if ($pub == 1): ?>
						<a href="<?php echo base_url(); ?>akademik/publikasi/pub_all/<?php echo $jdl_mk; ?>" class="btn btn-success">  Publikasi </a>	
					<?php endif ?>
					
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th width="80">Absensi (10%)</th>
                                <th width="80">Tugas (20%)</th>
                                <th width="80">UTS (30%)</th>
                                <th width="80">UAS (40%)</th>
	                            <th width="40">Nilai</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php error_reporting(0); $no = 1; foreach($look as $row){?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->NIMHSMSMHS; ?></td>
	                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
	                        	<?php $absensi = $this->app_model->getnilai($ding->kd_jadwal,$row->NIMHSMSMHS,2,$ding->kd_matakuliah)->row(); ?>
	                        	<?php $tugas = $this->app_model->getnilai($ding->kd_jadwal,$row->NIMHSMSMHS,1,$ding->kd_matakuliah)->row(); ?>
	                        	<?php $uts = $this->app_model->getnilai($ding->kd_jadwal,$row->NIMHSMSMHS,3,$ding->kd_matakuliah)->row(); ?>
	                        	<?php $uas = $this->app_model->getnilai($ding->kd_jadwal,$row->NIMHSMSMHS,4,$ding->kd_matakuliah)->row(); ?>

	                        	<?php $rt = ($absensi->nilai*0.1)+($tugas->nilai*0.2)+($uts->nilai*0.3)+($uas->nilai*0.4);
									$logged = $this->session->userdata('sess_login');
									if (($logged['userid'] == '61101') or ($logged['userid'] == '74101')) {
										$rtnew = number_format($rt,2);
										if (($rtnew >= 80) and ($rtnew <= 100)) {
											$rw = "A";
										} elseif (($rtnew >= 75) and ($rtnew <= 79.99)) {
											$rw = "A-";
										} elseif (($rtnew >= 70) and ($rtnew <= 74.99)) {
											$rw = "B+";
										} elseif (($rtnew >= 65) and ($rtnew <= 69.99)) {
											$rw = "B";
										} elseif (($rtnew >= 60) and ($rtnew <= 64.99)) {
											$rw = "B-";
										} elseif (($rtnew >= 55) and ($rtnew <= 59.99)) {
											$rw = "C";
										} elseif (($rtnew >= 40) and ($rtnew <= 54.99)) {
											$rw = "D";
										} elseif (($rtnew >= 0) and ($rtnew <= 39.99)) {
											$rw = "E";
										}  elseif ($rtnew >100) {
											$rw = "T";
										}
									} else {
										$rtnew = number_format($rt,2);
										if (($rtnew >= 80) and ($rtnew <= 100)) {
											$rw = "A";
										} elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
											$rw = "B";
										} elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
											$rw = "C";
										} elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
											$rw = "D";
										} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
											$rw = "E";
										}  elseif ($rtnew >100) {
											$rw = "T";
										}
									}
	                        	?>

                                <td><?php if($absensi->nilai > 100){ echo "-"; }  else { echo $absensi->nilai; } ?></td>
                                <td><?php if($tugas->nilai > 100){ echo "-"; }  else { echo $tugas->nilai; } ?></td>
                                <td><?php if($uts->nilai > 100){ echo "-"; }  else { echo $uts->nilai; } ?></td>
                                <td><?php if($uas->nilai > 100){ echo "-"; } else { echo $uas->nilai; } ?></td>
	                        	<td><?php echo $rw; ?></td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

