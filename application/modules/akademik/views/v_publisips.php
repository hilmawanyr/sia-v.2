<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('input[name^=npm]').autocomplete({
      source: '<?php echo base_url('akademik/publikasi/load_mhs_autocomplete');?>',
      minLength: 4,
      select: function (evt, ui) {
        this.form.npm.value = ui.item.value;
      }
    });
  });

  // untuk revisi
  jQuery(document).ready(function($) {
    $('input[name^=nim]').autocomplete({
      source: '<?php echo base_url('akademik/publikasi/load_mhs_autocomplete');?>',
      minLength: 4,
      select: function (evt, ui) {
        this.form.nim.value = ui.item.value;
      }
    });
  });

  function search() {
    var data = $('#nim').val();
    $.post('<?= base_url() ?>akademik/publikasi/loadtabrevisi/'+data, function(res){
      $('#here').html(res);
    });
  }
</script>

<div class="row">

  <div class="span12">                

    <div class="widget ">

      <div class="widget-header">

        <i class="icon-share"></i>

        <h3>Publikasi IPS</h3>

      </div> <!-- /widget-header -->

      <div class="widget-content">

        <div class="span11">


            <fieldset>

              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Publikasi per Angkatan</a></li>
                <li><a data-toggle="tab" href="#menu1">Publikasi per Mahasiswa</a></li>
                <li><a data-toggle="tab" href="#menu2">Revisi Data</a></li>
              </ul>

              <div class="tab-content">
                <!-- per angkatan -->
                <div id="home" class="tab-pane fade in active">
                  <form  method="post" id="form1" class="form-horizontal" action="<?= base_url(); ?>akademik/publikasi/generateIps">
                    <div class="control-group">
                      <label class="control-label">Angkatan</label>
                      <div class="controls">
                        
                        <select class="form-control span6" name="tahun">
                          <option>--Pilih Angkatan--</option>
                          <?php foreach ($get as $rows) { ?>
                          <option value="<?= $rows->akt; ?>"><?= $rows->akt ;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-actions">
                      <input type="submit" class="btn btn-large btn-success" value="Publikasi"/> 
                    </div>
                  </form>
                </div>

                <!-- per mahasiswa -->
                <div id="menu1" class="tab-pane fade">
                  <form method="post" id="form2" class="form-horizontal" action="<?= base_url(); ?>akademik/publikasi/unitPublish">
                    <div class="control-group">
                      <label class="control-label">NPM</label>
                      <div class="controls">
                        <input type="text" name="npm" class="form-control span4" id="npm">
                      </div>
                    </div> 
                    <div class="form-actions">
                      <input type="submit" class="btn btn-large btn-success" value="Publikasi"/> 
                    </div>
                  </form>
                </div>

                <!-- revisi -->
                <div id="menu2" class="tab-pane fade">
                  <form method="post" class="form-horizontal">
                    <div class="control-group">
                      <label class="control-label">NPM</label>
                      <div class="controls">
                        <input type="text" name="nim" class="form-control span4" id="nim">
                      </div>
                    </div> 
                    <div class="form-actions">
                      <button type="button" class="btn btn-large btn-success" onclick="search()">Cari</button> 
                    </div>
                  </form>
                  

                  <!-- show data from revisi tab -->
                  <div id="here"></div>
                </div>

              </div>

            </fieldset>


        </div>

      </div>

    </div>

  </div>

</div>