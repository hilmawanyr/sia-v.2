<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Upload Dokumen</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/upload_soal/save_data/<?php echo $id; ?>" method="post" enctype="multipart/form-data">
    <div class="modal-body"> 
        <div class="control-group" id="">
            <div class="control-group" id="">
                <label class="control-label">Upload Dokumen</label>
                <div class="controls">
                    <input type="file" class="form-control span4" name="userfile" placeholder="Input Kode" value="" required/>
                </div>
            </div>
        </div>              
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>