<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>NO</th>
			<th>NID</th>
			<th>NAMA</th>
			<th>FAKULTAS</th>
			<th>PRODI</th>
			<th>TOTAL SKS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($dosen as $value) { ?>
			
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $value->nid; ?></td>
			<td><?php echo $value->nama; ?></td>
			<td><?php echo $detail->fakultas; ?></td>
			<td><?php echo $detail->prodi; ?></td>
			<td><?php echo $value->sks; ?></td>
		</tr>

		<?php $no++; } ?>
	</tbody>
</table>