<!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
 -->
<script>
    function edit(id) {
        var URL = '<?php echo base_url() ?>akademik/ajar/check_pertemuan/' + id;
        $.post(URL, function(data) {
            var res = JSON.parse(data)

            if (res.success) {
                $("#myModal").modal("toggle")
                $('#absen').load('<?php echo base_url(); ?>akademik/ajar/view_absen/' + id);
            } else {
                alert(res.message)
            }
        });

    }

    function loadMeeting(idx) {
        $('#jml').load('<?= base_url('akademik/ajar/loadMeetAmount/') ?>' + idx);
    }
</script>

<style>
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
</style>

<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-book"></i>
                <h3>Jadwal Mengajar Dosen</h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Kelas</th>
                                <th>Dosen</th>
                                <th>Waktu</th>
                                <th width="40">Status Kelas</th>
                                <th width="40">Berita Acara</th>
                                <th width="150" style="text-align: center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $no = 1;
                            foreach ($dosen as $isi) : ?>

                                <tr>
                                    <td><?php echo number_format($no); ?></td>
                                    <td><?php echo $isi->kd_matakuliah; ?></td>
                                    <td><?php echo get_nama_mk($isi->kd_matakuliah, substr($isi->kd_jadwal, 0, 5)); ?></td>
                                    <td><?php echo $isi->kelas; ?></td>
                                    <td><?php echo nama_dsn($isi->kd_dosen); ?></td>
                                    <td>
                                        <?= notohari($isi->hari) . ' / ' . substr($isi->waktu_mulai, 0, 5) . '-' . substr($isi->waktu_selesai, 0, 5); ?>
                                    </td>

                                    <?php
                                    if ($isi->open > 0) {
                                        $akses = 0;
                                        $desk = "OPEN";
                                        $icwarna = "btn-primary";
                                    } else {
                                        $akses = 1;
                                        $desk = "CLOSE";
                                        $icwarna = "btn-danger";
                                    }
                                    ?>
                                    <td>
                                        <a href="<?= base_url('akademik/ajar/open_absen/' . $isi->id_jadwal . '/' . $akses); ?>" class="btn <?= $icwarna; ?> btn-small">
                                            <?= $desk; ?>
                                        </a>
                                    </td>

                                    <td>
                                        <a href="<?= base_url('akademik/absenDosen/event/' . $isi->id_jadwal); ?>" target="_blank" class="btn btn-info btn-small" title="Berita Acara">
                                            <i class="icon icon-file"></i>
                                        </a>
                                    </td>

                                    <td class="">
                                        <?php

                                        $logged = $this->session->userdata('sess_login');
                                        $pecah  = explode(',', $logged['id_user_group']);
                                        $jmlh   = count($pecah);

                                        for ($i = 0; $i < $jmlh; $i++) {
                                            $grup[] = $pecah[$i];
                                        }

                                        if (in_array(19, $grup) && $this->session->userdata('tahunajaran') == getactyear()) { ?>
                                            <a data-toggle="modal" onclick="edit(<?= $isi->id_jadwal; ?>)" href="#" data-toggle="tooltip" title="Input Absensi">
                                                <i class="btn-icon-only icon-plus"> </i>
                                            </a> |
                                            <a href="<?= base_url('akademik/ajar/edit_absen/' . $isi->id_jadwal); ?>" target='_blank' data-toggle="tooltip" title="Edit Absensi">
                                                <i class="btn-icon-only icon-edit"></i>
                                            </a> |
                                        <?php } ?>

                                        <a href="<?= base_url('akademik/ajar/cetak_absensi/' . $isi->id_jadwal); ?>" target='_blank' data-toggle="tooltip" title="Print Absensi Kehadiran">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a> |

                                        <a href="#meetAmount" data-toggle="modal" onclick="loadMeeting(<?= $isi->id_jadwal; ?>)" title="Lihat Jumlah Pertemuan">
                                            <i class="icon icon-eye-open"></i>
                                        </a> |

                                        <a href="<?= base_url('akademik/ajar/cetak_absensi_polos/' . $isi->id_jadwal); ?>" target='_blank' data-toggle="tooltip" title="Print Absensi Kosong">
                                            <i class="btn-icon-only icon-print"> </i>
                                        </a>
                                    </td>
                                </tr>

                            <?php $no++;
                            endforeach ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="meetAmount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="jml">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div