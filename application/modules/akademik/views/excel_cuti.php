<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_mahasiswa_cuti_per_prodi.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th>NO</th>
            <th>Prodi</th>
            <th>Cuti</th>
            <th>Cuti Aktif</th>
            <th>Non Aktif</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($cuty as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->prodi; ?></td>
            <td><?php echo $key->CUTI; ?></td>
            <td><?php echo $key->CUTAK; ?></td>
            <td><?php echo $key->NON; ?></td>
            <td><?php echo $key->TOTAL; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>