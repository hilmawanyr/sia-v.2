<?php 

	$pdf = new FPDF("P","mm","A4");

	$pdf->AliasNbPages();

	$pdf->AddPage();

	$pdf->SetMargins(3,3,0);

	$pdf->SetAutoPageBreak(TRUE, 3);


	// for header

	$pdf->image(FCPATH.'assets/img/albino.jpg',20,10,30);

	$pdf->Ln(33);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(27,5,'',0,0,'C');

	$pdf->Cell(10,5,'UNIVERSITAS',0,1,'C');

	$pdf->Cell(27,0,'',0,0,'C');

	$pdf->Cell(10,5,'BHAYANGKARA JAKARTA RAYA',0,0,'C');

	$pdf->setXY(70,10);

	$pdf->SetFont('Arial','',12);

	$pdf->Cell(45,7,'Ketua Kelas',1,0,'L');

	$pdf->Cell(5,7,':',1,0,'C');

	$pdf->Cell(80,7,'',1,1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(45,7,'Nama Mahasiswa',1,0,'L');

	$pdf->Cell(5,7,':',1,0,'C');

	$pdf->Cell(80,7,'',1,1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(45,7,'NPM',1,0,'L');

	$pdf->Cell(5,7,':',1,0,'C');

	$pdf->Cell(80,7,'',1,1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(45,7,'No. Telp. Mahasiswa',1,0,'L');

	$pdf->Cell(5,7,':',1,0,'C');

	$pdf->Cell(80,7,'',1,1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(45,7,'No. Telp. Dosen',1,0,'L');

	$pdf->Cell(5,7,':',1,0,'C');

	$pdf->Cell(80,7,'',1,1,'C');



	// content

	$pdf->Ln(33);

	$pdf->SetFont('Arial','B',16);

	$pdf->Cell(95,5,'',0,0,'C');

	$pdf->Cell(10,5,'DAFTAR HADIR KULIAH',0,1,'C');

	$pdf->Ln(3);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(95,5,'',0,0,'C');

	$pdf->Cell(10,3,'SEMESTER '.strtoupper(substr(get_thnajar($rows->kd_tahunajaran), 7,6)).', TAHUN AKADEMIK '.substr(get_thajar($rows->kd_tahunajaran), 0,9),0,1,'C');



	// CONTENT 1ST TABLE

	$pdf->Ln(3);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(45,7,'HARI',1,0,'C');

	$pdf->Cell(45,7,'JAM',1,0,'C');

	$pdf->Cell(45,7,'KELAS',1,0,'C');

	$pdf->Cell(45,7,'RUANG',1,1,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(45,7,notohari($rows->hari),1,0,'C');

	$pdf->Cell(45,7,substr($rows->waktu_mulai, 0, 5).' - '.substr($rows->waktu_selesai, 0, 5),1,0,'C');

	$pdf->Cell(45,7,$rows->kelas,1,0,'C');

	$pdf->Cell(45,7,$rows->kode_ruangan,1,1,'C');



	// CONTENT 2ND TABLE

	$pdf->Ln(7);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'FAKULTAS / JENJANG PENDIDIKAN','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	$pdf->SetFont('Arial','',10);

	// get fakultas
	$so_what = $this->db->where('kd_prodi', substr($rows->kd_jadwal, 0,5))->get('tbl_jurusan_prodi')->row();

	$pdf->Cell(105,7,get_fak($so_what->kd_fakultas).' / '.$so_what->jenjang,'T,R,B',1,'L');


	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'PROGRAM STUDI / SEMESTER','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	// get semester
	$so_why = $this->db->select('*')
						->from('tbl_matakuliah a')
						->join('tbl_kurikulum_matkul_new b','a.kd_matakuliah = b.kd_matakuliah')
						->where('a.kd_matakuliah', $rows->kd_matakuliah)
						->like('b.kd_kurikulum', substr($rows->kd_jadwal, 0,5),'after')
						->get()->row();

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(105,7,get_jur(substr($rows->kd_jadwal, 0,5)).' / '.$so_why->semester_kd_matakuliah,'T,R,B',1,'L');


	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'KODE MK / SKS / MATA KULIAH','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(105,7,$rows->kd_matakuliah.' / '.$so_why->sks_matakuliah.' / '.$so_why->nama_matakuliah,'T,R,B',1,'L');


	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'DOSEN UTAMA','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(105,7,get_nm_pa($rows->kd_dosen),'T,R,B',1,'L');


	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'DOSEN PEMBIMBING','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(105,7,'','T,R,B',1,'L');


	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(13,7,'',0,0,'C');

	$pdf->Cell(65,7,'JENIS PERTEMUAN','T,L,B',0,'L');

	$pdf->Cell(10,7,':','T,B',0,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(105,7,'','T,R,B',1,'L');



	// CONTENT NOTICE

	$pdf->Ln(9);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(69,5,'',0,0,'C');

	$pdf->Cell(65,5,'Apabila sesuai KRS yang diambil, nama mahasiswa belum tercantum dalam buku "BUKU DAFTAR HADIR",',0,1,'C');

	$pdf->Ln(1);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(69,5,'',0,0,'C');

	$pdf->Cell(65,5,'agar segera melapor kepada Tata Usaha Fakultas / BAA / program Pascasarjana.',0,1,'C');

	$pdf->Ln(5);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(69,5,'',0,0,'C');

	$pdf->Cell(65,5,'Untuk mahasiswa yang mengambil kuliah "diluar kelas yang diambil dalam KRS", maka perangkat',0,1,'C');

	$pdf->Ln(1);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(69,5,'',0,0,'C');

	$pdf->Cell(65,5,'administratif dan nilainya tidak akan diproses.',0,1,'C');



	// CONTENT FRAME KETERANGAN NILAI

	$pdf->setXY(20,210);

	$pdf->Cell(180,20,'',1,0,'C');



	// CONTENT KETERANGAN NILAI

	$pdf->Ln(9);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(20,3,'',0,0,'C');

	$pdf->Cell(55,3,'PERSENTASE KEHADIRAN  = ',0,0,'L');

	$pdf->Cell(65,1,'JUMLAH KEHADIRAN MAHASISWA',0,0,'C');

	$pdf->Cell(20,1,'X    100%',0,1,'C');

	$pdf->Ln();

	$pdf->Cell(78,10,'',0,0,'C');

	$pdf->Cell(60,5,'JUMLAH KEHADIRAN DOSEN','T',0,'C');


	

	// ============================================= page additional ===========================================================

	$pdf->AliasNbPages();

	$pdf->AddPage();

	$pdf->SetMargins(3,3,0);

	$pdf->SetAutoPageBreak(TRUE, 3);


	// for header

	$pdf->image(FCPATH.'assets/img/albino.jpg',20,10,30);

	$pdf->Ln(40);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(27,5,'',0,0,'C');

	$pdf->Cell(10,5,'UNIVERSITAS',0,1,'C');

	$pdf->Cell(27,0,'',0,0,'C');

	$pdf->Cell(10,5,'BHAYANGKARA JAKARTA RAYA',0,0,'C');

	$pdf->setXY(70,10);

	$pdf->SetFont('Arial','b',12);

	$pdf->Cell(130,7,'REALISASI PERKULIAHAN',1,1,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(130,7,'SEMESTER '.strtoupper(substr(get_thnajar($rows->kd_tahunajaran), 7,6)),'L,R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(130,6,'TAHUN AKADEMIK '.substr(get_thajar($rows->kd_tahunajaran), 0,9),'L,R',1,'C');

	// $pdf->Ln(5);

	$pdf->Cell(67,11,'',0,0,'C');

	$pdf->Cell(20,7,'Ketua Kelas','L',0,'L');

	$pdf->Cell(10,7,':',0,0,'C');

	$pdf->Cell(100,7,'','R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(20,7,'Nama Mhs.','L',0,'L');

	$pdf->Cell(10,7,':',0,0,'C');

	$pdf->Cell(100,7,'','R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(20,8,'No. HP','L,B',0,'L');

	$pdf->Cell(10,8,':','B',0,'C');

	$pdf->Cell(100,8,'','R,B',1,'C');



	// CONTENT 1ST TABLE

	$pdf->Ln(8);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(30,7,'MATA KULIAH',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,$so_why->nama_matakuliah,1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'HARI / WAKTU',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,notohari($rows->hari).' / '.substr($rows->waktu_mulai,0,5).' - '.substr($rows->waktu_selesai,0,5),1,1,'L');

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'NAMA DOSEN',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_nm_pa($rows->kd_dosen),1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'RUANG',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_room($rows->kd_ruangan),1,1,'L');

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'KELAS',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,$rows->kelas,1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'PRODI',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_jur(substr($rows->kd_jadwal, 0,5)),1,1,'L');



	// CONTENT PRIMARY HEADER

	$pdf->Ln(8);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(15,7,'TM KE',1,0,'C');

	$pdf->Cell(25,7,'TGL',1,0,'C');

	$pdf->Cell(52,7,'MATERI PERKULIAHAN',1,0,'C');

	$pdf->Cell(50,7,'BENTUK PEMBELAJARAN',1,0,'C');

	$pdf->Cell(30,7,'WAKTU KULIAH',1,0,'C');

	$pdf->Cell(20,7,'PARAF',1,1,'C');

	// CONTENT PRIMARY BODY

	$pdf->SetFont('Arial','',10);

	for ($i = 1; $i < 8; $i++) {

		$pdf->Cell(5,26,'',0,0,'C');

		$pdf->Cell(15,26,$i,1,0,'C');

		$pdf->Cell(25,26,'',1,0,'C');

		$pdf->Cell(52,26,'',1,0,'C');

		$pdf->Cell(50,26,'',1,1,'C');

	}

	$pdf->setXY(150,96);

	$pdf->SetFont('Arial','',7);

	$pdf->Cell(30,13,'MULAI',1,0,'L');

	$pdf->Cell(20,13,'DOSEN',1,1,'L');

	for ($a = 2; $a < 15; $a++) {

		if ($a%2 == 1) {
			$jam = 'MULAI';
			$par = 'DOSEN';
		} else {
			$jam = 'SELESAI';
			$par = 'MAHASISWA';
		}

		$pdf->Cell(147,13,'',0,0,'C');
		
		$pdf->Cell(30,13,$jam,1,0,'L');

		$pdf->Cell(20,13,$par,1,1,'L');

	}

	// CONTENT BENTUK PEMBELAJARAN

	$pdf->setXY(110,98);

	$pdf->SetFont('Arial','',8);

	$pdf->Cell(3,6,'',0,0,'L');

	$pdf->Cell(20,5,'CERAMAH',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'DISKUSI',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'PRESENTASI',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'QUIZ',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'LAIN-LAIN',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	for ($e = 1; $e < 7; $e++) {

		switch ($e) {
			case '1':
				$l = 6;
				break;
			
			case '2':
				$l = 6;
				break;

			case '3':
				$l = 6;
				break;

			case '4':
				$l = 7;
				break;

			case '5':
				$l = 5;
				break;

			case '6':
				$l = 7;
				break;
		}

		$pdf->Ln($l);

		$pdf->Cell(110,6,'',0,0,'L');
		
		$pdf->Cell(20,5,'CERAMAH',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'DISKUSI',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'PRESENTASI',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'QUIZ',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'LAIN-LAIN',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

	}

	$pdf->Ln(5);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(15,7,'8',1,0,'C');

	$pdf->Cell(25,7,'',1,0,'C');

	$pdf->Cell(152,7,'UJIAN TENGAH SEMESTER',1,0,'C');



	// ============================================= page additional II ===========================================================

	$pdf->AliasNbPages();

	$pdf->AddPage();

	$pdf->SetMargins(3,3,0);

	$pdf->SetAutoPageBreak(TRUE, 3);


	// for header

	$pdf->image(FCPATH.'assets/img/albino.jpg',20,10,30);

	$pdf->Ln(40);

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(27,5,'',0,0,'C');

	$pdf->Cell(10,5,'UNIVERSITAS',0,1,'C');

	$pdf->Cell(27,0,'',0,0,'C');

	$pdf->Cell(10,5,'BHAYANGKARA JAKARTA RAYA',0,0,'C');

	$pdf->setXY(70,10);

	$pdf->SetFont('Arial','b',12);

	$pdf->Cell(130,7,'REALISASI PERKULIAHAN',1,1,'C');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(130,7,'SEMESTER '.strtoupper(substr(get_thnajar($rows->kd_tahunajaran), 7,6)),'L,R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(130,6,'TAHUN AKADEMIK '.substr(get_thajar($rows->kd_tahunajaran), 0,9),'L,R',1,'C');

	// $pdf->Ln(5);

	$pdf->Cell(67,11,'',0,0,'C');

	$pdf->Cell(20,7,'Ketua Kelas','L',0,'L');

	$pdf->Cell(10,7,':',0,0,'C');

	$pdf->Cell(100,7,'','R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(20,7,'Nama Mhs.','L',0,'L');

	$pdf->Cell(10,7,':',0,0,'C');

	$pdf->Cell(100,7,'','R',1,'C');

	$pdf->Cell(67,7,'',0,0,'C');

	$pdf->Cell(20,8,'No. HP','L,B',0,'L');

	$pdf->Cell(10,8,':','B',0,'C');

	$pdf->Cell(100,8,'','R,B',1,'C');



	// CONTENT 1ST TABLE

	$pdf->Ln(8);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(30,7,'MATA KULIAH',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,$so_why->nama_matakuliah,1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'HARI / WAKTU',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,notohari($rows->hari).' / '.substr($rows->waktu_mulai,0,5).' - '.substr($rows->waktu_selesai,0,5),1,1,'L');

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'NAMA DOSEN',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_nm_pa($rows->kd_dosen),1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'RUANG',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_room($rows->kd_ruangan),1,1,'L');

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'KELAS',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,$rows->kelas,1,0,'L');

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(30,7,'PRODI',1,0,'L');

	$pdf->SetFont('Arial','',10);

	$pdf->Cell(66,7,get_jur(substr($rows->kd_jadwal, 0,5)),1,1,'L');



	// CONTENT PRIMARY HEADER

	$pdf->Ln(8);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(15,7,'TM KE',1,0,'C');

	$pdf->Cell(25,7,'TGL',1,0,'C');

	$pdf->Cell(52,7,'MATERI PERKULIAHAN',1,0,'C');

	$pdf->Cell(50,7,'BENTUK PEMBELAJARAN',1,0,'C');

	$pdf->Cell(30,7,'WAKTU KULIAH',1,0,'C');

	$pdf->Cell(20,7,'PARAF',1,1,'C');

	// CONTENT PRIMARY BODY

	$pdf->SetFont('Arial','',10);

	for ($i = 9; $i < 16; $i++) {

		$pdf->Cell(5,26,'',0,0,'C');

		$pdf->Cell(15,26,$i,1,0,'C');

		$pdf->Cell(25,26,'',1,0,'C');

		$pdf->Cell(52,26,'',1,0,'C');

		$pdf->Cell(50,26,'',1,1,'C');

	}

	$pdf->setXY(150,96);

	$pdf->SetFont('Arial','',7);

	$pdf->Cell(30,13,'MULAI',1,0,'L');

	$pdf->Cell(20,13,'DOSEN',1,1,'L');

	for ($a = 2; $a < 15; $a++) {

		if ($a%2 == 1) {
			$jam = 'MULAI';
			$par = 'DOSEN';
		} else {
			$jam = 'SELESAI';
			$par = 'MAHASISWA';
		}

		$pdf->Cell(147,13,'',0,0,'C');
		
		$pdf->Cell(30,13,$jam,1,0,'L');

		$pdf->Cell(20,13,$par,1,1,'L');

	}

	// CONTENT BENTUK PEMBELAJARAN

	$pdf->setXY(110,98);

	$pdf->SetFont('Arial','',8);

	$pdf->Cell(3,6,'',0,0,'L');

	$pdf->Cell(20,5,'CERAMAH',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'DISKUSI',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'PRESENTASI',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'QUIZ',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	$pdf->Cell(110,5,'',0,0,'L');

	$pdf->Cell(20,5,'LAIN-LAIN',0,0,'L');

	$pdf->Cell(5,4,'',1,1,'L');

	for ($e = 8; $e < 14; $e++) {

		switch ($e) {
			case '8':
				$l = 6;
				break;
			
			case '9':
				$l = 6;
				break;

			case '10':
				$l = 6;
				break;

			case '11':
				$l = 7;
				break;

			case '12':
				$l = 7;
				break;

			case '13':
				$l = 5;
				break;

			case '14':
				$l = 5;
				break;

			case '15':
				$l = 6;
				break;
		}

		$pdf->Ln($l);

		$pdf->Cell(110,6,'',0,0,'L');
		
		$pdf->Cell(20,5,'CERAMAH',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'DISKUSI',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'PRESENTASI',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'QUIZ',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

		$pdf->Cell(110,5,'',0,0,'L');

		$pdf->Cell(20,5,'LAIN-LAIN',0,0,'L');

		$pdf->Cell(5,4,'',1,1,'L');

	}

	$pdf->Ln(5);

	$pdf->SetFont('Arial','B',10);

	$pdf->Cell(5,7,'',0,0,'C');

	$pdf->Cell(15,7,'16',1,0,'C');

	$pdf->Cell(25,7,'',1,0,'C');

	$pdf->Cell(152,7,'UJIAN AKHIR SEMESTER',1,0,'C');


	// output pdf

	$pdf->Output('BERITA_ACARA.PDF','I');

 ?>