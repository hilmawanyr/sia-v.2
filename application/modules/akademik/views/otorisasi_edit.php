<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }

    function myFunction(){
        window.location.reload();
    }

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Cetak Kartu Ujian</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>akademik/otorisasi/update_flag"  method="post">

                        <a class="btn btn-cancel btn" href="<?php echo base_url(); ?>akademik/otorisasi"> Kembali</i></a>
                        <input type="submit" class="btn btn-primary btn" value="Submit">
                        <!-- <button type="submit" onClick="myFunction()" class="btn btn-primary btn" >Submit</button> -->
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                    <th>UTS</th>
                                    <th>UAS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no=1;
                                    foreach($rows as $row) { ?>
                                    <!--script type="text/javascript">
                                        $(document).ready(function(){
                                            if(document.getElementById('cek0').checked==false){
                                                document.getElementById('cek0').val()='z911'+<?php //echo $row->NIMHSMSMHS; ?>+'';
                                            }
                                            if(document.getElementById('cek1').checked!=true){
                                                document.getElementById('cek1').val()='z911'+<?php //echo $row->NIMHSMSMHS; ?>+'';
                                            }
                                            if(document.getElementById('cek2').checked!=true){
                                                document.getElementById('cek2').val()='z911'+<?php //echo $row->NIMHSMSMHS; ?>+'';
                                            }
                                        });
                                    </script-->
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->NIMHSMSMHS;//.' '.$apakah; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS;//.' '.$apakah; ?></td>
                                    <td><?php echo $row->TAHUNMSMHS;//.' '.$apakah; ?></td>
                                    <?php $cekrenkeu = $this->app_model->getmhsrenkeu($row->NIMHSMSMHS,$this->session->userdata('tahunajaran'))->row(); ?>
                                    <td>
                                        <?php if ($row->status_verifikasi == 10) {
                                            echo ' - ';
                                        } else {
                                            if ($cekrenkeu->status == 1) {
                                                echo "Hanya Daftar Ulang";
                                            } elseif (($cekrenkeu->status > 1 and $cekrenkeu->status != 8) and ($row->FLAG_RENKEU == 1)) {
                                                echo '<input type="checkbox" class="checkbox" name="mhs[]" value="'.$row->NIMHSMSMHS.'"/>';
                                            } elseif (($row->FLAG_RENKEU > 2) or ($row->FLAG_RENKEU == 5)) {       
                                                echo '<a href="'.base_url().'akademik/otorisasi/cetak_satuan/'.$row->NIMHSMSMHS.'" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>';
                                            }
                                        }
                                          ?>
                                    </td>
                                    <td>
                                        <?php 
                                        if (($row->status_verifikasi == 10)) { //or ($z != $q)) {
                                            echo ' - ';
                                        } else {
                                            if ($cekrenkeu->status < 4) {
                                                echo "-";
                                            } elseif ($cekrenkeu->status > 3 && $row->FLAG_RENKEU != 5) {
                                                echo '<input type="checkbox" class="checkbox" name="mhs[]" value="'.$row->NIMHSMSMHS.'"/>';
                                            } elseif ($cekrenkeu->status > 3 && $row->FLAG_RENKEU == 5) {       
                                                echo '<a href="'.base_url().'akademik/otorisasi/cetak_satuanuas/'.$row->kd_krs.'/'.$row->status_verifikasi.'" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>';
                                            }
                                        }
                                          ?>
                                    </td>
                                        
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                        <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>