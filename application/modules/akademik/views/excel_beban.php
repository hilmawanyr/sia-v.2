<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_beban_mengajar_dosen.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th>NO</th>
            <th>NID</th>
            <th>Nama Dosen</th>
            <th>Jumlah Beban SKS</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($weight as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->nid; ?></td>
            <td><?php echo $key->nama; ?></td>
            <td><?php echo $key->jumlah; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>