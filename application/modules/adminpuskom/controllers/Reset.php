<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_mbalia') != TRUE) {
		 	redirect('adminpuskom/home','refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'v_reset';
		$this->load->view('template/template', $data);
	}

	function ganti()
	{
		$a = $this->input->post('old');
		$e = $this->db->query('SELECT * from tbl_user_login where userid = "'.$a.'"')->row();
		if ($e == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/reset';</script>";
		} else {
			$kiw = sha1(md5('Ubharaj4y4').key);
			$this->db->query('UPDATE tbl_user_login set password = "'.$kiw.'", password_plain = "Ubharaj4y4" where userid = "'.$a.'"');
			echo "<script>alert('Reset Password Berhasil !');document.location.href='".base_url()."adminpuskom/reset';</script>";
		}

	}

	function load_dosen_autocomplete(){

		$bro = $this->session->userdata('sess_login');

		$prodi   = $bro['userid'];

        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');

        $this->db->like('b.kd_jadwal', $prodi, 'after');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        // $sql = $this->db->query("select distinct a.id_kary,a.nid,a.nama from tbl_karyawan a 
								// join tbl_jadwal_matkul b on a.nid = b.kd_dosen
								// where (nid LIKE '%".$_GET['term']."%' or nama like '%".$_GET['term']."%') and b.kd_jadwal like '".$prodi."%'");

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            // 'kode'       => $row->kd_obat,

                            // 'satuan'     => $row->nm_satuan,

                            // 'stok'       => $row->stok,

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nid.' - '.$row->nama

                            // 'diskon'     => $row->diskon,

                            // 'label'      => $row->kd_obat . " " . $row->nm_obat,

                            );

        }

        echo json_encode($data);

    }

}

/* End of file Reset.php */
/* Location: ./application/modules/adminpuskom/controllers/Reset.php */