<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('sess_mbalia') != '') {
			redirect('adminpuskom/board','refresh');
		} else {
			$this->load->view('v_home');
		}
	}

	public function login()
	{
		$kata = 'nQ3#YT'; 

		$pass = $this->input->post('password', TRUE);

		$cek = $this->login_model->cekmbalia($pass)->result();

		if (count($cek) > 0) {
			foreach ($cek as $key) {
				$session['kode'] = $key->kode;
				$this->session->set_userdata('sess_mbalia',$session);
				$this->index();
			}

		} else {
			echo "<script>alert('Gagal Login');history.go(-1);</script>";
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('sess_mbalia');
        redirect('adminpuskom/home', 'refresh');
	}
}

/* End of file Home.php */
/* Location: ./application/modules/mbalia/controllers/Home.php */