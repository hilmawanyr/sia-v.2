<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cari_pw extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_mbalia') != TRUE) {
		 	redirect('adminpuskom/home','refresh');
		} 

	}
	
	public function index()
	{
		$data['page'] = 'v_caripw';
		$this->load->view('template/template', $data);
	}

	function cari()
	{
		$ini = $this->input->post('npm');
		// var_dump($ini);exit();
		$pecah = explode(' - ', $ini);
		$data['query'] = $this->db->query('SELECT * from tbl_user_login where userid = "'.$pecah[0].'"')->row();
		if ($data['query'] == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/cari_pw';</script>";
		} else {
			$data['page'] = 'v_hasilcari';
			$this->load->view('template/template', $data);
		}
	}

	function load_dosen_autocomplete(){

		$bro = $this->session->userdata('sess_login');

		$prodi   = $bro['userid'];

        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nid.' - '.$row->nama

                            );

        }

        echo json_encode($data);

    }

}

/* End of file Cari_pw.php */
/* Location: ./application/modules/adminpuskom/controllers/Cari_pw.php */