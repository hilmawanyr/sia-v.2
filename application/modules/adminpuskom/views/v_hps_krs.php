<?php $user = $this->session->userdata('sess_mbalia'); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Hapus KRS</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/hapus_krs">hapus krs</a>
					<b><center>Hapus KRS Bermasalah</center></b><br>
					<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>adminpuskom/hapus_krs/load">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">NPM : </label>
								<div class="controls">
									<input type="text" class="span3" name="npm" placeholder="Masukan NPM" required>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">											
								<label class="control-label">Smester KRS : </label>
								<div class="controls">
									<select class="span3" name="smt" required>
										<option disabled="" selected="">-- Pilih Semester --</option>
										<?php for ($i=1; $i < 11; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div> <!-- /controls -->				
							</div>
							<div class="form-actions">
								<input type="submit" class="btn btn-primary" id="save" value="Submit"/>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>