<script type="text/javascript">

$(document).ready(function() {

<?php if ($cek->status_wn == 'WNA') { 
	echo "$('#kwn').show();";
} else {
	echo "$('#kwn').hide();";
}?>


<?php if ($cek->status_kerja == 'Y') {
	echo "$('#stsb_txt').show();";
}else{
	echo "$('#stsb_txt').hide();";
}?>

// $('#readmisi1').hide();
// $('#readmisi2').hide();

// $('#new1').hide();
// $('#new2').hide();
// $('#new3').hide();

// $('#konversi1').hide();
// $('#konversi2').hide();
// $('#konversi3').hide();
// $('#konversi4').hide();
// $('#konversi5').hide();

<?php if ($cek->jenis_pmb == 'MB') { ?>
	$('#new0').show(); $('#readmisi').hide(); $('#konversi').hide();
<?php }	elseif ($cek->jenis_pmb == 'RM') { ?>
	$('#readmisi').show(); $('#new0').hide(); $('#konversi').hide();
<?php }	elseif ($cek->jenis_pmb == 'KV') { ?>
	$('#konversi').show(); $('#new0').hide(); $('#readmisi').hide();
<?php } ?>


<?php if ($cek->bpjs == 'y') { ?>
	$('#bpjs-yes').show();
<?php }	elseif ($cek->bpjs == 'n') { ?>
	$('#bpjs-yes').hide();
<?php }	?>


$('#bpjs-y').click(function () {
	$('#bpjs-yes').show();
});	

$('#bpjs-n').click(function () {
	document.getElementById('men').value = '';
	$('#bpjs-yes').hide();
});	







								$('#r').click(function () {
				                    $('#konversi').hide();
									$('#new0').hide();
									$('#readmisi').show();
				                });
				                $('#new').click(function () {
				                	$('#konversi').hide();
									$('#new0').show();
									$('#readmisi').hide();
				                });
				                $('#k').click(function () {
				                	$('#konversi').show();
									$('#new0').hide();
									$('#readmisi').hide();
				                });


$('#tgl_lahir').datepicker({
 								dateFormat: "yy-mm-dd",

 								yearRange: "1945:2016",

								changeMonth: true,

      							changeYear: true

       							});

	

$('#wni').click(function () {
	$('#kwn').hide();
});

$('#wna').click(function () {
	$('#kwn').show();
});

$('#stsb_n').click(function () {
	$('#stsb_txt').hide();
});

$('#stsb_y').click(function () {
	$('#stsb_txt').show();
});
});

</script>

<?php 
if ($cek->jenis_pmb == 'MB') {
	$jpmb = 'MAHASISWA BARU';
}elseif ($cek->jenis_pmb == 'RM') {
	$jpmb = 'READMISI';
}elseif ($cek->jenis_pmb == 'KV') {
	$jpmb = 'KONVERSI';
}
 ?>

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Form Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>master/validasi_maba/simpan_form" method="post" enctype="multipart/form-data">
					<fieldset>
						<?php if (is_null($cek->foto) == false) { ?>
							<p><a href="<?php echo base_url(); ?><?php echo $cek->foto; ?>"><?php echo $pecah[2]; ?></a></p>
						<?php } ?>
						<div class="control-group">
							<label class="control-label">Foto</label>
							<div class="controls">
								<input type="file" class="form-control span2" name="userfile">
							</div>
						</div>
						<input type="hidden" name="kd_regis" value="<?php echo $cek->nomor_registrasi; ?>">
						<input type="hidden" name="jenis" value="<?php echo $cek->jenis_pmb; ?>">
						
						<div class="control-group">
							<label class="control-label">Jenis Form</label>
							<div class="controls">
								<select class="form-control span2 "  name="jenis_2" disabled>
									<option><?php echo $jpmb; ?></option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kampus</label>
							<div class="controls">
								<select class="form-control span2"  name="kampus">
									<option disabled="" >--Pilih Kampus--</option>
									<option value="jkt" <?php if ($cek->kampus == 'jkt') {echo 'selected=""';} ?>>Jakarta</option>
									<option value="bks" <?php if ($cek->kampus == 'bks') {echo 'selected=""';} ?>>Bekasi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kelas</label>
							<div class="controls">
								<select class="form-control span2"  name="kelas">
									<option>--Pilih Kelas--</option>
									<option value="pg" <?php if ($cek->kelas == 'pg') {echo 'selected=""';} ?>>Pagi</option>
									<option value="sr" <?php if ($cek->kelas == 'sr') {echo 'selected=""';} ?>>Sore</option>
									<option value="ky" <?php if ($cek->kelas == 'ky') {echo 'selected=""';} ?>>Karyawan</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">NIK</label>
							<div class="controls">
								<input type="text" class="form-control span6" value="<?php echo $cek->nik; ?>" placeholder="Isi dengan nama calon mahasiswa"  name="nama"><br>
								<small>*sesuai ktp / kartu keluarga</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6" value="<?php echo $cek->nama; ?>" placeholder="Isi dengan nama calon mahasiswa"  name="nama"><br>
								<small>*nama sesuai ijazah/akte kelahiran</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Kelamin</label>
							<div class="controls">
								<input type="radio" name="jk" value="L" <?php if ($cek->kelamin == 'L') {echo 'checked=""';} ?>> Laki - Laki <br>
								<input type="radio" name="jk" value="P" <?php if ($cek->kelamin == 'P') {echo 'checked=""';} ?>> Perempuan
							</div>
						</div>

						
						<div class="control-group">
							<label class="control-label">Kewarganegaraan</label>
							<div class="controls">
								<input type="radio" name="wn" id="wni" onclick="wni()" <?php if ($cek->status_wn == 'WNI') {echo 'checked=""';} ?> value="WNI" > WNI <br>
								<input type="radio" name="wn" id="wna" onclick="wna()" <?php if ($cek->status_wn == 'WNA') {echo 'checked=""';} ?> value="WNA"> WNA  &nbsp;&nbsp; 
								<input  type="text" class="form-control span3" id="kwn" value="<?php echo $cek->kewaganegaraan ?>" name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa">
							</div>
						</div>
				
						<div class="control-group">
							<label class="control-label">Kelahiran</label>
							<div class="controls">
								<input type="text" class="form-control span2" value="<?php echo $cek->tempat_lahir; ?>" placeholder="Tempat Lahir" name="tpt_lahir"  >
								<input type="text" class="form-control span2" value="<?php echo $cek->tgl_lahir; ?>" placeholder="Tanggal Lahir" id="tgl_lahir" name="tgl_lahir"  >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agama</label>
							<div class="controls">
								<select class="form-control span2"  name="agama">
									<option disabled="" >-- PILIH AGAMA --</option>
									<option value="Islam" <?php if ($cek->agama == 'Islam') {echo 'selected=""';} ?>>Islam</option>
									<option value="Katolik" <?php if ($cek->agama == 'Katolik') {echo 'selected=""';} ?>>Katolik</option>
									<option value="Protestan" <?php if ($cek->agama == 'Protestan') {echo 'selected=""';} ?>>Protestan</option>
									<option value="Budha" <?php if ($cek->agama == 'Budha') {echo 'selected=""';} ?>>Budha</option>
									<option value="Hindu" <?php if ($cek->agama == 'Hindu') {echo 'selected=""';} ?>>Hindu</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status Nikah</label>
							<div class="controls">
								<input type="radio" name="stsm" value="Y" <?php if ($cek->status_nikah == 'Y') {echo 'checked=""';} ?> > Menikah  <br>
								<input type="radio" name="stsm" value="N" <?php if ($cek->status_nikah == 'N') {echo 'checked=""';} ?> > Belum Menikah
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Status Kerja</label>
							<div class="controls">
								<input type="radio" name="stsb" id="stsb_n" value="N" <?php if ($cek->status_kerja == 'N') {echo 'checked=""';} ?> > Belum Bekerja <br>
								<input type="radio" name="stsb" id="stsb_y" value="Y"<?php if ($cek->status_kerja == 'Y') {echo 'checked=""';} ?>> Bekerja &nbsp;&nbsp; 
								<input type="text" class="form-control span3" id="stsb_txt"  name="stsb_txt" placeholder="Tempat Bekerja Calon Mahasiswa">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Alamat</label>
							<div class="controls">
								<textarea class="form-control span4" type="text" name="alamat"><?php echo $cek->alamat; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kode Pos</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Kode Pos" value="<?php echo $cek->kd_pos; ?>" type="text"  name="kdpos">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span3" value="<?php echo $cek->tlp; ?>" placeholder="Isi dengan Nomer Telpon" type="text"  name="tlp" maxlength=13 minlength=10 required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span3" value="<?php echo $cek->tlp2; ?>" placeholder="Isi dengan Nomer Telpon" type="text"  name="tlp2" maxlength=13 minlength=10>
							</div>
						</div>

						<!-- readmisi START -->
						<?php if ($cek->jenis_pmb == 'RM') { ?>
							<div id="readmisi">
							<div class="control-group" id="readmisi1">
								<label class="control-label">NPM Lama</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->nama; ?>" placeholder="Isi dengan Nomor Pokok Mahasiswa Lama"  type="text"  name="npm_readmisi">
								</div>
							</div>
							<div class="control-group" id="readmisi2">
								<label class="control-label">Tahun Masuk di UBJ</label>
								<div class="controls">
									<input class="form-control span1" type="text"  name="thmasuk_readmisi"> &nbsp; Sampai dengan semester &nbsp;
									<input class="form-control span1" type="text"  name="smtr_readmisi">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- readmisi END -->

						<!-- NEW START -->
						<?php if ($cek->jenis_pmb == 'MB') { ?>
							<div id="new0">
							<div class="control-group" id="new1">
								<label class="control-label">Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa" value="<?php echo $cek->asal_sch_maba ?>"  type="text"  name="asal_sch">
								</div>
							</div>
							<div class="control-group" id="new1">
								<label class="control-label">NISN</label>
								<div class="controls">
									<input class="form-control span3" placeholder="NISN Calon Mahasiswa"  type="text" value="<?php echo $cek->nisn ?>"  name="nisn">
								</div>
							</div>
							<div class="control-group" id="new2">
								<label class="control-label">Kota Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" placeholder="Isi dengan kota Asal Sekolah " value="<?php echo $cek->kota_sch_maba ?>" type="text"  name="kota_sch">
								</div>
							</div>
							<div class="control-group" id="new2">
								<label class="control-label">Kelurahan Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->daerah_sch_maba ?>" placeholder="Isi dengan Kelurahan Asal Sekolah "  type="text"  name="daerah_sch" >
								</div>
							</div>
							<div class="control-group" id="new4">
								<label class="control-label">Jenis Sekolah</label>
								<div class="controls">
									<input type="radio" <?php if ($cek->kategori_skl == 'ngr') {echo 'checked=""';} ?> name="jenis_skl" value="ngr" > NEGERI &nbsp;&nbsp;
									<input type="radio" <?php if ($cek->kategori_skl == 'swt') {echo 'checked=""';} ?> name="jenis_skl" value="swt" > SWASTA &nbsp;&nbsp; 
								</div>
							</div>
							<div class="control-group" id="new4">
								<!-- <label class="control-label">Jenis Sekolah</label> -->
								<div class="controls">
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMA') {echo 'checked=""';} ?> value="SMA" > SMA &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMK') {echo 'checked=""';} ?> value="SMK" > SMK &nbsp;&nbsp; 
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'MA') {echo 'checked=""';} ?> value="MA" > MA  &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMTB') {echo 'checked=""';} ?> value="SMTB" > SMTB  &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'LAIN') {echo 'checked=""';} ?> value="LAIN" > Lainnya  
								</div>
							</div>
							<div class="control-group" id="new3">
								<label class="control-label">Jurusan</label>
								<div class="controls">
									<input class="form-control span1" type="text" value="<?php echo $cek->jur_maba; ?>"  name="jur_sch"> &nbsp;&nbsp; Tahun Lulus &nbsp;&nbsp;
									<input class="form-control span1" type="text" value="<?php echo $cek->lulus_maba; ?>"  name="lulus_sch">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- NEW END -->

						<!-- KONVERSI START -->
						<?php if ($cek->jenis_pmb == 'KV') { ?>
							<div id="konversi">
							<div class="control-group" id="konversi1">
								<label class="control-label">Nama Perguruan Tinggi</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->asal_pts_konversi; ?>" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa"  type="text"  name="asal_pts">
								</div>
							</div>
							<div class="control-group" id="konversi4">
								<label class="control-label">Kota PTN/PTS</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->kota_pts_konversi; ?>" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="kota_pts">
								</div>
							</div>
							<div class="control-group" id="konversi2">
								<label class="control-label">Program Studi</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->prodi_pts_konversi; ?>" placeholder="Isi dengan Program Studi "  type="text"  name="prodi_pts">
								</div>
							</div>
							<div class="control-group" id="konversi3">
								<label class="control-label">Tahun Lulus / Semester</label>
								<div class="controls">
									<input class="form-control span1" value="<?php echo $cek->tahun_lulus_konversi; ?>" placeholder="Tahun" type="text"  name="lulus_pts">  / 
									<input class="form-control span1" value="<?php echo $cek->smtr_lulus_konversi; ?>" placeholder="Semester " type="text"  name="smtr_pts">
								</div>
							</div>
							
							<div class="control-group" id="konversi5">
								<label class="control-label">NPM/NIM Asal</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->npm_pts_konversi; ?>" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="npm_pts">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- KONVERSI END -->

						<div class="control-group">
							<label class="control-label">Pilih Program Studi</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi" disabled>
									<option disabled="" >-- PILIH PROGRAM STUDI--</option>
									<?php foreach ($rows as $isi): ?>
										<option value="<?php echo $isi->kd_prodi ?>" <?php if ($cek->prodi == $isi->kd_prodi) {echo 'selected=""';} ?>><?php echo $isi->prodi ?></option>
									<?php endforeach ?>
									
								</select>
							</div>
						</div>

						
						
						<div class="control-group">
							<label class="control-label">Nama Ayah</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ayah Calon Mahasiswa" value="<?php echo $cek->nm_ayah; ?>"  name="nm_ayah">
								<select class="form-control span2"  name="didik_ayah">
									<option disabled="" >-- Pendidikan Ayah --</option>
									<option value="TSD" <?php if ($cek->didik_ayah == 'TSD') {echo 'selected=""';} ?>>Tidak tamat SD</option>
									<option value="SD" <?php if ($cek->didik_ayah == 'SD') {echo 'selected=""';} ?>>Tamat SD</option>
									<option value="SLTP" <?php if ($cek->didik_ayah == 'SLTP') {echo 'selected=""';} ?>>Tamat SLTP</option>
									<option value="SLTA" <?php if ($cek->didik_ayah == 'SLTA') {echo 'selected=""';} ?>>Tamat SLTA</option>
									<option value="D" <?php if ($cek->didik_ayah == 'D') {echo 'selected=""';} ?>>Diploma</option>
									<option value="SM" <?php if ($cek->didik_ayah == 'SM') {echo 'selected=""';} ?>>Sarjana Muda</option>
									<option value="S" <?php if ($cek->didik_ayah == 'S') {echo 'selected=""';} ?>>Sarjana</option>
									<option value="PSC" <?php if ($cek->didik_ayah == 'PSC') {echo 'selected=""';} ?>>Pascasarjana</option>
									<option value="DTR" <?php if ($cek->didik_ayah == 'DTR') {echo 'selected=""';} ?>>Doctor</option>
									
								</select>
								<select class="form-control span2"  name="workdad">
									<option disabled="" >-- Pekerjaan Ayah --</option>
									<option value="PN" <?php if ($cek->workdad == 'TSD') {echo 'selected=""';} ?>>Pegawai Negeri</option>
									<option value="TP" <?php if ($cek->workdad == 'TP') {echo 'selected=""';} ?> >TNI / POLRI</option>
									<option value="PS" <?php if ($cek->workdad == 'PS') {echo 'selected=""';} ?>>Pegawai Swasta</option>
									<option value="WU" <?php if ($cek->workdad == 'WU') {echo 'selected=""';} ?>>Wirausaha</option>
									<option value="PSN" <?php if ($cek->workdad == 'PSN') {echo 'selected=""';} ?>>Pensiun</option>
									<option value="TK" <?php if ($cek->workdad == 'TK') {echo 'selected=""';} ?>>Tidak Bekerja</option>
									<option value="LL" <?php if ($cek->workdad == 'LL') {echo 'selected=""';} ?>>Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statdad">
									<option disabled="" >-- Status Hidup --</option>
									<option value="MH" <?php if ($cek->life_statdad == 'MH') {echo 'selected=""';} ?>>Masih Hidup</option>
									<option value="SM" <?php if ($cek->life_statdad == 'SM') {echo 'selected=""';} ?>>Sudah Meninggal</option>
									
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ibu Calon Mahasiswa" value="<?php echo $cek->nm_ibu; ?>" name="nm_ibu">
								<select class="form-control span2"  name="didik_ibu">
									<option disabled="" >-- Pendidikan Ibu --</option>
									<option value="TSD" <?php if ($cek->didik_ibu == 'TSD') {echo 'selected=""';} ?>>Tidak tamat SD</option>
									<option value="SD" <?php if ($cek->didik_ibu == 'SD') {echo 'selected=""';} ?>>Tamat SD</option>
									<option value="SLTP" <?php if ($cek->didik_ibu == 'SLTP') {echo 'selected=""';} ?>>Tamat SLTP</option>
									<option value="SLTA" <?php if ($cek->didik_ibu == 'SLTA') {echo 'selected=""';} ?>>Tamat SLTA</option>
									<option value="D" <?php if ($cek->didik_ibu == 'D') {echo 'selected=""';} ?>>Diploma</option>
									<option value="SM" <?php if ($cek->didik_ibu == 'SM') {echo 'selected=""';} ?>>Sarjana Muda</option>
									<option value="S" <?php if ($cek->didik_ibu == 'S') {echo 'selected=""';} ?>>Sarjana</option>
									<option value="PSC" <?php if ($cek->didik_ibu == 'PSC') {echo 'selected=""';} ?>>Pascasarjana</option>
									<option value="DTR" <?php if ($cek->didik_ibu == 'DTR') {echo 'selected=""';} ?>>Doctor</option>
								</select>
								<select class="form-control span2"  name="workmom">
									<option disabled="" >-- Pekerjaan Ibu --</option>
									<option value="PN" <?php if ($cek->workmom == 'TSD') {echo 'selected=""';} ?>>Pegawai Negeri</option>
									<option value="TP" <?php if ($cek->workmom == 'TP') {echo 'selected=""';} ?> >TNI / POLRI</option>
									<option value="PS" <?php if ($cek->workmom == 'PS') {echo 'selected=""';} ?>>Pegawai Swasta</option>
									<option value="WU" <?php if ($cek->workmom == 'WU') {echo 'selected=""';} ?>>Wirausaha</option>
									<option value="PSN" <?php if ($cek->workmom == 'PSN') {echo 'selected=""';} ?>>Pensiun</option>
									<option value="TK" <?php if ($cek->workmom == 'TK') {echo 'selected=""';} ?>>Tidak Bekerja</option>
									<option value="LL" <?php if ($cek->workmom == 'LL') {echo 'selected=""';} ?>>Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statmom">
									<option disabled="" >-- Status Hidup --</option>
									<option value="MH" <?php if ($cek->life_statmom == 'MH') {echo 'selected=""';} ?>>Masih Hidup</option>
									<option value="SM" <?php if ($cek->life_statmom == 'SM') {echo 'selected=""';} ?>>Sudah Meninggal</option>
									
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Penghasilan Orang Tua</label>
							<div class="controls">
								
								<input type="radio" ="1" <?php if ($cek->penghasilan == '1') {echo 'checked=""';} ?> value="" name="gaji">
								Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
							
								<input type="radio" value="2" <?php if ($cek->penghasilan == '2') {echo 'checked=""';} ?> name="gaji">
								Rp 2,100,000 - 4,000,000 <br>
							
								<input type="radio" value="3" <?php if ($cek->penghasilan == '3') {echo 'checked=""';} ?> name="gaji">
								Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
						
								<input type="radio" value="4" <?php if ($cek->penghasilan == '4') {echo 'checked=""';} ?> name="gaji">
								>= Rp 6,000,000
									
							</div>
						</div>
						<div class="control-group" id="">
							<label class="control-label">Pengguna BPJS </label>
							<div class="controls">
								<input type="radio" id="bpjs-y" <?php if ($cek->bpjs == 'y') {echo 'checked=""';} ?> name="bpjs" value="y" required> Ya &nbsp;&nbsp;
								<input type="radio" id="bpjs-n" <?php if ($cek->bpjs == 'n') {echo 'checked=""';} ?> name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">NO. BPJS</label>
							<div class="controls">
								<input id="men" class="form-control span3" value="<?php echo $cek->no_bpjs ?>" placeholder="No. BPJS Calon Mahasiswa"  type="text"  name="nobpjs">
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">Transportasi</label>
							<div class="controls">
								<select class="form-control span2"  name="transport">
									<?php 	if ($cek->transport != NULL and $cek->transport != '') {
												switch ($cek->transport) {
													case 'MBL':
														$transport = 'Mobil';
														break;
													case 'MTR':
														$transport = 'Motor';
														break;
													case 'AKT':
														$transport = 'Angkutan Umum';
														break;
													case 'SPD':
														$transport = 'Sepeda';
														break;
													case 'JKK':
														$transport = 'Jalan Kaki';
														break;
													case 'ADG':
														$transport = 'Andong';
														break;
													case 'KRT':
														$transport = 'Kereta';
														break;
												} ?>
												<option value="<?php echo $cek->transport;?>" selected><?php echo $transport; ?></option>
									<?php 	} ?>
									<option disabled="">-- Alat Transportasi --</option>
									<option value="MBL">Mobil</option>
									<option value="MTL">Motor</option>
									<option value="AKT">Angkutan Umum</option>
									<option value="SPD">Sepeda</option>
									<option value="JKK">Jalan Kaki</option>
									<option value="ADG">Andong</option>
									<option value="KRT">Kereta</option>
								</select>
							</div>
						</div>
						<!-- <div class="control-group" id="konversi5">
							<label class="control-label">Referensi </label>
							<div class="controls">
								<input class="form-control span6" placeholder="informasi mengenai ubhara diperoleh dari" value="<?php echo $cek->referensi; ?>"  type="text"  name="refer">
							</div>
						</div> -->

						<input class="form-control span3"   type="hidden"  name="nomor_registrasi" value="<?php echo $nomor_registrasi; ?>">

						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Update">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
