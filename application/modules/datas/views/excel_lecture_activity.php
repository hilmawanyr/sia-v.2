<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Aktifitas_Kuliah_Mahasiswa.xls");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <tr> 
    	<th>No</th>
        <th>NPM</th>
        <th>NAMA</th>
        <th>ANGKATAN</th>
        <th>IPS</th>
        <th>SKS SEMESTER</th>
        <th>TOTAL SKS</th>
        <th>IPK</th>
    </tr>
	<?php $no = 1; foreach($lectureActivity as $row){?>
    <tr>
    	<td><?php echo $no;?></td>
    	<td><?php echo $row->NIMHSTRAKM;?></td>
    	<td><?php echo $row->NMMHSMSMHS;?></td>
    	<td><?php echo $row->TAHUNMSMHS;?></td>
    	<td><?php echo $row->NLIPSTRAKM;?></td>
    	<td><?php echo number_format($row->SKSEMTRAKM);?></td>
    	<td><?php echo number_format($row->SKSTTTRAKM);?></td>
    	<td><?php echo $row->NLIPKTRAKM;?></td>
    </tr>
	<?php $no++;} ?>
</table>