<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list-alt"></i>
  				<h3>Data Aktifitas Kuliah Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a 
						href="<?= base_url('datas/aktifitas') ?>" 
						class="btn btn-warning">
						<i class="icon-chevron-left"></i> Kembali
					</a>
					<button 
						class="btn btn-success" 
						onclick="exportExcel()">
						<i class="icon-download-alt"></i> Eksport Excel
					</button>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                            <th>NPM</th>
	                            <th>NAMA</th>
	                            <th>ANGKATAN</th>
	                            <th>IPS</th>
	                            <th>SKS SEMESTER</th>
	                            <th>TOTAL SKS</th>
	                            <th>IPK</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($getaktifitas as $row){?>
	                        <tr>
	                        	<td><?= $no;?></td>
	                        	<td><?= $row->NIMHSTRAKM;?></td>
	                        	<td><?= $row->NMMHSMSMHS;?></td>
	                        	<td><?= $row->TAHUNMSMHS;?></td>
	                        	<td><?= $row->NLIPSTRAKM;?></td>
	                        	<td><?= number_format($row->SKSEMTRAKM);?></td>
	                        	<td><?= number_format($row->SKSTTTRAKM);?></td>
	                        	<td><?= $row->NLIPKTRAKM;?></td>
	                        </tr>
							<?php $no++;} ?>
	                      
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function exportExcel() {
		location.href = '<?= base_url('datas/aktifitas/exportLectureActivity/'.$data['prodi'].'/'.$data['tahunajar']) ?>';
	}
</script>