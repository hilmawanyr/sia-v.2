<script type="text/javascript">
    $(function() {
        $("#example4").dataTable();
    }); 
    ;
    /****/
</script>

<script type="text/javascript">
    function loadup(u,t,k) {
        $("#detailmodalcont").load('<?php echo base_url(); ?>datas/mahasiswapmb/load_det_berkas/' + u + '/' + t + '/' + k);
    }
</script>

<?php $datanya = $this->db2->query("SELECT * from tbl_form_pmb where id_form = ".$idcmb."")->row(); ?>

<?php // echo $datanya->user_input; ?>

<!-- <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Daftar Kelengkapan Arsip PMB</h4>
</div> -->

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Daftar Berkas Prasyarat <i><u><b><?php echo $datanya->nomor_registrasi.' - '.$datanya->nama; ?></b></u></i></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/save_lengkap_new" method="post">
                        <input type="hidden" name="id" value="<?php echo $mhspmb->nomor_registrasi; ?>">
                        <div class="control-group" id="">
                        <p><b>Tahun Lulus :  <?php echo $datanya->lulus_maba; ?></b></p>
                        <hr>
                            <table id="example4" class="table table-bordered table-striped">
                                <thead>
                                    <tr> 
                                        <th width="30">ID</th>
                                        <th>Data Arsip</th>
                                        <th width="50" style="text-align: center">Ket.</th>
                                        <th width="125" style="text-align: center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,1,$keybo) ?>>1</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,1,$keybo) ?>>Akta Kelahiran</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 1 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,1,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#akta" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+1+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/1/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,2,$keybo) ?>>2</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,2,$keybo) ?>>Kartu Keluarga</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 2 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,2,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#kk" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+2+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/2/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,3,$keybo) ?>>3</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,3,$keybo) ?>>Kartu Tanda Penduduk</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 3 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,3,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#ktp" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+3+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/3/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,4,$keybo) ?>>4</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,4,$keybo) ?>>Ijazah</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 4 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,4,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#ijz" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+4+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/4/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php if (($datanya->program == 1) and ($datanya->jenis_pmb == 'MB')) { ?>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,5,$keybo) ?>>5</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,5,$keybo) ?>>SKHUN</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 5 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,5,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#skhun" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+5+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/5/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,6,$keybo) ?>>6</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,6,$keybo) ?>>Raport</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 6 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,6,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#rapot" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+6+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/6/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,7,$keybo) ?>>7</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,7,$keybo) ?>>Foto</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 7 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,7,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#foto" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+7+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/7/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php if ($datanya->jenis_pmb == 'MB') { ?>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,8,$keybo) ?>>8</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,8,$keybo) ?>>Surat Keterangan Lulus</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 8 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,8,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#lulus" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+8+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/8/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <?php if ($datanya->jenis_pmb == 'KV') { ?>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,9,$keybo) ?>>9</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,9,$keybo) ?>>Surat Keterangan Pindah</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 9 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,9,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#pindah" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+9+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/9/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,10,$keybo) ?>>10</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,10,$keybo) ?>>Sertifikat Akreditasi</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 10 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,10,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#akre" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+10+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/10/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <?php if (($datanya->program != 1) or ($datanya->jenis_pmb != 'MB')) { ?>
                                    <tr>
                                        <td <?php echo giveColorForFile($datanya->user_input,11,$keybo) ?>>11</td>
                                        <td <?php echo giveColorForFile($datanya->user_input,11,$keybo) ?>>Transkrip</td>
                                        <?php $tanya = $this->db2->query("SELECT * from tbl_file where tipe = 11 and userid in (select user_input from tbl_form_pmb where id_form = ".$idcmb.") and key_booking = '".$keybo."'")->result(); ?>
                                        <td style="text-align: center"><?php if($tanya == TRUE){echo "<i class='icon-ok'></i>";} else { echo "-";} ?></td>
                                        <td <?php echo giveColorForFile($datanya->user_input,11,$keybo) ?>>
                                            <button data-toggle="modal" data-target="#trans" class="btn btn-info"><i class="icon icon-upload"></i></button>
                                            <button data-target="#detailModal" data-toggle="modal" onclick="loadup('<?php echo $datanya->user_input; ?>'+'/'+11+'/'+'<?php echo $keybo; ?>')" class="btn btn-success"><i class="icon icon-eye-open"></i></button>
                                            <a href="<?php echo base_url('datas/mahasiswapmb/voidfile/'.$datanya->user_input.'/11/'.$keybo) ?>" class="btn btn-danger"><i class="icon icon-remove"></i></button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <!--input type="hidden" name="jumlah" value="<?php //echo $no; ?>" /-->
                            
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="submit" class="btn btn-success" data-dismiss="modal">Simpan</button> -->
                            <a href="<?php echo base_url('datas/mahasiswapmb/validAllFile/'.$datanya->user_input.'/'.$keybo) ?>" class="btn btn-block btn-warning">Validasi Seluruh Berkas</a>
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                            <!-- <input type="submit" class="btn btn-primary" value="Simpan"/> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal lihat detail file -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">

        <div class="modal-content" id="detailmodalcont">

            

        </div>

    </div>

</div>


<!-- modal akta -->
<div class="modal fade" id="akta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Akta <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="1" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal KK -->
<div class="modal fade" id="kk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Kartu Keluarga <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="2" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal ktp -->
<div class="modal fade" id="ktp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan KTP <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="3" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal ijz -->
<div class="modal fade" id="ijz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Ijazah <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="4" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal skhun -->
<div class="modal fade" id="skhun" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan SKHUN <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="5" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal rapot -->
<div class="modal fade" id="rapot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Rapot <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="6" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal foto -->
<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Foto (3x4) <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="7" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal lulus -->
<div class="modal fade" id="lulus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Surat Lulus <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="8" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal pindah -->
<div class="modal fade" id="pindah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Surat Pindah <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="9" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal akre -->
<div class="modal fade" id="akre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Sertifikat Akreditasi <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="10" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal trans -->
<div class="modal fade" id="trans" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Transkrip <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="11" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal baa -->
<div class="modal fade" id="baa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Keterangan BAA <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="12" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- modal BPAK -->
<div class="modal fade" id="bpak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Scan Keterangan BPAK <small><i>(.jpg, .jpeg, .png)</i></small></h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/unggahberkas" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                    <div class="control-group" id="">
                        <label class="control-label">File <small><i>max. 2 MB</i></small> </label><br>
                        <div class="controls">
                            <input type="file" name="userfile">
                            <input type="hidden" value="13" name="tipe">
                            <input type="hidden" name="user" value="<?php echo $datanya->user_input; ?>">
                            <input type="hidden" name="key" value="<?php echo $keybo; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div>

    </div>

</div>