<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<a href="<?php echo base_url(); ?>akademik/viewkrs" class="btn btn-success"><< Kembali</a>
					<?php $cek = $this->app_model->validjadwal($kd_krs)->result(); if (count($cek) == 0) { ?>
						<!-- <a href="<?php //echo base_url(); ?>form/formkrs" class="btn btn-success"><< Kembali</a> -->
						<a href="<?php echo base_url(); ?>akademik/viewkrs/printkrs/<?php echo $kd_krs ?>" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KRS</a>
					<?php } ?>
					
					<p>* Apabila ingin melakukan perubahan jadwal , hubungi Pembimbing Akademik atau Kepala Program Studi yang bersangkutan</p>
					
					<hr>
					<table>
						<tr>
							<td>Mahasiswa</td>
							<td> : <?php echo $mhs->NIMHSMSMHS.' - '.$mhs->NMMHSMSMHS  ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
								<?php
									switch ($row->hari){
										case 1 :
											$hari = 'Senin';
										break;
										case 2 :
											$hari = 'Selasa';
										break;
										case 3 :
											$hari = 'Rabu';
										break;
										case 4 :
											$hari = 'Kamis';
										break;
										case 5 :
											$hari = 'Jumat';
										break;
										case 6 :
											$hari = 'Sabtu';
										break;
										case 7 :
											$hari = 'Minggu';
										break;
										default:
										$hari = '';
									}
								?>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->nama; ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
