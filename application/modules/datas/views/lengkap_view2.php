<script type="text/javascript">
    $(function() {
        $("#example4").dataTable();
    });
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Daftar Kelengkapan Arsip PMB (<?php echo $mhspmb->ID_registrasi; ?> - <?php echo $mhspmb->nama; ?>)</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/save_lengkap2" method="post">
<input type="hidden" name="id" value="<?php echo $mhspmb->ID_registrasi; ?>">
    <div class="modal-body">  
        <div class="control-group" id="">
        <p>Tahun Lulus :  <?php echo $mhspmb->th_lulus; ?></p>
            <table id="example4" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                        <th>No</th>
                        <th>Data Arsip</th>
                        <th width="50">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                	<?php //$no = 1; foreach ($kelas as $value) { ?>
                	<!--input type="hidden" name="jadwal" value="<?php //echo $value->kd_jadwal; ?>"/-->
                	<tr>
                		<td>1</td>
                		<td>Akte Kelahiran</td>
                        <?php $tanya = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%AKT%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                		<td><input type="checkbox" name="lengkap[]" value="AKT" <?php if($tanya == TRUE){echo "checked";} ?>/></td>
                	</tr>
                    <tr>
                        <td>2</td>
                        <td>Kartu Keluarga (KK)</td>
                        <?php $tanya2 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%KK%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="KK" <?php if($tanya2 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Kartu Tanda Penduduk (KTP)</td>
                        <?php $tanya3 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%KTP%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="KTP" <?php if($tanya3 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Rapot</td>
                        <?php $tanya4 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%RP%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="RP" <?php if($tanya4 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>SKHUN</td>
                        <?php $tanya5 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%SKHUN%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="SKHUN" <?php if($tanya5 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Ijazah</td>
                        <?php $tanya6 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%IJZ%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="IJZ" <?php if($tanya6 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Surat Kelulusan</td>
                        <?php $tanya7 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%SKL%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="SKL" <?php if($tanya7 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Foto (3x4 dan 4x6)</td>
                        <?php $tanya8 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%FT%' AND ID_registrasi = '".$mhspmb->ID_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="FT" <?php if($tanya8 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                    <tr>
                        <td>*</td>
                        <td><b>Lengkap Administratif</b></td>
                        <?php $tanya9 = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where status_kelengkapan LIKE '%LLKP%' AND ID_registrasi = '".$mhspmb->nomor_registrasi."'")->result(); ?>
                        <td><input type="checkbox" name="lengkap[]" value="LLKP" <?php if($tanya9 == TRUE){echo "checked";} ?>/></td>
                    </tr>
                	<?php //$no++; } ?>
                </tbody>
            </table>
            <!--input type="hidden" name="jumlah" value="<?php //echo $no; ?>" /-->
            
        </div>              
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>