<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifitas extends CI_Controller {

	private $userid, $group;

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login')) {
			$accessVerification = $this->role_model->cekakses(148)->result();
			if (!$accessVerification) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !');history.go(-1);</script>";
			}
			$this->load->model('datas/Aktifitas_model','activity');
			
			// set log session
			$session      = $this->session->userdata('sess_login');
			$this->userid = $session['userid'];
			$this->group  = $session['id_user_group'];

			// set timezone
			date_default_timezone_set('Asia/Jakarta');

		} else {
			redirect('auth','refresh');
		} 
	}

	public function index()
	{
		$this->session->unset_userdata('ta');
		$this->session->unset_userdata('prodi');

		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page']      = 'aktifitas_select';
		$this->load->view('template/template', $data);
	}

	/**
	 * Save session from filter
	 * 
	 * @return void
	 */
	public function save_session() : void
	{
		$group = get_group($this->group);

		if (in_array(8, $group) || in_array(19, $group)) {
			$this->session->set_userdata('prodi',$this->userid);
			$this->session->set_userdata('ta',$this->input->post('tahunajaran'));
		} else {
			$this->session->set_userdata('prodi',$this->input->post('jurusan'));
			$this->session->set_userdata('ta',$this->input->post('tahunajaran'));
		}
		
		redirect('datas/aktifitas/view','refresh');
	}

	/**
	 * Show list AKM
	 * 
	 * @return void
	 */
	public function view() : void
	{
		$prodi       = $this->session->userdata('prodi');
		$tahunajaran = $this->session->userdata('ta');

		$data['data'] = [
			'prodi'     => $prodi, 
			'tahunajar' => $tahunajaran
		];

		$data['getaktifitas'] = $this->app_model->getdataaktifitas($prodi,$tahunajaran);
		$data['page']         = 'aktifitas_view';
		$this->load->view('template/template', $data);
	}

	public function viewdetail($id)
	{
		$data['page'] = 'aktifitas_detail';
		$this->load->view('template/template', $data);
	}

	public function get_jurusan($id)
	{
		$kodeFakultas = explode('-',$id);
        $jurusan = $this
        				->app_model
        				->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $kodeFakultas[0], 'id_prodi', 'ASC')
        				->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option disabled>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	public function exportLectureActivity($prodi, $ta)
	{
		$data['lectureActivity'] = $this->app_model->getdataaktifitas($prodi,$ta);
		$this->load->view('excel_lecture_activity', $data);
	}

}

/* End of file Aktifitas.php */
/* Location: ./application/modules/datas/controllers/Aktifitas.php */