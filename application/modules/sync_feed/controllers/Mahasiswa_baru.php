<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_baru extends CI_Controller {

	public function index()
	{
		$data['page'] = "feed_maba";
		$this->load->view('template/template', $data);
	}

}

/* End of file Mahasiswa_baru.php */
/* Location: ./application/modules/sync_feed/controllers/Mahasiswa_baru.php */