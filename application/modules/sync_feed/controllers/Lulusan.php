<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lulusan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setting_model');
        error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(145)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
	}

	public function form()
	{
		$data['page']='v_lulusan_form';
        $this->load->view('template/template', $data);
	}

    function index(){
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_lulusan_select_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_lulusan_select_fak';

        } elseif ((in_array(8, $grup))) { // prodi
            $data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_lulusan_select_prodi';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_lulusan_select_baa';
        }

        $this->load->view('template/template', $data);   
    }

    function view(){
        $jurusan = $this->input->post('jurusan');
        $tahun = $this->input->post('tahun');

        $exp_jur = explode('_', $jurusan);

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$exp_jur[0]);
        $this->session->set_userdata('fakultas',$exp_jur[1]);

        redirect(base_url().'sync_feed/lulusan/view_lulusan','refresh');
    }

    function view_lulusan(){

        if ($this->session->userdata('jurusan')) {
            $data['rows']   = $this->db->query("SELECT * FROM tbl_lulusan lls 
                                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                                WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                                AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_lulusan_view';
            $this->load->view('template/template', $data);    
        }else{
            redirect(base_url().'sync_feed/lulusan','refresh');
        }        
    }

    function save()
    {
    	$data = array(
					'npm_mahasiswa' => $this->input->post('npm'), 
					'sk_yudisium' => $this->input->post('sk_yudi'),
					'tgl_yudisium' => $this->input->post('tgl_yudi'),
					'tgl_lulus' => $this->input->post('tgl_lulus'),
					'ipk' => $this->input->post('ipk'),
					'no_ijazah' => $this->input->post('no_ijazah'),
					'jdl_skripsi' => $this->input->post('jdl_skripsi'),
                    'dospem1' => $this->input->post('nidn1'),
                    'dospem2' => $this->input->post('nidn2'),
					'mulai_bim' => $this->input->post('mulai_bim'),
					'ahir_bim' => $this->input->post('ahir_bim'),
                    'ta_lulus' => $this->input->post('ta'),
                    'sks' => $this->input->post('sks'),
                    'flag_feeder' => 1
				);

        $isStudentExist = $this->db->where('npm_mahasiswa', $this->input->post('npm'))
                                    ->get('tbl_lulusan')
                                    ->num_rows();

        if ($isStudentExist > 0) {
            $this->db->where('npm_mahasiswa', $this->input->post('npm'))->update('tbl_lulusan', $data);
        } else {
            $this->db->insert('tbl_lulusan', $data);
        }

        $updt = [
            'STMHSMSMHS' => 'L',
            'TGLLSMSMHS' => $this->input->post('tgl_lulus')
        ];
        $this->db->where('NIMHSMSMHS', $this->input->post('npm'));
        $this->db->update('tbl_mahasiswa', $updt);

        // after added to lulusan table, student cannot login
        $this->db->where('userid', $this->input->post('npm'));
        $this->db->update('tbl_user_login', ['status' => 0]);

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    function edit($id)
    {
        //die($id);
        $data['row'] = $this->db->select('lls.*,mhs.NMMHSMSMHS')
                                ->from('tbl_lulusan lls')
                                ->join('tbl_mahasiswa mhs','lls.npm_mahasiswa = mhs.NIMHSMSMHS')
                                ->where('id_lulusan',$id)
                                ->get()->row();

        $data['page']='v_lulusan_form_edit';
        $this->load->view('template/template', $data);
    }

    function save_edit(){

        $id = $this->input->post('id');

        $data = array(
                        'sk_yudisium' => $this->input->post('sk_yudi'),
                        'tgl_yudisium' => $this->input->post('tgl_yudi'),
                        'tgl_lulus' => $this->input->post('tgl_lulus'),
                        'ipk' => $this->input->post('ipk'),
                        'no_ijazah' => $this->input->post('no_ijazah'),
                        'jdl_skripsi' => $this->input->post('jdl_skripsi'),
                        'mulai_bim' => $this->input->post('mulai_bim'),
                        'ahir_bim' => $this->input->post('ahir_bim'),
                        'sks' => $this->input->post('sks'),
                        );

        $this->db->where('id_lulusan', $id);
        $this->db->update('tbl_lulusan', $data);

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    function delete_data($id){
        $this->db->where('id_lulusan', $id)->delete('tbl_lulusan');

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    

    function get_jurusan($id)
    {
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
        $out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."_".$id."'>".$row->prodi. "</option>";
        }
        
        $out .= "</select>";

        echo $out;
    }

    function load_mhs_autocomplete()
    {
        $or_stm = "(a.NMMHSMSMHS like '%".$_GET['term']."%' OR a.NIMHSMSMHS like '%".$_GET['term']."%')"; 

        $st_mhs = array('C','K');

        $prodi = $this->session->userdata('jurusan');

        $this->db->distinct();

        $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS");

        $this->db->from('tbl_mahasiswa a');

        $this->db->where('a.KDPSTMSMHS', $prodi);

        $this->db->where($or_stm, NULL, FALSE);

        // $this->db->where_not_in('a.STMHSMSMHS', $st_mhs);

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(
                            'nama'       => $row->NMMHSMSMHS,
                            'npm'           => $row->NIMHSMSMHS,
                            'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS
                            );

        }

        echo json_encode($data);
    }

    function load_kry_autocomplete()
    {
        $or_stm = "(kry.nidn like '%".$_GET['term']."%' OR kry.nupn like '%".$_GET['term']."%' OR kry.nama like '%".$_GET['term']."%')"; 

        $nidn = "(nidn IS NOT NULL OR nupn IS NOT NULL)"; 

        $prodi = $this->session->userdata('jurusan');

        $this->db->distinct();

        $this->db->select("kry.nidn,kry.nupn,kry.nama");

        $this->db->from('tbl_karyawan kry');

        $this->db->where($nidn, null, false);

        //$this->db->or_where('nupn IS NOT NULL', null, false);

        $this->db->where($or_stm, NULL, FALSE);

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            if ($row->nupn != '') {
                $iddos = $row->nupn;
            } else {
                $iddos = $row->nidn;
            }
            

            $data[] = array(
                        'nama'  => $row->nama,
                        'nidn'  => $iddos,
                        'value' => $iddos.' - '.$row->nama
                        );
        }

        echo json_encode($data);

    }

    function sync_mhs()
    {
        if ($this->session->userdata('jurusan')) {
        
            $this->load->library("Nusoap_lib");
            //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
            $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
            $client = new nusoap_client($url, true);
            $proxy = $client->getProxy();
            $username = userfeeder;
            $password = passwordfeeder;
            $result = $proxy->GetToken($username, $password);

            $token = $result;
            $table = 'mahasiswa_pt';

            // get mahasiswa lulusan
            $mhs_data=$this->db->query("SELECT lls.* FROM tbl_lulusan lls 
                                        JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                        WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                        AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."")->result();

            foreach ($mhs_data as $value) {     

                $data = [
                            'bln_awal_bimbingan' => $value->mulai_bim, 
                            'bln_akhir_bimbingan' => $value->ahir_bim,
                            'judul_skripsi' => $value->jdl_skripsi,
                            'no_seri_ijazah' => $value->no_ijazah,
                            'ipk' => $value->ipk,
                            'tgl_keluar' => $value->tgl_lulus,
                            'tgl_sk_yudisium' => $value->tgl_yudisium,
                            'sk_yudisium' => $value->sk_yudisium,
                            'id_jns_keluar' => '1',
                            'jalur_skripsi' => 1
                            // 1 = lulus , 2 = mutasi, 3 = dikeluarkan, 4 = mengundurkan diri, 6 = wafat, 7 = hilang
                            //'sks_diakui' => 144                                
                        ];

                // make array of list student to update student's status
                $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$value->npm_mahasiswa."%'");
                $key = ['id_reg_pd' =>$sp['result']['id_reg_pd']];
                $records[] = ['key' =>$key, 'data' => $data];

                // get pembimbing 1
                $id_sdm = $proxy->GetRecord($token, 'dosen_pt' , "nidn = '".$value->dospem1."'");
                $dospem1['id_sdm'] = $id_sdm['result']['id_sdm'];
                $dospem1['id_reg_pd'] = $sp['result']['id_reg_pd'];
                $dospem1['urutan_promotor'] = 1;

                // get pembimbing 2
                $id_sdm2 = $proxy->GetRecord($token, 'dosen_pt' , "nidn = '".$value->dospem2."'");
                $dospem2['id_sdm'] = $id_sdm2['result']['id_sdm'];
                $dospem2['id_reg_pd'] = $sp['result']['id_reg_pd'];
                $dospem2['urutan_promotor'] = 2;

                // sync pembinmbing 1
                $result10 = $proxy->InsertRecord($token, 'dosen_pembimbing', json_encode($dospem1));
                echo "<pre>";
                print_r ($result10['result']);
                echo "</pre><hr>";//exit();

                // sync pembinmbing 2
                $result20 = $proxy->InsertRecord($token, 'dosen_pembimbing', json_encode($dospem2));
                echo "<pre>";
                print_r ($result10['result']);
                echo "</pre><hr>";//exit();
            }

            // update status to lulus
            foreach ($records as $record) {
                $result = $proxy->UpdateRecord($token, $table, json_encode($record));
                echo "<pre>";
                print_r ($result['result']);
                echo "</pre><hr>";
            }

        } else {
            redirect(base_url().'sync_feed/lulusan','refresh');
        }
    }

}

/* End of file Lulusan.php */
/* Location: ./application/controllers/Lulusan.php */