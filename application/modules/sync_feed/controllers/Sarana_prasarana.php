<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sarana_prasarana extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page']='v_sarana';
        $this->load->view('template/template', $data);
	}

}

/* End of file Sarana_prasarana.php */
/* Location: ./application/controllers/Sarana_prasarana.php */