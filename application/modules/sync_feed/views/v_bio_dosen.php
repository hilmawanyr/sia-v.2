<?php //var_dump($kelurahan);die(); ?>
<script type="text/javascript">
	var x = 0; /* Set Global Variable i */
	function increment(){
		x += 1; /* Function for automatic increment of field's "Name" attribute. */
	}

	function removeElement(parentDiv, childDiv){
		if (childDiv == parentDiv){
			alert("The parent div cannot be removed.");
		}
		else if (document.getElementById(parentDiv)){
			var child = document.getElementById(childDiv);
			var parent = document.getElementById(parentDiv);
			parent.removeChild(child);
		}
		else{
			alert("Child div has already been removed or does not exist.");
			return false;
		}
	}

	function addFields(){
	    var kerjaan = document.getElementById("kerja").value;
	    var contain = document.createElement("div");
	    var subcontain = document.createElement("INPUT");
	    var btn = document.createElement("button");
	    var icon = document.createElement("i");

	    increment();
	    
		contain.setAttribute("class", "controls");
		contain.setAttribute("id", "work_"+x)

		subcontain.setAttribute("type", "text");
		subcontain.setAttribute("value", kerjaan);
		subcontain.setAttribute("name", "work[]");
		subcontain.setAttribute("class", "span3");
		subcontain.setAttribute("readonly", "");

		btn.setAttribute("class", "btn btn-default");
		btn.setAttribute("style", "float:left");
		btn.setAttribute("onClick", "removeElement('xx', 'work_"+x+"')");
		btn.setAttribute("type", "button");

		icon.setAttribute("class", "icon icon-remove");
		btn.appendChild(icon);

	    document.getElementById("xx").appendChild(contain);
	    document.getElementById("work_"+x).appendChild(subcontain);
	    document.getElementById("work_"+x).appendChild(btn);
	    document.getElementById("work_"+x).appendChild(document.createElement("br"));
	    // document.getElementById("xx").appendChild(document.createElement("br"));
	}

	function addFieldsdidik(){
	    var didikan = document.getElementById("didik").value;
	    var konten = document.createElement("div");
	    var subkonten = document.createElement("INPUT");
	    var btn = document.createElement("button");
	    var icon = document.createElement("i");

	    increment();

	    konten.setAttribute("class", "controls");
	    konten.setAttribute("id", "edu"+x);

	    subkonten.setAttribute("type", "text");
		subkonten.setAttribute("value", didikan);
		subkonten.setAttribute("name", "didik[]");
		subkonten.setAttribute("class", "span3");
		subkonten.setAttribute("readonly", "");

		btn.setAttribute("class", "btn btn-default");
		btn.setAttribute("onClick", "removeElement('ww','edu" + x +"')");
		btn.setAttribute("type", "button");
		btn.setAttribute("style", "float:left");

		icon.setAttribute("class", "icon icon-remove");
		btn.appendChild(icon);
		
	    document.getElementById("ww").appendChild(konten);
	    document.getElementById("edu"+x).appendChild(subkonten);
	    document.getElementById("edu"+x).appendChild(btn);
	    document.getElementById("edu"+x).appendChild(document.createElement("br"));
	    // document.getElementById("isididik").appendChild(document.createElement("br"));
	    
	}
</script>
<script language="javascript">
	function numeric(e, decimal) { 
	var key;
	var keychar;
	 if (window.event) {
		 key = window.event.keyCode;
	 } else
	 if (e) {
		 key = e.which;
	 } else return true;
	
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		return true;
	} else 
	if ((("0123456789").indexOf(keychar) > -1)) {
		return true; 
	} else 
	if (decimal && (keychar == ".")) {
		return true; 
	} else return false; 
	}
</script> 


<div class="container">
	
	    <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>BIODATA DIRI</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">

						<ul class="nav nav-tabs">
						  	<li class="active"><a data-toggle="tab" href="#home">Biodata</a></li>
						  	<li><a data-toggle="tab" href="#menu1">Penelitian</a></li>
						  	<li><a data-toggle="tab" href="#menu2">Riwayat Penelitian</a></li>
						</ul>

						<div class="tab-content">

						  	<div id="home" class="tab-pane fade in active">

						  		<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url();?>sync_feed/biodata/tambah_biodosen" enctype="multipart/form-data">
									<fieldset>

										<div class="control-group">											

											<label class="control-label">NID</label>

											<div class="controls">

												<input type="text" class="span6" name="nid" placeholder="NID" value="<?php echo $dets->nid; ?>"  required readonly>


											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">NIDN</label>

											<div class="controls">

												<input type="text" class="span6" name="nidn" placeholder="NIDN" value="<?php echo $dets->nidn; ?>"  required readonly>

												<!-- <p class="help-block">Perubahan Nama Dapat Diajukan Melalui Biro Administrasi Akademik</p> -->

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Nama*</label>

											<div class="controls">

												<input type="text" class="span6" name="nama" placeholder="Nama Dosen" value="<?php echo $dets->nama; ?>" minlength=16 required readonly>

												<p class="help-block">Perubahan Nama Dapat Diajukan Melalui Biro Administrasi Akademik</p>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Alamat*</label>

											<div class="controls">

												<textarea type="text" class="span6" name="alamat" required><?php echo $detil->alamat; ?></textarea>

												<!-- <p class="help-block">Nomor Induk Siswa Nasional dapat anda lihat pada ijazah terahir anda.</p> -->

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Telepon*</label>

											<div class="controls">

												<input type="text" class="span6"  onkeypress="return numeric(event, false)" name="tlp" placeholder="Nomor Telepon" value="<?php echo $detil->tlp; ?>"  required>

												<!-- <p class="help-block">Nomor Induk Siswa Nasional dapat anda lihat pada ijazah terahir anda.</p> -->

											</div> <!-- /controls -->				

										</div>

										<div class="control-group">											

											<label class="control-label">E-mail*</label>

											<div class="controls">

												<input type="email" class="span6" name="email" placeholder="E-mail aktif dosen" value="<?php echo $detil->email; ?>"  required>

												<!-- <p class="help-block">Nomor Induk Siswa Nasional dapat anda lihat pada ijazah terahir anda.</p> -->

											</div> <!-- /controls -->				

										</div>

										<div class="control-group">											

											<label class="control-label">Tempat Lahir*</label>

											<div class="controls">

												<input type="text" class="span6" name="tempatlahir" placeholder="Tempat Lahir Sesuai KTP" value="<?php echo $detil->tpt_lahir; ?>"  required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script>
							                $(function() {
							                    $( "#tgl" ).datepicker({
							                        changeMonth: true,
							                        changeYear: true,
							                        yearRange: "-70:+0",
							                    });
							                });
							            </script>

										<div class="control-group">											

											<label class="control-label">Tanggal Lahir*</label>

											<div class="controls">

												<input type="text" class="span6" id="tgl" name="tgl" placeholder="Tanggal Lahir Sesuai KTP" value="<?php echo $detil->tgl_lahir; ?>"  required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->		
										
										<div class="control-group">											

											<label class="control-label">Jabatan Fungsional*</label>

						  					<div class="controls">
											
												<select name="jabfung" class="span6" required>
												
													<option selected disabled>--Pilih Jabatan--</option>	
													
													<option <?php if($detil->jabfung=='TPD'){ echo ' selected="selected"'; }?> value="TPD">Tenaga Pendidik</option>
													
													<option <?php if($detil->jabfung=='SAH'){ echo ' selected="selected"'; }?> value="SAH">Asisten Ahli</option>
													
													<option <?php if($detil->jabfung=='LKT'){ echo ' selected="selected"'; }?> value="LKT">Lektor</option>
													
													<option <?php if($detil->jabfung=='LKK'){ echo ' selected="selected"'; }?> value="LKK">Lektor Kepala</option>
													
													<option <?php if($detil->jabfung=='BIG'){ echo ' selected="selected"'; }?> value="BIG">Guru Besar</option>
													
												</select>
												
											</div>
										</div>
										
										<div class="control-group">											

											<label class="control-label">Tahun Jabatan*</label>

											<div class="controls">

												<input type="text" class="span6" onkeypress="return numeric(event, false)" maxlength="4" name="thnj" placeholder="Tahun Jabatan Fungsional" value="<?php echo $detil->thnj; ?>"  required>

											</div> <!-- /controls -->				

										</div>
								
								<!-- =============================================================== PENDIDIKAN =============================================================== -->	

										<script type="text/javascript">

											function delRow(id) {
												if (confirm('Anda ingin menghapus data ini ?')) {
													$.ajax({
											            type: 'POST',
											            url: '<?= base_url('sync_feed/biodata/delRows/');?>'+id,
											            error: function (xhr, ajaxOptions, thrownError) {
											                return false;           
											            },
											            success: function () {
											                resetedu(id);
											            }
											        });
												}
										    }  

											function resetedu(id) {
												$("#shwdiv_"+id).remove();
										    }

											$(document).ready(function (){

												var d = new Date();
			    								var n = d.getFullYear();

												// jika tahun bukan angka
												$("#stryear,#endyear").keyup(function (){
													if (isNaN($(this).val())) {
														$(this).val('');
													}
												});	

												// jika input tahun melebihi tahun aktif
												$("#endyear").blur(function (){
													if ($(this).val() > n) {
														alert('Input tahun tidak boleh melebihi tahun ini!');
														$(this).val(''); 
													}	
												});							

											});

											var count = 1;

											function addjenj() 
											{
												var inst = $('#ins').val();

												var strt = $('#stryear').val();

												var endd = $('#endyear').val();

												var cont = $('#kons').val();

												// alert jika kolom dikosongkan
												if (inst === '' || strt === '' || endd === '') {
													alert('Kolom tidak boleh dikosongkan!'); return;
												}

												count++;

												var newRows = $("#showYourDiv");

												var rows = "";
												
												rows += '<div class="controls" id="mydiv_'+count+'">';

												rows += '<input type="text" class="form-control" id="" style="margin-right:3px;" name="inst[]" value="'+inst+'" required="" readonly/>';

												rows += '<input type="text" class="form-control" id="" style="margin-right:3px;" name="konst[]" value="'+cont+'" required="" readonly/>';

												rows += '<input type="text" class="form-control span2" id="" style="margin-right:3px;" name="str[]" value="'+strt+'" required="" readonly/>';

												rows += '<input type="text" class="form-control span2" id="" style="margin-right:4px;" name="end[]" value="'+endd+'" required="" readonly/>';

												rows += '<a class="btn btn-danger" onclick="delsjenj('+count+')"><i class="icon icon-remove"></i> </a>';

												rows += '</div>';

												rows += '<br>';

												newRows.append(rows);

												$('#ins,#kons,#stryear,#endyear').val('');

											}

											function delsjenj(id)
											{
												$("#mydiv_"+id).remove();
											}

										</script>

										<div class="control-group">											

											<label class="control-label">Riwayat Pendidikan*</label>

						  					<div class="form-horizontal">

						  						<div id="showYourDiv">
						  							
						  						</div>

						  						<?php foreach ($pdd as $lue) { ?>
						  							
						  							<div class="controls" id="shwdiv_<?php echo $lue->id_pddkn; ?>">

								  						<input type="text" class="form-control"  value="<?php echo $lue->instansi; ?>" disabled="">

								  						<input type="text" class="form-control"  value="<?php echo $lue->jurusan; ?>" disabled="">

								  						<input type="text" class="form-control span2" value="<?php echo $lue->tahun_masuk; ?>" disabled="">

								  						<input type="text" class="form-control span2" value="<?php echo $lue->tahun_keluar; ?>" disabled="">

								  						<a href="javascript:void(0);" class="btn btn-danger" onclick="delRow('<?php echo $lue->id_pddkn; ?>')"><i class="icon icon-remove"></i></a>

							  						</div>

							  						<br>

						  						<?php } ?>

						  						<div class="controls">

							  						<input type="text" class="form-control" id="ins" placeholder="Nama Instansi" value="" />

							  						<input type="text" class="form-control" id="kons" placeholder="Konsentrasi" value="" />

							  						<input type="text" class="form-control span2" id="stryear" maxlength="4" minlength="4" placeholder="Tahun Mulai" value="" />

							  						<input type="text" class="form-control span2" id="endyear" maxlength="4" minlength="4" placeholder="Tahun Selesai" value="" />

							  						<a href="javascript:void(0);" class="btn btn-success" onclick="addjenj()"><i class="icon icon-plus"></i> </a>

						  						</div>

						  					</div>

										</div>
<!-- ================================================================================== pendidikan end ================================================================================== -->	


										<!-- riwayat pekerjaan dimatikan 

										<div class="control-group">											

											<label class="control-label">Riwayat Pekerjaan</label>

											<div class="controls">

												<input type="text" class="span3" placeholder="Tambah Riwayat Pekerjaan Anda" id="kerja">

												<a onClick="addFields()" type="button" class="btn btn-primary"><i class="icon icon-plus"></i> Tambah</a>

											</div>		

										</div>

										<div class="control-group" id="xx">											

											<label class="control-label"></label>

											<?php if (count($detil->nid) > 0) {
												$arra = explode(',', $detil->pekerjaan); 
												for ($i=0; $i < count($arra)-1; $i++) { ?>
												<div class="controls" id="kerj<?php echo $i; ?>">
												 	<input type="text" name="work[]" value="<?php echo $arra[$i]; ?>" class="span3" readonly required>
													<button onclick="removeElement('xx','kerj<?php echo $i; ?>')"  class="btn btn-default" style="float:left"><i class="icon icon-remove"></i></button>
												</div>
												<?php } 
											} ?>		

										</div>

										-->

										<!-- <div class="control-group">											

											<label class="control-label">Foto (max 1 MB)</label>

											<div class="controls">

												<input type="file" name="userfile">

											</div> <!-- /controls -->				

										<!-- </div> --> 

										<div class="control-group">											

											<label class="control-label">Beban Mengajar</label>

											<div class="controls">

												<a href="javascript:;" class="btn btn-success"><h4><?php echo $beban->jums; ?> SKS</h4></a>
												<a href="<?php echo base_url(); ?>sync_feed/biodata/detil_beban" class="">Lihat Detil</a>

											</div> <!-- /controls -->				

										</div>	

										<div class="control-group">											

											<label class="control-label">Lihat Profil</label>

											<div class="controls">

												<a href="<?php echo base_url(); ?>sync_feed/biodata/profil" class="btn btn-primary"><i class="icon icon-eye-open"></i> Lihat Profil</a>

											</div> <!-- /controls -->				

										</div>					
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary">Save</button> 
											<button class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
							    
						  	</div>

						  	<!-- ========================================================= batas ========================================================= -->

						  	<script type="text/javascript">

								var counter = 1;

								$(document).ready(function (){

									var d = new Date();
    								var n = d.getFullYear();

									// jika tahun bukan angka
									$("#thn,#thn-dev,#thn-pub").keyup(function (){
										if (isNaN($(this).val())) {
											alert('Karakter harus angka!');
											$(this).val('');
										}
									});	

									$("#thn,#thn-dev,#thn-pub").blur(function (){
										if ($(this).val() > n) {
											alert('Input tahun tidak boleh melebihi tahun ini!');
											$(this).val(''); 
										}	
									});							

								});

								// fungsi tambah row
						  		function addResearch(sdl,yrs,typ,pos,fee,apd,kids,btn)
						  		{
						  			if ($('#'+sdl).val() === '' || $('#'+yrs).val() === '') {
						  				alert('Kolom tidak boleh kosong!'); return;
						  			}

						  			counter++;

									var newRow = $("#"+apd);

									var cols = "";

									if (kids === 'pub') {
										// tipe
										if ($('#'+typ).val() === 'INT') {
											var typess = 'Internasional';
										} else if ($('#'+fee).val() === 'NSL') {
											var typess = 'Nasional Terakreditasi';
										} else {
											var typess = 'Tidak Terakreditasi';
										}

										// biaya diganti untuk jenis penelitian yg dipublikasi
										if ($('#'+fee).val() === 'YRS') {
											var feesy = 'Penelitian';
										} else if ($('#'+fee).val() === 'NRS') {
											var feesy = 'Non Penelitian';
										}

									} else {
										// tipe
										if ($('#'+typ).val() === 'INT') {
											var typess = 'Internasional';
										} else if ($('#'+fee).val() === 'NSL') {
											var typess = 'Nasional';
										} else {
											var typess = 'Regional';
										}

										// biaya
										if ($('#'+fee).val() === 'MDR') {
											var feesy = 'Mandiri';
										} else if ($('#'+fee).val() === 'BSW') {
											var feesy = 'Beasiswa';
										}
									}


									// posisi
									if ($('#'+pos).val() === 'KET') {
										var poss = 'Ketua';
									} else if ($('#'+fee).val() === 'AGT') {
										var poss = 'Anggota';
									}

									cols += '<div id="'+kids+'_'+counter+'" class="control-group '+kids+'">';
									cols += '<label class="control-label"></label>';
									cols += '<div class="controls">';

									// jika element research atau pengabdian
									if (kids === 'rsc' || kids === 'dev') {

										// judul
										cols += '<input type="text" name="titls[]" style="margin-right:3px" class="span4" value="'+$('#'+sdl).val()+'" readonly=""/>';

										// tahun
										cols += '<input type="text" name="years[]" style="margin-right:3px" class="span1" value="'+$('#'+yrs).val()+'" readonly=""/>';

										// tipe
										cols += '<input type="text" name="" style="margin-right:4px" class="span2" value="'+typess+'" readonly=""/>';
										cols += '<input type="hidden" name="types[]" id="'+kids+typ+'" style="padding-right:2px" class="span3" value="'+$('#'+typ).val()+'"/>';

										// posisi
										cols += '<input type="text" style="margin-right:3px" class="span2" value="'+poss+'" readonly=""/>';
										cols += '<input type="hidden" name="posy[]" style="padding-right:2px" class="span3" value="'+$('#'+pos).val()+'"/>';
										
										// biaya
										cols += '<input type="text" style="margin-right:3px" class="span1" value="'+feesy+'" readonly=""/>';
										cols += '<input type="hidden" name="fees[]" class="span3" value="'+$('#'+fee).val()+'"/>';

									// jika element publikasi
									} else {

										// judul
										cols += '<input type="text" name="titls[]" style="margin-right:3px" class="span4" value="'+$('#'+sdl).val()+'" readonly=""/>';

										// tahun
										cols += '<input type="text" name="years[]" style="margin-right:3px" class="span1" value="'+$('#'+yrs).val()+'" readonly=""/>';

										// jurnal
										cols += '<input type="text" name="jurnal[]" id="'+kids+typ+'" style="margin-right:3px" class="span2" value="'+$('#'+pos).val()+'" readonly=""/>';

										// tingkat
										cols += '<input type="text" style="margin-right:3px" class="span2" value="'+typess+'" readonly=""/>';
										cols += '<input type="hidden" name="tgkt[]" style="padding-right:2px" class="span3" value="'+$('#'+typ).val()+'"/>';

										// jenis
										cols += '<input type="text" style="margin-right:3px" class="span1" value="'+feesy+'" readonly=""/>';
										cols += '<input type="hidden" name="jens[]" style="padding-right:2px" class="span3" value="'+$('#'+fee).val()+'"/>';

									}

									cols += '<button class="btn btn-danger" onclick="delResearch(\''+kids+'_'+counter+'\',\''+kids+'\',\''+btn+'\')" style="margin-bottom: 1px"><i class="icon icon-remove"></i></button>';
									cols += '</div>';
									cols += '</div>';
									
									newRow.append(cols);

									if (kids === 'pub') {

										$('#'+sdl+',#'+yrs+',#'+pos).val('');

									} else {

										$('#'+sdl+',#'+yrs).val('');

									}
									
									$('#'+fee+' option:first').prop('selected',true);
									$('#'+typ+' option:first').prop('selected',true);
									$('#'+pos+' option:first').prop('selected',true);

									// delete prop disabled in #btnRsc if form != null
									if ($('#'+kids+typ).val() != '') {
										 $("#"+btn).removeAttr("disabled");
									}
						  		}

						  		// fungsi delete row
						  		function delResearch(skidrow,skiddy,btnSbm)
						  		{
						  			$("#"+skidrow).remove();

						  			// disable the button if input == 0
						  			if ($("."+skiddy).length === 0) {
										$("#"+btnSbm).attr("disabled","");
									}
						  		}

						  	</script>

						  	<div id="menu1" class="tab-pane fade">

						  		<fieldset>

						  			<div class="form-horizontal">

								  		<h3><i class="icon icon-book"></i> Penelitian</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label">Input Penelitian</label>									

											<div class="controls">

												<input type="text" class="span4" name="judul" placeholder="Judul Penelitian" id="jdl" value=""  required>

												<input type="text" class="span1" name="tahun" maxlength="4" minlength="4" placeholder="Tahun" id="thn" value=""  required>

												<select class="span2" name="jenis" id="jns">
													<option disabled="" selected="">- Jenis -</option>
													<option value="INT">Internasional</option>
													<option value="NSL">Nasional</option>
													<option value="REG">Regional/Lokal</option>
												</select>

												<select class="span2" name="posisi" id="pos">
													<option disabled="" selected="">- Posisi -</option>
													<option value="KET">Ketua</option>
													<option value="AGT">Anggota</option>
												</select>

												<select class="span1" name="biaya" id="dna">
													<option disabled="" selected="">- Dana -</option>
													<option value="MDR">Mandiri</option>
													<option value="BSW">Beasiswa</option>
												</select>

												<button class="btn btn-success" onclick="addResearch('jdl','thn','jns','pos','dna','appendHere','rsc','btnRsc')" id="klik" style="margin-bottom: 1px"><i class="icon icon-plus"></i></button>

											</div> <!-- /controls -->				

										</div>

										<form action="<?php echo base_url(); ?>sync_feed/biodata/addresearch" method="post">

											<div id="appendHere">
												
												<!-- <input type="hidden" name="tipersc" value="1"> -->

											</div>
										
											<br>

											<center><button disabled="" type="submit" id="btnRsc" class="btn btn-primary">Submit</button></center>

										</form>

										<hr>

										<br>

										<!-- ================================================= limit penelitian ================================================= -->

										<h3><i class="icon icon-leaf"></i> Pengabdian</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label">Input Pengabdian</label>									

											<div class="controls">

												<input type="text" class="span4" name="judul" placeholder="Judul Pengabdian" id="jdl-dev" value=""  required>

												<input type="text" class="span1" name="tahun" maxlength="4" minlength="4" placeholder="Tahun" id="thn-dev" value=""  required>

												<select class="span2" name="jenis" id="jns-dev">
													<option disabled="" selected="">- Jenis -</option>
													<option value="INT">Internasional</option>
													<option value="NSL">Nasional</option>
													<option value="REG">Regional/Lokal</option>
												</select>

												<select class="span2" name="posisi" id="pos-dev">
													<option disabled="" selected="">- Posisi -</option>
													<option value="KET">Ketua</option>
													<option value="AGT">Anggota</option>
												</select>

												<select class="span1" name="biaya" id="dna-dev">
													<option disabled="" selected="">- Dana -</option>
													<option value="MDR">Mandiri</option>
													<option value="BSW">Beasiswa</option>
												</select>
												
												<button class="btn btn-success" onclick="addResearch('jdl-dev','thn-dev','jns-dev','pos-dev','dna-dev','appendHere-dev','dev','btnDev')" style="margin-bottom: 1px"><i class="icon icon-plus"></i></button>

											</div> <!-- /controls -->				

										</div>

										<form action="<?php echo base_url(); ?>sync_feed/biodata/addDevotion" method="post">

											<div id="appendHere-dev">
												
												<!-- <input type="hidden" name="tipersc" value="2"> -->

											</div>
										
											<br>

											<center><button disabled="" type="submit" id="btnDev" class="btn btn-primary">Submit</button></center>

										</form>

										<br>

										<hr>

										<!-- ================================================= limit pengabdian ================================================= -->

										<h3><i class="icon icon-bell"></i> Publikasi</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label">Input Publikasi</label>									

											<div class="controls">

												<input type="text" class="span4" name="judul" placeholder="Judul Publikasi" id="jdl-pub" value=""  required>

												<input type="text" class="span1" name="tahun" maxlength="4" minlength="4" placeholder="Tahun Publikasi" id="thn-pub" value=""  required>

												<input type="text" class="span3" name="jurnal" placeholder="Jurnal Publikasi" id="jur-pub" value=""  required>

												<select class="span1" name="jenis" id="jns-pub">
													<option disabled="" selected="">- Tingkat -</option>
													<option value="INT">Internasional</option>
													<option value="NSL">Nasional Terakreditasi</option>
													<option value="NAK">Tidak Terakreditasi</option>
												</select>

												<select class="span1" name="tipe" id="tip-pub">
													<option disabled="" selected="">- Jenis -</option>
													<option value="YRS">Penelitian</option>
													<option value="NRS">Non Penelitian</option>
												</select>
												
												<button class="btn btn-success" onclick="addResearch('jdl-pub','thn-pub','jns-pub','jur-pub','tip-pub','appendHere-pub','pub','btnPub')" style="margin-bottom: 1px"><i class="icon icon-plus"></i></button>

											</div> <!-- /controls -->				

										</div>

										<form action="<?php echo base_url(); ?>sync_feed/biodata/addPublication" method="post">
											
											<div id="appendHere-pub">
											
												<!-- <input type="hidden" name="tipersc" value="3"> -->

											</div>

											<br>

											<center><button disabled="" type="submit" id="btnPub" class="btn btn-primary">Submit</button></center>

										</form>

									</div>

								</fieldset>
							    
						  	</div>

						  	<!-- ========================================================== riwayat begin ========================================================== -->

						  	<div id="menu2" class="tab-pane fade">

						  		<fieldset>

						  			<div class="form-horizontal">

								  		<h3><i class="icon icon-book"></i> Penelitian</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label"></label>									

											<div class="controls">

												<ul>
												    <?php foreach ($rsc as $val) { ?>
												    	<li>
												    		<b><?php echo $val->tahun; ?> - <?php echo $val->judul; ?>. </b>(Penelitian Tingkat <?php echo tridarma('rsc',$val->jenis); ?>, Sebagai <?php echo tridarma('rsc',$val->posisi); ?>, Sumber Dana <?php echo tridarma('rsc',$val->biaya); ?>)
												    	</li>
												    	<hr>
												    <?php } ?>
												</ul>

											</div> <!-- /controls -->				

										</div>

										<hr>

										<br>

										<!-- ================================================= limit penelitian ================================================= -->

										<h3><i class="icon icon-leaf"></i> Pengabdian</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label"></label>									

											<div class="controls">

												<ul>
													<?php foreach ($dev as $ops) { ?>
														<li>
															<b>
															<?php echo $ops->tahun; ?> - <?php echo $ops->judul; ?> (Penelitian Tingkat <?php echo tridarma('rsc',$ops->jenis); ?>, Sebagai <?php echo tridarma('rsc',$ops->posisi); ?>, Sumber Dana <?php echo tridarma('rsc',$ops->Dana); ?>)</b>
														</li>
														<hr>
													<?php } ?>
												</ul>

											</div> <!-- /controls -->				

										</div>

										
										<br>

										<hr>

										<!-- ================================================= limit pengabdian ================================================= -->

										<h3><i class="icon icon-bell"></i> Publikasi</h3>

								  		<br>

								  		<div class="control-group">

								  			<label class="control-label"></label>									

											<div class="controls">

												<ul>
													<?php foreach ($pub as $ups) { ?>
														<li>
															<b>
															<?php echo $ups->tahun; ?> - <?php echo $ups->judul; ?> </b>(Penelitian Tingkat <?php echo $ups->tingkat; ?>, yang dipublikasikan pada jurnal <?php echo tridarma('pub',$ups->jurnal); ?>)
														</li>
														<hr>
													<?php } ?>
												</ul>

											</div> <!-- /controls -->				

										</div>

									</div>

								</fieldset>
							    
						  	</div>

						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	    </div> <!-- /row -->
	
</div> <!-- /container -->


<div class="modal fade" id="tambahpendidikan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="dodoy">

        	<div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Tambah Data Pendidikan</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>" method="post">

                <div class="modal-body">    

					<label style="float:left;margin-right:20px;"><b>Pendidikan</b></label>

					<input type="text" class="span4" id="tbh_didik" name="tbh_didik" placeholder="Tambah Data Pendidikan" value="" >

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>
            </form>
            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="tambahkerja" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="dodoy">

        	<div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Tambah Data Pekerjaan</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>sync_feed/biodata/coba_tambah" method="post">

                <div class="modal-body">    

					<label style="float:left;margin-right:20px;"><b>Pekerjaan</b></label>

					<input type="text" class="span4" id="" name="tbh_kerja" placeholder="Tambah Data Pekerjaan" value=""  required>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>
            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div>
	   