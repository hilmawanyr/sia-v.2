<?php ini_set('memory_limit', '2048M'); set_time_limit(0); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Hasil Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">

					<!-- make switch for prodi who will sync -->
					<?php 
						$logged = $this->session->userdata('sess_login');
						$status = $this->app_model->getdetail('panel_switch','kd_prodi',$logged['userid'],'kd_prodi','asc')->row()->feedkhs;
						if ($status == 1) { ?>

						<form 
							action="<?php echo base_url('sync_feed/kelas/sync_mhs_ipk'); ?>" 
							method="post" 
							accept-charset="utf-8" 
							target="_blank">
							<input type="submit" class="btn btn-primary" value="Sinkronisasi Aktifitas Kuliah">  
							<a href="#" class="btn btn-danger"><i class="icon icon-remove"></i> Simpan Data</a>

					<?php } else { ?>

						<form action="<?php echo base_url('sync_feed/krs/in'); ?>" method="post" accept-charset="utf-8">
							<a href="#" class="btn btn-danger"><i class="icon icon-remove"></i> Sinkronisasi Aktifitas Kuliah</a>
							<input type="submit" class="btn btn-success" value="Simpan Data">

					<?php } ?>
					<!-- end of switch panel -->

						<hr>
						<table id="examplekrs" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
		                        	<th>NIM</th>
		                        	<th>Nama MHS</th>
		                        	<th>Tahun Masuk</th>
		                        	<th>SKS <?php echo $lastyear; ?></th>
		                        	<th>IPS <?php echo $lastyear; ?></th>
		                        	<th>SKS Total</th>
		                        	<th>IPK <?php echo $lastyear; ?></th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php $no=1; foreach ($getData as $value) { ?>
	                    		<?php   
	                    			$jumlah = 0;
	                    			$warna ='';
		                    	?>
		                        <tr>
		                        	<td <?= $warna; ?>><?= number_format($no); ?></td>
		                        	<td <?= $warna; ?>><?= $value->npm_mahasiswa; ?></td>
		                        	<td <?= $warna; ?>><?= get_nm_mhs($value->npm_mahasiswa); ?></td>
	                                <td <?= $warna; ?>><?= substr($value->npm_mahasiswa, 0, 4); ?></td>

	                                <?php
	                                /*
		                                $logged = $this->session->userdata('sess_login');
		                                
		                                $hitung_ips = $this->db->query('SELECT distinct 
		                                											a.`NIMHSTRLNM`,
		                                											a.`KDKMKTRLNM`,
		                                											a.`NLAKHTRLNM`,
		                                											a.`BOBOTTRLNM`,
		                                											b.`sks_matakuliah` 
		                                								FROM tbl_transaksi_nilai a
						                    							JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
						                    							WHERE a.`kd_transaksi_nilai` IS NOT NULL 
						                    							AND kd_prodi = "'.$logged['userid'].'" 
						                    							AND NIMHSTRLNM = "'.$value->npm_mahasiswa.'" 
						                    							and a.THSMSTRLNM = "'.$lastyear.'" 
						                    							and a.`NLAKHTRLNM` <> "T"')->result();
		                                
										
										
		                                $st=0;
					                    $ht=0;
					                    foreach ($hitung_ips as $iso) {
					                        $h = 0;

					                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
					                        $ht=  $ht + $h;

					                        $st=$st+$iso->sks_matakuliah;
					                    }

					                    $ips_nr = $ht/$st;

					                    $jml_sks 	= $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jml_sks 
					                    								FROM tbl_krs_feeder krs
																		JOIN tbl_matakuliah mk 
																		ON krs.`kd_matakuliah` = mk.`kd_matakuliah`
																		WHERE krs.`kd_krs` = "'.$value->kd_krs.'" 
																		AND mk.`kd_prodi` = "'.$logged['userid'].'"')->row()->jml_sks;
										*/
	                                ?>

	                                <!-- get aktifitas kuliah mahasiswa -->
	                                <?php 
	                                	$get_act_student = $this->db->where('THSMSTRAKM', $lastyear)
	                                								->where('NIMHSTRAKM', $value->npm_mahasiswa)
	                                								->get('tbl_aktifitas_kuliah_mahasiswa')->row();

	                                 ?>

	                                <td <?php echo $warna; ?>>
	                                	<a target="_blank" href="<?= base_url('sync_feed/krs/viewview/'.$value->kd_krs); ?>">
	                                		<?php echo number_format($get_act_student->SKSEMTRAKM); ?>
	                                	</a>
	                                </td>
	                                
	                                <!-- get ipk total dan sks total dari feeder -->
	                                <?php
	                                // ini baris dibawah awalnya yg $lastyear = 20162, nah yg di else nya itu 20161
	                                /*
	                                $sks = $this->db->where('npm_mahasiswa', $value->npm_mahasiswa)
	                                				->where('tahunajaran', $lastyear)
	                                				->get('tbl_khs')->row();

	                                if (count($sks) > 0) {
	                                	$ips 		= number_format($sks->ips,2); 
	                                	$ipk 		= number_format($sks->ipk,2);
								        $sks_total 	= $sks->total_sks;
	                                } else {

	                                	// menghitung tahun lalu, lalunya lagi
	                                	if ($lastyear%2 == 0) {
	                                		$lalu = $lastyear - 1;
	                                	} else {
	                                		$prefix = substr($lastyear, 0, 4);
	                                		$lalu 	= ($prefix-1).'2';
	                                	}
	                                	
	                                	$sks = $this->db->where('npm_mahasiswa', $value->npm_mahasiswa)
	                                					->where('tahunajaran',$lastyear)
	                                					->get('tbl_khs')->row();

	                                	$ips 		= number_format($ips_nr, 2);
								        $skss 		= $jml_sks;
								        $awal 		= number_format(($sks->ipk*$sks->total_sks),2);
								        $nambah 	= number_format(($ips*$skss),2);
								        $skstot 	= $sks->total_sks+$skss;
								        $ipk 		= number_format(($awal+$nambah)/($skss+$sks->total_sks),2);
								        $sks_total 	= $skstot;
								        
	                                } */ ?>
	                                
									<td <?php echo $warna; ?>><?php  echo number_format($get_act_student->NLIPSTRAKM, 2); ?></td>
									<td <?php echo $warna; ?>>
										<input style="width:70px;" 
												type="text" 
												name="sks[<?php echo $no;?>]" 
												value="<?php echo (int)$get_act_student->SKSTTTRAKM; ?>" 
												required/>
									</td>
									<td <?php echo $warna; ?>>
										<input style="width:70px;" 
												type="text" 
												name="ipk[<?php echo $no;?>]" 
												value="<?php echo number_format($get_act_student->NLIPKTRAKM, 2); ?>" 
												required/>
									</td>

									<input type="hidden" 
											name="ips[<?php echo $no;?>]" 
											value="<?php echo number_format($get_act_student->NLIPSTRAKM, 2); ?>"/>
	                                <input type="hidden" 
	                                		name="mhs[<?php echo $no;?>]" 
	                                		value="<?php echo $value->npm_mahasiswa; ?>"/>
	                                <input type="hidden" 
	                                		name="smt[<?php echo $no;?>]" 
	                                		value="<?php echo (int)$get_act_student->SKSEMTRAKM; ?>"/>
									</tr>
		                        <?php $no++;} ?>
		                    </tbody>
		               	</table>
	               	</form>
				</div>
			</div>
		</div>
	</div>
</div>