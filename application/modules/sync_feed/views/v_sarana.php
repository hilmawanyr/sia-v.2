<div class="container">
	
	    <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>PENDATAAN NAMA PIMPINAN DAN DATA TEKNISI/LABORAN (TRPIM)</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<form id="edit-profile" class="form-horizontal">
							<fieldset>

								<div class="control-group">											

									<label class="control-label">Kode P.T. </label>

									<div class="controls">

										<input type="text" class="span6" name="kd_pt" value="<?php echo $kd_pt->kd_pt.'  -  '.$kd_pt->nama_pt; ?>"  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<hr>

								<h4 style="text-align:center">DATA NAMA PIMPINAN YAYASAN SAAT INI</h4><br>
								<div class="control-group">											

									<label class="control-label">Ketua </label>

									<div class="controls">

										<input type="text" placeholder="Nama Pimpinan" class="span6" name="ketua_yys" value=""  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Sekretaris </label>

									<div class="controls">

										<input type="text" placeholder="Nama Pimpinan" class="span6" name="sekre_yys" value=""  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Bendahara </label>

									<div class="controls">

										<input type="text" placeholder="Nama Pimpinan" class="span6" name="benda_yys" value=""  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Nomor S.K </label>

									<div class="controls">

										<input type="text" class="span4" placeholder="Nomor Surat Keputusan" name="no_sk_yayasan"  value=""  required> 
										<input type="text" class="span3" placeholder="Surat Keputusan Tanggal" name="tgl_sk_yayasan" id="tgl_sk_yayasan" value=""  required>
										<input type="text" class="span3" placeholder="Berlaku s.d." name="berlaku_sk_yayasan" id="berlaku_sk_yayasan" value=""  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->


								<hr>

								<h4 style="text-align:center">DATA NAMA PIMPINAN UNIVERSITAS SAAT INI</h4><br>

								<div class="control-group">											

											<label class="control-label" for="rektor">Rektor</label>

											<div class="controls">

												<input type="text" class="span3 " name="nidn_rektor" placeholder="NIDN Pimpinan"  value="" required>
												<input type="text" class="span6 " name="nama_rektor" placeholder="Nama Pimpinan"  value="" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->												

										

										<div class="control-group">											

											<label class="control-label">Pembantu Rektor I</label>

											<div class="controls">

												<input type="text" class="span3" name="nidn_warek1" placeholder="NIDN Pimpinan" value=""  required>
												<input type="text" class="span6" name="nama_warek1" placeholder="Nama Pimpinan" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Pembantu Rektor II</label>

											<div class="controls">

												<input type="text" class="span3" name="nidn_warek2" placeholder="NIDN Pimpinan" value=""  required>
												<input type="text" class="span6" name="nama_warek2" placeholder="Nama Pimpinan" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->										

										<div class="control-group">											

											<label class="control-label">Pembantu Rektor III</label>

											<div class="controls">

												<input type="text" class="span3" name="nidn_warek3" placeholder="NIDN Pimpinan" value=""  required>
												<input type="text" class="span6" name="nama_warek3" placeholder="Nama Pimpinan" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Pembantu Rektor IV</label>

											<div class="controls">

												<input type="text" class="span3" name="nidn_warek4" placeholder="NIDN Pimpinan" value=""  required>
												<input type="text" class="span6" name="nama_warek4" placeholder="Nama Pimpinan" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Pembantu Rektor V</label>

											<div class="controls">

												<input type="text" class="span3" name="nidn_warek5" placeholder="NIDN Pimpinan" value="">
												<input type="text" class="span6" name="nama_warek5" placeholder="Nama Pimpinan" value="">

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

									<label class="control-label">Nomor S.K </label>

									<div class="controls">

										<input type="text" class="span4" placeholder="Nomor Surat Keputusan" name="no_sk_univ" value=""  required> 
										<input type="text" class="span3" placeholder="Surat Keputusan Tanggal" name="tgl_sk_univ" id="tgl_sk_univ" value=""  required>
										<input type="text" class="span3" placeholder="Berlaku s.d." name="berlaku_sk_univ" id="berlaku_sk_univ" value=""  required>

										<p class="help-block">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->
								
									
								 <br />
								
									
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button> 
									<button class="btn">Cancel</button>
								</div> <!-- /form-actions -->
							</fieldset>
						</form>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	    </div> <!-- /row -->
	
</div> <!-- /container -->