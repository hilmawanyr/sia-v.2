<!--link href="<?php echo base_url();?>assets/css/plugins/bootstrap.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css"-->

<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script>

<!--script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script-->

<!--link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"-->




<script type="text/javascript">

$(document).ready(function() {

	
$('#alamat_ortu').hide();


$('#tanggal_lahir').datepicker({
 								dateFormat: "yy-mm-dd",

 								yearRange: "1945:2015",

								changeMonth: true,

      							changeYear: true

       							});

$('#tgl_bayar').datepicker({dateFormat: 'yy-mm-dd'});

});

</script>



<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Form Biodata</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="tabbable">

							<ul class="nav nav-tabs">

							  <li class="active">

							    <a href="#formcontrols" data-toggle="tab">Data Pejabat</a>

							  </li>

							  <li>

							  	<a href="#jscontrols" data-toggle="tab">Inventaris Ubhara</a>

							  </li>

							</ul>

							<br>

							<form id="edit-profile" class="form-horizontal" method="POST" action="<?php echo base_url();?>data/biodata/save_data">

								<div class="tab-content ">

									<div class="tab-pane active" id="formcontrols">

										

									<fieldset>
										
										<div class="control-group">											

											<label class="control-label" for="rektor">Rektor</label>

											<div class="controls">

												<input type="text" class="span6 " name="rektor"  value="" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->												

										

										<div class="control-group">											

											<label class="control-label">Wakil Rektor I</label>

											<div class="controls">

												<input type="text" class="span6" name="warek1" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Wakil Rektor II</label>

											<div class="controls">

												<input type="text" class="span6" name="warek2" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->										

										<div class="control-group">											

											<label class="control-label">Wakil Rektor III</label>

											<div class="controls">

												<input type="text" class="span6" name="warek3" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Wakil Rektor IV</label>

											<div class="controls">

												<input type="text" class="span6" name="warek4" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->


										<hr>

										<div class="control-group">											

											<label class="control-label">Dekan</label>

											<div class="controls">

												<input type="text" class="span6" name="dekan" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Wakil Dekan I</label>

											<div class="controls">

												<input type="text" class="span6" name="wadek1" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Wakil Dekan II</label>

											<div class="controls">

												<input type="text" class="span6" name="wadek2" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Wakil Dekan III</label>

											<div class="controls">

												<input type="text" class="span6" name="wadek3" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Ketua Program Studi</label>

											<div class="controls">

												<input type="text" class="span6" name="prodi" value=""  required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										</fieldset>

									</div>

									<div class="tab-pane" id="jscontrols">
										<fieldset>

										<input type="hidden" name="bio_type" value="">

										
										<div class="control-group">											

											<label class="control-label" for="rektor">Luas Tanah</label>

											<div class="controls">

												<input type="text" class="span6 " name="tanah" value="" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->												

										

										<div class="control-group">											

											<label class="control-label">Lahan Perumahan</label>

											<div class="controls">

												<input type="text" class="span6" name="lahan_rumah" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Kebun/Hutan</label>

											<div class="controls">

												<input type="text" class="span6" name="kebun" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Asrama Mahasiswa</label>

											<div class="controls">

												<input type="text" class="span6" name="asrama" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Aula/Auditorium</label>

											<div class="controls">

												<input type="text" class="span6" name="aula" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Seminar/Rapat</label>

											<div class="controls">

												<input type="text" class="span6" name="rapat" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Ruang Kuliah</label>

											<div class="controls">

												<input type="text" class="span6" name="r_kuliah" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Ruang Lab./ Studio</label>

											<div class="controls">

												<input type="text" class="span6" name="lab" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Ruang Komputer</label>

											<div class="controls">

												<input type="text" class="span6" name="r_komp" value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Kegiatann Eks.Mhs.</label>

											<div class="controls">

												<input type="text" class="span6" name="k_eks_mhs"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Perpustakaan</label>

											<div class="controls">

												<input type="text" class="perpus" name="k_eks_mhs"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Administrasi</label>

											<div class="controls">

												<input type="text" class="span6" name="adminis"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Ruang Dosen Tetap</label>

											<div class="controls">

												<input type="text" class="span6" name="r_dostap"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Jumlah Judul Buku</label>

											<div class="controls">

												<input type="text" class="span6" name="Jml_buku"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label">Jumlah Eksemplar</label>

											<div class="controls">

												<input type="text" class="span6" name="jml_eksemplar"  value="" required>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->


										<div class="form-actions">

											<!-- <button type="submit" class="btn btn-primary">UNGGAH</button>

											<button type="submit" class="btn btn-primary">CETAK</button>  -->

											<button type="submit" class="btn btn-primary">SIMPAN</button>

										</div> <!-- /form-actions -->

										</fieldset>
									</div>

									</div>



								</div>

								</form>

						</div>

				

			</div>

		</div>

	</div>

</div>


