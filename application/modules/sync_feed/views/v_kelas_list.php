<?php //var_dump($matakuliah);die(); ?>

<style type="text/css">
	#circle { 
		width: 20px; 
		height: 20px; 
		background: #FFB200; 
		-moz-border-radius: 50px; 
		-webkit-border-radius: 50px; 
		border-radius: 50px; 
	}
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">

<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">


<script type="text/javascript">

	function member(id)
	{
		$('#members').load('<?= base_url('sync_feed/kelas/jmlPeserta/') ?>'+id);
	}

	function edit(idk){
		$('#form_edit').load('<?php echo base_url();?>sync_feed/kelas/edit_jadwal/'+idk);
	}

	function dosen(idk){
		$('#edit1').load('<?php echo base_url();?>sync_feed/kelas/load_dosen/'+idk);
	}

</script>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		$('input[name^=dosen]').autocomplete({

	        source: '<?php echo base_url('perkuliahan/jdl_kuliah/getdosen');?>',

	        minLength: 1,

	        select: function (evt, ui) {

	            this.form.nik.value = ui.item.nik;

	            this.form.dosen.value = ui.item.value;

	        }

	    });

	});

</script>



<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="form_edit">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Jadwal Perkuliahan Feeder <?php echo $this->session->userdata('tahunajaran') ?></h3>

			</div> <!-- /widget-header -->

			<div class="widget-content" style="padding:30px;">
            	<div class="tabbable">
		            <ul class="nav nav-tabs">
		            	<?php
		            	$no=1; foreach ($tabs as $isi) {
		            		
		            		if ($no==1) { 
		            			echo '<li class="active"><a href="#tab'.$isi->semester_kd_matakuliah.'" data-toggle="tab">Semester '.$isi->semester_kd_matakuliah.'</a></li>';
		            		}else {
		            			echo '<li><a href="#tab'.$isi->semester_kd_matakuliah.'" data-toggle="tab">Semester '.$isi->semester_kd_matakuliah.'</a></li>';
		            		}
		            	$no++; } ?>
		            </ul>

            		<br>
      
              		<div class="tab-content">

		            <?php $no=1; foreach ($tabs as $isi) {
		            		
		            	if ($no==1) { 

		            		echo '<div class="tab-pane active" id="tab'.$isi->semester_kd_matakuliah.'">'; ?>
	            				<div class="span11">

									<?php 
									if ($this->session->userdata('tahunajaran') == getactyear()) {
										$mk_by_smt = $this->app_model->jdl_matkul_by_semester_feed_new($isi->semester_kd_matakuliah);
									} else {
										$mk_by_smt = $this->app_model->jdl_matkul_by_semester_feed($isi->semester_kd_matakuliah);
									} ?>

				                    <a data-toggle="modal" class="btn btn-success" href="#myModal">
				                    	<i class="btn-icon-only icon-plus"> </i> Tambah Data
				                    </a>

				                    <a href="<?= base_url(); ?>sync_feed/kelas/cetak_jadwal_all" class="btn btn-info">
				                    	Print Jadwal
				                    </a>

				                    <?php if ($this->session->userdata('tahunajaran') == getactyear()) { ?>

				                    	<a href="<?= base_url('sync_feed/kelas/sync_kelas_add') ?>" class="btn btn-success" target="blank"> Sinkronisasi Kelas</a>

				                    <?php } elseif ($this->session->userdata('tahunajaran') == yearBefore()) { ?>

				                    	<a href="<?= base_url('sync_feed/kelas/sync_ajar_dosen') ?>" class="btn btn-success" target="blank"> Sinkronisasi Ajar Dosen</a>

				                    <?php } ?>

				                    <br><hr>

									<table id="<?= 'example'.$isi->semester_kd_matakuliah; ?>" class="table table-striped">

					                	<thead>

					                        <tr> 

					                        	<th>No</th>
												
												<th>Kelas</th>

					                            <th>Kode MK</th>

					                            <th>Matakuliah</th>

												<th>SKS</th>

												<th>Peserta</th>

												<th>Dosen</th>

												<th>NID</th>

												<th>NIDN</th>

												<th>NUPN</th>

					                            <th width="140">Aksi</th>

					                        </tr>

					                    </thead>

					                    <tbody>
					                    	<?php $no=1; foreach ($mk_by_smt as $isi)  { 

				                    			// $this->db->select('count(distinct kd_krs) as jml');
				                    			// $this->db->from('tbl_krs_feeder');
				                    			// $this->db->where('kd_jadwal', $isi->kd_jadwal);
				                    			// $jml_peserta = $this->db->get()->row()->jml;

				                    			if (($isi->nidn == '' || is_null($isi->nidn)) and ($isi->nupn == '' || is_null($isi->nupn))) {
				                    				$warna ='style="background:#FF6347;"';
				                    			}elseif ($isi->status_feeder == 1) {
				                    				$warna = 'style="background:#90EE90;"';
				                    			}else{
				                    				$warna ='';
				                    			}
				                    		?>
					                    		
					                    	<tr >

				                           		<td <?php echo $warna; ?> ><?php echo $no; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->kelas; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->kd_matakuliah; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->nama_matakuliah; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->sks_matakuliah; ?></td>							         
					                        	<td <?php echo $warna; ?>>
					                        		<a href="#pesertaModal" data-toggle="modal" class="btn btn-success" onclick="member(<?= $isi->id_jadwal; ?>)"><i class="icon icon-eye-open"></i></a>
					                        	</td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->nama; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->nid; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->nidn; ?></td>

					                        	<td <?php echo $warna; ?>><?php echo $isi->nupn; ?></td>

					                        	<td <?php echo $warna; ?>>
					                        		<a data-toggle="modal" onclick="dosen(<?= $isi->id_jadwal;?>)" href="#editModal1"class="btn btn-success btn-small" ><i class="btn-icon-only icon-user"> </i></a>

				                                    <a class="btn btn-primary btn-small" onclick="edit(<?= $isi->id_jadwal;?>)" data-toggle="modal" href="#edit_modal" ><i class="btn-icon-only icon-pencil"></i></a>

				                                    <a target="_blank" class="btn btn-warning btn-small" href="<?= base_url(); ?>sync_feed/kelas/detail_mhs/<?= $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-ok"></i></a>

				                                <?php if ($this->session->userdata('tahunajaran') == yearBefore()) { ?>

			                                    	<a target="_blank" class="btn btn-primary btn-small" href="<?php echo base_url();?>sync_feed/kelas/sync_kelas_nilai/<?php echo $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-refresh"></i></a>

			                                    <?php } elseif ($this->session->userdata('tahunajaran') == getactyear()) { ?>

			                                    	<a target="_blank" class="btn btn-danger btn-small" href="<?php echo base_url();?>sync_feed/kelas/sync_kelas_krs/<?php echo $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-refresh"></i></a>

			                                    <?php } ?>

												</td >
											</tr>
										<?php $no++; } ?>

					                    </tbody>

					               	</table>

								</div>

            				<?= '</div>';

            			} else {  
            				
            				echo '<div class="tab-pane " id="tab'.$isi->semester_kd_matakuliah.'">'; ?>
							<div class="span11">

								<?php 
								if ($this->session->userdata('tahunajaran') == getactyear()) {
									$mk_by_smt = $this->app_model->jdl_matkul_by_semester_feed_new($isi->semester_kd_matakuliah);
								} else {
									$mk_by_smt = $this->app_model->jdl_matkul_by_semester_feed($isi->semester_kd_matakuliah);
								}
								 ?>

			                    <a data-toggle="modal" class="btn btn-success" href="#myModal">
			                    	<i class="btn-icon-only icon-plus"> </i> Tambah Data
			                    </a>

			                    <a href="<?php echo base_url(); ?>sync_feed/kelas/cetak_jadwal_all" class="btn btn-info"> 
			                    	Print Jadwal
			                    </a>

			                    <?php 
			                    if ($this->session->userdata('tahunajaran') == getactyear()) { ?>
				                    
				                    <a href="<?= base_url('sync_feed/kelas/sync_kelas_add') ?>" class="btn btn-success" target="blank"> 
				                    	Sinkronisasi Kelas
				                    </a>

				                <?php } elseif ($this->session->userdata('tahunajaran') == yearBefore()) { ?>
				                    
				                    <a href="<?= base_url('sync_feed/kelas/sync_ajar_dosen') ?>" class="btn btn-success" target="blank"> 
				                    	Sinkronisasi Ajar Dosen
				                	</a>
				                
				                <?php } ?>

			                    <hr>

								<table id="<?= 'example'.$isi->semester_kd_matakuliah; ?>" class="table table-striped">

				                	<thead>

				                        <tr> 

				                        	<th>No</th>

											<th>Kelas</th>

				                            <th>Kode MK</th>

				                            <th>Matakuliah</th>

											<th>SKS</th>

											<th>Peserta</th>

											<th>Dosen</th>

											<th>NID</th>

											<th>NIDN</th>

											<th>NUPN</th>

				                            <th width="140">Aksi</th>

				                        </tr>

				                    </thead>

				                    <tbody>
				                    	<?php $no=1; foreach ($mk_by_smt as $isi)  { 

				                    		// $this->db->select('count(distinct kd_krs) as jml');
			                    			// $this->db->from('tbl_krs_feeder');
			                    			// $this->db->where('kd_jadwal', $isi->kd_jadwal);
			                    			// $jml_peserta = $this->db->get()->row()->jml;
				                    			
			                    			if (($isi->nidn == '' || is_null($isi->nidn)) && ($isi->nupn == '' || is_null($isi->nupn))) {
			                    				$warna ='style="background:#FF6347;"';
			                    			} elseif ($isi->status_feeder == 1) {
			                    				$warna = 'style="background:#90EE90;"';
			                    			} else {
			                    				$warna ='';
			                    			}
				                    	?>

				                    	<tr>

			                           		<td <?php echo $warna; ?> ><?php echo $no; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->kelas; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->kd_matakuliah; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nama_matakuliah; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->sks_matakuliah; ?></td>

											<td <?php echo $warna; ?>>
				                        		<a href="#pesertaModal" data-toggle="modal" class="btn btn-success" onclick="member(<?= $isi->id_jadwal; ?>)"><i class="icon icon-eye-open"></i></a>
				                        	</td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nama; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nid; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nidn; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nupn; ?></td>

				                        	<td <?php echo $warna; ?>>
				                        		<a data-toggle="modal" onclick="dosen(<?php echo $isi->id_jadwal;?>)" href="#editModal1"class="btn btn-success btn-small" ><i class="btn-icon-only icon-user"> </i></a>

			                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id_jadwal;?>)" data-toggle="modal" href="#edit_modal" ><i class="btn-icon-only icon-pencil"></i></a>

			                                    <a target="_blank" class="btn btn-warning btn-small" href="<?php echo base_url(); ?>sync_feed/kelas/detail_mhs/<?php echo $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-ok"></i></a>

			                                    <?php if ($this->session->userdata('tahunajaran') == yearBefore()) { ?>

			                                    	<a target="_blank" class="btn btn-primary btn-small" href="<?php echo base_url();?>sync_feed/kelas/sync_kelas_nilai/<?php echo $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-refresh"></i></a>

			                                    <?php } elseif ($this->session->userdata('tahunajaran') == getactyear()) { ?>

			                                    	<a target="_blank" class="btn btn-danger btn-small" href="<?php echo base_url();?>sync_feed/kelas/sync_kelas_krs/<?php echo $isi->id_jadwal; ?>" ><i class="btn-icon-only icon-refresh"></i></a>

			                                    <?php } ?>
											</td>
										
										</tr>

										<?php $no++; } ?>

				                    </tbody>

				               	</table>

							</div>	
						<?php echo '</div>'; } $no++; } ?>
              		</div>
				</div>
          	</div> <!-- /widget-content -->
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">FORM TAMBAH DATA</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>sync_feed/kelas/save_data" method="post">

                <div class="modal-body">

	                <script type="text/javascript">

	                	$(document).ready(function(){

							$('#semester').change(function(){

								$.post('<?php echo base_url();?>sync_feed/kelas/get_detail_matakuliah_by_semester_feeder/'+$(this).val(),{},function(get){

									$('#ala_ala').html(get);

								});

							});

						});

	                </script>
                 
                </div>

                <input type='hidden' class="form-control" name="jurusan" value="<?php echo $this->session->userdata('id_jurusan_prasyarat'); ?>" readonly>

                <div class="control-group">

                  <label class="control-label">Semester</label>

                  <div class="controls">

                    <select id="semester" class="form-control" name="semester" required="">

                      <option>--Pilih Semester--</option>

                      <option value="1">1</option>

                      <option value="2">2</option>

                      <option value="3">3</option>

                      <option value="4">4</option>

                      <option value="5">5</option>

                      <option value="6">6</option>

                      <option value="7">7</option>

                      <option value="8">8</option>

                    </select>

                  </div>

                </div>

                <div class="control-group">
					<label class="control-label">Kelompok Kelas</label>
					<div class="controls">
						<input type="radio" name="st_kelas" value="PG" required=""> PAGI &nbsp;&nbsp; 
						<input type="radio" name="st_kelas" value="SR" required=""> SORE &nbsp;&nbsp;
						<input type="radio" name="st_kelas" value="PK" required=""> P2K  &nbsp;&nbsp;
					</div>
				</div>

            	<div class="control-group">											

					<label class="control-label" for="kelas">Nama Kelas</label>

					<div class="controls">

						<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" required>

					</div> <!-- /controls -->				

				</div> <!-- /control-group -->

				<script type="text/javascript">

					$(document).ready(function() {

						$('#ala_ala').change(function() {

							$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_kode_mk/'+$(this).val(),{},function(get){

								$('#kode_mk').val(get);

							});

						});

					});

				</script>

				<div class="control-group">											

					<label class="control-label" for="firstname">Nama Matakuliah</label>

					<div class="controls">

						<select class="form-control" name="nama_matakuliah" id="ala_ala">

							<option>--Pilih MataKuliah--</option>

						</select>

					</div> <!-- /controls -->				

				</div> <!-- /control-group -->    

				<div class="control-group">										

					<label class="control-label" for="kode_mk">Kode MK</label>

					<div class="controls">

						<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk">

					</div> <!-- /controls -->				

				</div> <!-- /control-group -->

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit1">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="pesertaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="members">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->