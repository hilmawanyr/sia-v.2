<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<!-- <a href="<?php //echo base_url();?>akademik/khs/printkhsall"></a> -->
						<!-- <a  target="_blank" href=""><i class="btn-icon-only icon-sync"> </i></a> --><hr>
						<table id="example3" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
		                        	<th>NIM</th>
		                        	<th>Nama MHS</th>
		                        	<th>Tahun Masuk</th>
		                        	<th>SKS <?php echo $chooseyear; ?></th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php $no=1; foreach ($getData as $value) { ?>
		                    	<?php 
	                    			$jumlah = 0;
	                    			if ($jumlah == 0) {
	                    				//$warna = 'style="background:#90EE90;"';
	                    				$warna ='';
	                    			}else{
	                    				$warna ='';
	                    			}
		                    	?>
		                        <tr>
		                        	<td <?php echo $warna; ?>><?php echo number_format($no); ?></td>
		                        	<td <?php echo $warna; ?>><?php echo $value->npm_mahasiswa; ?></td>
		                        	<td <?php echo $warna; ?>><?php echo get_nm_mhs($value->npm_mahasiswa); ?></td>
	                                <td <?php echo $warna; ?>><?php echo substr($value->npm_mahasiswa, 0,4); ?></td>
	                                <?php
	                                $logged = $this->session->userdata('sess_login');
		                        	$q = $this->db->query("SELECT DISTINCT krs.`kd_matakuliah` FROM tbl_krs_feeder krs
															WHERE krs.`kd_krs` = '".$value->kd_krs."'")->result();
		                        	$sumsks = 0;
		                        	foreach ($q as $isi_krs) {
		                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$isi_krs->kd_matakuliah."' AND kd_prodi = ". $logged['userid']."")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
		                        	?>
	                                <td <?php echo $warna; ?>>
	                                	<a target="_blank" href="<?= base_url(); ?>sync_feed/krs/viewview/<?= $value->kd_krs; ?>">
	                                		<?php echo $sumsks; ?>
	                                	</a>
	                                </td>
		                        </tr>
		                        <?php $no++;} ?>
		                    </tbody>
		               	</table>
	           
				</div>
			</div>
		</div>
	</div>
</div>