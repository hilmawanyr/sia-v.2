<style type="text/css">
	.image {
		width: 10%;
		margin-bottom: 15px;
		border-radius: 20%;
		border : solid 2px;
	}
</style>
<div class="container">
	
	    <div class="row">
	      	
	      	<div class="span12">     

					<fieldset>

						<center>
							<?php if (count($que->nid) > 0) { ?>
								<img src="<?php echo base_url(); ?><?php echo $que->foto; ?>" class="image" alt="">
							<?php } else { ?>
								<i class="icon icon-user icon-5x"></i>
							<?php } ?>
						</center>
							<div class="row">
								<div class="span3">
									<div class="widget"></div>
								</div>
								<div class="span6">
									<div class="widget">
										<div class="widget-header">
										<i class="icon icon-list"></i>
											<h3>Profil <?php echo $quo->nama; ?></h3>
												
										</div>
										<div class="widget-content">
											<table class="table table-striped">
												<tbody>
													<tr>
														<td>NAMA</td>
														<td><?php echo $quo->nama; ?></td>
													</tr>
													<tr>
														<td>NID</td>
														<td><?php echo $quo->nid; ?></td>
													</tr>
													<tr>
														<td>NIDN</td>
														<td><?php echo $quo->nidn; ?></td>
													</tr>
													<tr>
														<td>ALAMAT</td>
														<td><?php echo $que->alamat; ?></td>
													</tr>
													<tr>
														<td>TELEPON</td>
														<td><?php echo $que->tlp; ?></td>
													</tr>
													<tr>
														<td>E-MAIL</td>
														<td><?php echo $que->email; ?></td>
													</tr>
													<tr>
														<td>TEMPAT LAHIR</td>
														<td><?php echo $que->tpt_lahir; ?></td>
													</tr>
													<tr>
														<td>TANGGAL LAHIR</td>
														<td><?php echo  dateIdn($que->tgl_lahir); ?></td>
													</tr>
													<tr>
														<td>Jabatan Fungsional</td>
														<td>
															<?php echo $que->thnj; ?> - <?php if($que->jabfung=='TPD'){ echo 'Tenaga Pendidik'; }?>
															<?php if($que->jabfung=='SAH'){ echo 'Asisten Ahli'; }?>
															<?php if($que->jabfung=='LKT'){ echo 'Lektor'; }?>
															<?php if($que->jabfung=='LKK'){ echo 'Lektor Kepala'; }?>
															<?php if($que->jabfung=='BIG'){ echo 'Guru Besar'; }?>
														</td>
													</tr>
													<tr>
														<td>RIWAYAT PENDIDIKAN</td>
														<td>
															<ul>
																<?php foreach ($pdd as $pd) { ?>
																	<li><?php echo $pd->tahun_masuk; ?> - <?php echo $pd->tahun_keluar; ?> , <?php echo $pd->jenjang; ?> <?php echo $pd->instansi; ?> (<?php echo $pd->jurusan; ?>)</li>
																	<hr>
																<?php } ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td>RIWAYAT PEKERJAAN</td>
														<td>
															<ul>
															<?php 
																$arra = explode(',', $que->pekerjaan);
																for ($i=0; $i < count($arra)-1; $i++) { ?>
																 	<li><?php echo $arra[$i]; ?></li>
																<?php } ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td>RIWAYAT PENELITIAN</td>
														<td>
															<ul>
																<?php foreach ($rsc as $val) { ?>
																	<li><?php echo $val->tahun; ?> - <?php echo $val->judul; ?> (Penelitian Tingkat <?php echo tridarma('rsc',$val->jenis); ?>, Sebagai <?php echo tridarma('rsc',$val->posisi); ?>, Sumber Dana <?php echo tridarma('rsc',$val->biaya); ?>)</li>
																	<hr>
																<?php } ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td>RIWAYAT PENGABDIAN</td>
														<td>
															<ul>
																<?php foreach ($dev as $ops) { ?>
																	<li><?php echo $ops->tahun; ?> - <?php echo $ops->judul; ?> (Penelitian Tingkat <?php echo tridarma('rsc',$ops->jenis); ?>, Sebagai <?php echo tridarma('rsc',$ops->posisi); ?>, Sumber Dana <?php echo tridarma('rsc',$ops->dana); ?>)</li>
																	<hr>
																<?php } ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td>RIWAYAT PUBLIKASI</td>
														<td>
															<ul>
																<?php foreach ($pub as $ups) { ?>
																	<li><?php echo $ups->tahun; ?> - <?php echo $ups->judul; ?> (Penelitian Tingkat <?php echo $ups->tingkat; ?>, yang dipublikasikan pada jurnal <?php echo tridarma('pub',$ups->jurnal); ?>)</li>
																	<hr>
																<?php } ?>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
												<center><a class="btn btn-success" href="<?php echo base_url('sync_feed/biodata/pdf/') ?>" target="_blank" data-toggle="tooltip" title="Cetak"><i class="icon icon-print"></i> Cetak PDF</a></center>
										</div>
									</div>	
								</div>
								<div class="span3">
									<div class="widget"></div>
								</div>
							</div>
					</fieldset>
						
				</div>
			</div>
		</div>
								