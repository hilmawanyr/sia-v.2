<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<b>KRS TAHUN AKADEMIK <?php echo $tahunakademik; ?></b>
					<hr>
					<div class="alert alert-danger">
						<b>PERHATIAN!</b><p> Warna <b>merah </b>pada tabel menandakan mata kuliah yang diulang oleh mahasiswa.</p>
					</div>
					<table>
						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Catatan</td>
							<td> : <?php echo $pembimbing['keterangan_krs']; ?></td>
							<td></td>
						</tr>
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Kelas</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>

                            <!-- cek jika matakuliah terdata sebagai mk ngulang -->
                            <?php 
                            	$load = $this->feeder_model->isthisngulang($row->kd_matakuliah,$npm,getCodeProdiMhs($npm),$tahunakademik);
                            	// var_dump($load->result());exit();
                            	if ($load->num_rows() > 0) {
                            		$color = 'style="background:#FA8072"';
                            	} else {
                            		$color = '';
                            	}
                            	
                            ?>

	                        <tr>
	                        	<td <?= $color; ?> ><?php echo $no; ?></td>
	                        	<td <?= $color; ?> ><?php echo $row->kd_matakuliah; ?></td>
                                <td <?= $color; ?> ><?php echo $row->nama_matakuliah; ?></td>
                                <td <?= $color; ?> ><?php echo $row->sks_matakuliah; ?></td>
                                <td <?= $color; ?> ><?php echo $row->nama; ?></td>
                                <td <?= $color; ?> ><?php echo $row->kelas; ?></td>
                            </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>