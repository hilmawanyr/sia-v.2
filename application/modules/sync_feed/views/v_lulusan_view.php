<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?php echo base_url();?>form/penugasandosen/load_detail/'+idk);
    }

function status(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status/'+idk);
    }

function status2(idk){
        $('#edit1').load('<?php echo base_url();?>form/penugasandosen/load_status2/'+idk);
    }

</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Lulusan</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
            	<center>
            		<h4>
            			Data Lulusan <?php echo get_jur($this->session->userdata('jurusan')).' - '.$this->session->userdata('tahun'); ?>
            		</h4>
            	</center>
                <a href="<?php echo base_url();?>sync_feed/lulusan/form" class="btn btn-primary" >
                	<i class="btn-icon-only icon-plus"> </i> Tambah Data
                </a>
                <a href="<?php echo base_url();?>sync_feed/lulusan/sync_mhs" class="btn btn-warning" target="_blank">
                	<i class="btn-icon-only icon-refresh"> </i> Sinkron Feeder
                </a>
                <hr>
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Nomor Ijazah</th>
								<th>Judul Skripsi</th>
								<th>IPK</th>
	                            <th class="td-actions">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($rows as $row){?>
	                        <tr>
	                        	<td><?php echo $row->npm_mahasiswa ?></td>
	                            <td><?php echo $row->NMMHSMSMHS ?></td>
	                            <td><?php echo $row->no_ijazah ?></td>
								<td><?php echo $row->jdl_skripsi ?></td>
								<td><?php echo $row->ipk ?></td>
	                        	<td class="td-actions">
									<?php if ($row->flag_feeder == 2){ ?>
										<button type="button" class="btn btn-warning btn-small edit">
											<i class="btn-icon-only icon-check"></i>
										</button>	
									<?php }else{ ?>
										<a href="<?= base_url('sync_feed/lulusan/edit/'.$row->id_lulusan);?>">
											<button type="button" class="btn btn-primary btn-small edit">
												<i class="btn-icon-only icon-pencil"></i>
											</button>
										</a>
									<?php } ?>
									
									<a 
										onclick="return confirm('Apakah Anda Yakin?');" 
										class="btn btn-danger btn-small" 
										href="<?= base_url('sync_feed/lulusan/delete_data/'.$row->id_lulusan);?>">
										<i class="btn-icon-only icon-remove"> </i>
									</a>
								</td>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>