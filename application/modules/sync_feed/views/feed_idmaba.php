<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Biodata Mahasiswa Baru</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <!-- <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="btn-icon-only icon-plus"></i>Tambah Data</a>
                    <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-fire"></i>Rekap Data</a>
                    <hr> -->
                    <form class ='form-horizontal' action="<?php echo base_url(); ?>sync_feed/maba/inputdata" method="post" enctype="multipart/form-data">
                    
            
                        <div class="">   
                            <div class="control-group" id="">
                                <label class="control-label">NPM </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="npm" value="<?php echo $query->npm_baru; ?>" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Nama </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="nama" value="<?php echo $query->nama; ?>" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">NIK </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="nik" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">NIS </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="nis" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><b><i>Alamat :</i></b></label>
                                <div class="controls">
                                    <textarea type="text" id="" name="alamat" value="" class="form-control span8" required></textarea>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><i>Provinsi</i></label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="prov" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><i>Kecamatan</i></label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="kec" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><i>Kelurahan / Desa</i></label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="kel" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><i>RT</i></label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="rt" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label"><i>RW</i></label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="rw" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">No. HP </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="hp" value="<?php echo $query->tlp; ?>" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">E-mail </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="email" value="" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Nama Ibu </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="ibu" value="<?php echo $query->nm_ibu; ?>" class="form-control span8" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Nama Ayah </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="ayah" value="<?php echo $query->nm_ayah; ?>" class="form-control span8" required>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-success" type="submit" style="margin-left:60px">SUBMIT</button>  
                        </div>
                    </form>
                </div>           
            </div>
        </div>
    </div>
</div>