<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Upload</h4>
</div>
<form class ="form-horizontal" action="<?php echo base_url();?>sync_feed/biodata/edit_foto" method="post" enctype="multipart/form-data">
    <div class="modal-body">  
        <div class="form-group">
			<input type="file" name="files" class="form-control-file"><br>
			<input type="hidden" name="npm" value="<?php echo $npm?>">
		<small id="fileHelp" class="form-text text-muted">Gunakan File dengan format .jpeg, .gif atau .png</small><br>
		<small id="fileHelp" class="form-text text-muted">Gunakan File dengan ukuran maksimal 512Kb</small>
		</div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan" id="save"/>
    </div>
</form>