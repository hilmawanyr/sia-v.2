<script>
	/**
	 * Load edit modal
	 * @param int id_menu
	 */
	function loadEdit(id_menu) {
		$('#contentEdit').load('<?= base_url('setting/academic_year/edit/') ?>' + id_menu);
		$.get('<?= base_url('setting/academic_year/edit/') ?>' + id_menu, function(response) {

		})
	}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-gear"></i>
  				<h3>Kelola Mutu</h3>
			</div>
			<div class="widget-content">
				
				<fieldset>
			    	<button type="button" id="btnAdd" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">
			    		<i class="icon icon-plus"></i> Tambah Data
			    	</button>
			    	<hr>
			    	<table class="table table-stripped" id="example1">
			    		<thead>
			    			<tr>
			    				<th>No</th>
			    				<th>Kode</th>
			    				<th>Nama Tahun Ajaran</th>
			    				<th>Status</th>
			    				<th style="text-align: center">Aksi</th>
			    			</tr>
			    		</thead>
			    		<tbody id="tblBody">
			    			<?php $no = 1; foreach ($years as $year) { ?>
								<tr>
									<td><?= $no; ?></td>
									<td><?= $year->kode ?></td>
									<td><?= $year->tahun_akademik ?></td>
									<td><?= $year->status == 0 ? 'Tidak aktif' : 'Aktif'; ?></td>
									<td style="text-align: center">
										<button 
											data-toggle="modal" 
											data-target="#editModal" 
											class="btn btn-primary" 
											title="Edit data" 
											onclick="loadEdit(<?= $year->id ?>)">
											<i class="icon icon-pencil"></i>
										</button>
										<a 
											class="btn btn-danger" 
											onclick="return confirm('Data akan dihapus. Lanjutkan?')"
											href="<?= base_url('setting/academic_year/remove/'.$year->id) ?>" 
											title="Hapus data">
											<i class="icon icon-remove"></i>
										</a>
										<a 
											class="btn btn-success" 
											onclick="return confirm('Aktifkan tahun akademik?')"
											href="<?= base_url('setting/academic_year/activate_year/'.$year->id) ?>" 
											title="Hapus data">
											<i class="icon icon-refresh"></i>
										</a>
									</td>
								</tr>
							<?php $no++; } ?>
			    		</tbody>
			    	</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<!-- modal add start -->
<div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h4 class="modal-title">Tambah Data</h4>
	      	</div>
	      	<div class="modal-body">
	    		<form class="form-horizontal" action="<?= base_url('setting/academic_year/prepare_store') ?>" method="post">
			    	<div class="control-group">
						<label class="control-label">Kode</label>
						<div class="controls">
							<input type="text" id="kode" class="span3" name="kode" placeholder="Masukan kode tahun akademik" required="">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Nama Tahun Akademik</label>
						<div class="controls">
							<input type="text" class="span3" id="name" name="name" placeholder="Masukan nama tahun akademik" required="">
						</div>
					</div>
					
					<input type="hidden" name="isUpdate" id="isUpdate" value="">
		      	</div>
		      	<div class="modal-footer">
		      		<input class="btn btn-success" id="sbm" type="submit" value="">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      	</div>
	    	</div>
    	</form>
  	</div>
</div>
<!-- modal end -->