<script type="text/javascript">
	$(document).ready(function() {
     	$("#tgl-1, #tgl-2, #tgl-3" ).datepicker({
          	changeMonth: true,
          	changeYear: true,
          	dateFormat: "yy-mm-dd"
      	});

      	jQuery(function($){
		   	$("#quota-1").mask("9999",{placeholder:" "});
		   	$("#yr-1").mask("9999",{placeholder:" "});
		});
   });

	$(function () {
		$('#sbmEdit').click(function (e) {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>setting/setup_gel/editRow',
				data: $('#formEdit').serialize(),
				error: function (xhr, ajaxOption, thrownError) {
					return false;
				},
				beforeSend: function () {
					$('#onCenter').html("<center><img style='width:10%' src='<?php base_url('assets/img/loader.gif'); ?>'></center>");
				},
				success: function () {
					$('#editModal').modal('hide');
	                getTable();
				}
			});
			e.preventDefault();
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Tambah Data Gelombang</h4>
</div>
<div class="modal-body">
	<form class="form-horizontal" id="formEdit" action="" method="post">
    	<div class="control-group">
			<label class="control-label">Gelombang</label>
			<div class="controls">
				<select class="form-control span3" name="gelombang" required>
					<option disabled="">--Pilih Gelombang--</option>
					<option <?php if ($rows->gelombang == 1) { echo "selected=''"; } ?> value="1">1</option>
					<option <?php if ($rows->gelombang == 2) { echo "selected=''"; } ?> value="2">2</option>
					<option <?php if ($rows->gelombang == 3) { echo "selected=''"; } ?> value="3">3</option>
					<option <?php if ($rows->gelombang == 4) { echo "selected=''"; } ?> value="4">4</option>
					<option <?php if ($rows->gelombang == 5) { echo "selected=''"; } ?> value="5">Ekstra</option>
				</select>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Rentang waktu</label>
			<div class="controls">
				<input type="text" class="span2" name="strdate" id="tgl-1" value="<?php echo $rows->str_date; ?>" placeholder="Tanggal Mulai">
				
				<input type="text" class="span2" name="enddate" id="tgl-2" value="<?php echo $rows->end_date; ?>" placeholder="Tanggal Akhir">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Tanggal ujian</label>
			<div class="controls">
				<input type="text" class="span3" name="exmdate" id="tgl-3" value="<?php echo $rows->exm_date; ?>" placeholder="Tanggal Mulai">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Kuota</label>
			<div class="controls">
				<input type="text" id="quota-1" class="span3" name="kuota" value="<?php echo $rows->kuota; ?>" placeholder="Kuota Gelombang">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Tahun</label>
			<div class="controls">
				<input type="text" class="span3" id="yr-1" name="tahun" value="<?php echo $rows->tahun; ?>" placeholder="Tahun PMB">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Keterangan</label>
			<div class="controls">
				<textarea class="span3" name="not"><?php echo $rows->notice; ?></textarea>
			</div>
		</div>

		<input type="hidden" name="id" value="<?php echo $rows->id_gel; ?>">
	
</div>
<div class="modal-footer">
	<input class="btn btn-success" id="sbmEdit" type="submit" value="Submit">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>