<?php foreach ($lod as $val) { ?>
	<tr>
		<td><?php echo $val->kd_gel; ?></td>
		<td><?php echo $val->gelombang; ?></td>
		<td><?php echo TanggalIndoRange($val->str_date,$val->end_date); ?></td>
		<td><?php echo TanggalIndo($val->exm_date); ?></td>
		<td><?php echo $val->kuota; ?></td>
		<td style="text-align: center">
			<button data-toggle="modal" data-target="#editModal" class="btn btn-primary" title="Edit data" onclick="edit(<?php echo $val->id_gel ?>)"><i class="icon icon-pencil"></i></button>
			<button class="btn btn-danger" onclick="delRow(<?php echo $val->id_gel ?>)" title="Hapus data"><i class="icon icon-remove"></i></button>
		</td>
	</tr>
<?php } ?>