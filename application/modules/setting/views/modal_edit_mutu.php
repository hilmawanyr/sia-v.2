<div class="modal-header">
	<h4 class="modal-title">Perbarui Data Mutu</h4>
</div>
<form class="form-horizontal" action="<?= base_url('setting/poin/updateMutu/'.$mutu->id) ?>" method="post">
	<div class="modal-body">
		<div class="alert alert-danger">
			*Untuk <b>Bobot</b>, <b>Nilai bawah</b>, <b>Nilai atas</b> Gunakan titik sebagai pemisah. Misal: <b>4.00 (empat titik nol nol)</b>
		</div>
		
		<div class="control-group">
			<label class="control-label">Mutu</label>
			<div class="controls">
				<input 
					type="text" 
					class="span3" 
					name="mutu" 
					placeholder="(A, A-, B, ...)" 
					value="<?= $mutu->nilai_huruf ?>" 
					required="">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Bobot</label>
			<div class="controls">
				<input 
					type="text" 
					class="span3" 
					id="bobot" 
					name="bobot" 
					value="<?= $mutu->nilai_mutu ?>" 
					placeholder="Bobot mutu (angka desimal)" 
					required="">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Nilai Bawah</label>
			<div class="controls">
				<input 
					type="text" 
					class="span3" 
					id="bawah" 
					value="<?= $mutu->nilai_bawah ?>" 
					name="bawah" 
					placeholder="Batas bawah nilai" 
					required="">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Nilai Atas</label>
			<div class="controls">
				<input 
					type="text" 
					class="span3" 
					id="atas" 
					name="atas" 
					value="<?= $mutu->nilai_atas ?>" 
					placeholder="Batas atas nilai" 
					required="">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Deskripsi</label>
			<div class="controls">
				<textarea name="deskripsi" class="span3" required=""><?= $mutu->deskripsi ?></textarea>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Description (english)</label>
			<div class="controls">
				<textarea name="description" class="span3" required=""><?= $mutu->description ?></textarea>
			</div>
		</div>
	</div>	
	<div class="modal-footer">
		<button class="btn btn-success" id="sbm" type="submit">Update</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>
