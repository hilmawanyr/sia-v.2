<link href="<?php echo base_url(); ?>assets/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<div class="row">
	<div class="span12">
		<div class="widget ">

			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>Tambah Versi</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<div class="span11">

					<form method="post" action="<?php echo base_url() ?>setting/version/ubah">

						<div class="control-group">
							<label class="control-label">Versi</label>
							<div class="controls">
								<input type="text" class="span3" placeholder="Versi Terbaru" id="versi" name="versi" value="<?php echo $versi->versi ?>" readonly>
							</div> <!-- /controls -->
						</div> <!-- /control-group -->
						<div class="control-group">
							<label class="control-label">Keterangan</label>
							<div class="controls">
								<textarea type="text" class="span11" placeholder="Keterangan Versi" id="keterangan" name="keterangan" required><?php echo $versi->keterangan ?></textarea>
							</div> <!-- /controls -->
						</div> <!-- /control-group -->
						<hr>

						<table width="100%" class="table table-bordered" id="dynamic_field">
							<tr>
								<th>Pengguna</th>
								<th>Log</th>
								<th><button type="button" id="add" class="add_field_button btn btn-primary"><i class="icon-plus"></i></button></th>
							</tr>
							<tr readonly>
								<?php foreach ($detail as $row) { ?>
									<td><input type="text" class="span4" value="<?php echo get_group_user($row->user) ?>" disabled></td>
									<td><input type="text" class="span6" value="<?php echo $row->vlog ?>" data-role="tagsinput" disabled></td>
									<td><a class="btn btn-danger" onclick="return confirmhapus();" href="<?php echo base_url(); ?>setting/version/hapus_log/<?php echo $row->id_v_detail; ?>"><i class="icon-minus"></i></a></td>
							</tr>
						<?php } ?>
						</table>

						<input type="hidden" name="id_versi" value="<?php echo $versi->id_versi ?>">
						<button type="submit" class="btn btn-primary" id="save">Simpan</button>

					</form>

				</div>
			</div>

		</div>
	</div>
</div>


<script>
	$(document).ready(function() {

		var i = 1;
		$('#add').click(function() {
			i++;
			$('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added"><td><select class="span4" id="user" name="user[]" required><option value="99">Semua</option><?php foreach ($group as $row) { ?> <option value="<?php echo $row->id_user_group ?>"> <?php echo $row->user_group ?></option><?php } ?></select></td>' +
				'<td><input type="text" class="span6 preview_input" placeholder="Pisahkan dengan koma" id="log" name="log[]" data-role="tagsinput" required></td><td><button type="button" class="btn_remove btn btn-danger" id="' + i + '" > <i class="icon-minus"> </i></button></td></tr>');
			$('.preview_input').tagsinput({
				allowDuplicates: false
			});
		});


		$(document).on('click', '.btn_remove', function() {
			var button_id = $(this).attr("id");
			$('#row' + button_id + '').remove();
		});

		function confirmhapus() {
			var x = confirm("Yakin menghapus data?")
			if (x) {
				return true;
			} else {
				return false;
			}
		}
	});
</script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo base_url(); ?>assets/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>