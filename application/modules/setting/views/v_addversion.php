<link href="<?php echo base_url(); ?>assets/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<div class="row">
	<div class="span12">
		<div class="widget ">

			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>Tambah Versi</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<div class="span11">

					<form method="post" action="<?php echo base_url() ?>setting/version/add">

						<div class="control-group">
							<label class="control-label">Versi</label>
							<div class="controls">
								<input type="text" class="span3" placeholder="Versi Terbaru" id="versi" name="versi" required>
								<div id="msg"></div>
							</div> <!-- /controls -->
						</div> <!-- /control-group -->
						<div class="control-group">
							<label class="control-label">Keterangan</label>
							<div class="controls">
								<textarea type="text" class="span11" placeholder="Keterangan Versi" id="keterangan" name="keterangan" required></textarea>
							</div> <!-- /controls -->
						</div> <!-- /control-group -->

						<hr>

						<table width="100%" class="table table-bordered" id="dynamic_field">
							<tr>
								<th>Pengguna</th>
								<th>Log</th>
								<th></th>
							</tr>
							<tr>
								<td>
									<select class="span4" id="user" name="user[]" required>
										<option value="99">Semua</option>
										<?php foreach ($group as $row) { ?>
											<option value="<?php echo $row->id_user_group ?>"><?php echo $row->user_group ?></option>
										<?php } ?>
									</select>
								</td>
								<td><input type="text" class="span6" placeholder="Pisahkan dengan koma" id="log" name="log[]" data-role="tagsinput"></td>
								<td><button type="button" id="add" class="add_field_button btn btn-primary"><i class="icon-plus"></i></button></td>
							</tr>
						</table>

						<button type="submit" class="btn btn-primary" id="save">Simpan</button>

					</form>

				</div>
			</div>

		</div>
	</div>
</div>


<script>
	$(document).ready(function() {

		var i = 1;
		$('#add').click(function() {
			i++;
			$('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added"><td><select class="span4" id="user" name="user[]" required><option value="99">Semua</option><?php foreach ($group as $row) { ?> <option value="<?php echo $row->id_user_group ?>"> <?php echo $row->user_group ?></option><?php } ?></select></td>' +
				'<td><input type="text" class="span6 preview_input" placeholder="Pisahkan dengan koma" id="log" name="log[]" data-role="tagsinput" required></td><td><button type="button" class="btn_remove btn btn-danger" id="' + i + '" > <i class="icon-minus"> </i></button></td></tr>');
			$('.preview_input').tagsinput({
				allowDuplicates: false
			});
		});


		$(document).on('click', '.btn_remove', function() {
			var button_id = $(this).attr("id");
			$('#row' + button_id + '').remove();
		});

		$("#versi").on("blur", function(e) {
			$('#msg').hide();
			if ($('#versi').val() == null || $('#versi').val() == "") {
				$('#msg').show();
				$("#msg").html("Versi harus diisi").css("color", "red");
			} else {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url() ?>/setting/version/cari/",
					data: $('#versi').serialize(),
					dataType: "HTML",
					cache: false,
					success: function(msg) {
						$('#msg').show();
						$("#msg").html(msg);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$('#msg').show();
						$("#msg").html(textStatus + " " + errorThrown);
					}
				});
			}
		});

	});
</script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo base_url(); ?>assets/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>