<script type="text/javascript">
$(document).ready(function() {
  $('#mulai').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

  $('#ahir').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Jadwal Peneriaam Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon icon-plus"></i>&nbsp; Jadwalkan Kegiatan</a>
				<hr>
					<fieldset>
						
						<table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>Gelombang</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($rows as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo "Gelombang ".$isi->gelombang ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                                <td>
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
                                    <a onclick="return confirm('Yakin ingin menghapus kegiatan ini ?');" href="<?php echo base_url(); ?>akademik/kalender/rem_isi_kalender/<?php echo $isi->id; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>

					</fieldset>
			</div>
		</div>
	</div>
</div>



<!-- Mpdal -->
<div class="modal modal-large fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <i class="icon icon-edit"></i> Buat Kalender Akademik</h4>
            </div>
            <div class="modal-body">
                <form class ='form-horizontal' action="<?php echo base_url();?>setting/jdl_pmb/jdl_save" method="post">
                <div class="control-group">
                  <label class="control-label">Gelombang </label>
                  <div class="controls">
                    <select class="form-control span4" name="gel" required>
                        <option>-- Pilih gelombang --</option>
                        <?php for ($i=1; $i <6 ; $i++) { 
                            echo '<option value="'.$i.'">Gelombang - '.$i.'</option>';
                        } echo '<option value="99">Gelombang - EXTRA</option>'; 
                        ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Kelompok Kegiatan </label>
                  <div class="controls">
                    <select class="form-control span4" name="keg" required>
                        <option>-- Pilih Kegiatan --</option>
                        <?php foreach ($kegiatan as $isi) {
                            echo '<option value="'.$isi->id.'">'.$isi->kegiatan.'</option>';
                        } 
                        ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Waktu Pelaksanaan </label>
                  <div class="controls">
                    <input class="form-control span2" type="text" placeholder="Waktu Mulai"    name="mulai" id="mulai" required>
                    <input class="form-control span2" type="text" placeholder="Waktu Selesai"  name="ahir" id="ahir" required>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Keterangan </label>
                  <div class="controls">
                    <textarea class="form-control span4" name="ket" placeholder="(OPSIONAL)" ></textarea>
                  </div>
                </div>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
            </div>
            </form>
            </div><!-- /.modal-body -->    
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->