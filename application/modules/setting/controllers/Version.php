<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Version extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['versi'] = $this->app_model->getdata('tbl_version', 'created_at', 'desc')->result();
		$data['page'] = 'v_version';
		$this->load->view('template/template', $data);
	}

	function tambah()
	{
		$data['group'] = $this->app_model->getdata('tbl_user_group', 'user_group', 'asc')->result();
		$data['page'] = 'v_addversion';
		$this->load->view('template/template', $data);
	}
	function edit($id)
	{
		$data['versi'] = $this->app_model->getdetail('tbl_version', 'id_versi', $id, 'id_versi', 'desc')->row();
		$data['detail'] = $this->app_model->getdetail('tbl_version_detail', 'versi_sia', $id, 'user', 'asc')->result();
		$data['group'] = $this->app_model->getdata('tbl_user_group', 'user_group', 'asc')->result();
		$data['page'] = 'v_version_edit';
		$this->load->view('template/template', $data);
	}

	function detail_version($id)
	{
		$data['versi'] = $this->app_model->getdetail('tbl_version', 'id_versi', $id, 'id_versi', 'desc')->row();
		$data['detail'] = $this->app_model->getdetail('tbl_version_detail', 'versi_sia', $id, 'user', 'asc')->result();
		$this->load->view('v_version_detail', $data);
	}
	function add()
	{
		$data = array(
			'versi' 		=> $this->input->post('versi'),
			'keterangan' 	=> $this->input->post('keterangan'),
			'created_at' 	=> date('Y-m-d H:i:s')
		);
		$this->app_model->insertdata('tbl_version', $data);
		$last_id = $this->db->insert_id();

		$user = $this->input->post('user');
		$dt = array();
		$n = 0;
		foreach ($user as $key => $val) {
			$dt[$n] = array(
				'versi_sia' 	=> $last_id,
				'user' 			=> $_POST['user'][$key],
				'vlog' 			=> $_POST['log'][$key]
			);
			$n++;
		}

		$this->db->insert_batch('tbl_version_detail', $dt);
		echo "<script>alert('Berhasil');
		document.location.href='" . base_url() . "setting/version'</script>";
	}
	function ubah()
	{
		$id_versi = $this->input->post('id_versi');
		$data = array(
			'keterangan' 	=> $this->input->post('keterangan'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		);
		$this->app_model->updatedata('tbl_version', 'id_versi', $id_versi, $data);
		$last_id = $id_versi;

		$user = $this->input->post('user');
		$dt = array();
		$n = 0;
		foreach ($user as $key => $val) {
			$dt[$n] = array(
				'versi_sia' 	=> $last_id,
				'user' 			=> $_POST['user'][$key],
				'vlog' 			=> $_POST['log'][$key]
			);
			$n++;
		}

		$this->db->insert_batch('tbl_version_detail', $dt);
		echo "<script>alert('Berhasil');
		document.location.href='" . base_url() . "setting/version'</script>";
	}
	function hapus_log($id)
	{
		$this->app_model->deletedata('tbl_version_detail', 'id_v_detail', $id);
		echo "<script>alert('Berhasil Menghapus Log versi !!');window.location.reload(history.go(-1));</script>";
	}
	function cari()
	{
		if (isset($_POST['versi'])) {
			$versi = $_POST['versi'];
			$results = $this->app_model->cari_kd('tbl_version', 'versi', $versi);
			if ($results === TRUE) {
				$a = '<span style="color:red;">Versi sudah digunakan sebelumnya</span>';
				$b = "<script>document.getElementById('save').disabled = true;</script>";
				echo $a, $b;
			} else {
				$a = '<span style="color:green;">Versi dapat digunakan</span>';
				$b = "<script>document.getElementById('save').disabled = false;</script>";
				echo $a, $b;
			}
		} else {
			$a = '<span style="color:red;">Versi harus diisi</span>';
			$b = "<script>document.getElementById('save').disabled = true;</script>";
			echo $a, $b;
		}
	}
}
