<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup_gel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') != TRUE) {
			redirect('auth');
		}
	}

	public function index()
	{	
		error_reporting(0);
		$this->db->where('status', 1);
		$data['qr'] = $this->db->get('tbl_gelombang_pmb');
		$data['qw'] = $this->db->get('tbl_gelombang_pmb');
		$data['pr'] = $this->db->select('a.kd_prodi, a.prodi, b.status_kuota')->from('tbl_jurusan_prodi a')->join('tbl_gel_kuotaprodi b','a.kd_prodi = b.kd_prodi')->get();
		$data['page'] = "v_setup";
		$this->load->view('template/template', $data);
	}

	function update_gel()
	{
		$gel = $this->input->post('tahun');
		$this->db->query("UPDATE tbl_gelombang_pmb set status = 0 where status = 1");
		$this->db->query("UPDATE tbl_gelombang_pmb set status = 1 where gelombang = '".$gel."'");
		redirect('setting/setup_gel');
	}

	function load_gel()
	{
		$data = $this->app_model->getdata('tbl_gelombang_pmb','gelombang','asc')->result();
		$enc = json_encode($data);
		echo $enc;
	}

	function add_wave()
	{
		$gel = $this->input->post('gelombang');
		$str = $this->input->post('strdate');
		$end = $this->input->post('enddate');
		$quo = $this->input->post('kuota');
		$thn = $this->input->post('tahun');
		$not = $this->input->post('not');
		$exm = $this->input->post('exmdate');

		$data = array(
			'kd_gel' 	=> 'G'.$gel.'-'.substr($thn, 2,4),
			'gelombang'	=> $gel,
			'str_date'	=> $str,
			'end_date'	=> $end,
			'exm_date'	=> $exm,
			'status'	=> 0,
			'kuota'		=> $quo,
			'tahun'		=> $thn,
			'notice'	=> $not
		);
		
		$this->db->insert('tbl_gelombang_pmb', $data);
	}

	function loadTable()
	{
		error_reporting(0);
		$data['lod'] = $this->db->get('tbl_gelombang_pmb')->result();
		$this->load->view('table_loadgel', $data);
	}

	function delRows($id)
	{
		$this->db->where('id_gel', $id);
		$this->db->delete('tbl_gelombang_pmb');
	}

	function loadEdit($id)
	{
		$this->db->where('id_gel', $id);
		$data['rows'] = $this->db->get('tbl_gelombang_pmb',1)->row();
		$this->load->view('contentedit', $data);
	}

	function editRow()
	{
		$gel = $this->input->post('gelombang');
		$str = $this->input->post('strdate');
		$end = $this->input->post('enddate');
		$quo = $this->input->post('kuota');
		$thn = $this->input->post('tahun');
		$id = $this->input->post('id');
		$not = $this->input->post('not');
		$exm = $this->input->post('exmdate');

		$data = array(
			'kd_gel' 	=> 'G'.$gel.'-'.substr($thn, 2,4),
			'gelombang'	=> $gel,
			'str_date'	=> $str,
			'end_date'	=> $end,
			'exm_date'	=> $exm,
			'status'	=> 0,
			'kuota'		=> $quo,
			'tahun'		=> $thn,
			'notice'	=> $not
		);
		$this->db->where('id_gel', $id);
		$this->db->update('tbl_gelombang_pmb', $data);
	}

	function updateOC($s,$p)
	{
		$data = array('status_kuota' => $s);
		$this->db->where('kd_prodi', $p);
		$this->db->update('tbl_gel_kuotaprodi', $data);
	}

}

/* End of file Setup_gel.php */
/* Location: ./application/modules/setting/controllers/Setup_gel.php */