<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpanel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(74)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['getData'] = $this->app_model->getdata('tbl_cpanel','id','asc')->result();
		$data['page'] = 'setting/cpanel_view';
		$this->load->view('template/template',$data);
	}

	function getdataedit($id)
	{
		$data['getEdit'] = $this->app_model->getdetail('tbl_cpanel','id',$id,'id','asc')->row();
		$this->load->view('cpanel_edit',$data);	
	}

	function update()
	{
		$id = $this->input->post('id', TRUE);
		$data['status']= $this->input->post('status', TRUE);
		$insert = $this->app_model->updatedata('tbl_cpanel','id',$id,$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/cpanel';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}

}

/* End of file viewkrs.php */
/* Location: .//C/Users/danum246/Desktop/viewkrs.php */