<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mhs_sync extends CI_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->model('setting_model');

		if ($this->session->userdata('sess_login') == TRUE) {

			$cekakses = $this->role_model->cekakses(73)->result();

			if ($cekakses != TRUE) {

				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";

			}

		} else {

			redirect('auth','refresh');

		}

	}



	function index()

	{

		$data = $this->setting_model->getmhsnonakun()->result();

		//var_dump($data);exit();

		if (count($data) > 0) {

			foreach ($data as $value) {

				$data1['username']= substr(str_replace(' ', '', $value->NIMHSMSMHS), 0,12);

				$data1['userid']= str_replace(' ', '', $value->NIMHSMSMHS);

				$data1['status']= 1;

				//$pass = explode('-', $value->TGLHRMSMHS);

				//$password = $pass[2].$pass[1].$pass[0];

				$data1['password_plain']= str_replace(' ', '', $data1['username']);

				$data1['user_type'] = 2;

				$data1['password']= sha1(md5($data1['username']).key);

				$data1['id_user_group']= 5;

				//var_dump($data1);

				$insert = $this->app_model->insertdata('tbl_user_login',$data1);

				if ($insert == TRUE) {

					echo "<script>alert('Berhasil');document.location.href='".base_url()."home';</script>";

				} else {

					echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";

				}

				//echo "<br/>";

			}

		} else {

			echo "<script>alert('Sinkronisasi Selesai');document.location.href='".base_url()."home';</script>";

		}

		//exit();

	}

	function index2()

	{

		$data = $this->setting_model->getmhsnews1(5)->result();
		//var_dump(count($data));exit();
		if (count($data) > 0) {

			foreach ($data as $value) {

				$NIM = $this->setting_model->getnimnews1($value->prodi,$value->jenis_pmb)->row();
				//var_dump($NIM);exit();
				if (is_null($NIM)) {
					
					if ($value->jenis_pmb == 'MB') {
						$kd = 5;
					} else {
						$kd = 7;
					}
					

					if ($value->prodi == '55201') {
						$npmprod = '1022'.$kd.'';
					} elseif ($value->prodi == '26201') {
						$npmprod = '1021'.$kd.'';
					} elseif ($value->prodi == '73201') {
						$npmprod = '1051'.$kd.'';
					} elseif ($value->prodi == '74201') {
						$npmprod = '1011'.$kd.'';
					} elseif ($value->prodi == '61201') {
						$npmprod = '1032'.$kd.'';
					} elseif ($value->prodi == '70201') {
						$npmprod = '1041'.$kd.'';
					} elseif ($value->prodi == '24201') {
						$npmprod = '1023'.$kd.'';
					} elseif ($value->prodi == '25201') {
						$npmprod = '1024'.$kd.'';
					} elseif ($value->prodi == '62201') {
						$npmprod = '1031'.$kd.'';
					} elseif ($value->prodi == '32201') {
						$npmprod = '1025'.$kd.'';
					}
					$npm = date('Y').$npmprod.'001';
					$data1['NIMHSMSMHS']= $npm;
				} else {
					$npm = substr($NIM->NIMHSMSMHS, -3,3)+1;
					if ($npm < 10) {
						$npmbaru = '00'.$npm;
					} elseif ($npm < 100) {
						$npmbaru = '0'.$npm;
					} else {
						$npmbaru = $npm;
					}

					$data1['NIMHSMSMHS']= substr($NIM->NIMHSMSMHS, 0,4).substr($NIM->NIMHSMSMHS, 4,5).$npmbaru;
					
				}
				
				$data1['KDPTIMSMHS']= '31036';

				$data1['KDPSTMSMHS']= $value->prodi;

				$data1['NMMHSMSMHS']= strtoupper($value->nama);

				$data1['SHIFTMSMHS']= 'R';

				$data1['TPLHRMSMHS']= $value->tempat_lahir;

				$data1['TGLHRMSMHS']= $value->tgl_lahir;

				$data1['KDJEKMSMHS']= '';

				$data1['TAHUNMSMHS']= date('Y');

				$data1['SMAWLMSMHS']= date('Y').'1';

				$data1['BTSTUMSMHS']= (date('Y')+7).'2';

				$data1['ASSMAMSMHS']= '';

				$data1['TGMSKMSMHS']= date('Y-m-d');

				$data1['STMHSMSMHS']= 'A';

				$data1['FLAG_RENKEU']= 1;

				$data1['STPIDMSMHS']= '';

				$data1['SKSDIMSMHS']= '';

				//var_dump($data1);exit();

				$insert = $this->app_model->insertdata('tbl_mahasiswa',$data1);

				$data2['npm_baru']= $data1['NIMHSMSMHS'];

				$this->app_model->updatedata('tbl_form_camaba','nomor_registrasi',$value->nomor_registrasi,$data2);

				//$this->db->query('update tbl_form_camaba_palsu set npm_baru = '.$data1["NIMHSMSMHS"].' where nomor_registrasi = '.$value->nomor_registrasi.'');

				if ($insert == TRUE) {

					echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/npm_sync';</script>";

				} else {

					echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";

				}

				//echo "<br/>";

			}

		} else {

			echo "<script>alert('Sinkronisasi Selesai');document.location.href='".base_url()."setting/npm_sync';</script>";

		}

		//exit();

	}

	function index3()

	{

		$data = $this->setting_model->getmhsnews2(5)->result();

		//var_dump($data);exit();

		if (count($data) > 0) {

			foreach ($data as $value) {

				$NIM = $this->setting_model->getnimnews2($value->opsi_prodi_s2,$value->jenis_pmb)->row();
				//var_dump($NIM);exit();
				if (count($NIM) == 0) {
					
					if ($value->jenis_pmb == 'MB') {
						$kd = 5;
					} else {
						$kd = 7;
					}

					if ($value->opsi_prodi_s2 == '61101') {
						$npmprod = '201'.$kd.'1';
					} elseif ($value->opsi_prodi_s2 == '74101') {
						$npmprod = '202'.$kd.'1';
					}
					//$npms2 = $this->app_model->getdetail('tbl_tahunakademik','status','1','kode','desc')->row()->kode;
					//$npm = $npms2.$npmprod.'001';
					$npm = '2016'.$npmprod.'001';
					$data1['NIMHSMSMHS']= $npm;
				} else {
					$npm = substr($NIM->NIMHSMSMHS, -3,3)+1;
					if ($npm < 10) {
						$npmbaru = '00'.$npm;
					} elseif ($npm < 100) {
						$npmbaru = '0'.$npm;
					} else {
						$npmbaru = $npm;
					}

					$data1['NIMHSMSMHS']= substr($NIM->NIMHSMSMHS, 0,4).substr($NIM->NIMHSMSMHS, 4,5).$npmbaru;
					
				}
				
				$data1['KDPTIMSMHS']= '31036';

				$data1['KDPSTMSMHS']= $value->opsi_prodi_s2;

				$data1['NMMHSMSMHS']= strtoupper($value->nama);

				$data1['SHIFTMSMHS']= 'R';

				$data1['TPLHRMSMHS']= $value->tempat_lahir;

				$data1['TGLHRMSMHS']= $value->tgl_lahir;

				$data1['KDJEKMSMHS']= '';

				$data1['TAHUNMSMHS']= '2016';

				$data1['SMAWLMSMHS']= '20161';

				$data1['BTSTUMSMHS']= (date('Y')+7).'1';

				$data1['ASSMAMSMHS']= '';

				$data1['TGMSKMSMHS']= date('Y-m-d');

				$data1['STMHSMSMHS']= 'A';

				$data1['FLAG_RENKEU']= 1;

				$data1['STPIDMSMHS']= '';

				$data1['SKSDIMSMHS']= '';

				//var_dump($data1);exit();

				$insert = $this->app_model->insertdata('tbl_mahasiswa',$data1);

				$data2['npm_baru']= $data1['NIMHSMSMHS'];

				$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$value->ID_registrasi,$data2);

				if ($insert == TRUE) {

					echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/npm_sync';</script>";

				} else {

					echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";

				}

				//echo "<br/>";

			}

		} else {

			echo "<script>alert('Sinkronisasi Selesai');document.location.href='".base_url()."setting/npm_sync';</script>";

		}

		//exit();

	}


}



/* End of file viewkrs.php */

/* Location: .//C/Users/danum246/Desktop/viewkrs.php */