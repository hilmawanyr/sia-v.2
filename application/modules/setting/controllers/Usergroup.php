<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(47)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['usergroup'] = $this->app_model->getdata('tbl_user_group','id_user_group','asc')->result();
		$data['page'] = 'setting/usergroup_view';
		$this->load->view('template/template',$data);
	}

	function savedata()
	{
		$data['user_group'] = $this->input->post('textfield1', TRUE);
		$q = $this->app_model->insertdata('tbl_user_group',$data);
		if ($q == TRUE) {
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."setting/usergroup';</script>";
		} else {
			echo "<script>alert('Gagal');
			history.go(-1);</script>";
		}
	}

	function updatedata()
	{
		$id = $this->input->post('id', TRUE);
		$data['user_group'] = $this->input->post('textfield1', TRUE);
		$q = $this->app_model->updatedata('tbl_user_group','id_user_group',$id,$data);
		if ($q == TRUE) {
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."setting/usergroup';</script>";
		} else {
			echo "<script>alert('Gagal');
			history.go(-1);</script>";
		}
	}

	function editdata($id)
	{
		$data['getEdit'] = $this->app_model->getdetail('tbl_user_group','id_user_group',$id,'id_user_group','asc')->row();
		$this->load->view('setting/usergroup_edit',$data);
	}

	function deletedata($id)
	{
		$q = $this->app_model->deletedata('tbl_user_group','id_user_group',$id);
		if ($q == TRUE) {
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."setting/usergroup';</script>";
		} else {
			echo "<script>alert('Gagal');
			history.go(-1);</script>";
		}
	}

}

/* End of file Usergroup.php */
/* Location: ./application/modules/setting/controllers/Usergroup.php */