<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academic_year extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'));
		}
	}

	public function index()
	{
		$data['years'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page']  = 'manage_academic_year_v';
		$this->load->view('template/template', $data);
	}

	public function prepare_store()
	{
		PopulateForm();

		$dataStored = [
			'kode' => $kode,
			'tahun_akademik' => $name
		];

		if (empty($isUpdate)) {
			$this->store($dataStored);
		} else {
			$this->update($dataStored, $isUpdate);
		}
	}

	public function store($data)
	{
		$this->db->insert('tbl_tahunakademik', $data);
		echo "<script>alert('Penyimpanan berhasil!');history.go(-1);</script>";
	}

	public function edit($id)
	{
		$year = $this->db->where('id', $id)->get('tbl_tahunakademik')->row();
		$data = [
			'id' => $year->id,
			'code' => $year->kode,
			'name' => $year->tahun_akademik
		];

		echo json_encode($data);
	}

	public function update($data, $yearId)
	{
		$this->db->where('id', $yearId)->update('tbl_tahunakademik',$data);
		echo "<script>alert('Update berhasil!');history.go(-1);</script>";
	}

	public function remove($id)
	{
		$this->_can_removed($id);
		$this->db->where('id', $id)->delete('tbl_tahunakademik');
		echo "<script>alert('Hapus data berhasil!');history.go(-1);</script>";
	}

	protected function _can_removed($id)
	{
		$isYearActive = $this->db->where('id', $id)->get('tbl_tahunakademik')->row()->status;
		if ($isYearActive > 0) {
			echo "<script>alert('Data tidak dapat dihapus. Tahun akademik sedang aktif!');history.go(-1);</script>";
			exit();
		}
		return;
	}

	public function activate_year($id)
	{
		$this->db->update('tbl_tahunakademik', ['status' => 0]);

		$this->db->where('id', $id)->update('tbl_tahunakademik', ['status' => 1]);
		echo "<script>alert('Tahun akademik diaktifkan!');history.go(-1);</script>";
	}

}

/* End of file Academic_year.php */
/* Location: ./application/modules/setting/controllers/Academic_year.php */