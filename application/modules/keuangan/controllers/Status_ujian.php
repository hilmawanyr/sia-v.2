<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status_ujian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		 
		error_reporting(E_ERROR);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(78)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "ujian_view";
		$this->load->view('template/template', $data);
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');

		$jurusan = $this->input->post('jurusan');

        $tahunajaran = $this->input->post('tahunajaran');

		$angkatan = $this->input->post('angkatan');  
		     

        $this->session->set_userdata('tahunajaran', $tahunajaran);

		$this->session->set_userdata('jurusan', $jurusan);

		$this->session->set_userdata('fakultas', $fakultas);

		$this->session->set_userdata('angkatan', $angkatan);
      
		redirect(base_url('keuangan/status_ujian/load_uji'));
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function load_uji()
	{
		$data['e'] = $this->db->query('SELECT DISTINCT a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS from tbl_mahasiswa a 
										join tbl_jurusan_prodi b on a.`KDPSTMSMHS`=b.`kd_prodi` 
										where b.`kd_prodi`="'.$this->session->userdata('jurusan').'"
										AND a.`TAHUNMSMHS`="'.$this->session->userdata('angkatan').'" and (a.STMHSMSMHS != "K" AND a.STMHSMSMHS != "L")
										order by a.`NIMHSMSMHS` asc')->result();
		$data['ta'] = $this->session->userdata('tahunajaran');

		$data['page'] = "keuangan/tbl_mhs";
		$this->load->view('template/template', $data);
	}

	function input_data()
	{
		$this->benchmark->mark('start');
		$sessLogin = $this->session->userdata('sess_login');
		$userid = $sessLogin['userid'];

		$jum = count($this->input->post('lunas', TRUE));
		
		$insertStatusCuti = [];
		$insertSinkRenkeu = [];
		$updateSinkRenkeu = [];

		for ($i=0; $i < $jum ; $i++) { 
			$v = explode('911', $this->input->post('lunas['.$i.']', TRUE));
			
			$status = $v[0];

			$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$v[1],'NIMHSMSMHS','asc')->row();
			$kodeProdi = $mhs->KDPSTMSMHS; 
			$smstrAwal = $mhs->SMAWLMSMHS;
			$npm = $mhs->NIMHSMSMHS;

			$exist = $this->db->query('SELECT id_sink from tbl_sinkronisasi_renkeu where npm_mahasiswa = "'.$npm.'" and tahunajaran = "'.$this->session->userdata('tahunajaran').'"')->row();

			if ($exist) {
				
				$dataUpdate = [
					'npm_mahasiswa'      => $npm, 
					'status'             => $status, 
					'tahunajaran'        => $v[2],
					'transaksi_terakhir' => date('Y-m-d'),
				];

				$updateSinkRenkeu[]  = $dataUpdate;
			} else {
				$semesterAwal = $this->app_model->get_semester($smstrAwal);

				$data['briva_mhs']     = '70306'.substr($v[1], 2);
				$data['npm_mahasiswa'] = $v[1];
				$data['tahunajaran']   = $v[2];
				
				if ($status == 'z') {
				 	$data['status'] = NULL;
				} elseif($status == 8) {
					$data2['status']      = 'C';
					$data2['tanggal']     = date('Y-m-d');
					$data2['npm']         = $v[1];
					$data2['tahunajaran'] = $v[2];
					$data2['semester']    = $semesterAwal;

					$insertStatusCuti[] = $data2;
					$data['status']     = $status;
				} else {
				 	$data['status'] = $status;
				}

				$data['transaksi_terakhir'] = date('Y-m-d');
				$data['userid']             = $userid;
				$data['semester']           = $semesterAwal;

				$insertSinkRenkeu[] = $data;
			}
		}

		if (count($insertStatusCuti) > 0) {
			$this->db->insert_batch('tbl_status_mahasiswa', $insertStatusCuti);
		}

		if (count($insertSinkRenkeu) > 0) {
			$this->db->insert_batch('tbl_sinkronisasi_renkeu', $insertSinkRenkeu);
		}

		if (count($updateSinkRenkeu) > 0) {
			
			$this->db->update_batch('tbl_sinkronisasi_renkeu', $updateSinkRenkeu, 'npm_mahasiswa');
		}

		echo "<script>alert('Sukses');
		 document.location.href='".base_url()."keuangan/status_ujian/load_uji';</script>";
	}

	function rekap_data()
	{
		$data['pong'] = $this->db->query('SELECT DISTINCT b.`NIMHSMSMHS`,b.`NMMHSMSMHS`,d.`fakultas`,c.`prodi`,a.`status`
		    from tbl_sinkronisasi_renkeu a join tbl_mahasiswa b
			on a.`npm_mahasiswa`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`=b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas` join tbl_verifikasi_krs e
			on e.`npm_mahasiswa`=b.`NIMHSMSMHS`
			where c.`kd_prodi`="'.$this->session->userdata('jurusan').'"
			AND a.`tahunajaran`="'.$this->session->userdata('tahunajaran').'"
			AND b.`TAHUNMSMHS`="'.$this->session->userdata('angkatan').'"
			GROUP BY b.`NIMHSMSMHS`')->result();
		$this->load->view('print_ujian', $data);

	}

	function havenotPay()
	{
		$this->load->model('temph_model');

		$prodi = $this->session->userdata('jurusan');
		$angkt = $this->session->userdata('angkatan');
		$years = $this->session->userdata('tahunajaran');

		$data['year'] = $years;
		$data['prod'] = $prodi;

		$data['load'] = $this->temph_model->belumdaftarulang($prodi,$angkt,$years);
		$this->load->view('excel_belum_du', $data);
	}
}

/* End of file status_ujian.php */
/* Location: ./application/controllers/status_ujian.php */