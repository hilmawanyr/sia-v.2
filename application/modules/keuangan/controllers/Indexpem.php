<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indexpem extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(55)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		if (($this->session->userdata('sess_jur') == TRUE) && ($this->session->userdata('sess_angk') == TRUE)){
			$data['getData'] = $this->app_model->getIndexPembayaran($this->session->userdata('sess_jur'),$this->session->userdata('sess_angk'));
			$data['jenis']=$this->app_model->getdata('tbl_jenis_pembayaran', 'kd_jenis', 'ASC')->result();
			$data['page'] = 'keuangan/index_lihat';
		} else {
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunajaran', 'id_tahunajaran', 'ASC')->result();
			$data['page'] = 'keuangan/indexpem_view';
		}
		$this->load->view('template/template',$data);
	}

	function back()
	{
		$this->session->unset_userdata('sess_jur');
		$this->session->unset_userdata('sess_angk');
		redirect('keuangan/indexpem','refresh');
	}

	function save_seasson(){
		$tipe = $this->input->post('id');
		$data['angkatan'] = $this->input->post('tahunajaran', TRUE);
		$data['kelas'] = $this->input->post('shift', TRUE);
		$jur = explode('-', $this->input->post('jurusan', TRUE));
		$data['kd_prodi'] = $jur[0];

		$this->session->set_userdata('tipe_index',$tipe);
		$this->session->set_userdata('sess_jur',$jur[0]);
		$this->session->set_userdata('prodi',$jur[1]);
		$this->session->set_userdata('sess_angk',$data['angkatan']);

		redirect(base_url().'keuangan/indexpem/view_index','refresh');
	}

	function view_index()
	{
		$data['rows']=$this->db->query("SELECT * FROM tbl_index_pembayaran a join tbl_jenis_pembayaran b on a.kd_jenis = b.kd_jenis join tbl_jurusan_prodi c on a.kd_prodi = c.kd_prodi where a.tipe = ".$this->session->userdata('tipe_index')." AND a.tahunajaran = ".$this->session->userdata('sess_angk')."")->result();
		$data['jenis'] = $this->db->where('tipe',$this->session->userdata('tipe_index'))->get('tbl_jenis_pembayaran')->result();
		$data['page'] = 'keuangan/index_lihat';
		$this->load->view('template/template',$data);
	}

	function get_jurusan($id){
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function savedata()
	{
		$data['kd_prodi']= $this->input->post('jurusan', TRUE);
		$data['jumlah']= str_replace(',', '', $this->input->post('jumlah', TRUE));


		$data['tahunajaran']= $this->input->post('angkatan', TRUE);
		$data['kelas']= $this->input->post('kelas', TRUE);
		$data['kd_jenis']= $this->input->post('jenis', TRUE);
		$data['tipe']= $this->input->post('tipe', TRUE);

		$cek = $this->db->select('count(*) as jml')
						->where('tahunajaran', $data['tahunajaran'])
						->where('kelas',$data['kelas'])
						->where('kd_jenis',$data['kd_jenis'])
						->where('tipe',$data['tipe'])
						->get('tbl_index_pembayaran')->row();

		
		if ($cek->jml >=1 ) {
			echo "<script>alert('Index yang dimasukan sudah ada, cek kembali index pembayaran');document.location.href='".base_url()."keuangan/indexpem/view_index';</script>";
		}else{
			$insert = $this->app_model->insertdata('tbl_index_pembayaran',$data);
			if ($insert == TRUE) {
				echo "<script>alert('Berhasil');document.location.href='".base_url()."keuangan/indexpem/view_index';</script>";
			} else {
				echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
			}	
		}

		
	}

	function deletedata($id)
	{
		$delete = $this->app_model->deletedata('tbl_index_pembayaran','id_index',$id);
		if ($delete == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."keuangan/indexpem';</script>";
		} else {
			echo "<script>alert('Gagal Hapus Data');history.go(-1);</script>";
		}
	}

	function edit($id)
	{
		$data['getEdit'] = $this->app_model->getdetail('tbl_index_pembayaran','id_index',$id,'id_index','asc')->row();
		$data['jenis']=$this->app_model->getdata('tbl_jenis_pembayaran', 'kd_jenis', 'ASC')->result();
		$this->load->view('keuangan/index_edit',$data);
	}

	function updatedata()
	{
	
		$data['jumlah']= str_replace(',', '', $this->input->post('jumlah', TRUE));
		$data['kd_jenis']= $this->input->post('jenis', TRUE);
		$insert = $this->app_model->updatedata('tbl_index_pembayaran','id_index',$this->input->post('id'),$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."keuangan/indexpem/view_index';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}
	
	

}

/* End of file index.php */
/* Location: ./application/modules/data/controllers/index.php */