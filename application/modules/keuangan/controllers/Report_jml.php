<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_jml extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(117)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "v_report";
		$this->load->view('template/template', $data);
	}

	function simpan_sesi()
	{
        $tahunajaran = $this->input->post('tahunajaran');  
		     
        $this->session->set_userdata('tahunajaran', $tahunajaran);
      
		redirect(base_url('keuangan/report_jml/load'));
	}

	function load()
	{
		$a = $this->session->userdata('tahunajaran');
		$data['query'] = $this->app_model->get_jml_sts($a);
		$data['kue'] = $this->app_model->get_sts_cuti($a);
		$data['page'] = "v_detil_report";
		$this->load->view('template/template', $data);
	}

	function detil_jml_aktv($prodi)
	{
		//var_dump($this->session->userdata('tahunajaran'));
		$th = $this->session->userdata('tahunajaran');
		$data['thn'] = $th;
		$this->session->set_userdata('pro',$prodi);
		$data['why'] = $this->app_model->jml_detil($prodi,$th);
		$data['prd'] = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_prodi = "'.$prodi.'"')->row();
		$data['ta'] = $this->db->query('SELECT * from tbl_tahunakademik where kode = "'.$th.'"')->row();
		$data['page'] = "v_jml_detil_aktv";
		$this->load->view('template/template', $data);
	}

	function detil_jml_aktv_old($prodi)
	{
		//var_dump($this->session->userdata('tahunajaran'));
		$th = $this->session->userdata('tahunajaran');
		$data['thn'] = $th;
		$this->session->set_userdata('pro',$prodi);
		$data['why'] = $this->app_model->jml_detil($prodi,$th);
		$data['why'] = $this->app_model->tabs_smtr_renkeu($prodi,$th);
		$data['prd'] = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_prodi = "'.$prodi.'"')->row();
		$data['ta'] = $this->db->query('SELECT * from tbl_tahunakademik where kode = "'.$th.'"')->row();
		$data['page'] = "v_jml_detil_aktv";
		$this->load->view('template/template', $data);
	}

	function view_krs_mhs($kd_krs){				

		$this->db->select('mk.kd_matakuliah,mk.nama_matakuliah,mk.sks_matakuliah');
		$this->db->from('tbl_krs krs');
		$this->db->join('tbl_matakuliah mk', 'krs.kd_matakuliah = mk.kd_matakuliah', 'left');
		$this->db->where('kd_prodi', $this->session->userdata('pro'));
		$this->db->where('kd_krs', $kd_krs);
		$data['rows']=$this->db->get()->result();

		$mhs = $this->db->where('NIMHSMSMHS',substr($kd_krs, 0,12))->get('tbl_mahasiswa', 1)->row();

		$data['nim'] = $mhs->NIMHSMSMHS;
		$data['nama']= $mhs->NMMHSMSMHS;

		$data['page'] = 'v_krs_mhs';
		$this->load->view('template/template', $data);
	}

	function detil_jml_cuti($prd)
	{
		$tah = $this->session->userdata('tahunajaran');
		$data['what'] = $this->app_model->jml_cuti($prd,$tah);
		$data['prdi'] = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_prodi = "'.$prd.'"')->row();
		$data['ta'] = $this->db->query('SELECT * from tbl_tahunakademik where kode = "'.$tah.'"')->row();
		$data['page'] = "v_detil_mhs_cuti";
		$this->load->view('template/template', $data);
	}

	function detil_jml_non($prd){
		$tah = $this->session->userdata('tahunajaran');
		$data['what'] = $this->app_model->jml_non($prd,$tah);
		$data['prdi'] = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_prodi = "'.$prd.'"')->row();
		$data['ta'] = $this->db->query('SELECT * from tbl_tahunakademik where kode = "'.$tah.'"')->row();
		$data['page'] = "v_detil_mhs_non";
		$this->load->view('template/template', $data);
	}

	function print_excel()
	{
		$a = $this->session->userdata('tahunajaran');
		$data['hasil'] = $this->app_model->jml_detil($this->session->userdata('pro'),$a);
		$this->load->view('excel_hasil', $data);
	}

}

/* End of file Report_jml.php */
/* Location: ./application/modules/keuangan/controllers/Report_jml.php */