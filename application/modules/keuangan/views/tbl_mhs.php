<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Keuangan Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/status_ujian/input_data" method="post">
                        <a class="btn btn-success btn-large" href="<?php echo base_url(); ?>keuangan/status_ujian/rekap_data"><i class="btn-icon-only icon-print"> Print</i></a>
                        <a class="btn btn-info btn-large" href="<?php echo base_url(); ?>keuangan/status_ujian/havenotPay"><i class="btn-icon-only icon-print"> Belum Daftar Ulang</i></a>
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                    <th>CUTI</th>
                                    <th>DAFTAR ULANG</th>
                                    <th>UTS</th>
                                    <th>UTS Susulan</th>
                                    <th>UAS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($e as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
                                    <td><?php echo $row->TAHUNMSMHS; ?></td>
                                    <?php $q = $this->db->query("SELECT status from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".$row->NIMHSMSMHS."' and tahunajaran = '".$ta."' ")->row();
                                        if (is_null($q->status)) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>"/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>"/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="4911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                        <?php } elseif ($q->status == 1) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="4911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                        <?php } elseif ($q->status == 2) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="4911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" /></td>
                                        <?php } elseif ($q->status == 3) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="5911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>"/></td>
                                        <?php } elseif ($q->status == 5) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="5911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                        <?php } elseif ($q->status == 8) { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="5911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                        <?php } else { ?>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="8911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek0" name="lunas[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek1" name="lunas[]" value="2911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek3" name="lunas[]" value="3911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" disabled/></td>
                                            <td><input type="checkbox" class="checkbox" id="cek2" name="lunas[]" value="4911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $ta; ?>" checked disabled/></td>
                                        <?php } ?>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>