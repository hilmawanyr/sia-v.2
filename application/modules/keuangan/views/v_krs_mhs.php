<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<?php //var_dump($why);die(); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Detail Kartu Rencana Studi - <?php echo $nama.' ('.$nim.')'?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/status_ujian/input_data" method="post">
                        <table>
                            <tr>
                                <td>Prodi</td>
                                <td> &emsp;:&emsp; </td>
                                <td><?php echo get_jur($this->session->userdata('pro')); ?></td>
                                <td class="span6"></td>
                                <td>Mahasiswa</td>
                                <td> &emsp;:&emsp; </td>
                                <td><?php echo $nama ?></td>
                            </tr>
                            <tr>
                                <td>Tahun Akademik</td>
                                <td>&emsp;:&emsp;</td>
                                <td><?php echo get_thnajar($this->session->userdata('tahunajaran')); ?></td>
                                <td class="span6"></td>
                                <td>Nomor Pokok</td>
                                <td>&emsp;:&emsp;</td>
                                <td><?php echo $nim ?></td>
                            </tr>
                        </table>
                        <br>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>Kode Matakuliah</th>
                                    <th>Nama Matakuliah</th>
                                    <th>SKS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sks=0; $no=1; foreach($rows as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->kd_matakuliah; ?></td>
                                    <td><?php echo $row->nama_matakuliah; ?></td>
                                    <td><?php echo $row->sks_matakuliah; ?></td>
                                </tr>
                                <?php $sks=$sks+$row->sks_matakuliah; $no++; } ?>
                            </tbody>
                            <tfoot>
                                <tr> 
                                    <td colspan="3">Total SKS</td>
                                    <td ><?php echo $sks; ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>