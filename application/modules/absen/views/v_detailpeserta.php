<!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
 -->
<script>
function edit(id){
$('#absen').load('<?php echo base_url();?>akademik/ajar/view_absen/'+id);
}
</script>

<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dosen Mengajar</h3>

            </div> <!-- /widget-header -->

            
            <?php // echo $id; ?>
            <div class="widget-content">

                <div class="span11">

                   <!-- <a href="<?php //echo base_url();?>akademik/ajar/cetak/" class="btn btn-success "><i class="btn-icon-only icon-print"> </i> Export Excel</a><hr> -->

                    <form class ='form-horizontal' action="<?= base_url('absen/absenDosen/saveMeeting/'.$id); ?>" method="post">
                          
                            <div class="control-group" id="">
                                <script>
                                    $(function() {
                                        $( "#tgl" ).datepicker({
                                            changeMonth: true,
                                            changeYear: true,
                                            dateFormat: "yy-mm-dd",
                                            minDate: -200,
                                            maxDate: 0,
                                        });
                                        $("#tgl").keydown(function (event) {
                                            event.preventDefault();
                                        });
                                    });
                                </script>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#RemoveAll").click(function() {
                                            if ($(this).is(':checked'))
                                                $(".idabsen1").attr("checked", "checked");
                                            else
                                                $(".idabsen1").removeAttr("checked");
                                        });
                                    });
                                </script>
                                <fieldset>
                                    <div class="control-group" style="margin-left: -20px;">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>NAMA</td>
                                                    <td> : <?php echo nama_dsn($onup['kd_dosen']); ?></td>
                                                    <td width="80"></td>
                                                    <td>NID</td>
                                                    <td> : <?php echo $onup['kd_dosen']; ?></td>
                                                </tr>
                                                <tr>
                                                    <?php $prodi = substr($onup['kd_jadwal'], 0,5) ?>
                                                    <td>MATAKULIAH</td>
                                                    <td> : <?= get_nama_mk($onup['kd_matakuliah'],$prodi); ?></td>
                                                    <td width="80"></td>
                                                    <td>KELAS</td>
                                                    <td> : <?php echo $onup['kelas']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        <label class="control-label">Pertemuan Ke</label>
                                        <div class="controls">
                                            <input class="span2" type="text" value="<?php echo $meet; ?>" required readonly=""/>
                                        </div>
                                        <label class="control-label">Tanggal Pertemuan</label>
                                        <div class="controls">
                                            <input class="span2" type="text" value="<?php echo TanggalIndo(date('Y-m-d')); ?>" required readonly=""/>
                                            <input type="hidden" name="tgl" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                        <label class="control-label">Hadir Semua</label>
                                        <div class="controls">
                                            <input type="checkbox" id="RemoveAll" name="absenall"/>
                                        </div>
                                        <hr>
                                        <label class="control-label">MATERI PEMBAHASAN</label>
                                        <div class="controls">
                                            <textarea name="bahas" style="width:500px" required=""></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                                <table id="" class="table table-bordered table-striped">
                                    <thead>
                                        <tr> 
                                            <th>No</th>
                                            <th width="90">NIM</th>
                                            <th>NAMA</th>
                                            <th width="20">HADIR</th>
                                            <th width="20">SAKIT</th>
                                            <th width="20">IZIN</th>
                                            <th width="20">ALPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($kelas as $value) { ?>
                                        <tr>
                                            <td><?php echo number_format($no); ?></td>
                                            <td><?php echo $value->npm_mahasiswa; ?></td>
                                            <td><?php echo get_nm_mhs($value->npm_mahasiswa); ?></td>
                                            <td><input type="radio" class="idabsen1" name="absen<?php echo $no; ?>[]" value="H-<?php echo $value->npm_mahasiswa; ?>" required/></td>
                                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="S-<?php echo $value->npm_mahasiswa; ?>"/></td>
                                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="I-<?php echo $value->npm_mahasiswa; ?>"/></td>
                                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="A-<?php echo $value->npm_mahasiswa; ?>"/></td>
                                        </tr>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                                <input type="hidden" name="jumlah" value="<?php echo $no; ?>" />
                               
                            </div>              
                        
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Simpan"/>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

</div>