<?php
    if ($kania->num_rows() > 0) {
        $kdmk = get_dtl_jadwal($kania->row()->kd_jadwal)['kd_matakuliah'];
        $kels = get_dtl_jadwal($kania->row()->kd_jadwal)['kelas'];
        $nmmk = get_nama_mk($kdmk,substr($kania->row()->kd_jadwal, 0,5));
        $keyy = md5($kania->row()->kd_jadwal.$kania->row()->pertemuan);
    }
 ?>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
    <?php if ($kania->num_rows() > 0) { ?>
        <h4 class="modal-title"><?php echo $nmmk.' - '.$kels.' - pertemuan '.$kania->row()->pertemuan ?></h4>
    <?php } ?>
</div>

<div class="modal-body">
    <table class="table table-stripped">
    	<thead>
    		<tr>
    			<th>No</th>
    			<th>Nama</th>
    			<th>Status Kehadiran</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php if ($kania->num_rows() > 0) { ?>
    			
    			<?php $no = 1; foreach ($kania->result() as $key) { ?>
	    			<tr>
		    			<td><?php echo $no; ?></td>
		    			<td><?php echo get_nm_mhs($key->npm_mahasiswa); ?></td>
		    			<td><?php echo $key->kehadiran; ?></td>
		    		</tr>
	    		<?php $no++; } ?>

    		<?php } else {  ?>
    			<tr>
    				<td colspan="3"><b><i>Dosen belum meng-<i>input</i> pertemuan hari ini.</i></b></td>
    			</tr>
    		<?php } ?>
    	</tbody>
    </table>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <?php if ($kania->num_rows() > 0) {
        echo $btnvalid;
    } ?>
</div>