<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>404 - Siakad UBJ</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
	    
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	
	<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
	    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	    
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span

                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#"><img src="<?php echo base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">

            <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK UBHARA JAYA</span></a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="<?php echo base_url();?>" class="">
							<i class="icon-chevron-left"></i>
							Back to Home
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="container">
	
	<div class="row">
		
		<div class="span12">
			
			<div class="error-container">
				<h1>404</h1>
				
				<h2>Halaman yang anda minta tidak tersedia.</h2>
				
				<div class="error-details">
					Mohon cek kembali url anda, atau anda dapat langsung kembali ke <a href="<?php echo base_url();?>">home page</a> , Terimakasih!
					
				</div> <!-- /error-details -->
				
				<div class="error-actions">
					<a href="<?php echo base_url();?>" class="btn btn-large btn-primary">
						<i class="icon-chevron-left"></i>
						&nbsp;
						Back to Home						
					</a>
					
					
					
				</div> <!-- /error-actions -->
							
			</div> <!-- /error-container -->			
			
		</div> <!-- /span12 -->
		
	</div> <!-- /row -->
	
</div> <!-- /container -->


<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

</body>

</html>
