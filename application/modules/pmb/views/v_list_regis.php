<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-calendar"></i>
                <h3>Data Pendaftar Per Tanggal <i><?php if($this->session->userdata('tgl_mulai') == $this->session->userdata('tgl_ahir')){echo $this->session->userdata('tgl_mulai');}else{echo $this->session->userdata('tgl_mulai').' - '.$this->session->userdata('tgl_ahir');} ?></i></h3>
            </div>
            <div class="widget-content">
                <form class="form-horizontal" action="<?php echo base_url(); ?>pmb/regist/update_stat" method="post">
                    <a href="<?php echo base_url('pmb/regist/destroy_date'); ?>" class="btn btn-warning"><i class="icon icon-chevron-left"></i> Kembali</a>
                    <input type="submit" value="Submit" class="btn btn-success">
                    <br><br>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th width="30">No</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>E-mail</th>
                                <th>Telepon</th>
                                <th width="40" style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($qry as $row) { ?>
                            <?php if ($row->status == 1) { $back = 'background:#7FFFD4'; } else { $back = ''; }?>
                            <tr>
                                <td style="<?php echo $back; ?>"><?php echo $no; ?></td>
                                <td style="<?php echo $back; ?>"><?php echo $row->nm_depan.' '.$row->nm_belakang; ?></td>
                                <td style="<?php echo $back; ?>"><?php echo $row->nik; ?></td>
                                <td style="<?php echo $back; ?>"><?php echo $row->email; ?></td>
                                <td style="<?php echo $back; ?>"><?php echo $row->tlp; ?></td>
                                <?php if ($row->status == 1) { ?>
                                    <td style="text-align:center;<?php echo $back; ?>"><i class="icon icon-check"></i></td>
                                <?php } else {  ?>
                                    <td style="text-align:center"><input type="checkbox" name="id[]" class="form-control" value="<?php echo $row->nik; ?>"></td>
                                <?php } ?>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>             