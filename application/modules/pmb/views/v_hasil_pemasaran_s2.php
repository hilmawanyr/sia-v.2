<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Hasil Test Calon Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                        <a class="btn btn-success btn-large" href="<?php echo base_url(); ?>pmb/hasil_tes/dwld2"><i class="btn-icon-only icon-print"></i>Print</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th width="30">No</th>
                                    <th>Nama Gelombang</th>
                                    <th>Peserta</th>
                                    <th>lulus</th>
                                    <th width="40">lihat</th>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($pong as $row) { ?>
                                <tr>
                                    <?php $k =  substr($row->ID_registrasi, 0,7); $j = substr($k, 6); ?>
                                    <td><?php echo number_format($no); ?></td>
                                    <td><?php echo 'GELOMBANG '.$j; ?></td>
                                    <?php ?>
                                    <td><?php echo $row->nama; ?></td>
                                    <?php ?>
                                    <td><?php if ($row->status == 1) {
                                        echo "LULUS";
                                    } elseif($row->status == 0) {
                                        echo "TIDAK LULUS";
                                    }
                                     ?>
                                     </td>
                                    <td><a href="" class="btn btn-success"><i class="icon-check"></i></a></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </div>
</div>
