<script>
function edit(id){
$('#myModal1').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap/'+id);
}

function edit_edit(kk){
$('#absen').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap_2016/'+kk);
}
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Calon Mahasiswa <?php echo $progr; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<a href="<?php echo base_url('pmb/peserta_pmb/breaksess'); ?>" class="btn btn-warning" title="Kembali"> << Kembali</a>
				<?php
				if ($tahun == date('Y')) { ?>
					<a href="<?php echo base_url('pmb/peserta_pmb/dwld'); ?>" class="btn btn-success"><i class="icon-excel"></i> Download File Excel</a> 
					<a data-toggle="modal" href="#myModal" class="btn btn-primary"><i class="icon-excel"></i> Upload File Kelulusan</a>
				<?php } ?>
                    <!-- <a href="<?php //echo base_url(); ?>datas/mahasiswapmb/printexcels1" class="btn btn-primary"><i class="icon-excel"></i> Print All</a> --><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
	                        	<th>ID Registrasi</th>
                                <th>Nama</th>
	                            <?php if ($progr == 'S1') { ?>
	                            	<th width="130">Asal Sekolah</th>
	                            <?php } ?>
                                <th width="120">Program Pilihan</th>
                                <th>Gelombang</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($list->result() as $row) { ?>
	                        <tr>
                                <td><?php echo $no;?></td>

                                <?php if ($progr == 'S1') { ?>
	                        		<td><?php echo $row->nomor_registrasi;?></td>
	                        	<?php } else { ?>
	                        		<td><?php echo $row->ID_registrasi;?></td>
	                        	<?php } ?>

	                        	<td><?php echo $row->nama;?></td>

	                        	<?php if ($progr == 'S1') { ?>
	                        		<td><?php echo $row->asal_sch_maba; ?></td>
	                        	<?php } ?>

	                        	<?php if ($progr == 'S1') { ?>
	                        		<td><?php echo get_jur($row->prodi);?></td>
	                        	<?php } else { ?>
	                        		<td><?php echo get_jur($row->opsi_prodi_s2);?></td>
	                        	<?php } ?>
	                        	
	                        	<td><?php echo $row->gelombang; ?></td>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script>
   $(document).ready(function() {
     $( "#tgl" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: "yy-mm-dd"
      });
   });
      
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Upload Data</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>pmb/peserta_pmb/update_byupload" method="post" enctype="multipart/form-data">
            	    
				<input type="hidden" name="prg" value="<?php echo $progr; ?>" placeholder="">
				<input type="hidden" name="glm" value="<?php echo $gelms; ?>" placeholder="">
                <div class="modal-body" style="margin-left: 30px;">   
	                <div class="control-group" id="">
	                	<label class="control-label">File (format <b>.xls</b>)</label>
	                	<div class="controls">
		                	<input type="file" id="" name="userfile" class="form-control">
		                </div>
	                </div>
				</div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->