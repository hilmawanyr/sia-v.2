<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs_baru_all_program.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>Nomor Registrasi</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Agama</th>
            <th>Asal Sekolah</th>
            <th>Kota Asal Sekolah</th>
            <th>Penghasilan</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Prodi</th>
            <th width="120">Gelombang</th>
            <th>Telepon</th>
            <th>Pekerjaan</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($satu as $row){?>
        <tr>
        	<td><?php echo $no; ?></td>
            <td><?php echo $row->nomor_registrasi; ?></td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo $row->kelamin; ?></td>
            <td><?php echo $row->agama; ?></td>
            <td><?php echo $row->asal_sch_maba; ?></td>
            <td><?php echo $row->kota_sch_maba; ?></td>
            <td>
                <?php if ($row->penghasilan == 1) {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif($row->penghasilan == 2) {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == 3) {
                    echo "Rp 4,100,000 - 5,999,000";
                } elseif ($row->penghasilan == 4) {
                    echo ">= Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td>
                <?php if ($row->status < 1) {
                    echo "Registrasi";
                } elseif($row->status == 1) {
                    echo "Lulus Tes";
                } elseif ($row->status > 1) {
                    echo "Daftar Ulang";
                }?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td>
                <?php
                switch ($row->prodi) {
                    case '55201':
                        $nama_prodi = 'Teknik Informatika'; 
                        break;
                    case '25201':
                        $nama_prodi = 'Teknik Lingkungan'; 
                        break;
                    case '26201':
                        $nama_prodi = 'Teknik Industri'; 
                        break;
                    case '24201':
                        $nama_prodi = 'Teknik Kimia'; 
                        break;
                    case '32201':
                        $nama_prodi = 'Teknik Perminyakan'; 
                        break;
                    case '62201':
                        $nama_prodi = 'Akuntansi'; 
                        break;
                    case '61201':
                        $nama_prodi = 'Manajemen'; 
                        break;
                    case '73201':
                        $nama_prodi = 'Psikologi'; 
                        break;
                    case '74201':
                        $nama_prodi = 'Ilmu Hukum'; 
                        break;
                    case '70201':
                        $nama_prodi = 'Ilmu Komunikasi'; 
                        break;
                }
                echo $nama_prodi;
                ?>
            </td>
            <td><?php echo $row->gelombang; ?></td>
            <td><?php echo $row->tlp; ?></td>
        </tr>
		<?php $no++; } ?>
        <tr>
            <td colspan="10" style="background:yellow;text-align:center;">PROGRAM PASCA SARJANA</td>
        </tr>
        <?php $noo = 1; foreach($dua as $row){?>
        <tr>
            <td><?php echo $noo; ?></td>
            <td><?php echo $row->ID_registrasi; ?></td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo $row->kelamin; ?></td>
            <td><?php echo $row->agama; ?></td>
            <td><?php echo $row->nm_univ; ?></td>
            <td><?php echo $row->kota_asl_univ; ?></td>
            <td>
                <?php if ($row->penghasilan == "1-2") {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif ($row->penghasilan == "2,1-4") {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == "4,1-6") {
                    echo "Rp 4,100,000 - 6,000,000";
                } elseif ($row->penghasilan == ">6") {
                    echo "> Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td>
                <?php if ($row->status < 1) {
                    echo "Registrasi";
                } elseif($row->status == 1) {
                    echo "Lulus Tes";
                } elseif ($row->status > 1) {
                    echo "Daftar Ulang";
                }?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td>
                <?php switch ($row->opsi_prodi_s2) {
                    case '61101':
                        $prods = 'Magister Manajemen';
                        break;
                    case '74101':
                        $prods = 'Magister Hukum';
                        break;
                }
                echo $prods; ?>
            </td>
            <td><?php echo $row->gelombang; ?></td>
            <td><?php echo $row->tlp; ?></td>
            <td><?php echo $row->pekerjaan; ?></td>
        </tr>
        <?php $noo++; } ?>
    </tbody>
</table>