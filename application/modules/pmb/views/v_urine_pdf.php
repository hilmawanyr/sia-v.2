<?php
//ob_start();
error_reporting(0);
$pdf = new FPDF("P","mm", "A5");
$now = dateIdn(date('Y-m-d'));
$now = explode(' ',$now);
date_default_timezone_set('Asia/Jakarta'); 
foreach($query as $row){

$address=explode(',',$row->alamat);
if (!empty($address[4]) or !empty($address[5])){
	$kota=$address[4];
	$jl=$address[5];
}else{
	$kota='';
	$jl='';
}
/* echo "<pre>";
print_r($address);
echo "</pre>"; */
if ($row->kelamin == 'P'){
	$jenkel = 'Perempuan';
}elseif($row->kelamin == 'L'){
	$jenkel = 'Laki-laki';
}else{
	$jenkel='';
}

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(0, 0 ,0);

$pdf->SetFont('Arial','',10); 
$pdf->setY(5);
$pdf->Cell(108,5,'KODE POT',0,0,'R');
$pdf->Cell(30,5,'',1,0,'C');
$pdf->Cell(10,5,'',0,1,'C');

$pdf->SetFont('Arial','B',12); 
$pdf->image(base_url().'/logo/bw_ubj.png',64,12,20);

$pdf->setY(33);
$pdf->Cell(148,5,'FORMULIR PENGAMBILAN SAMPEL URINE',0,1,'C');
$pdf->Cell(148,5,'PEMERIKSAAN NARKOBA',0,1,'C');
$pdf->Cell(148,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');
$pdf->Line(10,50,138,51);
$pdf->Line(10,51,138,52);

$pdf->ln(5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'1.',0,0,'L');
$pdf->Cell(35,5,'Nama Lengkap',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,$row->nama,0,1,'L');


$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'2.',0,0,'L');
$pdf->Cell(35,5,'Tempat, Tanggal Lahir',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,$row->tempat_lahir.', '.dateIdn($row->tgl_lahir),0,1,'L');

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'3.',0,0,'L');
$pdf->Cell(35,5,'Alamat Lengkap',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->multiCell(87,5,$jl.' RT: '.$address[0].' RW: '.$address[1].' '.$address[2].' '.$address[3].' '.$kota,0,1);

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'4.',0,0,'L');
$pdf->Cell(35,5,'Jenis Kelamin',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,$jenkel,0,1,'L');

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'5.',0,0,'L');
$pdf->Cell(35,5,'Nama Orangtua',0,1,'L');

$pdf->Cell(14,5,'',0,0,'L');
$pdf->Cell(5,5,'a.',0,0,'L');
$pdf->Cell(30,5,'Ayah',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,$row->nm_ayah,0,1,'L');

$pdf->Cell(14,5,'',0,0,'L');
$pdf->Cell(5,5,'b.',0,0,'L');
$pdf->Cell(30,5,'Ibu',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,$row->nm_ibu,0,1,'L');

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'6.',0,0,'L');
$pdf->Cell(35,5,'Fakultas / Prodi',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,get_fak_byprodi($row->prodi).' / '.get_jur($row->prodi),0,1,'L');

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'7.',0,0,'L');
$pdf->Cell(35,5,'Hasil Tes Urine',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(1,5,'',0,0,'C');
$pdf->Cell(20,5,'- THC','BTL',0,'L');
$pdf->Cell(2,5,':','BT',0,'C');
$pdf->Cell(30,5,'+','BTR',0,'C');
$pdf->Cell(30,5,'-','BTR',1,'C');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(20,5,'- MOP','BL',0,'L');
$pdf->Cell(2,5,':','B',0,'C');
$pdf->Cell(30,5,'+','BR',0,'C');
$pdf->Cell(30,5,'-','BR',1,'C');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(20,5,'- BZO','BL',0,'L');
$pdf->Cell(2,5,':','B',0,'C');
$pdf->Cell(30,5,'+','BR',0,'C');
$pdf->Cell(30,5,'-','BR',1,'C');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(20,5,'- AMP','BL',0,'L');
$pdf->Cell(2,5,':','B',0,'C');
$pdf->Cell(30,5,'+','BR',0,'C');
$pdf->Cell(30,5,'-','BR',1,'C');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(20,5,'- MET','BL',0,'L');
$pdf->Cell(2,5,':','B',0,'C');
$pdf->Cell(30,5,'+','BR',0,'C');
$pdf->Cell(30,5,'-','BR',1,'C');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(20,5,'- COC','BL',0,'L');
$pdf->Cell(2,5,':','B',0,'C');
$pdf->Cell(30,5,'+','BR',0,'C');
$pdf->Cell(30,5,'-','BR',1,'C');

$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(4,5,'8.',0,0,'L');
$pdf->Cell(35,5,'Keterangan',0,0,'L');
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(87,5,'Apabila sakit, obat apa yang sedang dikonsumsi?',0,1,'L');
$pdf->Cell(52,5,'',0,0,'L');
$pdf->Cell(85,5,'','B',1,'L');


$pdf->setY(-60);
$pdf->SetFont('Arial','',8);
$pdf->Cell(95,5,'',0,0,'L');
$pdf->Cell(13,5,'Bekasi,',0,0,'R');
$pdf->Cell(25,5,$now[1].' '.$now[2],0,1,'R');
$pdf->Cell(100,5,'',0,0,'L');
$pdf->Cell(38,5,'Tanda Tangan',0,1,'C');

$pdf->ln(10);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(100,5,'',0,0,'L');
$pdf->Cell(38,5,$row->nama,0,1,'C');

$pdf->ln(5);
$pdf->SetFont('Arial','',6);
$pdf->Cell(148,8,'copyright - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'C');

}

//exit();
$pdf->Output('Hasil_Tes_Urine_'.$prodd.'_'.date('ymd_his').'.PDF','I');

?>

