<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('A6:I7')->applyFromArray($BStyle);


$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Data Nilai PMB');
//header
$excel->getActiveSheet()->setCellValue('B1', 'NILAI UJIAN MASUK UNIVERSITAS BHAYANGKARA TAHUN '.$ta);
$excel->getActiveSheet()->setCellValue('B2', 'PROGRAM STUDI '.get_jur($prodi));
$excel->getActiveSheet()->setCellValue('B3', 'GELOMBANG '.$gel);
//$getta = $this->app_model->getdetail('tbl_tahunakademik','kode',$rows->kd_tahunajaran,'kode','asc')->row();



//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'NO');
$excel->getActiveSheet()->setCellValue('B6', 'NOMOR REGISTRASI');
$excel->getActiveSheet()->setCellValue('C6', 'NAMA');
$excel->getActiveSheet()->setCellValue('D6', 'NILAI UJIAN');
$excel->getActiveSheet()->setCellValue('D7', 'TPA');
$excel->getActiveSheet()->setCellValue('E7', 'MTK');
$excel->getActiveSheet()->setCellValue('F7', 'B.IND');
$excel->getActiveSheet()->setCellValue('G7', 'B.ING');
$excel->getActiveSheet()->setCellValue('H6', 'TOTAL');
$excel->getActiveSheet()->setCellValue('I6', 'KELULUSAN');


$r= 8;
$r1=$r;
$n=1;

foreach ($camaba as $isi) {

	if (is_null($isi->tpa) or $isi->tpa == '') {
		$tpa = 0;
	}else{
		$tpa = $isi->tpa;
	}

	if (is_null($isi->mtk) or $isi->mtk == '') {
		$mtk = 0;
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	}else{
		$mtk = $isi->mtk;
	}

	if (is_null($isi->ind) or $isi->ind == '') {
		$ind = 0;
	}else{
		$ind = $isi->ind;
	}

	if (is_null($isi->ing) or $isi->ing == '') {
		$ing = 0;
	}else{
		$ing = $isi->ing;
	}

	$rata2 = '=(D'.$r.'+E'.$r.'+F'.$r.'+G'.$r.')/4';
	$lls = '=IF(H'.$r.'>70,"LULUS","TIDAK LULUS")';

	$excel->getActiveSheet()->setCellValue('A'.$r, $n);
	$excel->getActiveSheet()->setCellValue('B'.$r, $isi->nomor);
	$excel->getActiveSheet()->setCellValue('C'.$r, $isi->nama);
	$excel->getActiveSheet()->setCellValue('D'.$r, $tpa);
	$excel->getActiveSheet()->setCellValue('E'.$r, $mtk);
	$excel->getActiveSheet()->setCellValue('F'.$r, $ind);
	$excel->getActiveSheet()->setCellValue('G'.$r, $ing);
	$excel->getActiveSheet()->setCellValue('H'.$r, $rata2);
	$excel->getActiveSheet()->setCellValue('I'.$r, $lls);

	$r++;$n++;
}

$r2=$r1+$n-2;
$r++;


$xx = 8;
$xw = $xx - 1;
$xy = $xx + 1;
//footer
$excel->getActiveSheet()->setCellValue('B'.$r.'', '*Harap Memasukkan Nilai Maksimal 2 Angka Di Belakang Koma');


	$a = $xy+1; 
	$b = $xy+2;
	$c = $xy+3;
	$d = $xy+4;
	


$e = $xy+5;
//merge cell
$excel->getActiveSheet()->mergeCells('B'.$r.':I'.$r.'');
$excel->getActiveSheet()->mergeCells('L'.$a.':N'.$a.'');
$excel->getActiveSheet()->mergeCells('L'.$d.':N'.$d.'');
$excel->getActiveSheet()->mergeCells('B1:M1');
$excel->getActiveSheet()->mergeCells('B2:M2');
$excel->getActiveSheet()->mergeCells('B3:M3');
$excel->getActiveSheet()->mergeCells('E2:H2');
$excel->getActiveSheet()->mergeCells('H3:H4');
$excel->getActiveSheet()->mergeCells('A6:A7');
$excel->getActiveSheet()->mergeCells('B6:B7');
$excel->getActiveSheet()->mergeCells('C6:C7');
$excel->getActiveSheet()->mergeCells('D6:G6');//NILAI UJIAN
$excel->getActiveSheet()->mergeCells('H6:H7');
$excel->getActiveSheet()->mergeCells('I6:I7');


//change the font size
$excel->getActiveSheet()->getStyle('B1:M3')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('B1:M3')->getFont()->setSize(14);

$excel->getActiveSheet()->getStyle('A6:I7')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A6:I7')->getFont()->setSize(12);

$excel->getActiveSheet()->getStyle('B'.$r.':I'.$r.'')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('B'.$r.':I'.$r.'')->getFont()->setSize(12);

//border
$excel->getActiveSheet()->getStyle('A'.$r1.':I'.$r2.'')->applyFromArray($BStyle);

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("A6:N7")->applyFromArray($style);


$filename = 'Form_Nilai_PMB_'.$prodi.'_GEL'.$gel.'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>