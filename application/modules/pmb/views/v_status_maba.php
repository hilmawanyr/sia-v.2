<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename='data_mhs_baru_s1.xls'");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>Nomor Registrasi</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Agama</th>
            <th>Negeri/Swasta</th>
            <th>Asal Sekolah</th>
            <th>Kota Asal Sekolah</th>
            <th>Penghasilan</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Prodi</th>
            <th width="120">Gelombang</th>
            <th>Telepon</th>
            <th>Pekerjaan</th>
            <th>Pekerjaan Orang Tua</th>
            <th>Berkas</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($wow as $row){?>
        <tr>
        	<td><?php echo $no; ?></td>
        	<td><?php echo $row->nomor_registrasi; ?></td>
        	<td><?php echo $row->nama; ?></td>
            <td><?php echo $row->kelamin; ?></td>
            <td><?php echo $row->agama; ?></td>
            <?php if ($row->kategori_skl == 'ngr') {
                $jadi = 'Negeri';
            } else {
                $jadi = 'Swasta';
            }
             ?>
            <td><?php echo $jadi;?></td>
            <td><?php echo $row->asal_sch_maba; ?></td>
            <td><?php echo $row->kota_sch_maba; ?></td>
            <td>
                <?php if ($row->penghasilan == 1) {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif($row->penghasilan == 2) {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == 3) {
                    echo "Rp 4,100,000 - 5,999,000";
                } elseif ($row->penghasilan == 4) {
                    echo ">= Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td>
                <?php if ($row->status < 1) {
                    echo "Registrasi";
                } elseif($row->status == 1) {
                    echo "Lulus Tes";
                } elseif ($row->status > 1) {
                    echo "Daftar Ulang";
                }?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td>
                <?php
                switch ($row->prodi) {
                    case '55201':
                        $nama_prodi = 'Teknik Informatika'; 
                        break;
                    case '25201':
                        $nama_prodi = 'Teknik Lingkungan'; 
                        break;
                    case '26201':
                        $nama_prodi = 'Teknik Industri'; 
                        break;
                    case '24201':
                        $nama_prodi = 'Teknik Kimia'; 
                        break;
                    case '32201':
                        $nama_prodi = 'Teknik Perminyakan'; 
                        break;
                    case '62201':
                        $nama_prodi = 'Akuntansi'; 
                        break;
                    case '61201':
                        $nama_prodi = 'Manajemen'; 
                        break;
                    case '73201':
                        $nama_prodi = 'Psikologi'; 
                        break;
                    case '74201':
                        $nama_prodi = 'Ilmu Hukum'; 
                        break;
                    case '70201':
                        $nama_prodi = 'Ilmu Komunikasi'; 
                        break;
                }
                echo $nama_prodi;
                ?>
            </td>
            <td><?php echo $row->gelombang; ?></td>
            <td><?php echo $row->tlp; ?></td>
            <?php if ($tahun < 2018) { ?>
                    <td><?php echo $row->perkerjaan; ?></td>
            <?php } else { ?>
                    <td><?php echo $row->pekerjaan; ?></td>
            <?php } ?>
              
            <?php
                if ($row->workdad == 'LL') {
                    $parentwork = 'Lain-lain';
                } elseif ($row->workdad == 'PN') {
                    $parentwork  = 'Pegawai Negeri';
                } elseif ($row->workdad == 'PS') {
                    $parentwork = 'Pegawai Swasta';
                } elseif ($row->workdad == 'PSN') {
                    $parentwork = 'Pensiun';
                } elseif ($row->workdad == 'TK') {
                    $parentwork = 'Tidak Bekerja';
                } elseif ($row->workdad == 'TP') {
                    $parentwork = 'TNI / POLRI';
                } elseif ($row->workdad == 'WU') {
                    $parentwork = 'Wirausaha';
                }
            ?>
            <td><?php echo $parentwork ?></td>
            <?php $ask = $this->db->query("SELECT status_kelengkapan from tbl_form_camaba where ((status_kelengkapan LIKE '%FT%' AND status_kelengkapan LIKE '%SKL%' AND status_kelengkapan LIKE '%IJZ%' AND status_kelengkapan LIKE '%SKHUN%' AND status_kelengkapan LIKE '%RP%' AND status_kelengkapan LIKE '%KTP%' AND status_kelengkapan LIKE '%KK%' AND status_kelengkapan LIKE '%AKT%') OR status_kelengkapan LIKE '%LLKP%') AND nomor_registrasi = '".$row->nomor_registrasi."'")->result();
                if ($ask == true) { ?>
                    <td>LENGKAP</td>
            <?php } else { ?>
                    <td> - </td>
            <?php } ?>
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>