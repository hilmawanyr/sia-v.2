<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Rekapitulasi</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/rekap_jml/load" method="post" enctype="multipart/form-data">
					<fieldset>

						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Jenis</label>
							<div class="controls">
								<select class="form-control span4" name="jns" required/>
									<option disabled="" selected="">--Pilih Jenis--</option>
									<option value="MB">Mahasiswa Baru</option>
									<option value="RM">Readmisi</option>
									<option value="KV">Konversi</option>
									<option value="AL">Semua</option>
								</select>
							</div>
						</div>

						<?php $sesi = $this->session->userdata('sess_login'); if ($sesi['id_user_group'] != 8) { ?>
							<div class="control-group">
								<label class="control-label">Program</label>
								<div class="controls">
									<select class="form-control span4" name="program"/>
										<option disabled="" selected="">--Pilih Program--</option>
										<option value="S1">S1</option>
										<option value="S2">S2</option>
									</select>
								</div>
							</div>
						<?php } ?>

						<?php $sesi = $this->session->userdata('sess_login');
						if ($sesi['userid'] == '74101' || $sesi['userid'] == '62101') { ?>
						 	<input type="hidden" name="program" value="S2">
						<?php } else { ?>
						 	<input type="hidden" name="program" value="S1">
						<?php } ?>

						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="ALL">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select class="form-control span4" name="status" required/>
									<option disabled="" selected="">--Pilih Status--</option>
									<option value="IS NULL">Daftar / Registrasi</option>
									<option value="=1">Lulus Tes</option>
									<option value=">1">Daftar Ulang</option>
									<option value="ALL">Semua</option>
								</select>
							</div>
						</div>

						<?php 
							$mari = $this->session->userdata('sess_login');
							$pecah = explode(',', $mari['id_user_group']);
							$jmlh = count($pecah);
							for ($i=0; $i < $jmlh; $i++) { 
								$grup[] = $pecah[$i];
							}
							if ((in_array(8, $grup))) { ?>
								<input type="hidden" name="pro" value="<?php echo $pr->kd_prodi; ?>" placeholder="">
							<?php } elseif ((in_array(9, $grup)) || (in_array(10, $grup)) || (in_array(14, $grup)) || (in_array(1, $grup))) { ?>
								<div class="control-group">
									<label class="control-label">Prodi</label>
									<div class="controls">
										<select class="form-control span4" name="pro" required/>
											<option disabled="" selected="">--Pilih Prodi--</option>
											<?php foreach ($pr as $key) { ?>
												<option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
											<?php } ?>									
										</select>
									</div>
								</div>
						<?php } ?>

						<?php $sesi = $this->session->userdata('sess_login');
						if ($sesi['userid'] == '74101' || $sesi['userid'] == '62101') { ?>
						 	<div class="control-group">
								<label class="control-label">Kategori</label>
								<div class="controls">
									<select class="form-control span4" name="ktg" required/>
										<option disabled="" selected="">--Pilih Kategori--</option>
										<option value="wrk">Pekerjaan</option>
										<option value="cty">Asal Kota</option>
										<option value="grd">Tahun Lulus</option>
									</select>
								</div>
							</div>
						<?php } else { ?>
						 	<div class="control-group">
								<label class="control-label">Kategori</label>
								<div class="controls">
									<select class="form-control span4" name="ktg" required/>
										<option disabled="" selected="">--Pilih Kategori--</option>
										<option value="edu0">Pendidikan Orang Tua (ayah)</option>
										<option value="wrk0">Pekerjaan Orang Tua (ayah)</option>
										<option value="edu1">Pendidikan Orang Tua (ibu)</option>
										<option value="wrk1">Pekerjaan Orang Tua (ibu)</option>
										<option value="sch">Kategori Sekolah Asal</option>
										<option value="cty">Asal Kota</option>
										<option value="grd">Lulus Sekolah</option>
									</select>
								</div>
							</div>
						<?php } ?>
						
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
							<!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

