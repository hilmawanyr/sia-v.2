<?php $head= $this->session->userdata('sess_form'); ?>

<div class="span12">                    
    <div class="widget ">
        <div class="widget-header">
            <i class="icon-list"></i>
            <h3>Data Calon Mahasiswa Baru <?php echo $head['ta'] ?></h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="span11">
                <!-- <button class="btn btn-success" id="select_all"><i class="icon icon-ok"></i> Ceklis Semua Mahasiswa</button> -->
            <b><center>INPUT NILAI PMB GELOMBANG <?php echo $head['gel']; ?> </center></b><br>
            <a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai </a>
			<a target="_blank" href="<?php echo base_url(); ?>pmb/nilai/dwld_excel/" class="btn btn-success"> Download Form </a>
			<!-- <a href="<?php echo base_url(); ?>form/formnilai/cetak_nilai/" target="_blank" class="btn btn-success"> Cetak </a> -->
                <hr>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr> 
                            <th width="5%">No</th>
                            <th width="18%">Nomor</th>
                            <th>Nama</th>
                            <th width="5%">TPA</th>
                            <th width="5%">MTK</th>
                            <th width="5%">B.IND</th>
                            <th width="5%">B.ING</th>
                            <th width="5%">TOTAL</th>
                            <th width="15%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=0; foreach ($camaba as $mhs): ?>
                        <?php $no++; echo 
                            '<tr>
                                <td width="5%">'.$no.'</td>
                                <td width="18%">'.$mhs->nomor.'</td>
                                <td>'.$mhs->nama.'</td>';

                            echo ' 
                                <td width="5%">'.zerotodash($mhs->tpa).'</td>
                                <td width="5%">'.zerotodash($mhs->mtk).'</td>
                                <td width="5%">'.zerotodash($mhs->ind).'</td>
                                <td width="5%">'.zerotodash($mhs->ing).'</td>
                                <td width="5%">'.zerotodash($mhs->total).'</td>
                                <td width="15%">'.lulus($mhs->kelulusan).'</td>
                            </tr>';
                            
                        ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM UPLOAD NILAI</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>pmb/nilai/upload_excel" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="control-group" id="">
                        <label class="control-label">Upload Data Nilai (.xls) </label>
                        <div class="controls">
                            <input type="file" class="span4 form-control" name="userfile" required/>
                            <input type="hidden" name="ta" value="<?php echo $head['ta'] ?>" />
                            <input type="hidden" name="prodi" value="<?php echo $head['prodi']; ?>">
                            <input type="hidden" name="gel" value="<?php echo $head['gel']; ?>">
                            <input type="hidden" name="peserta" value="<?php echo $no; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                  <input type="submit" class="btn btn-primary" value="Submit"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->