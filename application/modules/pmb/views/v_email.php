<html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <meta name="viewport" content="width=device-width">
    <style type="text/css">
      @media only screen and (min-width: 620px) {
        .wrapper{min-width:600px !important}
        .wrapper h1{}
        .wrapper h1{font-size:64px !important;line-height:63px !important}
        .wrapper h2{}
        .wrapper h2{font-size:30px !important;line-height:38px !important}
        .wrapper h3{}
        .wrapper h3{font-size:22px !important;line-height:31px !important}
        .column{}
        .wrapper .size-8{font-size:8px !important;line-height:14px !important}
        .wrapper .size-9{font-size:9px !important;line-height:16px !important}
        .wrapper .size-10{font-size:10px !important;line-height:18px !important}
        .wrapper .size-11{font-size:11px !important;line-height:19px !important}
        .wrapper .size-12{font-size:12px !important;line-height:19px !important}
        .wrapper .size-13{font-size:13px !important;line-height:21px !important}
        .wrapper .size-14{font-size:14px !important;line-height:21px !important}
        .wrapper .size-15{font-size:15px !important;line-height:23px !important}
        .wrapper .size-16{font-size:16px !important;line-height:24px !important}
        .wrapper .size-17{font-size:17px !important;line-height:26px !important}
        .wrapper .size-18{font-size:18px !important;line-height:26px !important}
        .wrapper .size-20{font-size:20px !important;line-height:28px !important}
        .wrapper .size-22{font-size:22px !important;line-height:31px !important}
        .wrapper .size-24{font-size:24px !important;line-height:32px !important}
        .wrapper .size-26{font-size:26px !important;line-height:34px !important}
        .wrapper .size-28{font-size:28px !important;line-height:36px !important}
        .wrapper .size-30{font-size:30px !important;line-height:38px !important}
        .wrapper .size-32{font-size:32px !important;line-height:40px !important}
        .wrapper .size-34{font-size:34px !important;line-height:43px !important}
        .wrapper .size-36{font-size:36px !important;line-height:43px !important}
        .wrapper .size-40{font-size:40px !important;line-height:47px !important}
        .wrapper .size-44{font-size:44px !important;line-height:50px !important}
        .wrapper .size-48{font-size:48px !important;line-height:54px !important}
        .wrapper .size-56{font-size:56px !important;line-height:60px !important}
        .wrapper .size-64{font-size:64px !important;line-height:63px !important}
      }
    </style>
    <style type="text/css">
      body {
        margin: 0;
        padding: 0;
      }
      table {
        border-collapse: collapse;
        table-layout: fixed;
      }
      * {
        line-height: inherit;
      }
      [x-apple-data-detectors],
      [href^="tel"],
      [href^="sms"] {
        color: inherit !important;
        text-decoration: none !important;
      }
    </style>
    
  <!--[if !mso]><!-->
    <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic);
    </style>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic" rel="stylesheet" type="text/css"><!--<![endif]-->
    <style type="text/css">
      body{background-color:#fff}
      .logo a:hover,
      .logo a:focus{color:#859bb1 !important}
      .mso .layout-has-border{border-top:1px solid #ccc;border-bottom:1px solid #ccc}
      .mso .layout-has-bottom-border{border-bottom:1px solid #ccc}
      .mso .border,.ie .border{background-color:#ccc}
      .mso h1,.ie h1{}
      .mso h1,.ie h1{font-size:64px !important;line-height:63px !important}
      .mso h2,.ie h2{}
      .mso h2,.ie h2{font-size:30px !important;line-height:38px !important}
      .mso h3,.ie h3{}
      .mso h3,.ie h3{font-size:22px !important;line-height:31px !important}
      .mso .layout__inner,.ie .layout__inner{}
      .mso .footer__share-button p{}
      .mso .footer__share-button p{font-family:sans-serif}
    </style>
    <meta name="robots" content="noindex,nofollow">
    <meta property="og:title" content="My First Campaign">
  </head>
  <body class="no-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
<!--<![endif]-->
    <table class="wrapper" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #fff;" role="presentation" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <div role="banner">
              <div class="preheader" style="Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 167440px);">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                <!--[if (mso)|(IE)]><table align="center" class="preheader" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="width: 280px" valign="top"><![endif]-->
                  <div class="snippet" style="display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #adb3b9;font-family: sans-serif;"></div>
                </div>
              </div>
              <div class="header" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);" id="emb-email-header-container">
              <!--[if (mso)|(IE)]><table align="center" class="header" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="width: 600px"><![endif]-->
                <div class="logo emb-logo-margin-box" style="font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,Tahoma,sans-serif;Margin-left: 20px;Margin-right: 20px;" align="center">
                  <div class="logo-center" id="emb-email-header" align="center">
                    <img style="display: block;height: auto;width: 100%;border: 0;max-width: 141px;" src="<?php echo base_url('assets/logo.png'); ?>" alt="" width="141">
                  </div>
                </div>
              <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
              </div>
            </div>
            <div role="section">
              <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;">
                  <div class="column" style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">
                
                    <div style="Margin-left: 20px;Margin-right: 20px;">
                      <h2 style="Margin-top: 0;Margin-bottom: 16px;font-style: normal;font-weight: normal;color: #e31212;font-size: 26px;line-height: 34px;font-family: ubuntu,sans-serif;text-align: center;"><span class="font-ubuntu"><strong><span style="color:#01020a">Universitas Bhayangkara Jakarta Raya</span></strong></span></h2>
                    </div>
                
                    <div style="Margin-left: 20px;Margin-right: 20px;">
                      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;Margin-left: auto;Margin-right: auto;width: 40px;background-color: #ccc;Margin-bottom: 20px;">&nbsp;</div>
                    </div>
                
                    <div style="Margin-left: 20px;Margin-right: 20px;">
              
                      <p style="Margin-top: 0;Margin-bottom: 0;font-family: ubuntu,sans-serif;">
                        <span class="font-ubuntu">
                          <strong>
                            <span style="color:#000000">Hallo, <?php echo $usr->nm_depan.' '.$usr->nm_belakang; ?></span>
                          </strong>
                        </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom: 0;font-family: ubuntu,sans-serif;">
                        <span class="font-ubuntu">
                          <span style="color:#000000">Data yang anda daftarkan melalui formulir pendaftaran online untuk calon mahasiswa baru Universitas Bhayangkara Jakarta Raya telah diverifikasi oleh pihak kami.</span>
                        </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom: 0;font-family: ubuntu,sans-serif;">
                        <span class="font-ubuntu">
                          <span style="color:#000000">Berikut akun anda:</span>
                            </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom: 0;font-family: ubuntu,sans-serif;text-align: center;">
                        <span class="font-ubuntu">
                          <strong><span style="color:#000000">Username : <?php echo $usr->email; ?></span></strong>
                        </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom:0;font-family: ubuntu,sans-serif;text-align: center;">
                        <span class="font-ubuntu">
                          <strong><span style="color:#000000">Password&nbsp; : <?php echo $usr->userid; ?></span></strong>
                        </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom: 0;font-family: ubuntu,sans-serif;">
                        <span class="font-ubuntu">
                          <span style="color:#000000">Untuk memverifikasi akun anda harap klik link berikut
                            <strong>
                              <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #18527c;" href="http://172.16.2.65/registration/board/login/vrif_login/<?php echo $usr->userid; ?>" target="_blank"> registration.ubharajaya.ac.id</a>
                            </strong>
                          </span>
                        </span>
                      </p>
                      <p style="Margin-top: 20px;Margin-bottom: 20px;font-family: ubuntu,sans-serif;">
                        <span class="font-ubuntu">
                          <span style="color:#000000">Terimakasih.</span>
                        </span>
                      </p>
                    </div>
                
                    <div style="Margin-left: 20px;Margin-right: 20px;">
                      <div style="line-height:10px;font-size:1px">&nbsp;</div>
                    </div>
                
                  </div>
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </div>
              </div>
  
              <div style="line-height:20px;font-size:20px;">&nbsp;</div>
    
              <div style="background-color: #125439;">
                <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                  <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                    <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">
              
                      <div style="Margin-left: 20px;Margin-right: 20px;">
                        <div style="line-height:50px;font-size:1px">&nbsp;</div>
                      </div>
                
                      <div style="Margin-left: 20px;Margin-right: 20px;">
                        <p style="Margin-top: 0;Margin-bottom: 20px;text-align: center;">
                          <span style="color:#cdd9e6">
                            Copyright - <a href="" title="">Universitas Bhayangkara Jakarta Raya 2017</a>
                          </span>
                        </p>
                      </div>
                
                      <div style="Margin-left: 20px;Margin-right: 20px;">
                        <div style="line-height:15px;font-size:1px">&nbsp;</div>
                      </div>
              
                    </div>
                  </div>
                </div>
              </div>
    
              <div style="line-height:20px;font-size:20px;">&nbsp;</div>
              <div style="line-height:40px;font-size:40px;">&nbsp;</div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <img style="overflow: hidden;position: fixed;visibility: hidden !important;display: block !important;height: 1px !important;width: 1px !important;border: 0 !important;margin: 0 !important;padding: 0 !important;" src="https://o.createsend1.com/t/d-o-kijukkd-l/o.gif" alt="" width="1" height="1" border="0">
    <script type="text/javascript" src="https://js.createsend1.com/js/jquery-1.7.2.min.js?h=C99A465920170504"></script>
    <script type="text/javascript">$(function(){$('area,a').attr('target', '_blank');});</script>
</body>
</html>