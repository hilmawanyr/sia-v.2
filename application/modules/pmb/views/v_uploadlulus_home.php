<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Kegiatan Perkuliahan</h3>

      </div> <!-- /widget-header -->

      

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/ajar/save_session_prodi">
              <fieldset>

                <div class="control-group">
                  <label class="control-label">Gelombang</label>
                  <div class="controls">
                    <select class="form-control span4" name="tahunajaran" id="tahunajaran" required>
                      <option disabled selected>-- Pilih Gelombang --</option>
                      <?php foreach ($gel as $row) { ?>
                      <option value="<?php echo $row->kode;?>"><?php echo 'Gelombang - '.$row->gelombang;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Program Studi</label>
                  <div class="controls">
                    <select class="form-control span4" name="tahunajaran" id="tahunajaran" required>
                      <option disabled selected>-- Pilih Prodi --</option>
                      <?php foreach ($prodi as $row) { ?>
                      <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Gelombang</label>
                  <div class="controls">
                    <select class="form-control span4" name="tahunajaran" id="tahunajaran" required>
                      <option disabled selected>-- Pilih Gelombang --</option>
                      <?php foreach ($gel as $row) { ?>
                      <option value="<?php echo $row->kode;?>"><?php echo 'Gelombang - '.$row->gelombang;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-actions">
                    <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                </div> <!-- /form-actions -->
              </fieldset>
          </form>
        </div>

      </div>

    </div>

  </div>

</div>

