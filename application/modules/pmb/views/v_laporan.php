

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Form Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/forms1/simpan_form" method="post">
					<fieldset>
						<div class="control-group" id="konversi5">
							<label class="control-label"> Jenis Laporan </label>
							<div class="controls">
								<select class="form-control span2"  name="jenis">
									<option disabled="" selected="">--Pilih Jenis Laporan--</option>
									<option value="RM"  id="r">Readmisi</option>
									<option value="MB" id="new">Mahasiswa Baru</option>
									<option value="KV"  id="k">Mahasiswa Konversi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Mulai</label>
							<div class="controls">
								<input type="text" class="form-control span2" placeholder="Tempat Lahir" name="tpt_lahir"  >
								<input type="text" class="form-control span2" placeholder="Tanggal Lahir" id="tgl_lahir" name="tgl_lahir"  >
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
