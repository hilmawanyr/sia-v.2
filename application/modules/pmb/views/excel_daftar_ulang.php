<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<table border="3">
    <thead>
        <tr> 
            <th rowspan="2">Fakultas</th>
            <th colspan="3">Gelombang I</th>
            <th colspan="3">Gelombang II</th>
            <th colspan="3">Gelombang III</th>
            <th colspan="3">Gelombang IV & tambahan</th>
            <th colspan="3">Jumlah</th>
            <th rowspan="2">Jumlah Total</th>
        </tr>
       
        <tr>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
        </tr>
    </thead>
    <tbody>
        
         <?php
            $t_pg1 = 0;
            $t_sr1 = 0;
            $t_ky1 = 0;

            $t_pg2 = 0;
            $t_sr2 = 0;
            $t_ky2 = 0;

            $jml_by_prodi_all= 0;

        foreach ($fakultas as $isi) { ?>
            
        <tr>
            <td style="color:#000000; background-color:#ffff00"><b><?php echo $isi->fakultas; ?></b></td>
            <td colspan="16" style="background-color:#ffff00"></td>
        </tr>

        <?php $q = $this->db->query('select * from tbl_jurusan_prodi where kd_fakultas = '.$isi->kd_fakultas.'')->result(); ?>
        
            <?php 
           
            foreach ($q as $rows) { ?>
                <tr>
                    <td><b><?php echo $rows->prodi; ?></b></td>
                    <?php if ($ss == 'S1') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { ?>
                                    <?php $pg = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = 55201 AND kelas = "pg" AND status = 3')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND status = 3')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND status = 3')->row();
                                
                                } else { ?>
                            
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" AND status = 3')->row();
                                
                            } 
                        
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { ?>

                                    <?php $pg = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND status = 3')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND status = 3')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND status = 3')->row();
                                
                            } else { ?>
                                
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" AND status = 3')->row();
                                
                            } 
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { ?>
                                    <?php $pg = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND status = 3')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND status = 3')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND status = 3')->row();
                                
                                
                            } else { ?>

                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$gel.'" AND status = 3')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$gel.'" AND status = 3')->row();
                            }
                        } 
                        
                    } elseif($ss == 'S2') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                } else { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "bks" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                }
                            }
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                } else { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "jkt" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                }
                                
                            }
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                } else { ?>
                                    <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                    <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                    <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR"')->row();
                                }
                                 
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'"')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'"')->row();
                                }
                            }
                        }

                    } elseif($ss == 'ALL') { ?>
                            <?php if ($isi->kd_fakultas == 6){ ?>
                                <?php if ($gel == 'ALL') { ?>
                                    <?php if ($jenis == 'ALL') { ?>
                                        <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                        <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                        <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                                    } else { ?>
                                        <?php $pg = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                        <?php $sr = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                        <?php $ky = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                                    }
                                    
                                } else { ?>
                                    <?php if ($jenis == 'ALL') { ?>
                                        <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                    } else { ?>
                                        <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                    }
                                    
                                }
                                
                            } else { ?>
                                <?php if ($gel == 'ALL') { ?>
                                    <?php if ($jenis == 'ALL') { ?>
                                        <?php $pg = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                        <?php $sr = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                        <?php $ky = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                                    } else { ?>
                                        <?php $pg = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                        <?php $sr = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                        <?php $ky = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                                    }
                                    
                                } else { ?>
                                    <?php if ($jenis == 'ALL') { ?>
                                        <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                    } else { ?>
                                        <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'"')->row();?>
                                        <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'"')->row();
                                    }
                                }
                            }
                    } ?>
                    

                        
                        
                        <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                        <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                        
                        <?php if ($gel == '1') { ?>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        <?php } elseif($gel == '2') { ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        <?php } elseif ($gel == 'ALL') { ?>
                            <td><?php echo $pg->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $pg->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        <?php }?>
                        
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td><?php echo $jml_by_prodi; ?></td>
                    
                </tr>

               
            <?php } ?>
            
            
            
        <?php } ?> 
        <tr>
            <td><b>JUMLAH</b></td>
                <?php if ($gel == '1') { ?>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '2') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } ?>
                        
        </tr> 
    </tbody>
</table>