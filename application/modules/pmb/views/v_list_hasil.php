<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Detail Lulus Tes Calon Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                        <a class="btn btn-success btn-large" href="#"><i class="btn-icon-only icon-print"></i>Print</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>ID Registrasi</th>
                                    <th>Nama</th>
                                    <th>Asal Sekolah</th>
                                    <th>Prodi</th>
                                    <th width="80">Status</th>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($cmhs->result() as $row) { ?>
                                <tr>
                                    <td><?php echo $row->ID_registrasi; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->asal_skl; ?></td>
                                    <?php $prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$row->kd_prodi,'kd_prodi','asc')->row(); ?>
                                    <td><?php echo $prodi->prodi; ?></td>
                                    <?php if ($row->status == 0) {
                                        $status = 'pendaftaran';
                                    } elseif ($row->status == 1) {
                                        $status = 'penerimaan';
                                    } elseif ($row->status == 2) {
                                        $status = 'tidak lulus ujian';
                                    } elseif ($row->status >= 0) {
                                        $status = 'lulus seleksi';
                                    }
                                     ?>
                                    <td><?php echo strtoupper($status); ?></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </div>
</div>