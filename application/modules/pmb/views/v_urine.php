<?php 
	$tgl1 = '2016';
	$tgl2 = date('Y')+1;
 ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Rekapitulasi</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/sampel_urine/load" method="post" enctype="multipart/form-data">
					<fieldset>

						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option value="" selected disabled hidden>--Pilih Tahun--</option>
									<?php for ($i = $tgl1; $i <= $tgl2 ; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

					
						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option value="" selected disabled hidden>--Pilih Gelombang--</option>
									<option value="ALL">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option>
								</select>
							</div>
						</div>

						
						<div class="control-group">
							<label class="control-label">Prodi</label>
							<div class="controls">
								<select class="form-control span4" name="prodi" required/>
									<option value="" selected disabled hidden>--Pilih Prodi--</option>
									<?php foreach ($prodi as $row) {?>
										<option value="<?php echo $row->kd_prodi; ?>"><?php echo $row->prodi; ?></option>
									<?php	}?>
								</select>
							</div>
						</div>

						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
							<!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

