<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gelombangpmb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(88)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['gel'] = $this->app_model->getdata('tbl_gel_pmb','id_gel','desc')->result();
		$data['page'] = 'gelombang_pmb';
		$this->load->view('template/template', $data);
	}

	function view_edit($id)
	{
		$data['pos'] = $this->db->query('SELECT * from tbl_gel_pmb where id_gel = "'.$id.'"')->row();
		$this->load->view('edit_gelombang', $data);
	}

	function add()
	{
		$m = $this->input->post('mulai');
		$q = explode('/', $m);
		$tahun = $q[2];
		$bulan = $q[0];
		$tgl = $q[1];
		$mulai2 = ''.$tahun.'-'.$bulan.'-'.$tgl.'';

		$w = $this->input->post('akhir');
		$a = explode('/', $w);
		$th = $a[2];
		$bl = $a[0];
		$hr = $a[1];
		$mulai3 = ''.$th.'-'.$bl.'-'.$hr.'';

		$data = array(
			'nm_gel'	=> $this->input->post('kode'),
			'mulai_gel'	=> $mulai2,
			'akhir_gel'	=> $mulai3,
			'tahun_ajar'=> date('Y')
			);
		//var_dump($data);exit();
		$this->app_model->insertdata('tbl_gel_pmb', $data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."pmb/gelombangpmb/';</script>";
	}
    
    function delete($id)
    {
    	$this->app_model->deletedata('tbl_gel_pmb','id_gel',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."pmb/gelombangpmb';</script>";
    }

    function edit_gel()
    {
    	$fit = $this->input->post('plik');
		$ri = explode('/', $fit);
		$tahun = $ri[2];
		$bulan = $ri[0];
		$tgl = $ri[1];
		$mulaii = ''.$tahun.'-'.$bulan.'-'.$tgl.'';

		$sya = $this->input->post('pluk');
		$din = explode('/', $sya);
		$th = $din[2];
		$bl = $din[0];
		$hr = $din[1];
		$mulaiy = ''.$th.'-'.$bl.'-'.$hr.'';

		$datax = array(
			'nm_gel'	=> $this->input->post('plak'),
			'mulai_gel'	=> $mulaii,
			'akhir_gel'	=> $mulaiy,
			'tahun_ajar'=> date('Y')
			);
		//var_dump($datax);exit();
		$this->app_model->updatedata('tbl_gel_pmb','id_gel',$this->input->post('id'), $datax);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."pmb/gelombangpmb/';</script>";
    }

}

/* End of file Gelombangpmb.php */
/* Location: ./application/modules/pmb/Gelombangpmb.php */