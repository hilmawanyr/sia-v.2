<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jumlah_maba extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('regis', TRUE);
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(98)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['page'] = "v_jml_maba";
		$this->load->view('template/template', $data);
	}

	function load_data()
	{
		// die('kadal');
		//error_reporting(0);
		$qw = $this->input->post('kampus');
		$rt = $this->input->post('program');
		$rw = $this->input->post('jenis');
		$rr = $this->input->post('gelombang');
		$tn = $this->input->post('tahun');

		if ($tn == '17') {
			$table = 'tbl_form_camaba';
			$tables2 = 'tbl_pmb_s2';
			$data['table'] = 'tbl_form_camaba';
			$data['tables2'] = 'tbl_pmb_s2';
		}elseif ($tn == '16') {
			$table = 'tbl_form_camaba_2016';
			$tables2 = 'tbl_pmb_s2_2016';
			$data['table'] = 'tbl_form_camaba_2016';
			$data['tables2'] = 'tbl_pmb_s2_2016';
		} else {
			$table = 'tbl_form_pmb';
			$data['table'] = 'tbl_form_pmb';
		}	

 

		$data['kampus'] = $qw;
		$data['jenis']  = $rw;
		$data['ss']  = $rt;
		$data['gel'] = $rr;
		$data['tn'] = $tn;
		// die($qw.$rt.$rw);

		if ($tn < '18') {
			if ($rt == 'S1') {
				$data['fakultas'] = $this->db->query("select * from tbl_fakultas where kd_fakultas != 6")->result();
				if ($rr == 'ALL') {
					$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM ".$table."")->row()->maksbro;
					$data['rekap'] = $this->db->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'"')->result();
				} else {
					$data['rekap'] = $this->db->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'"')->result();
				}
				$this->load->view('excel_maba_jml', $data);
			} elseif ($rt == 'S2') {
				$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas','6','kd_fakultas','asc')->result();
				if ($rr == 'ALL') {
					$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM ".$table."")->row()->maksbro;
					$data['rekap'] = $this->db->query('SELECT * from '.$tables2.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'"')->result();
				} else {
					$data['rekap'] = $this->db->query('SELECT * from '.$tables2.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'"')->result();
				}
				$this->load->view('excel_maba_jml', $data);
			} elseif ($rt == 'ALL') {
				//$data['gelombang'] = $this->db->get('tbl_gel_pmb')->result();
				$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
				if ($rr == 'ALL') {
					$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM ".$table."")->row()->maksbro;
					$data['rekaps1'] = $this->db->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'"')->result();
					$data['rekaps2'] = $this->db->query('SELECT * from '.$tables2.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'"')->result();
				} else {
					$data['rekaps1'] = $this->db->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'"')->result();
					$data['rekaps2'] = $this->db->query('SELECT * from '.$tables2.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'"')->result();
				}
				$this->load->view('excel_maba_jml', $data);
			}
		} else {
			if ($rt == 'ALL') {
				$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
				if ($rr == 'ALL') {
					$data['jumlahgel'] = $this->db2->query('SELECT MAX(gelombang) as maksbro FROM '.$table.' where nomor_registrasi like "'.$tn.'%"')->row()->maksbro;
					$data['rekaps1'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and nomor_registrasi like "'.$tn.'%"')->result();
					$data['rekaps2'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and nomor_registrasi like "'.$tn.'%"')->result();
				} else {
					$data['rekaps1'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'" and nomor_registrasi like "'.$tn.'%"')->result();
					$data['rekaps2'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'" and nomor_registrasi like "'.$tn.'%"')->result();
				}
			} else {
				if ($rt == 'S1') {
					$prg = 1;
				} else {
					$prg = 2;
				}
				$data['fakultas'] = $this->db->query("SELECT * from tbl_fakultas where kd_fakultas != 6")->result();
				if ($rr == 'ALL') {
					$data['jumlahgel'] = $this->db2->query('SELECT MAX(gelombang) as maksbro FROM '.$table.' where nomor_registrasi like "'.$tn.'%"')->row()->maksbro;
					$data['rekap'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and program = '.$prg.' and nomor_registrasi like "'.$tn.'%"')->result();
				} else {
					$data['rekap'] = $this->db2->query('SELECT * from '.$table.' where kampus = "'.$qw.'" and jenis_pmb ="'.$rw.'" and gelombang = "'.$rr.'" and program = '.$prg.' and nomor_registrasi like "'.$tn.'%"')->result();
				}
			}
			$this->load->view('excel_maba_jml_new', $data);
		}
	}
}

/* End of file Jumlah_maba.php */
/* Location: .//tmp/fz3temp-1/Jumlah_maba.php */