<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_tes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	public function index()
	{
		$logged = $this->session->userdata('sess_login');
		//var_dump($logged['id_user_group']);exit();
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(13, $grup))) {
			$data['gelombang'] = $this->app_model->getdata('tbl_gelombang_pmb','gelombang','asc');
			//$data['page'] = 'v_hasil_pemasaran';
			$data['page'] = "v_sess_hasil";
		} elseif ((in_array(4, $grup))) {
			$data['page'] = 'v_hasil_cmhs';
		}
		$this->load->view('template/template',$data);
	}

	function simpan_sesi()
	{
		$jenjang = $this->input->post('jenjang');
		$jenis = $this->input->post('jenis');
		$gel = $this->input->post('gel');


        $this->session->set_userdata('cuss', $jenjang);

        $this->session->set_userdata('jenis', $jenis);   

		$this->session->set_userdata('gelombang', $gel);        
      
		redirect(base_url('pmb/hasil_tes/muatData'));
	}

	function muatData()
	{
		$prog = $this->session->userdata('cuss');
		$wave = $this->session->userdata('gelombang');
		$kind = $this->session->userdata('jenis');

		// var_dump($prog.'-'.$wave.'-'.$kind);exit();

		// var_dump($prog);exit();

		if ($prog == 1) {
			if ($kind == 'ALL') {
				$arr = [1,''];
			} else {
				$arr = [1,'and jenis_pmb = "'.$kind.'"'];
			}
			
		} else {
			if ($kind == 'ALL') {
				$arr = [2,''];
			} else {
				$arr = [2,'and jenis_pmb = "'.$kind.'"'];
			}
			
		}

		if ($wave == '0') {
			$que = 'SELECT * from tbl_form_pmb where program = '.$arr[0].' '.$arr[1].' and nomor_registrasi like "19%" order by nomor_registrasi asc';
		} else {
			$que = 'SELECT * from tbl_form_pmb where program = '.$arr[0].' '.$arr[1].' and nomor_registrasi like "19%" and gelombang = "'.$wave.'" order by nomor_registrasi asc';
		}

		$data['raws'] = $this->dbreg->query($que);
		// var_dump($data['pong']);exit();
		$data['page'] = "v_muatdata";
		$this->load->view('template/template', $data);
	}

	function updatelulus()
	{
		$y = $this->input->post('y');
		$u = $this->input->post('id');
		// echo "<pre>";
		// print_r ($u);
		// echo "</pre>";exit();
		for ($i = 0; $i < count($y); $i++) {
			$data = array('status_lulus' => $y[$i][$u[$i]], 'status' => $y[$i][$u[$i]]);
			$this->dbreg->where('user_input', $u[$i]);
			$this->dbreg->update('tbl_form_pmb', $data);
		}
		echo "<script>alert('Berhasil!');history.go(-1)</script>";
	}

	// function load_data()
	// {
	// 	if ($this->session->userdata('cuss') == 's1') {
	// 		if ($this->session->userdata('jenis') == 'ALL') {
	// 			$data['pong'] = $this->db->query('SELECT * from tbl_form_camaba where gelombang = "'.$this->session->userdata('gelombang').'"')->result();
	// 		} else {
	// 			$data['pong'] = $this->db->query('SELECT * from tbl_form_camaba where jenis_pmb = "'. $this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'"')->result();
	// 		}
	// 		$data['page'] = "v_hasil_pemasaran_s1";
	// 	} elseif($this->session->userdata('cuss') == 's2') {
	// 		if ($this->session->userdata('jenis') == 'ALL') {
	// 			$data['pong'] = $this->db->query('SELECT * from tbl_pmb_s2 where gelombang = "'.$this->session->userdata('gelombang').'"')->result();
	// 		} else {
	// 			$data['pong'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'. $this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'"')->result();
	// 		}
	// 		$data['page'] = "v_hasil_pemasaran_s2";
	// 	}
	// 	$this->load->view('template/template', $data);
	// }

	function list_hasil($value)
	{
		$data['cmhs'] = $this->app_model->getdetail('tbl_calon_mhs','gelombang',$value,'ID_registrasi','asc');
		$data['page'] = "v_list_hasil";
		$this->load->view('template/template', $data);
	}

	function dwld()
	{

			$data['pong'] = $this->db->query('SELECT * from tbl_form_camaba where gelombang = "'.$this->session->userdata('gelombang').'"')->result();
			
			$this->load->view('v_excel_hasil', $data);
	}

	function dwld2()
	{

			$data['pong'] = $this->db->query('SELECT * from tbl_pmb_s2 where gelombang = "'.$this->session->userdata('gelombang').'"')->result();
			
			$this->load->view('v_excel_hasil2', $data);
	}

}

/* End of file Hasil_tes.php */
/* Location: .//tmp/fz3temp-1/Hasil_tes.php */