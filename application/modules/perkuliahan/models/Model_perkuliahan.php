<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_perkuliahan extends CI_Model {
		
	Public function get_perkuliahan($config,$ta,$search)
	{	
		$this->db->distinct();
		$this->db->select('a.kd_jadwal,a.kd_matakuliah,a.kd_tahunajaran,a.hari,a.waktu_mulai,a.waktu_selesai,a.id_jadwal,b.kode_ruangan,a.kd_dosen');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_ruangan b', 'a.kd_ruangan = b.id_ruangan','left');
		$this->db->join('tbl_matakuliah c', 'a.kd_matakuliah = c.kd_matakuliah');
		$this->db->join('tbl_karyawan e', 'a.kd_dosen = e.nid','left');
		$this->db->where('a.kd_tahunajaran', $ta);
		$this->db->where('a.audit_user', 'baa');

		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('a.kd_matakuliah',$search,'both');
			$this->db->or_like('c.nama_matakuliah',$search,'both');
			$this->db->or_like('e.nama',$search,'both');
			$this->db->or_like('a.kd_dosen',$search,'both');
			$this->db->or_like('b.kode_ruangan',$search,'both');
			$this->db->group_end();
		}

		$this->db->limit($config, $this->uri->segment(4));
		$hasil = $this->db->get();
		if ($hasil->num_rows() > 0){
			return $hasil->result();
		}
		
		
	}

	public function rowAmount($year)
	{
		$this->db->distinct();
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_ruangan b', 'a.kd_ruangan = b.id_ruangan', 'left');
		$this->db->where('audit_user', 'baa');
		$this->db->where('kd_tahunajaran', $year);
		return $this->db->get();
	}
	
	function get_mku($year)
	{
		$this->db->distinct();
		$this->db->select('
			a.kd_jadwal,
			a.kd_matakuliah,
			a.kelas,
			a.kd_tahunajaran,
			a.hari,
			a.waktu_mulai,
			a.waktu_selesai,
			a.id_jadwal,
			a.kd_ruangan,
			a.kd_dosen,
			a.gabung,
			a.referensi,
			a.open');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->where('a.kd_tahunajaran', $year);
		$this->db->group_start();
		$this->db->like('a.audit_user', 'baa', 'after');
		$this->db->or_like('a.kd_matakuliah', 'MKU', 'after');
		$this->db->or_like('a.kd_matakuliah', 'MKDU', 'after');
		$this->db->or_like('a.kd_matakuliah', 'MKWU', 'after');
		$this->db->group_end();
		return $this->db->get();
	}	
		
}