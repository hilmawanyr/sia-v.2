<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Matakuliah</h3>
			</div> <!-- /widget-header -->			
			<div class="widget-content">
				<div class="span11">
				<form method="post" class="form-horizontal" action="<?php echo base_url(); ?>perkuliahan/matakuliah/save_session">
                        <fieldset>
                              <div class="control-group">
                                <label class="control-label">Jurusan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs" required>
                                    <option disabled selected>--Pilih Jurusan--</option>
                                    <?php foreach ($jurusan as $value) {
                                      echo "<option value='".$value->kd_prodi."'> ".$value->prodi." </option>";
                                    } ?>
                                  </select>
                                </div>
                              </div>          
                            <br />                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>					
				</div>
			</div>
		</div>
	</div>
</div>
