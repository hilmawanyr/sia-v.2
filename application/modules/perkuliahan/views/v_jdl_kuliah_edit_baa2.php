<script type="text/javascript">

$(document).ready(function(){

	$('#jam_masuk').timepicker();

	$('#jam_keluar').timepicker();

	$('#jam_masukedit').timepicker();
});

</script>

			<div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">FORM EDIT DATA</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/jdl_kuliah_baa/update_data_matakuliah" method="post">

                <div class="modal-body">    
                                    <script type="text/javascript">

                                    	$(document).ready(function(){

											$('#semester').change(function(){

												$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah_baa/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){

													$('#ala_ala').html(get);

												});

											});

										});

                                    </script>
                                       <div class="control-group">
										<label class="control-label">Kelompok Kelas</label>
										<div class="controls">
											<input type="radio" name="st_kelas" value="PG" <?php if ($rows->waktu_kelas == 'PG') {echo 'checked=""';} ?> > PAGI &nbsp;&nbsp; 
											<input type="radio" name="st_kelas" value="SR" <?php if ($rows->waktu_kelas == 'SR') {echo 'checked=""';} ?>> SORE &nbsp;&nbsp;
											<input type="radio" name="st_kelas" value="PK" <?php if ($rows->waktu_kelas == 'PK') {echo 'checked=""';} ?>> P2K  &nbsp;&nbsp;
										</div>
									</div>

										<script type="text/javascript">

										$(document).ready(function() {

											$('#ala_ala').change(function() {

												$('#kode_mk').val($('#ala_ala').val());

											});

										});

										</script>



										<div class="control-group">											

											<label class="control-label" for="firstname">Hari Kuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="hari_kuliah" id="hari_kuliah" required>
													<option value="<?php echo $rows->hari ?>"><?php echo notohari($rows->hari); ?></option>
													<option disabled>--Hari Kuliah--</option>

													<option value="1">Senin</option>

													<option value="2">Selasa</option>

													<option value="3">Rabu</option>

													<option value="4">Kamis</option>

													<option value="5">Jum'at</option>

													<option value="6">Sabtu</option>

													<option value="7">Minggu</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->	

                                    

                						<div class="control-group">											

											<label class="control-label" for="firstname">Nama Matakuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="nama_matakuliah" id="ala_ala" >
													<option value="<?php echo $rows->id_matakuliah ?>"><?php echo $rows->nama_matakuliah; ?> </option>
													<option>--Pilih MataKuliah--</option>
													<?php foreach ($matkul as $isi) { ?>
					                                    <option value="<?php echo $isi->kd_matakuliah; ?>"><?php echo $isi->kd_matakuliah; ?> - <?php echo $isi->nama_matakuliah; ?> (<?php echo $isi->sks_matakuliah; ?> sks)</option>
					                                <?php } ?>
												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->    

                    					

                    					<div class="control-group">										

											<label class="control-label" for="kode_mk">Kode MK</label>

											<div class="controls">

												<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk" value="<?php echo $rows->kd_matakuliah; ?>" readonly>
												<input type="hidden" name="id_jadwal" value="<?php echo $rows->id_jadwal; ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->
										
										<div class="control-group">											

											<label class="control-label" for="jam">Jam Masuk</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masukedit" name="jam_masuk" value="<?php echo $rows->waktu_mulai; ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script type="text/javascript">
					                        $(document).ready(function(){
					                            $('#gabung1').change(function(){
					                                $.post('<?php echo base_url();?>perkuliahan/Jdl_kuliah_baa/get_kelas_gabungan/'+$(this).val()+'/'+$('#kode_mk').val(),{},function(get){
					                                    $('#kelas_gab1').html(get);
					                                });
					                            });
					                        });
					                    </script>

					                    <div class="control-group" id="">
					                        <label class="control-label">Kelas Gabungan</label>
					                        <div class="controls">
					                            <select id="gabung1" name="gabung" class="span2" class="form-control">
					                                <option value="<?php echo $rows->gabung; ?>"><?php if($rows->gabung > 0){echo "Gabungan";}else{echo "Tidak";}; ?></option>
					                                <option>--Pilihan--</option>
					                                <option value="1">Gabungan</option>
					                                <option value="0">Tidak</option>
					                            </select>
					                        </div>
					                    </div>

					                    <div class="control-group" id="">
					                        <label class="control-label">Kelas</label>
					                        <div class="controls">
					                            <select name="kelas_gab" id="kelas_gab1" class="span2" class="form-control" required>
					                                <?php
					                                $year = getactyear();

					                                if ($rows->gabung > 0) {
					                                	$que = $this->db->query("SELECT a.*,b.prodi,c.nama_matakuliah from tbl_jadwal_matkul a
																			join tbl_jurusan_prodi b on SUBSTR(a.kd_jadwal, 1,5) = b.kd_prodi
																			join tbl_matakuliah c on c.kd_matakuliah = a.kd_matakuliah
																			where c.kd_prodi = SUBSTR(a.kd_jadwal, 1,5) and kd_tahunajaran = '".$year."'
																			and a.kd_jadwal = '".$rows->referensi."' order by id_jadwal asc")->row();
					                                } else {
					                                	$que = $this->db->query("SELECT a.*,b.prodi,c.nama_matakuliah from tbl_jadwal_matkul a
																			join tbl_jurusan_prodi b on SUBSTR(a.kd_jadwal, 1,5) = b.kd_prodi
																			join tbl_matakuliah c on c.kd_matakuliah = a.kd_matakuliah
																			where c.kd_prodi = SUBSTR(a.kd_jadwal, 1,5) and kd_tahunajaran = '".$year."'
																			order by id_jadwal asc")->row();
					                                }
					                                
					                                 ?>
													<option value='<?= $que->kd_jadwal;?>'>
														<?= $que->kd_matakuliah.' - '.$que->kelas.' - '.$que->nama_matakuliah.' - '.$que->prodi.' - '.$que->id_jadwal;?></option>
					                            </select>
					                        </div>
					                    </div>

					                    <script type="text/javascript">

					                        $(document).ready(function(){

					                            $('#gedung2').change(function(){

					                                $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

					                                    $('#lantai2').html(get);

					                                });

					                            });

					                        });

					                    </script>

					                    <div class="control-group">                                         

					                        <label class="control-label" for="gedung">Gedung</label>

					                        <div class="controls">

					                            <select class="form-control" name="gedung" id="gedung2">

					                                <option value="">--Pilih Gedung--</option>

					                                <?php foreach ($gedung as $isi) {?>

					                                    <option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>   

					                                <?php } ?>

					                            </select>

					                        </div> <!-- /controls -->               

					                    </div> <!-- /control-group -->

					                    <script type="text/javascript">

					                        $(document).ready(function(){

					                            $('#lantai2').change(function(){

					                                $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

					                                    $('#ruangan2').html(get);

					                                });

					                            });

					                        });

					                    </script>

					                    <div class="control-group">                                         

					                        <label class="control-label" for="lantai">Lantai</label>

					                        <div class="controls">

					                            <select name="lantai" id="lantai2">

					                                <option value="">--Pilih Lantai--</option>

					                            </select>

					                        </div> <!-- /controls -->               

					                    </div> <!-- /control-group -->

					                    

					                    <div class="control-group">                                         

					                        <label class="control-label" for="ruangan">Ruangan</label>

					                        <div class="controls">

					                            <select name="ruangan" id="ruangan2" required>
					                            	<option value="<?php echo $rows->id_ruangan ?>"><?php echo $rows->kode_ruangan.' - '.$rows->ruangan ?></option>
					                                <option value="">--Pilih Ruangan--</option>

					                            </select>

					                        </div> <!-- /controls -->               

					                    </div> <!-- /control-group -->
										
					                </div> 

					                <div class="modal-footer">

					                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					                    <input type="submit" class="btn btn-primary" value="Save changes"/>

					                </div>

					            </form>