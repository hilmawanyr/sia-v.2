<style>
    #dup label{
        margin-bottom: 0px;
    }
</style>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"/>

<script>
    function edit(idk) {
        $('#form_edit').load('<?php echo base_url();?>perkuliahan/jdl_kuliah_baa/edit_jadwal/'+idk);
    }

    function dosen(idk,gabungan) {
        console.log(gabungan)
        $('#edit1').load('<?= base_url();?>perkuliahan/jdl_kuliah_baa/load_dosen/'+idk+'/'+gabungan);
    }

    function seeAmount(id) {
        $.get('<?= base_url("perkuliahan/jdl_kuliah_baa/jumlahpeserta/") ?>'+id, function(response){
            $('#modaljumlah').modal('show');
            var data = JSON.parse(response);
            $('#titlejumlah').text(data.title);
            $('#dosen').val(data.nmdosen);
            $('#amount').val(data.jumlah);
        });
    }

    function see_child(id) {
        $('#see_child').load('<?= base_url('perkuliahan/jdl_kuliah_baa/see_child/') ?>' + id)
    }

    function see_parent(id) {
        $('#see_child').load('<?= base_url('perkuliahan/jdl_kuliah_baa/see_parent/') ?>' + id)
    }

    $(function() {
        $('#gabung').change(function(){
            if ($(this).val() === '1') {
                $('#nm_kelas').removeAttr('required').val('');
                $('#hari').removeAttr('required').val('');
                $('#jam_masuk').removeAttr('required').val('');
                $('#gedung').removeAttr('required').val('');
                $('#lantai').removeAttr('required').val('');
                $('#ruangan').removeAttr('required').val('');

                $('#nm_kelas_group').hide();
                $('#hari_group').hide();
                $('#jam_masuk_group').hide();
                $('#gedung_group').hide();
                $('#lantai_group').hide();
                $('#ruangan_group').hide();

            } else {
                $('#nm_kelas').attr('required','required');
                $('#hari').attr('required','required');
                $('#jam_masuk').attr('required','required');
                $('#gedung').attr('required','required');
                $('#lantai').attr('required','required');
                $('#ruangan').attr('required','required');

                $('#nm_kelas_group').show();
                $('#hari_group').show();
                $('#jam_masuk_group').show();
                $('#gedung_group').show();
                $('#lantai_group').show();
                $('#ruangan_group').show();
            }
        });
    })

    $(document).ready(function(){
        $('#jam_masuk').timepicker();
        $('#jam_keluar').timepicker();
        $('#jam_masukedit').timepicker();

        $('#gabung').change(function(){
            $.post('<?php echo base_url();?>perkuliahan/Jdl_kuliah_baa/get_kelas_gabungan/'+$(this).val()+'/'+$('#mk').val(),{},function(get){
                $('#kelas_gab').html(get);
            });
        });

        $('#gedung').change(function(){
            $.post('<?= base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){
                $('#lantai').html(get);
            });
        });

        $('#lantai').change(function(){
            $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){
                $('#ruangan').html(get);
            });
        });
    });
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-calendar"></i>
                <h3>Data Jadwal MKDU <?= $year ?></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form action="<?php echo base_url('perkuliahan/jdl_kuliah_baa/view'); ?>" method="post">
                        <a data-toggle="modal" class="btn btn-primary" href="#new"><i class="icon-plus"></i> New Data </a>
                        <!-- <a href="#" class="btn btn-success"><i class="icon-print"></i> Print Data </a> -->
                        <hr>
                        <table class="table table-bordered table-striped" id="example1">
                            <thead>
                                <tr> 
                                    <th>Kode MK</th>
                                    <th>Mata Kuliah</th>
                                    <th>Kelas</th>

                                    <?php if ($year < 20192) { ?>
                                        <th>Prodi</th>
                                    <?php } ?>
                                    
                                    <th>Fakultas</th>
                                    <th>Hari/Waktu</th>
                                    <th>Dosen</th>
                                    <th>Ruangan</th>

                                    <?php if ($year > 20191) { ?>
                                        <th>Kelas Induk</th>
                                        <th>Status Kelas</th>
                                    <?php } ?>

                                    <th>Peserta</th>
                                    <th width="200">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($dataMku as $mku) { ?>
                                <tr>
                                    <td><?php echo $mku->kd_matakuliah; ?></td>
                                    <td><?= get_nama_mku($mku->kd_matakuliah); ?></td>
                                    <td><?= $mku->gabung == 0 ? $mku->kelas : getNamaKelas(get_dtl_jadwal($mku->referensi)['id_jadwal']); ?></td>

                                    <?php if ($year < 20192) { ?>
                                        <td><?= get_jur(substr($mku->kd_jadwal, 0,5)); ?></td>
                                    <?php } ?>
                                    
                                    <td><?= get_fak(substr($mku->kd_jadwal, 0,1)); ?></td>
                                    <td><?=  $mku->gabung == 0 ? notohari($mku->hari).' / '.substr($mku->waktu_mulai,0,5).'-'.substr($mku->waktu_selesai,0,5) : '-'; ?></td>
                                    <td><?= nama_dsn($mku->kd_dosen) ?></td>
                                    <td><?= empty($mku->kd_ruangan) ? '-' : get_room($mku->kd_ruangan) ?></td>

                                    <?php if ($year > 20191) { ?>
                                        <td><?= $mku->gabung == 0 ? 'YA' : '-' ?></td>
                                        <td>
                                            <a 
                                                href="<?=  base_url('perkuliahan/jdl_kuliah_baa/openCloseClass/'.$mku->id_jadwal) ?>" 
                                                class="btn <?= is_null($mku->open) ? 'btn-danger' : 'btn-success' ?>">
                                                <?= is_null($mku->open) ? 'CLOSE' : 'OPEN' ?></a>
                                        </td>
                                    <?php } ?>
                                    
                                    <td>
                                        <button 
                                            class="btn btn-primary btn-small" 
                                            data-toggle="modal" 
                                            data-target="#modaljumlah" 
                                            onclick="seeAmount('<?= $mku->id_jadwal ?>')">
                                            <i class="btn-icon-only icon-eye-open"></i>
                                        </button> 
                                    </td>
                                    <td>
                                        <?php if ($mku->gabung == 0) { ?>
                                            <a 
                                                data-toggle="modal" 
                                                onclick="dosen(<?= $mku->id_jadwal;?>, <?= $mku->gabung ?>)" 
                                                href="#editModal1"
                                                class="btn btn-success btn-small" >
                                                <i class="btn-icon-only icon-user"> </i>
                                            </a>
                                        <?php } ?>
                                        
                                        <a 
                                            class="btn btn-primary btn-small" 
                                            onclick="edit(<?php echo $mku->id_jadwal;?>)" 
                                            data-toggle="modal" 
                                            href="#editModal" >
                                            <i class="btn-icon-only icon-pencil"></i>
                                        </a>

                                        <a 
                                            class="btn btn-warning btn-small" 
                                            href="<?= base_url('akademik/ajar/cetak_absensi/'.$mku->id_jadwal); ?>">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a>
                                        
										<a 
                                            onclick="return confirm('Yakin ingin menghapus data jadwal ini?')"
                                            class="btn btn-danger btn-small" 
                                            href="<?= base_url('perkuliahan/jdl_kuliah_baa/delete/'.$mku->id_jadwal); ?>" >
                                            <i class="btn-icon-only icon-remove"></i>
                                        </a>

                                        <?php if ($mku->gabung == 0) { ?>
                                            <a 
                                                data-toggle="modal"
                                                title="lihat kelas turunan"
                                                onclick="see_child(<?= $mku->id_jadwal ?>)"
                                                class="btn btn-info btn-small" 
                                                href="#seeChild" >
                                                <i class="btn-icon-only icon-list"></i>
                                            </a>

                                        <?php } else { ?>
                                            <a 
                                                data-toggle="modal"
                                                title="lihat kelas induk"
                                                onclick="see_parent(<?= $mku->id_jadwal ?>)"
                                                class="btn btn-default btn-small" 
                                                href="#seeChild" >
                                                <i class="btn-icon-only icon-list"></i>
                                            </a>

                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                            
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
<!-- data table serverside processing -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#examplee').dataTable( {
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?= base_url(); ?>perkuliahan/jdl_kuliah_baa/serverside_loadmku",
                type: "POST"
            }
        });
    });
</script>

<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM DATA</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url(); ?>perkuliahan/jdl_kuliah_baa/save" method="post">
                <div class="modal-body">
                    <div class="control-group" id="">
                        <label class="control-label">Fakultas</label>
                        <div class="controls">
                            <select name="faculty" class="span3" class="form-control" required="">
                                <option value="">-- Pilih Fakultas --</option>
                                <?php foreach ($faculties as $faculty) { ?>
                                    <option value="<?= $faculty->kd_fakultas; ?>"><?= $faculty->fakultas; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kelompok Kelas</label>
                        <div class="controls">
                            <input type="radio" name="st_kelas" value="PG" required=""> PAGI &nbsp;&nbsp; 
                            <input type="radio" name="st_kelas" value="SR" required=""> SORE &nbsp;&nbsp;
                            <input type="radio" name="st_kelas" value="PK" required=""> P2K  &nbsp;&nbsp;
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Mata Kuliah</label>
                        <div class="controls">
                            <select name="mk" class="span3" class="form-control" id="mk" required="">
                                <option value="" disabled="" selected="">-- Pilih Matakuliah --</option>
                                <?php foreach ($matkul as $isi) { ?>
                                    <option value="<?= $isi->kd_matakuliah.'___'.$isi->id_matakuliah; ?>">
                                        <?= $isi->kd_matakuliah; ?> - <?= $isi->nama_matakuliah; ?> (<?= $isi->sks_matakuliah; ?> sks)
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Kelas Gabungan</label>
                        <div class="controls">
                            <select id="gabung" name="gabung" class="form-control span3">
                                <option>-- Pilihan --</option>
                                <option value="1">Gabungan</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Kelas Induk</label>
                        <div class="controls">
                            <select name="kelas_gab" id="kelas_gab" class="form-control span3" required="">
                                
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="nm_kelas_group">
                        <label class="control-label">Nama Kelas</label>
                        <div class="controls">
                            <input type="text" class="form-control span3" placeholder="Input Nama Kelas" id="nm_kelas" name="nm_kelas" required="">
                        </div>
                    </div>

                    <div class="control-group" id="hari_group">
                        <label class="control-label">Hari</label>
                        <div class="controls">
                            <select id="hari" name="hari" class="form-control span3" required="">
                                <option value="" selected="" disabled="">-- Hari Kuliah --</option>
                                <option value="1">Senin</option>
                                <option value="2">Selasa</option>
                                <option value="3">Rabu</option>
                                <option value="4">Kamis</option>
                                <option value="5">Jum'at</option>
                                <option value="6">Sabtu</option>
                                <option value="7">Minggu</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="jam_masuk_group">
                        <label class="control-label">Waktu</label>
                        <div class="controls">
                            <input type="text" class="form-control span3" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk" required="">
                        </div>
                    </div>

                    <div class="control-group" id="gedung_group">                                         
                        <label class="control-label" for="gedung">Gedung</label>
                        <div class="controls">
                            <select class="form-control span3" name="gedung" id="gedung" required="">
                                <option value="">-- Pilih Gedung --</option>
                                <?php foreach ($gedung as $isi) {?>
                                    <option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>   
                                <?php } ?>
                            </select>
                        </div> <!-- /controls -->               
                    </div> <!-- /control-group -->

                    <div class="control-group" id="lantai_group">                                         
                        <label class="control-label" for="lantai">Lantai</label>
                        <div class="controls">
                            <select name="lantai" id="lantai" class="form-control span3" required="">
                                <option value="">-- Pilih Lantai --</option>
                            </select>
                        </div> <!-- /controls -->               
                    </div> <!-- /control-group -->

                    <div class="control-group" id="ruangan_group">                                         
                        <label class="control-label" for="ruangan">Ruangan</label>
                        <div class="controls">
                            <select name="ruangan" id="ruangan" class="form-control span3" required="">
                                <option value="">-- Pilih Ruangan --</option>
                            </select>
                        </div> <!-- /controls -->               
                    </div> <!-- /control-group -->

                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="save" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="seeChild" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="see_child">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal untuk mnampilkan jumlah peserta -->
<div class="modal fade" id="modaljumlah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="titlejumlah"></h3>
            </div>
            <div class="modal-body">
                <div class="control-group">                                         
                    <label class="control-label" for="dosen">Nama Dosen</label>
                    <div class="controls">
                        <input type="text" class="form-control span5" id="dosen" readonly="">
                    </div> <!-- /controls -->               
                </div>
                <div class="control-group">                                         
                    <label class="control-label" for="amount">jumlah Peserta</label>
                    <div class="controls">
                        <input type="text" class="form-control span5" id="amount" readonly="">
                    </div> <!-- /controls -->               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>