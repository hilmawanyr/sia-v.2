
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Kegiatan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/Kalender/update" method="post">
                <div class="modal-body" style="margin-left:-20px;">  
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $.post('<?php echo base_url()?>perkuliahan/Kalender/get_list_kal'+$(this).val(),{},function(get){
                                $("#jns_keg").html(get);
                            });
                        });
                    </script>  
                    <div class="control-group" id="">
                        <label class="control-label">Jenis Kegiatan</label>
                        <div class="controls">
                            <input type="hidden" name="id_kalender" value="<?php echo $list->id_kalender;?>">
                            <input type="text" class="span4" id="jns_keg" name="jk_keg" placeholder="input jenis kegiatan" class="form-control" value="<?php echo $list->jns_kegiatan;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                        	<textarea class="span4 form-control" name="desk"><?php echo $list->deskripsi;?></textarea>
                            <!-- <input type="text" class="span4" name="deskripsi_keg" placeholder="Input Nama Fakultas" class="form-control" value="" required/> -->
                        </div>
                    </div>
                     <script>
                              $(function() {
                                $( "#from1" ).datepicker({
                                  changeMonth: true,
                                  numberOfMonths: 2,
                                  onClose: function( selectedDate ) {
                                    $( "#to1" ).datepicker( "option", "minDate", selectedDate );
                                  }
                                });
                                $( "#to1" ).datepicker({
                                  changeMonth: true,
                                  numberOfMonths: 2,
                                  onClose: function( selectedDate ) {
                                    $( "#from1" ).datepicker( "option", "maxDate", selectedDate );
                                  }
                                });
                              });
                            </script>
                    <div class="control-group" id="">
                        <label class="control-label">Mulai</label>
                        <div class="controls">
                            <input type="text" class="span4" name="mulai_kegiatan" id="from1" placeholder="dd/mm/yyyy" class="form-control" value="<?php echo $list->mulai;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Akhir</label>
                        <div class="controls">
                            <input type="text" class="span4" name="akhir_kegiatan" id="to1" placeholder="dd/mm/yyyy" class="form-control" value="<?php echo $list->akhir;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Peserta</label>
                        <div class="controls">
                            <input type="text" class="span4" name="peserta" placeholder="input peserta" class="form-control" value="<?php echo $list->peserta;?>" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>