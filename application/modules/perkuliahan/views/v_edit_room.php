<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Ruang</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/ruang/change_room" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -60px;">  
        <div class="control-group" id="">
            <label class="control-label">Gedung</label>
            <div class="controls">
                <input type="text" value="<?php echo $ruang->gedung; ?>" class="form-control span4" disabled="">
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Lantai</label>
            <div class="controls">
                <input type="text" value="<?php echo $ruang->lantai; ?>" class="form-control span4" disabled="">
                <input type="hidden" value="<?php echo $ruang->id_lantai; ?>" name="lants">
            </div>
        </div>
        <hr>
        <div class="control-group" id="">
            <label class="control-label">Kode Ruang</label>
            <div class="controls">
                <input type="text" name="kode" value="<?php echo $ruang->kode_ruangan; ?>" class="form-control span4">
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Ruang</label>
            <div class="controls">
                <input type="text" name="room" value="<?php echo $ruang->ruangan; ?>" class="form-control span4">
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Kuota</label>
            <div class="controls">
                <div class="input-prepend input-append">
                    <input type="text" name="kuota" value="<?php echo $ruang->kuota; ?>" class="form-control span2">
                    <span class="add-on">Kursi</span>
                </div>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Tipe</label>
            <div class="controls">
                <select class="form-control span4" name='tipe' id="">
                    <option selected="" disabled="">--Pilih Tipe--</option>
                    <option value="1">Kelas</option>
                    <option value="2">Non Kelas</option>
                </select>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Deskripsi</label>
            <div class="controls">
                <textarea name="desc" class="form-control span4"><?php echo $ruang->deskripsi; ?></textarea>
                <input type="hidden" name="id" value="<?php echo $ruang->id_ruangan; ?>">
            </div>
        </div>              
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>