<?php $warna="";?>

<style type="text/css">
	#circle { 
		width: 20px; 
		height: 20px; 
		background: #FFB200; 
		-moz-border-radius: 50px; 
		-webkit-border-radius: 50px; 
		border-radius: 50px; 
	}
</style>

<link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?= base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?= base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script>
	function edit(idk){
		$('#edit1').load('<?= base_url();?>perkuliahan/jdl_kuliah2/edit_jadwal_new/'+idk);
	}

	function edit2(idk){
		$('#edit2').load('<?= base_url();?>perkuliahan/jdl_kuliah/penugasan_new/'+idk);
	}

	function edit3(idk){
		$('#edit3').load('<?= base_url();?>perkuliahan/jdl_kuliah/edit_nama_kelas/'+idk);
	}

	function pesertaShow(id_jadwal) {
		$.get( '<?= base_url();?>perkuliahan/jdl_kuliah2/view_peserta/'+id_jadwal, function( data ) {
		  alert( "Jumlah Peserta: " + data );
		});
	}

	function see_child(id) {
        $('#see_child').load('<?= base_url('perkuliahan/jdl_kuliah2/see_child/') ?>' + id)
    }

    function see_parent(id) {
        $('#see_child').load('<?= base_url('perkuliahan/jdl_kuliah2/see_parent/') ?>' + id)
    }

</script>


<script type="text/javascript">

	$(document).ready(function(){

		$('#jam_masuk').timepicker();

		$('#jam_keluar').timepicker();

		$('#jam_masukedit').timepicker();
	});

</script>



<script type="text/javascript">

jQuery(document).ready(function($) {

	$('input[name^=dosen]').autocomplete({

        source: '<?= base_url('perkuliahan/jdl_kuliah/getdosen');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nik.value = ui.item.nik;

            this.form.dosen.value = ui.item.value;

        }

    });

});

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>
  					<?php if ($this->session->userdata('sess_login')['userid'] == 'BAA') {
  						echo "Jadwal Perkuliahan ".$this->session->userdata('tahunajaran')." - Semester ".$this->session->userdata('semester')." - Fakultas ".get_fak($this->session->userdata('id_fakultas_prasyarat'));
  					} else {
  						echo "Jadwal Perkuliahan ".$this->session->userdata('tahunajaran')." - Semester ".$this->session->userdata('semester');
  					} ?>
  				</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content" style="padding:30px;">
				<div class="span11">

					<?php

					$user 	= $this->session->userdata('sess_login');
			        $pecah 	= explode(',', $user['id_user_group']);
			        $jmlh 	= count($pecah);
			        for ($i = 0; $i < $jmlh; $i++) { 
			            $grup[] = $pecah[$i];
			        }

					if ($user['id_user_group'] == 1 || $user['id_user_group'] == 10) { ?>

			            <a href="<?= base_url(); ?>perkuliahan/jdl_kuliah" class="btn btn-warning"> << Kembali</a>

				    <?php } ?>

				    <?php if ((count($aktif) == 1) and ($crud->create > 0))  { if ($user['id_user_group'] != 5) { ?> 
	                    
	                    <a data-toggle="modal" class="btn btn-success" href="#myModal">
	                    	<i class="btn-icon-only icon-plus"> </i> Tambah Data
	                    </a>

                    <?php } } ?>

                    <a href="<?= base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info">Print Jadwal</a>
                    
                    <br><hr>

					<table id='example1' class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
								<th>Kelas</th>
								<th>Hari</th>
	                            <th>Kode MK</th>
	                            <th>Matakuliah</th>
								<th>SKS</th>
								<th>Waktu</th>
								<th>Ruang</th>
								<th>Kapasitas</th>
								<th>Peserta</th>
								<th>Dosen</th>

								<?php if ($user['id_user_group'] != 5) { ?>
	                            	<th width="200">Aksi</th>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $no=1; foreach ($jadwal as $isi)  { ?>
	                    	<tr >
                           		<td <?= $warna; ?> ><?= $no; ?></td>
	                        	<td <?= $warna; ?> ><?= $isi->gabung == 0 ? $isi->kelas : getNamaKelas(get_dtl_jadwal($isi->referensi)['id_jadwal']); ?></td>
	                        	<td <?= $warna; ?> ><?= notohari($isi->hari) == "" ? "-" : notohari($isi->hari); ?></td>
	                        	<td <?= $warna; ?> ><?= $isi->kd_matakuliah; ?></td>
	                        	<td <?= $warna; ?> ><?= $isi->nama_matakuliah; ?></td>
	                        	<td <?= $warna; ?> ><?= $isi->sks_matakuliah; ?></td>
	                        	<td <?= $warna; ?> >
	                        		<?= substr($isi->waktu_mulai,0,5).'-'.substr($isi->waktu_selesai,0,5); ?>
	                        	</td>
	                        	<?php if ($isi->kode_ruangan == NULL || $isi->kode_ruangan == '') { ?>
	                        		<td <?= $warna; ?> >-</td> 
	                        		<td <?= $warna; ?> >-</td> 

	                        	<?php }else{ ?>
	                        		<td <?= $warna; ?> ><?= $isi->kode_ruangan; ?></td> 
	                        		<td <?= $warna; ?> ><?= substr($isi->kuota, 0,2)?></td>
	                        	<?php }?>	                        
	                
	                        	<td <?= $warna; ?> >
									<a 
										class="btn btn-primary btn-small" 
										onclick="pesertaShow(<?= $isi->id_jadwal;?>)">
										<i class="btn-icon-only icon-eye-open"></i>
									</a>
	                        	</td>
	                        	<td <?= $warna; ?> ><?= $isi->nama; ?></td>

	                        	<?php if ($user['id_user_group'] != 5) { ?>
		                        	<?php if (($isi->audit_user != 'baa')) { ?>
			                        	<td <?= $warna; ?> >

			                        		<?php if ((count($aktif) == 1) and ($crud->edit > 0)) { ?>
				                        		<a 
				                        			class="btn btn-success btn-small" 
				                        			onclick="edit2(<?= $isi->id_jadwal;?>)" 
				                        			data-toggle="modal" 
				                        			href="#editModal2">
				                        			<i class="btn-icon-only icon-user"></i>
				                        		</a>

			                                    <a 
			                                    	class="btn btn-primary btn-small" 
			                                    	onclick="edit(<?= $isi->id_jadwal;?>)" 
			                                    	data-toggle="modal" href="#editModal1" >
			                                    	<i class="btn-icon-only icon-pencil"></i>
			                                    </a>
		                                    <?php } ?>
		                                    <?php if ($user['id_user_group'] == 19) { ?>
		                                    	<a onclick="return confirm('Apakah Anda Yakin?');" 
			                                    	class="btn btn-danger btn-small" 
			                                    	href="<?= base_url('perkuliahan/jdl_kuliah/delete/'.$isi->id_jadwal); ?>">
			                                    	<i class="btn-icon-only icon-remove"></i>
			                                    </a>
		                                    <?php } ?>
		                                    <a 
		                                    	href="<?= base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
		                                    	target="_blank" 
		                                    	class="btn btn-info btn-small" 
		                                    	title="Berita Acara">
		                                    	<i class="icon icon-file"></i>
		                                    </a>
		                                    
		                                    <a 
		                                    	class="btn btn-warning btn-small" 
		                                    	href="<?= base_url(); ?>akademik/ajar/cetak_absensi/<?= $isi->id_jadwal; ?>" >
		                                    	<i class="btn-icon-only icon-print"></i>
		                                    </a>
										</td>

									<?php } else { ?>

									<td <?= $warna; ?> >

	                                    <?php if (substr($isi->kd_matakuliah, 0,2) == 'MK' && $this->session->userdata('sess_login')['id_user_group'] == 10) { ?>
	                                    	<a 
	                                    		class="btn btn-primary btn-small" 
	                                    		onclick="edit3(<?= $isi->id_jadwal;?>)" 
	                                    		data-toggle="modal" href="#editModal3" >
	                                    		<i class="btn-icon-only icon-pencil"></i>
	                                    	</a>
	                                    <?php } ?>
	                                    
	                                    <a 
	                                    	href="<?= base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
	                                    	target="_blank" 
	                                    	class="btn btn-info btn-small" 
	                                    	title="Berita Acara">
	                                    	<i class="icon icon-file"></i>
	                                    </a>

	                                    <a 
	                                    	class="btn btn-warning btn-small" 
	                                    	href="<?= base_url(); ?>akademik/ajar/cetak_absensi/<?= $isi->id_jadwal; ?>" >
	                                    	<i class="btn-icon-only icon-print"></i>
	                                    </a>

	                                    <?php if ($isi->gabung == 0) { ?>
                                        <a 
                                            data-toggle="modal"
                                            title="lihat kelas turunan"
                                            onclick="see_child(<?= $isi->id_jadwal ?>)"
                                            class="btn btn-info btn-small" 
                                            href="#seeChild" >
                                            <i class="btn-icon-only icon-list"></i>
                                        </a>

                                        <?php } else { ?>
                                            <a 
                                                data-toggle="modal"
                                                title="lihat kelas induk"
                                                onclick="see_parent(<?= $isi->id_jadwal ?>)"
                                                class="btn btn-default btn-small" 
                                                href="#seeChild" >
                                                <i class="btn-icon-only icon-list"></i>
                                            </a>

                                        <?php } ?>
	                                    
									</td>
									<?php } ?>

								<?php } ?>
							</tr>
							<?php $no++; } ?>
	                    </tbody>

	               	</table>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Buat Jadwal Baru</h4>

            </div>

            <form class ='form-horizontal' action="<?= base_url();?>perkuliahan/jdl_kuliah/save_data" method="post">

                <div class="modal-body">

					<div class="control-group">

                      <label class="control-label">Jurusan</label>

                      <div class="controls">

                        <input type='hidden' class="form-control" name="jurusan" value="<?= $this->session->userdata('id_jurusan_prasyarat'); ?>" readonly >

                        <input type='text' class="form-control"  value="<?= $this->session->userdata('nama_jurusan_prasyarat'); ?>" readonly >

                      </div>

                    </div>

                    <script type="text/javascript">

                    	$(document).ready(function(){

							$('#semester').change(function(){

								$.post('<?= base_url();?>perkuliahan/jdl_kuliah2/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){

									$('#ala_ala').html(get);

								});

							});

						});

                    </script>

                    <div class="control-group">

                      <label class="control-label">Semester</label>

                      <div class="controls">

                        <select id="semester" class="form-control" name="semester" required="">

                          <option>--Pilih Semester--</option>

                          <option value="1">1</option>

                          <option value="2">2</option>

                          <option value="3">3</option>

                          <option value="4">4</option>

                          <option value="5">5</option>

                          <option value="6">6</option>

                          <option value="7">7</option>

                          <option value="8">8</option>

                        </select>

                      </div>

                    </div>

                    <div class="control-group">
						<label class="control-label">Kelompok Kelas</label>
						<div class="controls">
							<input type="radio" name="st_kelas" value="PG" required=""> A &nbsp;&nbsp; 
							<input type="radio" name="st_kelas" value="SR" required=""> B &nbsp;&nbsp;
							<input type="radio" name="st_kelas" value="PK" required=""> C  &nbsp;&nbsp;
						</div>
					</div>

                    	<div class="control-group">											

							<label class="control-label" for="kelas">Nama Kelas</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" required>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<script type="text/javascript">

						$(document).ready(function() {

							$('#ala_ala').change(function() {

								$.post('<?= base_url();?>perkuliahan/jdl_kuliah/get_kode_mk/'+$(this).val(),{},function(get){

									$('#kode_mk').val(get);

								});

							});

						});

						</script>



						<div class="control-group">											

							<label class="control-label" for="firstname">Hari Kuliah</label>

							<div class="controls">

								<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

								<select class="form-control" name="hari_kuliah" id="hari_kuliah" required>

									<option disabled>--Hari Kuliah--</option>

									<option value="1">Senin</option>

									<option value="2">Selasa</option>

									<option value="3">Rabu</option>

									<option value="4">Kamis</option>

									<option value="5">Jum'at</option>

									<option value="6">Sabtu</option>

									<option value="7">Minggu</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->	

                    

						<div class="control-group">											

							<label class="control-label" for="firstname">Nama Matakuliah</label>

							<div class="controls">

								<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

								<select class="form-control" name="nama_matakuliah" id="ala_ala">

									<option>--Pilih MataKuliah--</option>

									

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->    

    					

    					<div class="control-group">										

							<label class="control-label" for="kode_mk">Kode MK</label>

							<div class="controls">

								<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk" readonly>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->



						<script type="text/javascript">

                        	$(document).ready(function(){

								$('#gedung').change(function(){

									$.post('<?= base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

										$('#lantai').html(get);

									});

								});

							});

                    	</script>

						<div class="control-group">											

							<label class="control-label" for="gedung">Gedung</label>

							<div class="controls">

								<select class="form-control" name="gedung" id="gedung">

									<option value="">--Pilih Gedung--</option>

									<?php foreach ($gedung as $isi) {?>

										<option value="<?= $isi->id_gedung ?>"><?= $isi->gedung ?></option>	

									<?php } ?>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<script type="text/javascript">

                        	$(document).ready(function(){

								$('#lantai').change(function(){

									$.post('<?= base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

										$('#ruangan').html(get);

									});

								});

							});

                    	</script>

						<div class="control-group">											

							<label class="control-label" for="lantai">Lantai</label>

							<div class="controls">

								<select name="lantai" id="lantai">

									<option value="">--Pilih Lantai--</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						

						<div class="control-group">											

							<label class="control-label" for="ruangan">Ruangan</label>

							<div class="controls">

								<select name="ruangan" id="ruangan" required>

									<option value="">--Pilih Ruangan--</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						

						<!-- <div class="control-group">											

							<label class="control-label" for="dosen">Dosen</label>

							<div class="controls">

								<input type="hidden" name="id_kary" id="id_kary">

								<input type="text" name="dosen" id="dosen">

							</div> 				

						</div>  -->



					

						<!-- <div class="control-group">											

							<label class="control-label" for="firstname">Perasyarat</label>

							<div class="controls">

							<select class="form-control" name="prasyarat_matakuliah[]" multiple>

								<?php //foreach($matakuliah as $row){ ?>

									<option value="<?php //echo $row->kd_matakuliah; ?>"><?php //echo $row->nama_matakuliah; ?></option>

								<?php //} ?>

							</select>												

							</div>				

						</div> -->

						<div class="control-group">											

							<label class="control-label" for="jam">Jam Masuk</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk" required>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<!-- <div class="control-group">											

							<label class="control-label" for="jam">Jam Keluar</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Jam keluar" id="jam_keluar" name="jam_keluar">

							</div> 

						</div> -->

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">            

        </div>
    </div>
</div>

<div class="modal fade" id="editModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit2">
        	
        </div>
    </div>
</div>

<div class="modal fade" id="editModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit3">
        	
        </div>
    </div>
</div>

<div class="modal fade" id="seeChild" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="see_child">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
