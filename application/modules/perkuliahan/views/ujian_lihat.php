<link href="<?php echo base_url();?>assets/css/plugins/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script type="text/javascript">
$(document).ready(function(){
	$('#dup').hide();
	$('#simpan').prop('disabled', true);
	
	function edit(idk){
		$('#edit').load('<?php echo base_url();?>perkuliahan/jadwalujian/edit/'+idk);
	}
	
	var table = $('#tabel_jadwal');
	
	var oTable = table.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }],
		"bLengthChange": false,
		"bInfo": false,
	});
	
	oTable.on( 'click', 'tr', function () {
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
        }
        else {
            table.$('tr.active').removeClass('active');
            $(this).addClass('active');
			var aData = oTable.fnGetData(this);
			$('#kd_jadwal').val(aData[5]);
			$('#sks_matakuliah').val(aData[3]);
        }
    } );
	
	$('#jam_masuk').timepicker();
	
	$('#jam_masuk').change(function(){
		var jam_masuk = $('#jam_masuk').val();
		var sks_matakuliah = $('#sks_matakuliah').val();
		var ruangan_ujian = $('#ruangan_ujian').val();
		var hari = $('#hari').val();
		var tanggal = $('#tanggal').val();
		$.ajax({
			url: "<?php echo base_url('perkuliahan/jadwalujian/cek_jam'); ?>",
            type: "post",
            data: {jam_masuk:jam_masuk,sks_matakuliah:sks_matakuliah, ruangan_ujian:ruangan_ujian, hari:hari, tanggal:tanggal},
            success: function(d) {
				if(d!=0){
					alert('bentrok');
					$('#dup').show();
					$('#jam_masuk').addClass('alert');
					$('#simpan').prop('disabled', true);
				}else{
					alert('tidak bentrok');
					$('#dup').hide();
					$('#jam_masuk').removeClass('alert');
					$('#simpan').prop('disabled', false);
				}
            }
        });
    });
	$('#tanggal').datepicker({dateFormat: 'yy-mm-dd'});
	
});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Jadwal Ujian <?php echo $fakultas; ?> <?php echo $prodi; ?> <?php $angk = $this->app_model->getdetail('tbl_tahunajaran', 'id_tahunajaran', $angkatan, 'id_tahunajaran', 'ASC')->row(); echo $angk->tahunajaran; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url(); ?>perkuliahan/jadwalujian" class="btn btn-warning"> << Kembali </a>
                    <a data-toggle="modal" class="btn btn-success" href="#myModal"><i class="btn-icon-only icon-plus"> </i> Tambah Data</a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Hari/Tanggal</th>
                                <th>Waktu</th>
                                <th>SKS</th>
                                <th>Ruang</th>
                                <th>Dosen</th>
                                <th>Pengawas</th>
                                <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($jadwal as $value) { ?>
	                        <tr>
	                        	<td>1</td>
                                <td><?php echo $value->kd_matakuliah; ?></td>
                                <td><?php echo $value->nama_matakuliah; ?></td>
                                <td><?php echo $value->hari_jadwal_ujian; ?></td>
                                <td><?php echo $value->mulai_jadwal_ujian.'-'.$value->akhir_jadwal_ujian; ?></td>
                                <td><?php echo $value->sks_matakuliah; ?></td>
                                <td><?php echo $value->kode_ruangan; ?></td>
                                <td><?php echo $value->kd_dosen; ?></td>
                                <td>ALLAH SWT</td>
                                <td class="td-actions">
                                    <a onclick="edit(1)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM TAMBAH DATA JADWAL UJIAN</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>perkuliahan/jadwalujian/save_jadwal" method="post">
                <input type="hidden" name="jurusan" value=""/>
                <input type="hidden" name="angkatan" value=""/>
                <input type="hidden" name="kelas" value=""/>
				<input type="hidden" name="kd_jadwal" id="kd_jadwal" value=""/>
				<input type="hidden" name="sks_matakuliah" id="sks_matakuliah" value=""/>
                <div class="modal-body" >
					<table id="tabel_jadwal" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
								<th>SKS</th>
								<th>Dosen</th>
								<th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($jadwal_matkul as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
                                <td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td>Hendarman Lubis,M.Kom</td>
								<td><?php echo $row->kd_jadwal; ?></td>
                            </tr>
                            <?php $no++;} ?>
	                    </tbody>
	               	</table>
                    <div class="control-group" id="">
                        <label class="control-label">Hari/Tanggal</label>
                        <div class="controls">
                            <select name="hari_ujian" id="hari" class="span2 form-control" required>
								<option value="">- Pilih Hari -</option>
								<option value="1">Senin</option>
								<option value="2">Selasa</option>
								<option value="3">Rabu</option>
								<option value="4">Kamis</option>
								<option value="5">Jumat</option>
								<option value="6">Sabtu</option>
								<option value="7">Minggu</option>
							</select> / <input type="text" name="tanggal_ujian" id="tanggal" class="span2 form-control" value="" required/>
                        </div>
                    </div>
					<script type="text/javascript">
	                                    	$(document).ready(function(){
												$('#gedung').change(function(){
													$.post('<?php echo base_url();?>perkuliahan/jadwalujian/get_lantai/'+$(this).val(),{},function(get){
														$('#lantai').html(get);
													});
												});
											});
                                    	</script>
										<div class="control-group">											
											<label class="control-label" for="gedung">Gedung</label>
											<div class="controls">
												<select class="form-control" name="gedung" id="gedung">
													<option>--Pilih Gedung--</option>
													<?php foreach ($gedung as $isi) {?>
														<option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>	
													<?php } ?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<script type="text/javascript">
	                                    	$(document).ready(function(){
												$('#lantai').change(function(){
													$.post('<?php echo base_url();?>perkuliahan/jadwalujian/get_ruangan/'+$(this).val(),{},function(get){
														$('#ruangan_ujian').html(get);
													});
												});
											});
                                    	</script>
										<div class="control-group">											
											<label class="control-label" for="lantai">Lantai</label>
											<div class="controls">
												<select name="lantai" id="lantai">
													<option>--Pilih Lantai--</option>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ruangan">Ruangan</label>
											<div class="controls">
												<select name="ruangan_ujian" id="ruangan_ujian">
													<option>--Pilih Lantai--</option>
												</select>
											</div> <!-- /controls -->				
										</div>
                    
                    <div class="control-group" id="">
                        <label class="control-label">Waktu</label>
                        <div class="controls">
                            <input type="text" class="form-control span3" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk">
							<div id="dup" class="span3" style="float: none;margin: 0px;">
								<p></p>
								<div class="alert" style="margin: 0px;">Jam Berbenturan</div>
							</div>
					    </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Pengawas</label>
                        <div class="controls">
                            <select name="pengawas_ujian" >
								<option value="">- Pilih Pengawas -</option>
							</select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <input type="submit" id="simpan" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->