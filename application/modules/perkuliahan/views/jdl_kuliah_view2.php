<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Jadwal Perkuliahan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>perkuliahan/jdl_kuliah2/save_session">
                        <fieldset>
                      <script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>perkuliahan/jadwal/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>
                              <div class="control-group">
                                <label class="control-label">Fakultas</label>
                                <div class="controls">
                                  <select class="form-control span6" name="fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              

                              <div class="control-group">
                                <label class="control-label">Jurusan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs">
                                    <option>--Pilih Jurusan--</option>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>  

                              <div class="control-group">
                                <label class="control-label">Semester</label>
                                <div class="controls">
                                  <select class="form-control span6" name="smt" id="">
                                    <option>--Pilih Semester--</option>
                                    <?php for ($i=1; $i < 9; $i++) { ?>
                                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
