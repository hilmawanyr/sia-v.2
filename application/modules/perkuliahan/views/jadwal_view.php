<script>
function edit(id){
$('#edit_tipe').load('<?php echo base_url();?>perkuliahan/jadwal/edit/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>Data Jadwal Mata Kuliah</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<?php 
						if($this->session->userdata('id_fakultas_prasyarat')%2 == 1){
							$sts = 1;
						}else{
							$sts = 0;
						}
						
						for ($i=1; $i <= 8; $i++) { 
							if($i%2==$sts){
					?>
					
					<h3>Semester <?php echo $i; ?></h3>
						<table id="example<?php echo $i; ?>" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th width="40">No</th>
		                        	<th>Kode MK</th>
	                                <th>Mata Kuliah</th>
	                                <th>SKS</th>
	                                <th>Dosen</th>
	                                <th>Hari/Waktu</th>
	                                <th>Ruang</th>
		                            <th width="40">Aksi</th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                            <?php $no=1;foreach (${'data_table'.$i} as $value) { ?>
		                        <tr>
		                        	<td><?php echo $no ?></td>
		                        	<td><?php echo $value->kd_matakuliah; ?></td>
		                        	<td><?php echo $value->nama_matakuliah; ?></td>
		                        	<td><?php echo $value->sks_matakuliah; ?></td>
		                        	<td><?php  ?></td>
		                        	<td><?php  ?></td>
		                        	<td><?php  ?></td>
		                        	<td class="td-actions">
										<a onclick="edit(1)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-user"> </i></a>
									</td>
		                        </tr>
								<?php $no++; } ?>
		                    </tbody>
		               	</table>
		               	<br><hr>
					<?php } } ?>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Kegiatan</h4>
            </div>
            <form class ='form-horizontal' action="#" method="post">
                <div class="modal-body" style="margin-left:-20px;">
                    <div class="control-group" id="">
                        <label class="control-label">Tipe Mata Kuliah</label>
                        <div class="controls">
                            <input type="text" class="span4" name="" placeholder="" class="form-control" value="" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_tipe">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->