<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(35)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');
        if ($user['id_user_group'] == 1 || $user['id_user_group'] == 10) {
            $data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'id', 'ASC')->result();
            
            $data['page']='jdl_kuliah_view2';
            $this->load->view('template/template', $data);
            
        }elseif ($user['id_user_group'] == 8) {
            
            $q = $this->db->query('
                            SELECT * FROM tbl_karyawan 
                            LEFT JOIN tbl_jabatan ON tbl_karyawan.`jabatan_id` = tbl_jabatan.`id_jabatan`
                            LEFT JOIN tbl_jurusan_prodi ON tbl_jabatan.`kd_divisi` = tbl_jurusan_prodi.`kd_prodi`
                            LEFT JOIN tbl_fakultas ON tbl_fakultas.`kd_fakultas` = tbl_jurusan_prodi.`kd_fakultas`
                            WHERE tbl_karyawan.`nik`="'.$user["userid"].'"')->row();


            $this->session->set_userdata('id_fakultas_prasyarat', $q->kd_fakultas);
            $this->session->set_userdata('nama_fakultas_prasyarat', $q->fakultas);
            $this->session->set_userdata('id_jurusan_prasyarat', $q->kd_prodi);
            $this->session->set_userdata('nama_jurusan_prasyarat', $q->prodi);

            redirect(base_url('perkuliahan/jadwal/view_matakuliah'));
        }
	}
	
	function get_jurusan($id){
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}
	
	function save_session(){
		$fakultas = explode('-',$this->input->post('fakultas'));
		$jurusan = explode('-',$this->input->post('jurusan'));
		$this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);
		$this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);
		$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);		
		$this->session->set_userdata('kd_tahunajaran', $this->input->post('jurusan'));
		
		redirect(base_url('perkuliahan/jadwal/view_matakuliah'));
	}

	function view_matakuliah(){
		$id = $this->session->userdata('id_jurusan_prasyarat');
		if($this->session->userdata('id_fakultas_prasyarat')%2 == 1){
			$data['data_table1']=$this->app_model->get_matakuliah_jadwal($id, 1);
			$data['data_table3']=$this->app_model->get_matakuliah_jadwal($id, 3);
			$data['data_table5']=$this->app_model->get_matakuliah_jadwal($id, 5);
			$data['data_table7']=$this->app_model->get_matakuliah_jadwal($id, 7);
		}else{
			$data['data_table2']=$this->app_model->get_matakuliah_jadwal($id, 2);
			$data['data_table4']=$this->app_model->get_matakuliah_jadwal($id, 4);
			$data['data_table6']=$this->app_model->get_matakuliah_jadwal($id, 6);
			$data['data_table8']=$this->app_model->get_matakuliah_jadwal($id, 8);
		}
		
		$data['gedung'] = $this->db->get('tbl_gedung')->result();

		$data['page'] ='jadwal_view';
		$this->load->view('template/template', $data);

	}

	function edit($id)
	{
		$this->load->view('jadwal_edit');
	}

}

/* End of file viewkrs.php */
/* Location: .//C/Users/danum246/Desktop/viewkrs.php */