<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jdl_kuliah_baa extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('Cfpdf');
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(136)->result();
            if (!$cekakses) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
    }

	public function index()
	{
        $data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
        $data['page']      ='jdl_kuliah_view3';
        $this->load->view('template/template', $data);		
	}

    public function view()
    {
        // error_reporting(0);
        $this->load->model('perkuliahan/model_perkuliahan', 'perkuliahan');
        $data['year']      = $this->session->userdata('ta');
        $data['faculties'] = $this->app_model->getfakultas();
        $data['gedung']    = $this->db->get('tbl_gedung')->result();
        $data['matkul']    = $this->app_model->getdatamku()->result();
        $data['dataMku']   = $this->perkuliahan->get_mku($data['year'])->result();
        $data['page']      = 'jdl_kuliah_view_baa';
        $this->load->view('template/template', $data);       
    }

    function jumlahpeserta($idjdl)
    {
        // get detail jadwal
        $this->db->select('kd_matakuliah,kd_dosen,kd_jadwal');
        $this->db->where('id_jadwal', $idjdl);
        $detail = $this->db->get('tbl_jadwal_matkul')->row();
        // get amount of student
        $kdjdl = get_kd_jdl($idjdl);
        $this->db->select('npm_mahasiswa');
        $this->db->from('tbl_krs');
        $this->db->where('kd_jadwal', $kdjdl);
        $amount = $this->db->get()->num_rows();

        $data = [
            'title' => $detail->kd_matakuliah.' - '.get_nama_mk($detail->kd_matakuliah,substr($detail->kd_jadwal, 0,5)),
            'nmdosen' => nama_dsn($detail->kd_dosen),
            'jumlah' => $amount
        ];

        echo json_encode($data);
    }

	function sessdestroy()
	{
		$this->session->unset_userdata('sess_ringkasan');
        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
		
	}
    
    function serverside_loadmku()
    {
        $data = array();

        $load = $this->app_model->getdatajadwalmku($this->session->userdata('ta'))->result();

        foreach ($load as $val) {
            $row = array();
            $row[] = $val->kd_matakuliah;
            $row[] = get_nama_mk($val->kd_matakuliah,substr($val->kd_jadwal, 0,5));
            $row[] = $val->kd_tahunajaran;
            $row[] = get_jur(substr($val->kd_jadwal, 0,5));
            $row[] = get_fak_byprodi(substr($val->kd_jadwal, 0,5));
            $row[] = notohari($val->hari).' / '.substr($val->waktu_mulai,0,5).'-'.substr($val->waktu_selesai,0,5);
            $row[] = nama_dsn($val->kd_dosen);
            $row[] = $val->kode_ruangan;
            $row[] = get_amount_mhs($val->kd_jadwal);
            $row[] = 
                '<a 
                    data-toggle="modal" 
                    onclick="dosen('.$row->id_jadwal.')" 
                    href="#editModal1"
                    class="btn btn-success btn-small" >
                    <i class="btn-icon-only icon-user"></i>
                </a>'.
                '<a 
                    class="btn btn-primary btn-small" 
                    onclick="edit('.$row->id_jadwal.')" 
                    data-toggle="modal" 
                    href="#editModal" >
                    <i class="btn-icon-only icon-pencil"></i>
                </a>'.
                '<a 
                    class="btn btn-warning btn-small" 
                    href="'.base_url().'akademik/ajar/cetak_absensi/'.$row->id_jadwal.'" >
                    <i class="btn-icon-only icon-print"></i>
                </a>';
            $data[] = $row;
        }

        $columns = array(
                        "recordsTotal" => 10,
                        "recordsFiltered" => 20,
                        "data" => $data
                    );

        echo json_encode($columns);
    }

    public function save_session()
    {
        $ta = $this->input->post('tahunajaran', TRUE);
        $this->session->set_userdata('ta',$ta);
        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
    }

    public function openCloseClass($id)
    {
        $openOrClose = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();

        if (is_null($openOrClose->open)) {
            $this->db->where('id_jadwal', $id);
            $this->db->update('tbl_jadwal_matkul', ['open' => 1]);
        } else {
            $this->db->where('id_jadwal', $id);
            $this->db->update('tbl_jadwal_matkul', ['open' => NULL]);
        }
        redirect(base_url('perkuliahan/jdl_kuliah_baa/view'));
    }

    function get_detail_matakuliah_by_semester($id)
    {
        $user = $this->session->userdata('sess_login');
        $kode = $user['userid'];
        $data = $this->db->query('SELECT 
                                    DISTINCT a.kd_matakuliah, 
                                    a.nama_matakuliah, 
                                    a.id_matakuliah 
                                FROM tbl_matakuliah a
                                JOIN tbl_kurikulum_matkul_new b ON a.kd_matakuliah = b.kd_matakuliah
                                WHERE a.kd_prodi = '.$kode.' 
                                AND b.semester_kd_matakuliah = '.$id.' 
                                AND b.`kd_kurikulum` LIKE "%'.$kode.'%" 
                                AND (a.kd_matakuliah like "MKU-%" 
                                OR a.kd_matakuliah like "MKDU-%")
                                ORDER BY nama_matakuliah asc')->result();

        $list = "<option value=''> -- </option>";

        foreach($data as $row) {
            $list .= "<option value='".$row->id_matakuliah."'>".$row->kd_matakuliah." - ".$row->nama_matakuliah."</option>";
        }

        echo $list;
    }

	public function save()
	{
        $faculty       = $this->input->post('faculty');
        $matakuliah    = explode('___',$this->input->post('mk'));
        $jamMasuk      = $this->input->post('jam_masuk');
        $hari          = $this->input->post('hari');
        $ruang         = $this->input->post('ruangan');
        $isJoin        = $this->input->post('gabung');
        $reference     = $this->input->post('kelas_gab');
        $kelas         = $this->input->post('nm_kelas');
        $kategoriKelas = $this->input->post('st_kelas');

        if ($isJoin == '0') {
            // if ruangan 999 dont do conflict check
            $getRoom = get_room($ruang);
            $room    = str_replace('R. ', '', $getRoom);
            if ($room != '999') {
                $this->_isScheduleConflict($jamMasuk, $this->session->userdata('ta'), $hari, $ruang);   
            }
        }

        if ($isJoin == '1') {
            $this->_join_validation($reference);
            $this->_is_ref_match($matakuliah[0],$reference);
        }

        $randomChar = $this->_randChar();
        $kodeJadwal = $this->_generateKodeJadwal($faculty, $randomChar);
        $endTime    = $this->_sksAmount($matakuliah[0], $jamMasuk);

		$datax = [
            'kelas'         => $kelas,
            'waktu_kelas'   => $kategoriKelas,
            'kd_jadwal'     => $kodeJadwal,
            'hari'          => $hari,
            'id_matakuliah' => $matakuliah[1],
            'kd_matakuliah' => $matakuliah[0],
            'kd_ruangan' 	=> $ruang,
            'waktu_mulai'   => $jamMasuk,
            'waktu_selesai' => $endTime,
            'kd_tahunajaran'=> $this->session->userdata('ta'),
            'audit_date'    => date('Y-m-d H:i:s'),
            'audit_user'    => 'baa',
            'gabung' 		=> $isJoin,
            'referensi' 	=> $reference
		];

		$this->db->insert('tbl_jadwal_matkul',$datax);
        echo "<script>alert('Sukses');
		document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
	}

    function _join_validation($kodeJadwal)
    {
        $isJoisSchedule = $this->db->where('kd_jadwal', $kodeJadwal)->get('tbl_jadwal_matkul')->row()->gabung;
        if ($isJoisSchedule > 0) {
            echo "<script>alert('Jadwal yang anda gabungkan bukan merupakan jadwal induk!');history.go(-1);</script>";
            exit(); 
        }
        return;
    }

    function _is_ref_match($kodeMatakuliah, $kodeJadwalReferensi)
    {
        $getMkRef = $this->db->where('kd_jadwal', $kodeJadwalReferensi)->get('tbl_jadwal_matkul')->row()->kd_matakuliah;
        if ($kodeMatakuliah != $getMkRef) {
            echo "<script>alert('Gagal menyimpan! Kode matakuliah tidak sama dengan kode matakuliah induk');history.go(-1);</script>";
            exit(); 
        }
        return;
    }

    function isLectureConflict($lecture, $day, $startTime, $year)
    {
        $check  = $this->db->query("SELECT * FROM tbl_jadwal_matkul 
                                    WHERE kd_dosen = $lecture 
                                    AND hari = $day
                                    AND waktu_mulai = $startTime
                                    AND kd_tahunajaran = $year
                                    ORDER BY id_jadwal DESC LIMIT 1");
        if ($check->num_rows() > 0) {
            echo "<script>alert('Jadwal dosen berbenturan dengan jadwal ".$check->row()->kd_matakuliah." kelas ".$check->row()->kelas."')</script>";
            exit();
        }
        return;
    }

    function _isScheduleConflict($startTime, $year, $day, $room, $isUpdate=NULL)
    {
        $strTime = new DateTime($startTime);
        $strTime->modify('-150 minutes');

        $time    = (array) $strTime;
        $getTime = explode(' ', explode('.',$time['date'])[0]);
        
        if (!is_null($isUpdate)) {
            $check  = $this->db->query("SELECT * FROM tbl_jadwal_matkul 
                                    WHERE kd_tahunajaran = $year
                                    AND hari = $day
                                    AND kd_ruangan = $room
                                    AND id_jadwal <> $isUpdate
                                    AND (waktu_mulai = '".$startTime."' AND waktu_mulai BETWEEN '".$getTime[1]."' AND '".$startTime."')
                                    ORDER BY id_jadwal DESC LIMIT 1");

        } else {
            $check  = $this->db->query("SELECT * FROM tbl_jadwal_matkul 
                                        WHERE kd_tahunajaran = $year
                                        AND hari = $day
                                        AND kd_ruangan = $room
                                        AND (waktu_mulai = '".$startTime."' AND waktu_mulai BETWEEN '".$getTime[1]."' AND '".$startTime."')
                                        ORDER BY id_jadwal DESC LIMIT 1");    
        }
        
        if ($check->num_rows() > 0) {
            echo "<script>alert('Jadwal kelas berbenturan');
            location.href='".base_url('perkuliahan/jdl_kuliah_baa/conflict_schedule_list/'.$startTime.'/'.$getTime[1].'/'.$day.'/'.$year.'/'.$room)."';</script>";
            exit();
        }
        return;
    }

    function conflict_schedule_list($startTime, $minTime, $day, $year, $room)
    {
        $data['year'] = $year;
        $data['jadwals']    = $this->db->query("SELECT * FROM tbl_jadwal_matkul 
                                                WHERE kd_tahunajaran = $year
                                                AND hari = $day
                                                AND kd_ruangan = $room
                                                AND (waktu_mulai = '".$startTime."' AND waktu_mulai BETWEEN '".$minTime."' AND '".$startTime."')
                                                ORDER BY id_jadwal")->result();
        $data['page'] = "conflict_schedule_v";
        $this->load->view('template/template', $data);
    }

    function see_child($id)
    {
        $kodeJadwal           = get_kd_jdl($id);
        $data['parentDetail'] = $this->db->where('kd_jadwal', $kodeJadwal)->get('tbl_jadwal_matkul')->row();
        $data['jadwals']      = $this->db->where('referensi', $kodeJadwal)->get('tbl_jadwal_matkul')->result();
        $this->load->view('modal_see_child', $data);
    }

    function see_parent($id)
    {
        $getParent            = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();
        $data['parentDetail'] = $this->db->where('kd_jadwal', $getParent->referensi)->get('tbl_jadwal_matkul')->row();
        $this->load->view('modal_see_parent', $data);
    }

    function _randChar()
    {
        $n   = 6; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        $gpass = NULL;
        for ($i = 0; $i < $n; $i++) {
            $rIdx  = rand(1, strlen($chr));
            $gpass .=substr($chr, $rIdx, 1);
        }
        return $gpass;
    }

    function _generateKodeJadwal($faculty, $randomChar)
    {
        $noUrutJadwal = $this->db->query('SELECT COUNT(kd_jadwal) as jml 
                                    FROM tbl_jadwal_matkul 
                                    WHERE kd_jadwal LIKE "'.$faculty.'/%" 
                                    AND kd_tahunajaran = '.$this->session->userdata('ta').'')->row();
        $hasil = $noUrutJadwal->jml;
        
        if ($hasil == 0) {
            $kodeJadwal = $faculty."/".$randomChar."/001";
        } elseif ($hasil < 10) {
            $kodeJadwal = $faculty."/".$randomChar."/00".($hasil+1);
        } elseif ($hasil < 100) {
            $kodeJadwal = $faculty."/".$randomChar."/0".($hasil+1);
        } elseif ($hasil < 1000) {
            $kodeJadwal = $faculty."/".$randomChar."/".($hasil+1);
        } elseif ($hasil < 10000) {
            $kodeJadwal = $faculty."/".$randomChar."/".($hasil+1);
        }

        return $kodeJadwal;
    }

    function _sksAmount($kd_mk, $jam_masuk)
    {
        $mataKuliah  = $this->db->where('kd_matakuliah', $kd_mk)->get('tbl_matakuliah')->row();
        $waktuSks    = ($mataKuliah->sks_matakuliah)*50;
        $time2       = strtotime($jam_masuk) + $waktuSks*60;
        $jumlah_time = date('H:i', $time2);
        return $jumlah_time;
    }

    function load_dosen($idk,$gabungan)
    {
        $data['jadwal']   = $this->db->where('id_jadwal',$idk)->get('tbl_jadwal_matkul')->row();
        $data['dosen']    = $this->db->get('tbl_karyawan')->result();
        $data['gabungan'] = $gabungan;
        $this->load->view('penugasan_dosen_baa',$data);

    }

    function load_dosen_autocomplete()
    {
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        $this->db->like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = [
                        'id_kary' => $row->id_kary,
                        'nid'     => $row->nid,
                        'value'   => $row->nama
                    ];
        }
        echo json_encode($data);
    }

    function update_dosen()
    {
        $dosen    = $this->input->post('kd_dosen');
        $id       = $this->input->post('id_jadwal');
        $gabungan = $this->input->post('gabungan');
        $jadwal   = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();

        if ($gabungan != 1) {
            $cekjumlahsks   = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                                JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                                WHERE 
                                                    (mk.nama_matakuliah not like "tesis%" 
                                                    AND mk.nama_matakuliah not like "skripsi%" 
                                                    AND mk.nama_matakuliah not like "kerja praktek%" 
                                                    AND mk.nama_matakuliah not like "magang%" 
                                                    AND mk.nama_matakuliah not like "kuliah kerja%"
                                                    AND mk.nama_matakuliah not like "kkn%") 
                                                AND jdl.kd_tahunajaran = "'.$this->session->userdata('ta').'" 
                                                AND jdl.gabung = 0
                                                AND jdl.`kd_dosen` = "'.$dosen.'" ')->row()->jumlah;

            if ($cekjumlahsks > 18) {
                echo "<script>alert('SKS dosen melebihi ketentuan!');history.go(-1);</script>";
                exit();
            }
        }

        $jadwal_dosen = $this->db->query('SELECT * FROM tbl_jadwal_matkul jdl 
                                          WHERE jdl.`kd_tahunajaran` = "'.$this->session->userdata('ta').'" 
                                          AND jdl.`kd_dosen` = "'.$dosen.'" 
                                          AND jdl.`hari` = '.$jadwal->hari.'')->result();

        /**
        $jadwal_masuk1 = $jadwal->waktu_mulai;
        $jadwal_keluar1= $jadwal->waktu_selesai;
         
        $msk1 = explode(":", $jadwal_masuk1);
        $menit_masuk1 = $msk1[0] * 60 + $msk1[1];

        $klr1 = explode(":", $jadwal_keluar1);
        $menit_keluar1 = $klr1[0] * 60 + $klr1[1];

        foreach ($jadwal_dosen as $key) {

            $jadwal_masuk2 = $key->waktu_mulai;
            $jadwal_keluar2= $key->waktu_selesai;
             
            $msk2 = explode(":", $jadwal_masuk2);
            $menit_masuk2 = $msk2[0] * 60 + $msk2[1];

            $klr2 = explode(":", $jadwal_keluar2);
            $menit_keluar2 = $klr2[0] * 60 + $klr2[1];

            if (($menit_masuk1 > $menit_masuk2 && $menit_masuk1 < $menit_keluar2) || ($menit_keluar1 > $menit_masuk2 && $menit_keluar1 < $menit_keluar2) || ($menit_masuk1 == $menit_masuk2)) {
                echo "<script>alert('JadwaL Dosen Bentrok');
                document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
                exit();
            }   
        }
        */
        
        $data = array( 'kd_dosen' => $dosen );

        $this->db->where('id_jadwal',$id)->update('tbl_jadwal_matkul', $data);

        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
    }

    function edit_jadwal($id)
    {
        $data['years']    = $this->session->userdata('ta'); 
        $data['gedung']   = $this->db->get('tbl_gedung')->result();
        $data['idJadwal'] = $id;
        $data['rows']     = $this->db->query('SELECT 
                                                jdl.*,
                                                mk.`nama_matakuliah`,
                                                mk.`semester_matakuliah`,
                                                b.* 
                                            FROM tbl_jadwal_matkul jdl
        									LEFT JOIN tbl_ruangan b ON jdl.`kd_ruangan` = b.`id_ruangan` 
                                            JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                            WHERE id_jadwal = '.$id.'
                                            GROUP BY jdl.`kd_jadwal` ')->row();

        $prodi          = substr($data['rows']->kd_jadwal, 0,5);
        
        $data['matkul'] = $this->app_model->getdatamku_edit($prodi)->result();

        if ($data['rows']->gabung > 0) {
            $idJadwalKelasInduk = $this->db->where('kd_jadwal', $data['rows']->referensi)->get('tbl_jadwal_matkul')->row()->id_jadwal;
            $data['kelasInduk'] = $this->db->query("SELECT a.id_jadwal, a.kd_jadwal, a.kd_matakuliah, a.kelas, b.nama_matakuliah FROM tbl_jadwal_matkul a 
                                                    JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah
                                                    WHERE a.kd_matakuliah = '".$data['rows']->kd_matakuliah."'
                                                    AND a.kd_tahunajaran = '".$data['years']."'
                                                    AND a.id_jadwal != '".$idJadwalKelasInduk."'
                                                    AND a.gabung = 0
                                                    GROUP BY a.kd_matakuliah")->result();   
        }

        $this->load->view('v_jdl_kuliah_edit_baa', $data);

    }

    function update_data_matakuliah()
    {
        $idJadwal   = $this->input->post('id_jadwal');
        $matakuliah = explode('___', $this->input->post('matakuliah'));
        $hari       = $this->input->post('hari_kuliah');
        $jam_masuk  = $this->input->post('jam_masuk');
        $kelas      = $this->input->post('kelas');
        $ruang      = $this->input->post('ruangan');
        $waktuKelas = $this->input->post('st_kelas');
        $isJoin     = $this->input->post('gabung');
        $reference  = $this->input->post('kelas_gab');
        
        // set end time of class
        $sks         = $this->db->where('kd_matakuliah', $matakuliah[0])->get('tbl_matakuliah')->row();
        $waktuSks    = ($sks->sks_matakuliah)*50;
        $time2       = strtotime($jam_masuk) + $waktuSks*60;
        $jumlah_time = date('H:i', $time2);

        if ($isJoin < 1) {
            $this->_isScheduleConflict($jam_masuk, $this->session->userdata('ta'), $hari, $ruang, $idJadwal);
        }
        
        $data['hari']          = $hari;
        $data['waktu_mulai']   = $jam_masuk;
        $data['waktu_selesai'] = $jumlah_time;
        $data['waktu_kelas']   = $waktuKelas;
        $data['kd_ruangan']    = $ruang;
        $data['kelas']         = $kelas;
        $data['gabung']        = $isJoin;
        $data['referensi']     = $reference;        

        $this->db->where('id_jadwal',$idJadwal)->update('tbl_jadwal_matkul', $data);

        echo "<script>alert('Update Sukses!');
        document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
    }

    function get_lantai($id)
    {
        $data = $this->app_model->get_lantai($id);
        $list = "<option> -- </option>";
        foreach($data as $row){
            $list .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";
        }
        die($list);
    }

    function get_kelas_gabungan($isJoined,$mk,$idjadwal=0)
    {
    	$this->load->model('monitoring_model');
        $tahunakademik     = $this->session->userdata('ta');
        $getKodeMatakuliah = explode('___', $mk);

    	$data = $this->monitoring_model->get_mk_gabung($getKodeMatakuliah[0],$idjadwal,$tahunakademik);

		if ($isJoined > 0) {
			$list = "<option value='' selected='' disabled=''> Pilih Kelas </option>";
			foreach($data as $row) {
				$list .= "<option value='".$row->kd_jadwal."'>".$row->kd_matakuliah.' - '.$row->nama_matakuliah.' ('.$row->kelas.')'."</option>";
			}
		} else {
			$list = "<option value='0'> - </option>";
		}

		echo $list;
    }
	
	function delete ($id)
	{
        $get_kdjadwal        = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();
        $isScheduleUsed      = $this->db->where('kd_jadwal', $get_kdjadwal->kd_jadwal)->get('tbl_krs')->num_rows();
        $isScheduleHaveChild = $this->db->where('referensi', $get_kdjadwal->kd_jadwal)->get('tbl_jadwal_matkul')->num_rows();

    	if ($isScheduleUsed > 0) { 
    		echo "<script>alert('Tidak dapat menghapus jadwal! Jadwal sudah digunakan oleh mahasiswa.');history.go(-1);</script>";
    		exit();
    	}

        if ($isScheduleHaveChild > 0) { 
            echo "<script>alert('Tidak dapat menghapus jadwal! Jadwal memiliki turunan.');history.go(-1);</script>";
            exit();
        }

        $this->db->query('DELETE FROM tbl_jadwal_matkul WHERE id_jadwal = '.$id.'');

        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
	}
	

}

/* End of file Jdl_kuliah_baa.php */
/* Location: ./application/modules/perkuliahan/controllers/Jdl_kuliah_baa.php */