<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_status_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>NAMA</th>
			<th>FAKULTAS</th>
			<th>JURUSAN</th>
			<th>TAHUN MASUK</th>
			<th>STATUS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($q as $row) { if ($row->NIMHSMSMHS == true) {
			 $w =  "AKTIF";
		} else {
			$w = "CUTI";
		}?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->NIMHSMSMHS; ?></td>
			<td><?php echo $row->NMMHSMSMHS; ?></td>
			<td><?php echo $row->fakultas; ?></td>
			<td><?php echo $row->prodi; ?></td>
			<td><?php echo $row->TAHUNMSMHS; ?></td>
			<td><?php echo $w; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>