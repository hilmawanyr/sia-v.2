<?php 
  $parentMenu = $this->role_model->getparentmenu()->result(); 
  foreach ($parentMenu as $menu) { 
    
    if ($menu->url == '-') {
      $menuType = '<li class="dropdown active"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">';
    } elseif ($menu->url == '#') {
      $menuType = '<li class="active" ><a href="'.$menu->url.'">';
    } else {
      $menuType = '<li class="active" ><a href="'.base_url($menu->url).'">';
    } ?>
    
    <?= $menuType ?>
      <i class="<?= $menu->icon; ?>"></i><span ><?= $menu->menu; ?></span> </a>

        <?php if ($menu->url == '-') { ?>

          <ul class="dropdown-menu">

            <?php 
            $menus = $this->role_model->getmenu($menu->id_menu)->result(); 
            foreach ($menus as $row) { 
              echo '<li>';
                if ($row->url == '#') {
                  echo anchor($row->url, $row->menu);
                } else {
                  echo anchor(base_url($row->url), $row->menu);
                }
              echo '</li>'; 
            } ?>

          </ul>
        <?php } ?>

    </li>
<?php } ?>