<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="UTF-8">
	<?php $user = $this->session->userdata('sess_login');
  if ($user['user_type'] == 1) {
    $nama = $this->app_model->getdetail('tbl_karyawan','nid',$user['userid'],'nik','asc')->row(); 
    $name = $nama->nama;

  } elseif($user['user_type'] == 3) {
    $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row(); 
    $name = $nama->divisi;

  } elseif($user['user_type'] == 4) {
    $nama = $this->app_model->getdetail('tbl_karyawan_2','nip',$user['userid'],'nip','asc')->row(); 
    $name = $nama->nama;

  } else {
    $nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$user['userid'],'NIMHSMSMHS','asc')->row(); 
    $name = $nama->NMMHSMSMHS;
  } ?>

  <title>SIA | <?= $name?></title>
  <link rel="shortcut icon" href="<?= base_url(); ?>assets/logo.ico"/>
  <meta name="description" content="Sistem Informasi Akademik">
  <meta name="keywords" content="SIA,SIAKAD, SISTEM INFORMASI AKADEMIK">
  <meta name="author" content="Puskom">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link href="<?= base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
  <link href="<?= base_url();?>assets/css/font-awesome.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url();?>assets/css/pages/reports.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
  <script src="<?= base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
  <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
</head>

<body style="background: rgba(0, 0, 0, 0) url('https://www.toptal.com/designers/subtlepatterns/patterns/floor-tile.png') repeat scroll 0% 0%;">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> 
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </a>
      <a class="brand" href="#">
        <img src="<?= base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">
        <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK</span>
      </a>

      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:16px">
              <i class="icon-user"></i> <?= $name; ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url();?>extra/account">Ganti Password</a></li>
              <li><a href="<?= base_url();?>auth/logout">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="active" ><a href="<?= base_url();?>home"><i class="icon-home"></i><span >Dashboard</span> </a>
        <?php $this->load->view('menu'); ?>
      </ul>
    </div>
  </div>
</div>

<!-- /subnavbar -->

<div class="main">
  <div class="main-inner">
    <div class="container">
      <!-- load page -->
      <?php $this->load->view($page); ?>
    </div>
  </div>
</div>

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> 
          &copy; <?php date_default_timezone_set('Asia/Jakarta'); echo date('Y'); ?> - 
          <a target="_blank" href="http://ubharajaya.ac.id/">Universitas Bhayangkara Jakarta Raya</a>. 
          <b>
            <i>Last Login : 
            <?php $cek = $this
                            ->app_model
                            ->getdetail('tbl_user_login','userid',$user['userid'],'userid','asc')
                            ->row()
                            ->last_login; 
                            echo $cek; ?>
            </i>
          </b>
        </div>
      </div>
    </div>
  </div>
</div> 

<script src="<?= base_url();?>assets/js/bootstrap.js"></script>

<script type="text/javascript" src="<?= base_url();?>assets/js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendar = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },

    selectable: true,

    selectHelper: true,

    select: function(start, end, allDay) {
      var title = prompt('Event Title:');
      if (title) {
        calendar.fullCalendar('renderEvent',
          {
            title: title,
            start: start,
            end: end,
            allDay: allDay
          },
          true // make the event "stick"
        );
      }
      calendar.fullCalendar('unselect');
    },

    editable: true,

    <?php $kalender = $this->app_model->getdata('tbl_kalender_dtl','mulai','asc')->result(); ?>

    events: [
      <?php foreach ($kalender as $value) : ?>
      {
        title: '<?= trim($value->kegiatan); ?>',
        start: '<?= $value->mulai; ?>',
        end: '<?= $value->ahir; ?>'
      },
      <?php endforeach; ?>
    ]
  });
});
</script>

<script src="<?= base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
    $("#example1").dataTable();
    $('#example2').dataTable();
    $("#example3").dataTable();
    $("#example4").dataTable();
    $("#example5").dataTable();
    $("#example6").dataTable();
    $("#example7").dataTable();
    $("#example8").dataTable({
      "iDisplayLength": 200
    });
  });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93675871-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>

