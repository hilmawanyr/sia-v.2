<link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" /><div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-group"></i>
  				<h3>Detail Status Mahasiswa</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#actv">Mahasiswa Aktif</a></li>
					    <li><a data-toggle="tab" href="#nact">Mahasiswa Non Aktif</a></li>
					    <li><a data-toggle="tab" href="#cuti">Mahasiswa Cuti</a></li>
					</ul>
					<div class="tab-content">

						<!-- aktiv -->
					    <div id="actv" class="tab-pane fade in active">
							<a class="btn btn-warning" href="<?php echo base_url(); ?>"><i class="icon-arrow-left"></i> Kembali</a>
	                        <hr>
	                        <table id="example1" class="table table-bordered table-striped">
	                            <thead>
	                                <tr> 
	                                    <th>No</th>
	                                    <th>NPM</th>
	                                    <th>Nama</th>
	                                    <th>SKS</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php $no=1; foreach($actv as $row) { ?>
	                                <tr>
	                                    <td><?php echo $no; ?></td>
	                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
	                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
	                                    <td><a target="_blank" href="<?php echo base_url()?>home/sess/<?php echo $row->kd_krs?>"><?php echo $row->tot; ?></a></td>
	                                </tr>
	                                <?php $no++; } ?>
	                            </tbody>
	                        </table>
						</div>

						<!-- non aktiv -->
					    <div id="nact" class="tab-pane fade">
							<a class="btn btn-warning" href="<?php echo base_url(); ?>"><i class="icon-arrow-left"></i> Kembali</a>
	                        <hr>
	                        <table id="example2" class="table table-bordered table-striped">
	                            <thead>
	                                <tr> 
	                                    <th>No</th>
	                                    <th>NPM</th>
	                                    <th>Nama</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php $no=1; foreach($nact as $row) { ?>
	                                <tr>
	                                    <td><?php echo $no ?></td>
	                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
	                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
	                                </tr>
	                                <?php $no++; } ?>
	                            </tbody>
	                        </table>		
						</div>

						<!-- cuti -->
						<div id="cuti" class="tab-pane fade">
							<a class="btn btn-warning" href="<?php echo base_url(); ?>"><i class="icon-arrow-left"></i> Kembali</a>
		                    <hr>
		                    <table id="example3" class="table table-bordered table-striped">
		                        <thead>
		                            <tr> 
		                                <th>No</th>
		                                <th>NPM</th>
		                                <th>Nama</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php $no=1; foreach($cuti as $row) { ?>
		                            <tr>
		                                <td><?php echo $no ?></td>
		                                <td><?php echo $row->NIMHSMSMHS; ?></td>
		                                <td><?php echo $row->NMMHSMSMHS; ?></td>
		                            </tr>
		                            <?php $no++; } ?>
		                        </tbody>
		                    </table>	
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function() {

        $('#example1').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });
        $('#example2').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });
        $('#example3').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

    });

</script>