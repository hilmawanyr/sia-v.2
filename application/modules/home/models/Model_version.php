<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Model_version extends CI_Model
{

    function version()
    {
        $this->db->select('*');
        $this->db->from('tbl_version');
        $this->db->order_by('id_versi', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->id_versi;
        } else {
            return FALSE;
        }
    }
    function userVersion($versi, $user)
    {
        $this->db->select('*');
        $this->db->from('tbl_version a');
        $this->db->join('tbl_version_detail b', 'a.id_versi = b.versi_sia');
        $this->db->group_start();
        $this->db->where('b.user', $user);
        $this->db->or_where('b.user', '99');
        $this->db->group_end();
        $this->db->where('a.id_versi', $versi);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
}



/* End of file app_model.php */

/* Location: ./application/models/app_model.php */
