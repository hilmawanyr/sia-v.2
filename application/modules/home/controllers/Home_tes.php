<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_tes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
		error_reporting(0);
		if ($this->session->userdata('sess_login') != TRUE) {
		 	redirect('auth','refresh');
		}
	}

	function index()
	{
		//die('home bego');
		// $content = file_get_contents('http://ubharajaya.ac.id/wp-ubj/feed/');
	 	//$data['x'] = new SimpleXmlElement($content);
	 	$logged = $this->session->userdata('sess_login');
	 	$pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);
        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        if ((in_array(1, $grup)) || (in_array(10, $grup))) {
        	$data['notif'] = 'baa';
        }elseif ((in_array(8, $grup))) {
        	$data['notif'] = 'prd';
        } elseif ((in_array(7, $grup))) {
           $data['notif'] = 'dpa';
        } elseif ((in_array(5, $grup))) {
        	$data['notif'] = 'mhs';
        } elseif ((in_array(6, $grup))) {
        	$data['notif'] = 'dsn';
        }

		$data['page'] = 'home_tes';
		$this->load->view('template/template',$data);

		
	}

	function index2()
	{
		$user = $this->session->userdata('sess_login');
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		

		if ( (in_array(6, $grup))) {
			
			
	    }elseif ((in_array(5, $grup))){

	    	$thn = $this->db->select('kode')->where('status',1)->get('tbl_tahunakademik',1)->row()->kode;

	    	$kd_krs = $user['userid'].$thn;
		
	    	$data['krs'] = $this->db->where('notif', 1)->like('kd_krs',$kd_krs,'after')->order_by('id', 'desc')->get('tbl_bimbingan_krs',1)->row();
	    }

		$data['page'] = 'home';
		$this->load->view('template_test',$data);

		
	}

	function notif_update_krs($kode)
	{
		//tes
		$src = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$kode."%'")->row_array();
		//live
		// $src = $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs like '".$kode."%'")->row_array();
		$data = array(
			'notif' => 1
			);
		$this->db->where('kd_krs', $src['kd_krs']);
		$this->db->update('tbl_verifikasi_krs_tes', $data);
		redirect(base_url('akademik/krs_mhs'));
	}

	function notif_update_khs($npm,$thn)
	{
		$src = $this->db->query("SELECT * from tbl_nilai_detail where kd_krs like '".$npm.$thn."%'")->row_array();
		$data = array(
			'notif' => 1
			);
		$this->db->where('kd_krs', $src['kd_krs']);
		$this->db->update('tbl_nilai_detail', $data);
		redirect(base_url('akademik/khs/detailkhs/'.$npm.'/'.$thn));
	}

	function notif_pa($thn)
	{
		$this->session->set_userdata('yearacademic', $thn);
		redirect(base_url('akademik/bimbingan/list_bimbingan'));
	}

	function loadkrsprodi($usr,$thn)
	{
		$data['getData']=$this->app_model->getkrsmhsbyprodi($usr,'all',$thn)->result();
		$data['page'] = 'akademik/viewkrs_prodi';
		$this->load->view('template/template',$data);
	}

	function notif_baa($akad)
	{
		$data['getData']=$this->app_model->getkrsmhsbyprodi('all','all',$akad)->result();
		$data['page'] = 'akademik/viewkrs_prodi';
		$this->load->view('template/template',$data);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */