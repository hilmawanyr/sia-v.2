<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	private $group;

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if (!$this->session->userdata('sess_login')) {
			redirect('auth', 'refresh');
		}

		// log session
		$session = $this->session->userdata('sess_login');
		$this->group = $session['id_user_group'];
	}

	public function index()
	{
		$this->load->model('temph_model');
		$this->load->model('model_version');
		$this->load->model('mahasiswa_model', 'mahasiswa');
		$this->load->model('karyawan_model', 'karyawan');

		$grup = get_group($this->group);

		if ((in_array(1, $grup)) || (in_array(10, $grup))) {
			$data['notif'] = 'baa';
		} elseif ((in_array(8, $grup))) {
			$data['notif'] = 'prd';

			if ($loginSession['userid'] == 70201) {
				$data['kuesionerFIKOM'] = true;
			}
		} elseif ((in_array(7, $grup))) {
			$data['notif'] = 'dpa';

			$dosen = $this->karyawan->find(['nid' => $loginSession['userid']])->row();

			if ($dosen->jabatan_id == 70201) {
				$data['kuesionerFIKOM'] = true;
			}
		} elseif ((in_array(5, $grup))) {
			$data['notif'] = 'mhs';

			$mhs = $this->mahasiswa->find(['NIMHSMSMHS' => $loginSession['userid']])->row();

			if ($mhs->KDPSTMSMHS == 70201) {
				$data['kuesionerFIKOM'] = true;
			}
		} elseif ((in_array(6, $grup))) {
			$data['notif'] = 'dsn';
		}

		$data['calender1']   = $this->app_model->calender_akademik1(1);
		$data['calender2']   = $this->app_model->calender_akademik1(2);
		$data['calender3']   = $this->app_model->calender_akademik1(3);
		$data['page']        = 'home_tes';
		
		$version             = $this->model_version->version();
		$data['versi']       = get_version($version);
		$data['dtl_version'] = $this->model_version->userVersion($version, $loginSession['id_user_group']);
		$this->load->view('template/template', $data);
	}

	public function notif_update_krs($kode)
	{
		$data = ['notif' => 1];
		$this->db->like('kd_krs', $kode, 'AFTER');
		$this->db->update('tbl_verifikasi_krs', $data);
		redirect(base_url('akademik/krs_mhs'));
	}

	public function notif_update_khs($npm, $thn)
	{
		$kd_krs = $npm . $thn;
		$data 	= ['notif' => 1];
		$this->db->like('kd_krs', $kd_krs, 'AFTER');
		$this->db->update('tbl_nilai_detail', $data);
		redirect(base_url('akademik/khs/detailkhs/' . $npm . '/' . $thn));
	}

	public function notif_pa($thn)
	{
		$this->session->set_userdata('yearacademic', $thn);
		redirect(base_url('akademik/bimbingan/list_bimbingan'));
	}

	/**
	 * Load KRS for each prodi (department)
	 * @param string $prodi
	 * @param string $tahunAkademik
	 * @return page
	 */
	public function loadkrsprodi($prodi, $tahunAkademik)
	{
		$data['getData']	= $this->app_model->getkrsmhsbyprodi($prodi, 'all', $tahunAkademik)->result();
		$data['page'] 		= 'akademik/viewkrs_prodi';
		$this->load->view('template/template', $data);
	}

	public function notif_baa($tahunAkademik)
	{
		$data['getData']	= $this->app_model->getkrsmhsbyprodi('all', 'all', $tahunAkademik)->result();
		$data['page'] 		= 'akademik/viewkrs_prodi';
		$this->load->view('template/template', $data);
	}

	public function notif_prd_sp($tahunAkademik)
	{
		$this->session->set_userdata('ta', $tahunAkademik + 2);
		redirect(base_url('sp/validasisp/list_mk'), 'refresh');
	}

	public function load_stmhs($prodi, $tahunAkademik)
	{
		$this->load->model('temph_model');
		$data['actv'] = $this->temph_model->load_stmhs_prd_actv($prodi, $tahunAkademik)->result();
		$data['nact'] = $this->temph_model->load_stmhs_prd_nctv($prodi, $tahunAkademik)->result();
		$data['cuti'] = $this->temph_model->load_stmhs_prd_cuti($prodi, $tahunAkademik)->result();
		$data['page'] = "v_sts_mhs_prd";
		$this->load->view('template/template', $data);
	}

	public function sess($kd_krs)
	{
		$npm 			= substr($kd_krs, 0, 12);
		$tahunAkademik 	= substr($kd_krs, 12, 5);
		$kodeProdi 		= $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'NIMHSMSMHS', 'ASC')->row();

		$this->session->set_userdata('pro', $kodeProdi->KDPSTMSMHS);
		$this->session->set_userdata('tahunajaran', $tahunAkademik);

		$this->view_krs_mhs($kd_krs);
	}

	public function view_krs_mhs($kd_krs)
	{
		$this->db->select('mk.kd_matakuliah,mk.nama_matakuliah,mk.sks_matakuliah');
		$this->db->from('tbl_krs krs');
		$this->db->join('tbl_matakuliah mk', 'krs.kd_matakuliah = mk.kd_matakuliah', 'left');
		$this->db->where('kd_prodi', $this->session->userdata('pro'));
		$this->db->where('kd_krs', $kd_krs);
		$data['rows'] = $this->db->get()->result();

		$mhs = $this->db->where('NIMHSMSMHS', substr($kd_krs, 0, 12))->get('tbl_mahasiswa', 1)->row();

		$data['nim'] = $mhs->NIMHSMSMHS;
		$data['nama'] = $mhs->NMMHSMSMHS;

		$data['page'] = 'keuangan/v_krs_mhs';
		$this->load->view('template/template', $data);
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
