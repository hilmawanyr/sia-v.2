<?php

$lang['Program Pendidikan Tinggi']          = 'Education Program';
$lang['Program Studi']                      = 'Study Program';
$lang['Nama']                               = 'Name';
$lang['Tempat dan Tanggal Lahir']           = 'Place and Date of Birth';
$lang['Nomor Pokok Mahasiswa']              = 'Student Registration Number';
$lang['Tanggal Kelulusan']                  = 'Date of Graduation';
$lang['Indeks Prestasi Kumulatif']          = 'Average of Cumulative Grade';
$lang['Predikat Kelulusan']                 = 'Predicate of Graduation';
$lang['Kode']                               = 'Code';
$lang['Mata Kuliah']                        = 'Subject';
$lang['Agama']                              = 'Religion';
$lang['Sks']                                = 'Credit';
$lang['Nilai']                              = 'Grade';
$lang['Jumlah Satuan Kredit yang Ditempuh'] = 'Total of Cumulative Credit';
$lang['Judul Skripsi']                      = 'Title of Undergraduate Thesis';
$lang['TRANSKRIP AKADEMIK'] = 'ACADEMIC TRANSCRIPT';
$lang['Nomor'] = 'Number';
$lang['Nomor Induk Ijazah'] = 'National Certificate Number';
$lang['Nip'] = 'Staff Identification Number';


$lang['Dekan']                              = 'Dean';
$lang['Fakultas Psikologi']                 = 'Faculty of Psychology';
$lang['Fakultas Ekonomi']                   = 'Faculty of Economy';
$lang['Fakultas Hukum']                     = 'Faculty of Law';
$lang['Fakultas Ilmu Komunikasi']           = 'Faculty of Communication';
$lang['Fakultas Ilmu Pendidikan']           = 'Faculty of Education';
$lang['Fakultas Teknik']                    = 'Faculty of Technical';

/*Month*/
$lang['Januari']   = 'January';
$lang['Februari']  = 'February';
$lang['Maret']     = 'March';
$lang['April']     = 'April';
$lang['Mei']       = 'May';
$lang['Juni']      = 'June';
$lang['Juli']      = 'July';
$lang['Agustus']   = 'August';
$lang['September'] = 'September';
$lang['Oktober']   = 'October';
$lang['November']  = 'November';
$lang['Desember']  = 'December';

/*Teknik*/
$lang['KALKULUS II'] = 'Calculus II';


/*Magister Manajemen*/
$lang['Intelligence Business and Security Management for Strategic Management (Kons. Manajemen Strategy)'] = 'Intelligence Business and Security Management for Strategic Management (Kons. Manajemen Strategy)';