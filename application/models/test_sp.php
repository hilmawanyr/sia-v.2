<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_sp extends CI_Model {

	function call( $procedure )
	{
	    $result = @$this->db->conn_id->query($procedure);

	    while ( $this->db->conn_id->next_result() )
	    {
	        //free each result.
	        $not_used_result = $this->db->conn_id->use_result();

	        if ( $not_used_result instanceof mysqli_result )
	        {
	            $not_used_result->free();
	        }
	    }

	    return $result;

	}

	
}

/* End of file test_sp.php */
/* Location: ./application/models/test_sp.php */