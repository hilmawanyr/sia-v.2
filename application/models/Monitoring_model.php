<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_model extends CI_Model {

	function getmonitoring($nid,$bln,$thn,$prodi)
	{
		if ($thn == '2017' and $bln < 8) {
			$tabel = 'tbl_absensi_mhs_new';
		} else {
			$tabel = 'tbl_absensi_mhs_new_20171';
		}
		
		return $this->db->query("SELECT DISTINCT b.kelas,a.pertemuan,a.kd_jadwal,c.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,a.tanggal,COUNT(a.npm_mahasiswa) AS total, 
								COUNT(CASE kehadiran WHEN 'H' THEN 1 ELSE NULL END) AS hadir,
								COUNT(CASE kehadiran WHEN 'I' THEN 1 ELSE NULL END) AS ijin,
								COUNT(CASE kehadiran WHEN 'S' THEN 1 ELSE NULL END) AS sakit,
								COUNT(CASE kehadiran WHEN 'A' THEN 1 ELSE NULL END) AS alfa
								FROM ".$tabel." a
								JOIN tbl_jadwal_matkul b ON a.kd_jadwal = b.kd_jadwal
								JOIN tbl_matakuliah c ON b.kd_matakuliah = c.kd_matakuliah
								WHERE (b.gabung IS NULL or b.gabung = 0) AND b.kd_dosen = '".$nid."' AND c.kd_prodi = '".$prodi."' AND MONTH(a.tanggal) = '".$bln."' AND YEAR(a.tanggal) = '".$thn."'
								GROUP BY a.kd_jadwal,a.tanggal
								ORDER BY a.kd_jadwal")->result();
	}

	function getmonitoring_rekap($nid,$thn,$prodi)
	{
		if ($thn != '20171') {
			$tabel = 'tbl_absensi_mhs_new';
		} else {
			$tabel = 'tbl_absensi_mhs_new_20171';
		}
		
		return $this->db->query("SELECT DISTINCT b.kelas,a.pertemuan,a.kd_jadwal,c.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,a.tanggal,COUNT(a.npm_mahasiswa) AS total, 
								COUNT(CASE kehadiran WHEN 'H' THEN 1 ELSE NULL END) AS hadir,
								COUNT(CASE kehadiran WHEN 'I' THEN 1 ELSE NULL END) AS ijin,
								COUNT(CASE kehadiran WHEN 'S' THEN 1 ELSE NULL END) AS sakit,
								COUNT(CASE kehadiran WHEN 'A' THEN 1 ELSE NULL END) AS alfa
								FROM ".$tabel." a
								JOIN tbl_jadwal_matkul b ON a.kd_jadwal = b.kd_jadwal
								JOIN tbl_matakuliah c ON b.kd_matakuliah = c.kd_matakuliah
								WHERE (b.gabung IS NULL or b.gabung = 0) AND b.kd_dosen = '".$nid."' AND c.kd_prodi = '".$prodi."' and b.kd_tahunajaran = '".$thn."'
								GROUP BY a.kd_jadwal,a.tanggal
								ORDER BY a.kd_jadwal")->result();
	}

	function getskstotalmhs($npm,$prodi)
	{
		$q = $this->db->query('SELECT nl.`KDKMKTRLNM`,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
								MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,nl.`THSMSTRLNM`,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
								WHERE nl.`NIMHSTRLNM` = "'.$npm.'"
								GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC
								')->result();
		$tskss = 0;
		$tnl=0;
		foreach ($q as $row) {
			$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
			$tskss=$tskss+$row->sks_matakuliah;
			$tnl = $tnl+$nk;
		}
		$data['sks'] = $tskss;
		$data['ipk'] = number_format($tnl/$tskss,2);
		return $data;
	}

	function get_all_khs_mahasiswa_konversi($npm){		
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa WHERE NIMHSMSMHS="'.$npm.'"')->row();
		
		//ditambahin like -
		return $this->db->query('SELECT distinct b.THSMSTRLNM,b.KDPSTTRLNM from tbl_transaksi_nilai_konversi b
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'" ORDER BY THSMSTRLNM ASC');

	}

	function get_mk_gabung($mk,$idJadwal,$ta)
	{
		// pra 20192, MKDU masih berdasarkan prodi, setelah itu menjadi per fakultas kepemilikannya
		if ($ta < 20192) {
			return $this->db->query("SELECT a.*,b.prodi,c.nama_matakuliah from tbl_jadwal_matkul a
									JOIN tbl_jurusan_prodi b ON SUBSTR(a.kd_jadwal, 1,5) = b.kd_prodi
									JOIN tbl_matakuliah c ON c.kd_matakuliah = a.kd_matakuliah
									WHERE c.kd_prodi = SUBSTR(a.kd_jadwal, 1,5) and kd_tahunajaran = '".$ta."'
									and a.kd_matakuliah = '".$mk."' ORDER BY id_jadwal ASC")->result();

		} else {
			return $this->db->query("SELECT a.*,c.nama_matakuliah FROM tbl_jadwal_matkul a
									JOIN tbl_matakuliah c ON c.kd_matakuliah = a.kd_matakuliah
									WHERE a.kd_tahunajaran = '".$ta."'
									AND a.kd_matakuliah = '".$mk."'
									AND a.id_jadwal != '".$idJadwal."'
									AND gabung = 0 GROUP BY a.id_jadwal")->result();	
		}
	}

	function dapetip($mhs,$ta)
	{
		$this->db->distinct();
		$this->db->select('*');
		$this->db->from('tbl_aktifitas_kuliah_mahasiswa');
		$this->db->WHERE('NIMHSTRAKM', $mhs);
		$this->db->WHERE('THSMSTRAKM', $ta);
		$q = $this->db->get();
		return $q;
	}
}

/* End of file Monitoring_model.php */
/* Location: ./application/models/Monitoring_model.php */