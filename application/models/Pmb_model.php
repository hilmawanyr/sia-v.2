<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pmb_model extends CI_Model {

	

	function get_camaba($ta,$gel,$prodi){
		$query = $this->db->select('maba.`nomor_registrasi` AS nomor,maba.`nama`,nilai.*')
						  ->from('tbl_form_camaba maba')
						  ->join('tbl_nilai_pmb nilai','nilai.`nomor_registrasi` = maba.`nomor_registrasi`','left')
						  ->where('maba.gelombang',$gel)
						  ->where('maba.prodi',$prodi)
						  ->like('maba.nomor_registrasi',substr($ta, -2),'right')
						  ->get();

		return $query;
	}

}

/* End of file Pmb_model.php */
/* Location: ./application/models/Pmb_model.php */